/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_SUBSCRIPTION_MANAGER_H
#define RSD_SUBSCRIPTION_MANAGER_H

#include <glib.h>
#include <glib-object.h>

#include <librhosydd/subscription-manager.h>
#include <librhosydd/types.h>
#include <librhosydd/zone.h>

G_BEGIN_DECLS

#define RSD_TYPE_SUBSCRIPTION_MANAGER rsd_subscription_manager_get_type ()
G_DECLARE_FINAL_TYPE (RsdSubscriptionManager, rsd_subscription_manager, RSD,
                      SUBSCRIPTION_MANAGER, GObject)

RsdSubscriptionManager *rsd_subscription_manager_new                  (void);

void                    rsd_subscription_manager_update_subscriptions (RsdSubscriptionManager *self,
                                                                       GPtrArray              *subscriptions,
                                                                       GPtrArray              *unsubscriptions);
gboolean                rsd_subscription_manager_is_subscribed        (RsdSubscriptionManager *self,
                                                                       GPtrArray              *changed_attributes,
                                                                       GPtrArray              *invalidated_attributes,
                                                                       RsdTimestampMicroseconds            notify_time);

GPtrArray              *rsd_subscription_manager_get_subscriptions    (RsdSubscriptionManager *self);
gboolean                rsd_subscription_manager_has_subscription     (RsdSubscriptionManager *self,
                                                                       RsdZone                *zone,
                                                                       const gchar            *attribute_name);

G_END_DECLS

#endif /* !RSD_SUBSCRIPTION_MANAGER_H */
