/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_STATIC_ZONE_H
#define RSD_STATIC_ZONE_H

#include <glib.h>
#include <glib-object.h>

#include <librhosydd/types.h>

G_BEGIN_DECLS

#define RSD_TYPE_STATIC_ZONE rsd_static_zone_get_type ()
G_DECLARE_FINAL_TYPE (RsdStaticZone, rsd_static_zone, RSD, STATIC_ZONE, GObject)

RsdStaticZone *rsd_static_zone_new (const gchar *path);

G_END_DECLS

#endif /* !RSD_STATIC_ZONE_H */
