/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_UTILITIES_H
#define RSD_UTILITIES_H

#include <glib.h>

#include <librhosydd/attribute.h>


G_BEGIN_DECLS

GPtrArray *rsd_attribute_info_array_from_variant     (GVariant                  *variant,
                                                      RsdTimestampMicroseconds   current_time,
                                                      GError                   **error);
GVariant  *rsd_attribute_info_array_to_variant       (GPtrArray                 *array);

GPtrArray *rsd_attribute_metadata_array_from_variant (GVariant                  *variant,
                                                      GError                   **error);
GVariant  *rsd_attribute_metadata_array_to_variant   (GPtrArray                 *array);

RsdAttributeInfo *rsd_attribute_info_from_variant    (GVariant                  *variant,
                                                      const gchar               *zone_path,
                                                      const gchar               *attribute_name,
                                                      RsdTimestampMicroseconds  *current_time,
                                                      GError                   **error);

RsdAttributeMetadata *rsd_attribute_metadata_from_variant (GVariant                  *variant,
                                                           const gchar               *zone_path,
                                                           const gchar               *attribute_name,
                                                           RsdTimestampMicroseconds  *current_time,
                                                           GError                   **error);

GPtrArray *rsd_zone_array_from_variant (GVariant  *variant,
                                        GError   **error);

GVariant  *rsd_subscription_array_to_variant   (GPtrArray *array);
GPtrArray *rsd_subscription_array_from_variant (GVariant  *variant,
                                                GError   **error);

G_END_DECLS

#endif /* !RSD_UTILITIES_H */
