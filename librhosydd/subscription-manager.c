/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include "libinternal/logging.h"
#include "librhosydd/attribute.h"
#include "librhosydd/subscription.h"
#include "librhosydd/subscription-manager.h"
#include "librhosydd/vehicle.h"


static void rsd_subscription_manager_dispose (GObject *object);

typedef struct
{
  RsdSubscription *aggregate_subscription;  /* owned */
  GArray/*<owned RsdSubscription>*/ *array;  /* owned */
} SubscriptionData;

static void
subscription_data_free (SubscriptionData *data)
{
  rsd_subscription_free (data->aggregate_subscription);
  g_array_free (data->array, TRUE);
  g_free (data);
}

static void
subscription_clear (RsdSubscription *subscription)
{
  g_clear_pointer (&subscription->minimum_value, g_variant_unref);
  g_clear_pointer (&subscription->maximum_value, g_variant_unref);
}

/**
 * RsdSubscriptionManager:
 *
 * A manager object which tracks a client’s subscriptions to various attributes,
 * including the criteria which the client has placed on receiving notifications
 * of changes to those attributes. It can be used to check whether an incoming
 * notification should be propagated to the client, or whether it can be
 * ignored.
 *
 * If no subscriptions are added to the #RsdSubscriptionManager, it will not
 * forward **any** signals — to subscribe to all signals, add a wildcard
 * subscription (see the documentation for #RsdSubscription).
 *
 * Since: 0.2.0
 */
struct _RsdSubscriptionManager
{
  GObject parent;

  /* Mapping from zone_path_and_attribute_name to #SubscriptionData. */
  GHashTable/*<unowned utf8, owned SubscriptionData>*/ *subscriptions;
};

G_DEFINE_TYPE (RsdSubscriptionManager, rsd_subscription_manager, G_TYPE_OBJECT)

static void
rsd_subscription_manager_class_init (RsdSubscriptionManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = rsd_subscription_manager_dispose;
}

static void
rsd_subscription_manager_init (RsdSubscriptionManager *self)
{
  self->subscriptions = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                               (GDestroyNotify) subscription_data_free);
}

static void
rsd_subscription_manager_dispose (GObject *object)
{
  RsdSubscriptionManager *self = RSD_SUBSCRIPTION_MANAGER (object);

  g_clear_pointer (&self->subscriptions, g_hash_table_unref);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (rsd_subscription_manager_parent_class)->dispose (object);
}

/**
 * rsd_subscription_manager_new:
 *
 * Create a new #RsdSubscriptionManager, with no initial subscriptions.
 *
 * Returns: (transfer full): a new #RsdSubscriptionManager
 * Since: 0.2.0
 */
RsdSubscriptionManager *
rsd_subscription_manager_new (void)
{
  return g_object_new (RSD_TYPE_SUBSCRIPTION_MANAGER, NULL);
}

/**
 * rsd_subscription_manager_update_subscriptions:
 * @self: an #RsdSubscriptionManager
 * @subscriptions: (element-type RsdSubscription) (transfer none): potentially
 *    empty array of #RsdSubscriptions giving new subscriptions to add
 * @unsubscriptions: (element-type RsdSubscription) (transfer none): potentially
 *    empty array of #RsdSubscriptions giving existing subscriptions to remove
 *
 * Update the subscriptions stored in the #RsdSubscriptionManager, removing all
 * the subscriptions in @unsubscriptions, then adding all those in
 * @subscriptions. @unsubscriptions are matched using rsd_subscription_equal(),
 * rather than by pointer equality.
 *
 * Since: 0.2.0
 */
void
rsd_subscription_manager_update_subscriptions (RsdSubscriptionManager *self,
                                               GPtrArray              *subscriptions,
                                               GPtrArray              *unsubscriptions)
{
  GHashTableIter iter;
  SubscriptionData *data;
  gsize i;
  g_autoptr (GHashTable/*<unowned SubscriptionData, unowned SubscriptionData>*/) modified_data = NULL;

  g_return_if_fail (RSD_IS_SUBSCRIPTION_MANAGER (self));
  g_return_if_fail (subscriptions != NULL);
  g_return_if_fail (unsubscriptions != NULL);

  /* Track which SubscriptionDatas we need to re-calculate the aggregate values
   * for at the end. */
  modified_data = g_hash_table_new (g_direct_hash, g_direct_equal);

  /* Remove old subscriptions. */
  for (i = 0; i < unsubscriptions->len; i++)
    {
      const RsdSubscription *unsubscription = unsubscriptions->pdata[i];
      g_autofree gchar *zone_path_and_attribute_name = NULL;
      gsize j;
      gboolean removed = FALSE;

      zone_path_and_attribute_name = g_strconcat (unsubscription->zone_path,
                                                  unsubscription->attribute_name,
                                                  NULL);
      data = g_hash_table_lookup (self->subscriptions,
                                  zone_path_and_attribute_name);

      if (data == NULL)
        {
          DEBUG ("Tried to remove a non-existent subscription from the "
                 "subscriptions table.");
          continue;
        }

      /* Remove the first matching entry from the array of RsdSubscriptions from
       * different callers. Duplicate entries act as a kind of reference count,
       * so only remove one of them. */
      for (j = 0; j < data->array->len; j++)
        {
          if (rsd_subscription_equal (&g_array_index (data->array,
                                                      const RsdSubscription, j),
                                      unsubscription))
            {
              g_array_remove_index_fast (data->array, j);
              removed = TRUE;
              break;
            }
        }

      if (removed && data->array->len == 0)
        g_hash_table_remove (self->subscriptions, zone_path_and_attribute_name);
      else if (removed)
        g_hash_table_add (modified_data, data);
      else
        DEBUG ("Tried to remove a non-existent subscription from the "
               "subscriptions table.");
    }

  /* Add new subscriptions. */
  for (i = 0; i < subscriptions->len; i++)
    {
      const RsdSubscription *subscription = subscriptions->pdata[i];
      g_autofree gchar *zone_path_and_attribute_name = NULL;

      zone_path_and_attribute_name = g_strconcat (subscription->zone_path,
                                                  subscription->attribute_name,
                                                  NULL);
      data = g_hash_table_lookup (self->subscriptions,
                                  zone_path_and_attribute_name);

      if (data == NULL)
        {
          data = g_new0 (SubscriptionData, 1);

          data->array = g_array_new (FALSE, TRUE, sizeof (RsdSubscription));
          g_array_set_clear_func (data->array,
                                  (GDestroyNotify) subscription_clear);

          g_hash_table_insert (self->subscriptions,
                               g_steal_pointer (&zone_path_and_attribute_name),
                               data);
        }

      g_array_insert_val (data->array, data->array->len, *subscription);
      g_hash_table_add (modified_data, data);
    }

  /* Recalculate the aggregates for all the modified attributes. */
  g_hash_table_iter_init (&iter, modified_data);

  while (g_hash_table_iter_next (&iter, (gpointer *) &data, NULL))
    {
      g_clear_pointer (&data->aggregate_subscription, rsd_subscription_free);
      data->aggregate_subscription = rsd_subscription_new_aggregate (data->array);
    }
}

/**
 * rsd_subscription_manager_is_subscribed:
 * @self: an #RsdSubscriptionManager
 * @changed_attributes: (element-type RsdAttributeInfo) (transfer none):
 *    potentially empty array of attributes whose values have changed
 * @invalidated_attributes: (element-type RsdAttributeMetadata) (transfer none):
 *    potentially empty array of attributes whose values have changed, but the
 *    new value is not known
 * @notify_time: time when the notification signal was received
 *
 * Check whether any of the subscriptions in the #RsdSubscriptionManager match
 * any of the attributes in @changed_attributes or @invalidated_attributes.
 *
 * Returns: %TRUE if any of the attributes in @changed_attributes or
 *    @invalidated_attributes matches a subscription in the
 *    #RsdSubscriptionManager and hence a notification should be emitted
 * Since: 0.2.0
 */
gboolean
rsd_subscription_manager_is_subscribed (RsdSubscriptionManager *self,
                                        GPtrArray              *changed_attributes,
                                        GPtrArray              *invalidated_attributes,
                                        RsdTimestampMicroseconds            notify_time)
{
  gsize i;
  SubscriptionData *wildcard_data;
  gboolean retval = FALSE;

  g_return_val_if_fail (RSD_IS_SUBSCRIPTION_MANAGER (self), TRUE);
  g_return_val_if_fail (changed_attributes != NULL, TRUE);
  g_return_val_if_fail (invalidated_attributes != NULL, TRUE);

  /* Retrieve the wildcard subscription, if it exists. */
  wildcard_data = g_hash_table_lookup (self->subscriptions, "/");

  /* Check each of the @changed_attributes against any subscriptions registered
   * for that zone path and attribute name, and against the wildcard
   * subscription. */
  for (i = 0; i < changed_attributes->len; i++)
    {
      const RsdAttributeInfo *info = changed_attributes->pdata[i];
      g_autofree gchar *zone_path_and_attribute_name = NULL;
      SubscriptionData *data;

      zone_path_and_attribute_name = g_strconcat (info->metadata.zone_path,
                                                  info->metadata.name,
                                                  NULL);
      data = g_hash_table_lookup (self->subscriptions,
                                  zone_path_and_attribute_name);

      if ((data != NULL &&
           rsd_subscription_matches (data->aggregate_subscription,
                                     &info->attribute, notify_time)) ||
          (wildcard_data != NULL &&
           rsd_subscription_matches (wildcard_data->aggregate_subscription,
                                     &info->attribute, notify_time)))
        {
          retval = TRUE;
        }
    }

  /* We can’t check the values for invalidated attributes, but we can check
   * their time periods. */
  for (i = 0; i < invalidated_attributes->len; i++)
    {
      const RsdAttributeMetadata *metadata = invalidated_attributes->pdata[i];
      g_autofree gchar *zone_path_and_attribute_name = NULL;
      SubscriptionData *data;

      zone_path_and_attribute_name = g_strconcat (metadata->zone_path,
                                                 metadata->name,
                                                 NULL);
      data = g_hash_table_lookup (self->subscriptions,
                                  zone_path_and_attribute_name);

      if ((data != NULL &&
           rsd_subscription_matches_period (data->aggregate_subscription,
                                            notify_time)) ||
          (wildcard_data != NULL &&
           rsd_subscription_matches_period (wildcard_data->aggregate_subscription,
                                            notify_time)))
        {
          retval = TRUE;
        }
    }

  /* Update all the attributes to set whether they were notified. */
  for (i = 0; i < changed_attributes->len; i++)
    {
      const RsdAttributeInfo *info = changed_attributes->pdata[i];
      g_autofree gchar *zone_path_and_attribute_name = NULL;
      SubscriptionData *data;

      zone_path_and_attribute_name = g_strconcat (info->metadata.zone_path,
                                                  info->metadata.name,
                                                  NULL);
      data = g_hash_table_lookup (self->subscriptions,
                                  zone_path_and_attribute_name);

      if (data != NULL)
        {
          rsd_subscription_notify (data->aggregate_subscription,
                                   &info->attribute, notify_time, retval);
        }
    }

  for (i = 0; i < invalidated_attributes->len; i++)
    {
      const RsdAttributeMetadata *metadata = invalidated_attributes->pdata[i];
      g_autofree gchar *zone_path_and_attribute_name = NULL;
      SubscriptionData *data;

      zone_path_and_attribute_name = g_strconcat (metadata->zone_path,
                                                  metadata->name,
                                                  NULL);
      data = g_hash_table_lookup (self->subscriptions,
                                  zone_path_and_attribute_name);

      if (data != NULL)
        {
          rsd_subscription_notify (data->aggregate_subscription,
                                   NULL, notify_time, retval);
        }
    }

  return retval;
}

/**
 * rsd_subscription_manager_get_subscriptions:
 * @self: an #RsdSubscriptionManager
 *
 * Get the subscriptions currently active in the #RsdSubscriptionManager.
 *
 * Returns: (transfer container) (element-type RsdSubscription): potentially
 *    empty array of the active #RsdSubscriptions in the manager
 * Since: 0.2.0
 */
GPtrArray *
rsd_subscription_manager_get_subscriptions (RsdSubscriptionManager *self)
{
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;
  GHashTableIter iter;
  SubscriptionData *data;

  g_return_val_if_fail (RSD_IS_SUBSCRIPTION_MANAGER (self), NULL);

  subscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_hash_table_iter_init (&iter, self->subscriptions);

  while (g_hash_table_iter_next (&iter, NULL, (gpointer *) &data))
    {
      gsize i;

      for (i = 0; i < data->array->len; i++)
        {
          const RsdSubscription *subscription;

          subscription = &g_array_index (data->array, const RsdSubscription, i);
          g_ptr_array_add (subscriptions, rsd_subscription_copy (subscription));
        }
    }

  return g_steal_pointer (&subscriptions);
}

/**
 * rsd_subscription_manager_has_subscription:
 * @self: an #RsdSubscriptionManager
 * @zone: (nullable) (transfer none): zone, or %NULL for a wildcard
 * @attribute_name: (nullable): attribute name, or %NULL for a wildcard
 *
 * Checks whether a subscription is active which covers the given attribute. A
 * wildcard subscription covers all attributes.
 *
 * If @zone and @attribute_name are %NULL, this will check purely for an active
 * wildcard subscription.
 *
 * Returns: %TRUE if at least one subscription is active which covers the given
 *    attribute; %FALSE otherwise
 * Since: 0.5.0
 */
gboolean
rsd_subscription_manager_has_subscription (RsdSubscriptionManager *self,
                                           RsdZone                *zone,
                                           const gchar            *attribute_name)
{
  g_autofree gchar *zone_path_and_attribute_name = NULL;
  gboolean has_wildcard, has_attribute;

  g_return_val_if_fail (RSD_IS_SUBSCRIPTION_MANAGER (self), FALSE);
  g_return_val_if_fail (zone == NULL || RSD_IS_ZONE (zone), FALSE);
  g_return_val_if_fail (attribute_name == NULL ||
                        rsd_attribute_name_is_valid (attribute_name), FALSE);

  has_wildcard = g_hash_table_contains (self->subscriptions, "/");

  if (zone != NULL && attribute_name != NULL)
    {
      zone_path_and_attribute_name = g_strconcat (rsd_zone_get_path (zone),
                                                  attribute_name,
                                                  NULL);
      has_attribute = g_hash_table_contains (self->subscriptions,
                                             zone_path_and_attribute_name);
    }
  else
    {
      has_attribute = FALSE;
    }

  return (has_wildcard || has_attribute);
}
