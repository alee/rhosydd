/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>

#include "libinternal/logging.h"
#include "libinternal/timestamped-pointer.h"
#include "librhosydd/proxy-vehicle.h"
#include "librhosydd/static-zone.h"
#include "librhosydd/subscription-manager.h"
#include "librhosydd/types.h"
#include "librhosydd/utilities.h"
#include "librhosydd/vehicle-interface-generated.c"
#include "librhosydd/vehicle.h"
#include "librhosydd/zone.h"


static void rsd_proxy_vehicle_vehicle_init        (RsdVehicleInterface *iface);
static void rsd_proxy_vehicle_initable_init       (GInitableIface      *iface);
static void rsd_proxy_vehicle_async_initable_init (GAsyncInitableIface *iface);
static void rsd_proxy_vehicle_constructed         (GObject             *object);
static void rsd_proxy_vehicle_dispose             (GObject             *object);
static void rsd_proxy_vehicle_get_property        (GObject             *object,
                                                   guint                property_id,
                                                   GValue              *value,
                                                   GParamSpec          *pspec);
static void rsd_proxy_vehicle_set_property        (GObject             *object,
                                                   guint                property_id,
                                                   const GValue        *value,
                                                   GParamSpec          *pspec);

static gboolean         rsd_proxy_vehicle_init_failable                     (GInitable            *initable,
                                                                             GCancellable         *cancellable,
                                                                             GError              **error);

static void             rsd_proxy_vehicle_init_async                        (GAsyncInitable       *initable,
                                                                             int                   io_priority,
                                                                             GCancellable         *cancellable,
                                                                             GAsyncReadyCallback   callback,
                                                                             gpointer              user_data);
static gboolean         rsd_proxy_vehicle_init_finish                       (GAsyncInitable       *initable,
                                                                             GAsyncResult         *result,
                                                                             GError              **error);

static const gchar     *rsd_proxy_vehicle_vehicle_get_id                    (RsdVehicle           *vehicle);
static GPtrArray       *rsd_proxy_vehicle_vehicle_get_zones                 (RsdVehicle           *vehicle);
static void             rsd_proxy_vehicle_vehicle_get_attribute_async        (RsdVehicle           *vehicle,
                                                                             RsdZone              *zone,
                                                                             const gchar          *attribute_name,
                                                                             GCancellable         *cancellable,
                                                                             GAsyncReadyCallback   callback,
                                                                             gpointer              user_data);
static RsdAttributeInfo *rsd_proxy_vehicle_vehicle_get_attribute_finish     (RsdVehicle           *vehicle,
                                                                             GAsyncResult         *result,
                                                                             RsdTimestampMicroseconds *current_time,
                                                                             GError              **error);

static void             rsd_proxy_vehicle_vehicle_get_metadata_async        (RsdVehicle           *vehicle,
                                                                             RsdZone              *zone,
                                                                             const gchar          *attribute_name,
                                                                             GCancellable         *cancellable,
                                                                             GAsyncReadyCallback   callback,
                                                                             gpointer              user_data);
static RsdAttributeMetadata *rsd_proxy_vehicle_vehicle_get_metadata_finish   (RsdVehicle           *vehicle,
                                                                             GAsyncResult         *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                             GError              **error);

static void             rsd_proxy_vehicle_vehicle_set_attribute_async        (RsdVehicle           *vehicle,
                                                                             RsdZone              *zone,
                                                                             const gchar          *attribute_name,
                                                                             GVariant             *value,
                                                                             GCancellable         *cancellable,
                                                                             GAsyncReadyCallback   callback,
                                                                             gpointer              user_data);
static void             rsd_proxy_vehicle_vehicle_set_attribute_finish       (RsdVehicle           *vehicle,
                                                                             GAsyncResult         *result,
                                                                             GError              **error);
static void             rsd_proxy_vehicle_vehicle_get_all_attributes_async  (RsdVehicle           *vehicle,
                                                                             RsdZone              *zone,
                                                                             GCancellable         *cancellable,
                                                                             GAsyncReadyCallback   callback,
                                                                             gpointer              user_data);
static GPtrArray       *rsd_proxy_vehicle_vehicle_get_all_attributes_finish (RsdVehicle           *vehicle,
                                                                             GAsyncResult         *result,
                                                                             RsdTimestampMicroseconds *current_time,
                                                                             GError              **error);
static void             rsd_proxy_vehicle_vehicle_get_all_metadata_async    (RsdVehicle           *vehicle,
                                                                             RsdZone              *zone,
                                                                             GCancellable         *cancellable,
                                                                             GAsyncReadyCallback   callback,
                                                                             gpointer              user_data);
static GPtrArray       *rsd_proxy_vehicle_vehicle_get_all_metadata_finish   (RsdVehicle           *vehicle,
                                                                             GAsyncResult         *result,
                                                                             RsdTimestampMicroseconds *current_time,
                                                                             GError              **error);

static void             rsd_proxy_vehicle_vehicle_update_subscriptions_async  (RsdVehicle           *vehicle,
                                                                               GPtrArray            *subscriptions,
                                                                               GPtrArray            *unsubscriptions,
                                                                               GCancellable         *cancellable,
                                                                               GAsyncReadyCallback   callback,
                                                                               gpointer              user_data);
static void             rsd_proxy_vehicle_vehicle_update_subscriptions_finish (RsdVehicle           *vehicle,
                                                                               GAsyncResult         *result,
                                                                               GError              **error);

static void properties_changed_cb      (GDBusProxy  *proxy,
                                        GVariant    *changed_properties,
                                        GStrv        invalidated_properties,
                                        gpointer     user_data);
static void signal_cb                  (GDBusProxy  *proxy,
                                        const gchar *sender_name,
                                        const gchar *signal_name,
                                        GVariant    *parameters,
                                        gpointer     user_data);
static void proxy_notify_name_owner_cb (GObject     *obj,
                                        GParamSpec  *pspec,
                                        gpointer     user_data);


/**
 * RsdProxyVehicle:
 *
 * An implementation of #RsdVehicle which proxies an object implementing
 * `org.apertis.Rhosydd1.Vehicle` on D-Bus. It implements minimal caching, so
 * method calls should be considered as expensive.
 *
 * It validates the timestamps seen from the proxied vehicle, and will
 * disconnect from the vehicle (#RsdVehicle::invalidated) if any invalid
 * timestamps are seen. A timestamp is invalid if it is lower than the
 * previously most recently seen timestamp. i.e. (Assuming messages are received
 * in order from D-Bus) timestamps must be monotonically increasing.
 *
 * Since: 0.2.0
 */
struct _RsdProxyVehicle
{
  GObject parent;

  GDBusProxy *proxy;  /* owned; NULL during initialisation */
  GDBusConnection *connection;  /* owned */
  gchar *name;  /* owned; NULL if not running on a message bus */
  gchar *object_path;  /* owned */

  GPtrArray/*<owned RsdStaticZone>*/ *zones;  /* owned; always valid */
  gchar *vehicle_id;  /* owned; always valid */
  RsdSubscriptionManager *subscriptions;
  RsdTimestampMicroseconds last_seen_time;  /* most recently seen timestamp */

  /* Exactly one of these will be set after initialisation completes (or
   * fails). */
  GError *init_error;  /* nullable; owned */
  gboolean init_success;
  gboolean initialising;
};

typedef enum
{
  PROP_CONNECTION = 1,
  PROP_NAME,
  PROP_OBJECT_PATH,
  PROP_PROXY,
  /* Overridden properties: */
  PROP_ID,
} RsdProxyVehicleProperty;

G_DEFINE_TYPE_WITH_CODE (RsdProxyVehicle, rsd_proxy_vehicle,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RSD_TYPE_VEHICLE,
                                                rsd_proxy_vehicle_vehicle_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                rsd_proxy_vehicle_initable_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE,
                                                rsd_proxy_vehicle_async_initable_init))

static void
rsd_proxy_vehicle_class_init (RsdProxyVehicleClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GParamSpec *props[PROP_PROXY + 1] = { NULL, };

  object_class->constructed = rsd_proxy_vehicle_constructed;
  object_class->dispose = rsd_proxy_vehicle_dispose;
  object_class->get_property = rsd_proxy_vehicle_get_property;
  object_class->set_property = rsd_proxy_vehicle_set_property;

  /**
   * RsdProxyVehicle:connection:
   *
   * D-Bus connection to proxy the object from.
   *
   * Since: 0.2.0
   */
  props[PROP_CONNECTION] =
      g_param_spec_object ("connection", "Connection",
                           "D-Bus connection to proxy the object from.",
                           G_TYPE_DBUS_CONNECTION,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * RsdProxyVehicle:name:
   *
   * Well-known or unique name of the peer to proxy the object from. This must
   * be %NULL if and only if the #RsdProxyVehicle:connection is not a message
   * bus connection.
   *
   * Since: 0.2.0
   */
  props[PROP_NAME] =
      g_param_spec_string ("name", "Name",
                           "Well-known or unique name of the peer to proxy the "
                           "object from.",
                           NULL,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * RsdProxyVehicle:object-path:
   *
   * Object path to proxy. The object must implement
   * `org.apertis.Rhosydd1.Vehicle`.
   *
   * Since: 0.2.0
   */
  props[PROP_OBJECT_PATH] =
      g_param_spec_string ("object-path", "Object Path",
                           "Object path to proxy.",
                           "/",
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * RsdProxyVehicle:proxy:
   *
   * D-Bus proxy to use when interacting with the object. If this is %NULL at
   * construction time, one will be created. If provided, it **must** have
   * cached copies of its properties already.
   *
   * Since: 0.2.0
   */
  props[PROP_PROXY] =
      g_param_spec_object ("proxy", "Proxy",
                           "D-Bus proxy to use when interacting with the "
                           "object.",
                           G_TYPE_DBUS_PROXY,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);

  g_object_class_override_property (object_class, PROP_ID, "id");
}

static void
rsd_proxy_vehicle_vehicle_init (RsdVehicleInterface *iface)
{
  iface->get_id = rsd_proxy_vehicle_vehicle_get_id;
  iface->get_zones = rsd_proxy_vehicle_vehicle_get_zones;
  iface->get_attribute_async = rsd_proxy_vehicle_vehicle_get_attribute_async;
  iface->get_attribute_finish = rsd_proxy_vehicle_vehicle_get_attribute_finish;
  iface->get_metadata_async = rsd_proxy_vehicle_vehicle_get_metadata_async;
  iface->get_metadata_finish = rsd_proxy_vehicle_vehicle_get_metadata_finish;
  iface->set_attribute_async = rsd_proxy_vehicle_vehicle_set_attribute_async;
  iface->set_attribute_finish = rsd_proxy_vehicle_vehicle_set_attribute_finish;
  iface->get_all_attributes_async = rsd_proxy_vehicle_vehicle_get_all_attributes_async;
  iface->get_all_attributes_finish = rsd_proxy_vehicle_vehicle_get_all_attributes_finish;
  iface->get_all_metadata_async = rsd_proxy_vehicle_vehicle_get_all_metadata_async;
  iface->get_all_metadata_finish = rsd_proxy_vehicle_vehicle_get_all_metadata_finish;
  iface->update_subscriptions_async = rsd_proxy_vehicle_vehicle_update_subscriptions_async;
  iface->update_subscriptions_finish = rsd_proxy_vehicle_vehicle_update_subscriptions_finish;
}

static void
rsd_proxy_vehicle_initable_init (GInitableIface *iface)
{
  iface->init = rsd_proxy_vehicle_init_failable;
}

static void
rsd_proxy_vehicle_async_initable_init (GAsyncInitableIface *iface)
{
  iface->init_async = rsd_proxy_vehicle_init_async;
  iface->init_finish = rsd_proxy_vehicle_init_finish;
}

static void
rsd_proxy_vehicle_init (RsdProxyVehicle *self)
{
  self->subscriptions = rsd_subscription_manager_new ();
}

static void
rsd_proxy_vehicle_get_property (GObject    *object,
                                guint       property_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (object);

  switch ((RsdProxyVehicleProperty) property_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->connection);
      break;
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;
    case PROP_OBJECT_PATH:
      g_value_set_string (value, self->object_path);
      break;
    case PROP_PROXY:
      g_value_set_object (value, self->proxy);
      break;
    case PROP_ID:
      g_value_set_string (value, self->vehicle_id);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
rsd_proxy_vehicle_set_property (GObject      *object,
                                guint         property_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (object);

  switch ((RsdProxyVehicleProperty) property_id)
    {
    case PROP_CONNECTION:
      /* Construct only. */
      g_assert (self->connection == NULL);
      self->connection = g_value_dup_object (value);
      break;
    case PROP_NAME:
      /* Construct only. */
      g_assert (self->name == NULL);
      g_assert (g_value_get_string (value) == NULL ||
                g_dbus_is_name (g_value_get_string (value)));
      self->name = g_value_dup_string (value);
      break;
    case PROP_OBJECT_PATH:
      /* Construct only. */
      g_assert (self->object_path == NULL);
      g_assert (g_variant_is_object_path (g_value_get_string (value)));
      self->object_path = g_value_dup_string (value);
      break;
    case PROP_PROXY:
      /* Construct only. */
      g_assert (self->proxy == NULL);
      self->proxy = g_value_dup_object (value);
      break;
    case PROP_ID:
      /* Construct only. Validated in validate_properties() at the end of
       * the async construction. */
      g_assert (self->vehicle_id == NULL);
      self->vehicle_id = g_value_dup_string (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
rsd_proxy_vehicle_constructed (GObject *object)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (object);
  gboolean is_message_bus;

  /* Chain up to the parent class */
  G_OBJECT_CLASS (rsd_proxy_vehicle_parent_class)->constructed (object);

  /* Ensure that :name is %NULL iff :connection is not a message bus
   * connection. */
  is_message_bus = (g_dbus_connection_get_unique_name (self->connection) != NULL);
  g_assert (is_message_bus == (self->name != NULL));
}

static void
rsd_proxy_vehicle_dispose (GObject *object)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (object);

  if (self->proxy != NULL)
    {
      /* Disconnect from signals. */
      g_signal_handlers_disconnect_by_func (self->proxy, properties_changed_cb,
                                            self);
      g_signal_handlers_disconnect_by_func (self->proxy, signal_cb, self);
      g_signal_handlers_disconnect_by_func (self->proxy,
                                            proxy_notify_name_owner_cb, self);
    }

  g_clear_object (&self->proxy);
  g_clear_object (&self->connection);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->object_path, g_free);
  g_clear_pointer (&self->zones, g_ptr_array_unref);
  g_clear_pointer (&self->vehicle_id, g_free);
  g_clear_object (&self->subscriptions);
  g_clear_error (&self->init_error);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (rsd_proxy_vehicle_parent_class)->dispose (object);
}

/* Assumes the GVariants are either NULL or of the correct type. */
static gboolean
validate_properties (RsdProxyVehicle  *self,
                     GVariant         *vehicle_id,
                     GVariant         *zones,
                     GError          **error)
{
  g_autoptr (GPtrArray/*<owned RsdStaticZone>*/) zones_array = NULL;

  g_return_val_if_fail (vehicle_id == NULL ||
                        g_variant_is_of_type (vehicle_id, G_VARIANT_TYPE ("s")),
                        FALSE);
  g_return_val_if_fail (zones == NULL ||
                        g_variant_is_of_type (zones, G_VARIANT_TYPE ("as")),
                        FALSE);

  if (vehicle_id == NULL || zones == NULL)
    {
      g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE,
                   _("Required VehicleID or Zones property is missing. Might "
                     "not have permission to access the vehicle."));
      return FALSE;
    }

  if (!rsd_vehicle_id_is_valid (g_variant_get_string (vehicle_id, NULL)))
    {
      g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE,
                   _("Invalid VehicleID property ‘%s’."),
                   g_variant_get_string (vehicle_id, NULL));
      return FALSE;
    }

  zones_array = rsd_zone_array_from_variant (zones, error);

  if (zones_array == NULL)
    return FALSE;

  /* Update internal state. */
  g_clear_pointer (&self->zones, g_ptr_array_unref);
  self->zones = g_steal_pointer (&zones_array);

  g_free (self->vehicle_id);
  self->vehicle_id = g_variant_dup_string (vehicle_id, NULL);

  g_dbus_proxy_set_cached_property (self->proxy, "VehicleId", vehicle_id);
  g_dbus_proxy_set_cached_property (self->proxy, "Zones", zones);

  return TRUE;
}

/* Check that a timestamp is monotonically increasing with respect to all
 * previously seen timestamps for this proxy. Note: This does not update the
 * stored seen value. */
static gboolean
validate_timestamp (RsdProxyVehicle           *self,
                    RsdTimestampMicroseconds   current_time,
                    GError                   **error)
{
  if (current_time < self->last_seen_time)
    {
      g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                   _("Invalid clock value %" G_GINT64_FORMAT " predates most "
                     "recently seen value %" G_GINT64_FORMAT "."),
                   current_time, self->last_seen_time);

      return FALSE;
    }

  return TRUE;
}

/* Report an error with the proxied object's interactions; for example,
 * providing an incorrectly-typed attribute or an invalid update signal. */
static void
vehicle_report_error (RsdProxyVehicle *self,
                      const GError    *error)
{
  g_assert (self->proxy != NULL);

  /* Disconnect from signals. */
  g_signal_handlers_disconnect_by_func (self->proxy, properties_changed_cb,
                                        self);
  g_signal_handlers_disconnect_by_func (self->proxy, signal_cb, self);
  g_signal_handlers_disconnect_by_func (self->proxy,
                                        proxy_notify_name_owner_cb, self);

  /* Clear the proxy, which marks this #RsdProxyVehicle as invalidated. */
  DEBUG ("Marking vehicle ‘%s’ as invalidated due to error: %s",
         rsd_vehicle_get_id (RSD_VEHICLE (self)), error->message);

  g_clear_object (&self->proxy);
  g_object_notify (G_OBJECT (self), "proxy");

  g_signal_emit_by_name (self, "invalidated", error);
}

static void
properties_changed_cb (GDBusProxy *proxy,
                       GVariant   *changed_properties,
                       GStrv       invalidated_properties,
                       gpointer    user_data)
{
  RsdProxyVehicle *self;
  g_autoptr (GVariant) vehicle_id = NULL, zones = NULL;
  GVariantDict dict;
  g_autoptr (GError) error = NULL;

  self = RSD_PROXY_VEHICLE (user_data);

  /* If the object tries to change its vehicle ID or zones, error out. They’re
   * static. */
  g_variant_dict_init (&dict, changed_properties);

  vehicle_id = g_variant_dict_lookup_value (&dict, "VehicleId",
                                            G_VARIANT_TYPE ("s"));
  zones = g_variant_dict_lookup_value (&dict, "Zones", G_VARIANT_TYPE ("as"));

  g_variant_dict_clear (&dict);

  if (vehicle_id != NULL || zones != NULL ||
      g_strv_contains ((const gchar * const *) invalidated_properties, "VehicleId") ||
      g_strv_contains ((const gchar * const *) invalidated_properties, "Zones"))
    {
      g_set_error (&error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_ILLEGAL_BEHAVIOUR,
                   _("Vehicle IDs and zones cannot change after "
                     "initialisation."));
      vehicle_report_error (self, error);
    }
}

static void
signal_cb (GDBusProxy  *proxy,
           const gchar *sender_name,
           const gchar *signal_name,
           GVariant    *parameters,
           gpointer     user_data)
{
  RsdProxyVehicle *self;

  self = RSD_PROXY_VEHICLE (user_data);

  /* @sender_name is validated by the #GDBusProxy code so we can trust it. */
  if (g_strcmp0 (signal_name, "AttributesChanged") == 0)
    {
      g_autoptr (GVariant) changed_attributes = NULL;
      g_autoptr (GVariant) invalidated_attributes = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) changed_attributes_array = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) invalidated_attributes_array = NULL;
      RsdTimestampMicroseconds current_time;
      g_autoptr (GError) error = NULL;

      /* Marshal the attributes to the #RsdVehicle signal. */
      g_assert (g_variant_n_children (parameters) == 3);
      g_variant_get_child (parameters, 0, "x", &current_time);
      changed_attributes = g_variant_get_child_value (parameters, 1);
      invalidated_attributes = g_variant_get_child_value (parameters, 2);

      if (!validate_timestamp (self, current_time, &error))
        {
          vehicle_report_error (self, error);
          return;
        }

      changed_attributes_array = rsd_attribute_info_array_from_variant (changed_attributes,
                                                                        current_time,
                                                                        &error);
      if (error != NULL)
        {
          vehicle_report_error (self, error);
          return;
        }

      invalidated_attributes_array = rsd_attribute_metadata_array_from_variant (invalidated_attributes,
                                                                                &error);
      if (error != NULL)
        {
          vehicle_report_error (self, error);
          return;
        }

      /* Update state. */
      self->last_seen_time = current_time;

      if (rsd_subscription_manager_is_subscribed (self->subscriptions,
                                                  changed_attributes_array,
                                                  invalidated_attributes_array,
                                                  g_get_monotonic_time ()))
        {
          g_signal_emit_by_name (self, "attributes-changed",
                                 current_time,
                                 changed_attributes_array,
                                 invalidated_attributes_array);
        }
    }
  else if (g_strcmp0 (signal_name, "AttributesMetadataChanged") == 0)
    {
      g_autoptr (GVariant) changed_attributes = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) changed_attributes_array = NULL;
      RsdTimestampMicroseconds current_time;
      g_autoptr (GError) error = NULL;

      /* Marshal the attributes to the #RsdVehicle signal. */
      g_assert (g_variant_n_children (parameters) == 2);
      g_variant_get_child (parameters, 0, "x", &current_time);
      changed_attributes = g_variant_get_child_value (parameters, 1);

      if (!validate_timestamp (self, current_time, &error))
        {
          vehicle_report_error (self, error);
          return;
        }

      changed_attributes_array = rsd_attribute_metadata_array_from_variant (changed_attributes,
                                                                            &error);
      if (error != NULL)
        {
          vehicle_report_error (self, error);
          return;
        }

      /* Update state. */
      self->last_seen_time = current_time;

      if (changed_attributes_array->len > 0)
        {
          g_signal_emit_by_name (self, "attributes-metadata-changed",
                                 current_time,
                                 changed_attributes_array);
        }
    }
}

static void
proxy_notify_name_owner_cb (GObject    *obj,
                            GParamSpec *pspec,
                            gpointer    user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (user_data);

  DEBUG ("Name owner for proxy ‘%s’ has changed.", self->object_path);

  if (g_dbus_proxy_get_name_owner (G_DBUS_PROXY (obj)) == NULL)
    {
      g_autoptr (GError) error = NULL;

      g_set_error_literal (&error, G_DBUS_ERROR, G_DBUS_ERROR_DISCONNECTED,
                           _("Vehicle proxy owner has disconnected."));
      vehicle_report_error (self, error);
    }
}

static gboolean
set_up_proxy (RsdProxyVehicle  *self,
              GError          **error)
{
  g_autoptr (GVariant) id = NULL, zones = NULL;
  g_autoptr (GError) child_error = NULL;

  g_assert (self->proxy != NULL);

  /* Ensure the proxy has its interface info specified, so we can rely on GDBus
   * to check return value types, etc. (See #GDBusProxy:g-interface-info.) */
  if (g_dbus_proxy_get_interface_info (self->proxy) == NULL)
    g_dbus_proxy_set_interface_info (self->proxy,
                                     (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface);

  /* Subscribe to signals. */
  g_signal_connect (self->proxy, "g-properties-changed",
                    (GCallback) properties_changed_cb, self);
  g_signal_connect (self->proxy, "g-signal", (GCallback) signal_cb, self);
  g_signal_connect (self->proxy, "notify::g-name-owner",
                    (GCallback) proxy_notify_name_owner_cb, self);

  /* The vehicle’s ID and zones should be cached already, since the proxy should
   * auto-load them; if not, then the properties must be invalid. */
  id = g_dbus_proxy_get_cached_property (self->proxy, "VehicleId");
  zones = g_dbus_proxy_get_cached_property (self->proxy, "Zones");

  self->init_success = validate_properties (self, id, zones, &child_error);

  if (child_error != NULL)
    {
      self->init_error = g_error_copy (child_error);
      g_propagate_error (error, g_steal_pointer (&child_error));
    }

  return self->init_success;
}

static void
proxy_init_cb (GObject      *obj,
               GAsyncResult *result,
               gpointer      user_data)
{
  RsdProxyVehicle *self;
  g_autoptr (GTask) task = G_TASK (user_data);
  GError *error = NULL;

  /* Get the proxy. */
  self = g_task_get_task_data (task);
  g_assert (self->proxy == NULL);
  self->proxy = g_dbus_proxy_new_finish (result, &error);

  g_assert (self->initialising);
  self->initialising = FALSE;

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  if (set_up_proxy (self, &error))
    g_task_return_boolean (task, TRUE);
  else
    g_task_return_error (task, error);
}

static gboolean
rsd_proxy_vehicle_init_failable (GInitable     *initable,
                                 GCancellable  *cancellable,
                                 GError       **error)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (initable);

  /* For the moment, this only supports the case where we’ve been constructed
   * with a suitable proxy already. */
  if (self->init_error != NULL)
    {
      g_propagate_error (error, g_error_copy (self->init_error));
      return FALSE;
    }
  else if (self->init_success)
    {
      return TRUE;
    }
  else
    {
      return set_up_proxy (self, error);
    }
}

static void
rsd_proxy_vehicle_init_async (GAsyncInitable      *initable,
                              int                  io_priority,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (initable);
  g_autoptr (GTask) task = NULL;

  /* FIXME:This doesn’t support parallel initialisation. */
  g_assert (!self->initialising);

  task = g_task_new (initable, cancellable, callback, user_data);
  g_task_set_source_tag (task, rsd_proxy_vehicle_init_async);
  g_task_set_task_data (task, g_object_ref (initable), g_object_unref);

  if (self->init_error != NULL)
    g_task_return_error (task, g_error_copy (self->init_error));
  else if (self->init_success)
    g_task_return_boolean (task, TRUE);
  else
    {
      self->initialising = TRUE;
      g_dbus_proxy_new (self->connection, G_DBUS_PROXY_FLAGS_NONE,
                        (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface, self->name,
                        self->object_path, "org.apertis.Rhosydd1.Vehicle",
                        cancellable, proxy_init_cb, g_steal_pointer (&task));
    }
}

static gboolean
rsd_proxy_vehicle_init_finish (GAsyncInitable  *initable,
                               GAsyncResult    *result,
                               GError         **error)
{
  return g_task_propagate_boolean (G_TASK (result), error);
}

static const gchar *
rsd_proxy_vehicle_vehicle_get_id (RsdVehicle *vehicle)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);

  return self->vehicle_id;
}

static GPtrArray/*<owned RsdZone>*/ *
rsd_proxy_vehicle_vehicle_get_zones (RsdVehicle *vehicle)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);

  return g_ptr_array_ref (self->zones);
}

static gboolean
check_invalidated (RsdProxyVehicle *self,
                   GTask           *task)
{
  /* Invalidated? */
  if (self->proxy == NULL)
    {
      g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_INVALIDATED,
                               _("Vehicle ‘%s’ has been invalidated."),
                               self->object_path);
      return FALSE;
    }

  return TRUE;
}

static void
get_attribute_cb (GObject      *obj,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  RsdProxyVehicle *self;
  GDBusProxy *proxy = G_DBUS_PROXY (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GVariant) out = NULL;
  GVariant *in;
  const gchar *zone_path;
  const gchar *attribute_name;
  g_autoptr (RsdAttributeInfo) info = NULL;
  RsdTimestampMicroseconds current_time;
  GError *error = NULL;

  self = RSD_PROXY_VEHICLE (g_task_get_source_object (task));
  out = g_dbus_proxy_call_finish (proxy, result, &error);

  if (out == NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Convert the output from a #GVariant to an #RsdAttribute. */
  in = g_task_get_task_data (task);
  g_variant_get (in, "(&s&s)", &zone_path, &attribute_name);

  info = rsd_attribute_info_from_variant (out, zone_path, attribute_name,
                                          &current_time, &error);

  if (info == NULL ||
      !validate_timestamp (self, current_time, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  self->last_seen_time = current_time;
  timestamped_pointer_g_task_return (task, g_steal_pointer (&info),
                                     (GDestroyNotify) rsd_attribute_info_free,
                                     current_time);
}

static void
rsd_proxy_vehicle_vehicle_get_attribute_async (RsdVehicle          *vehicle,
                                               RsdZone             *zone,
                                               const gchar         *attribute_name,
                                               GCancellable        *cancellable,
                                               GAsyncReadyCallback  callback,
                                               gpointer             user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GVariant) in = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task, rsd_proxy_vehicle_vehicle_get_attribute_async);

  if (!check_invalidated (self, task))
    return;

  /* Send the input parameters through with the #GTask so get_attribute_cb()
   * has them. */
  in = g_variant_new ("(ss)", rsd_zone_get_path (zone), attribute_name);
  g_task_set_task_data (task, g_variant_ref (in),
                        (GDestroyNotify) g_variant_unref);

  g_dbus_proxy_call (self->proxy, "GetAttribute", g_steal_pointer (&in),
                     G_DBUS_CALL_FLAGS_NONE, -1,
                     cancellable, get_attribute_cb, g_steal_pointer (&task));
}

static RsdAttributeInfo *
rsd_proxy_vehicle_vehicle_get_attribute_finish (RsdVehicle                *vehicle,
                                                GAsyncResult              *result,
                                                RsdTimestampMicroseconds  *current_time,
                                                GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
get_metadata_cb (GObject      *obj,
                 GAsyncResult *result,
                 gpointer      user_data)
{
  RsdProxyVehicle *self;
  GDBusProxy *proxy = G_DBUS_PROXY (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GVariant) out = NULL;
  GVariant *in;
  const gchar *zone_path;
  const gchar *attribute_name;
  g_autoptr (RsdAttributeMetadata) metadata = NULL;
  RsdTimestampMicroseconds current_time;
  GError *error = NULL;

  self = RSD_PROXY_VEHICLE (g_task_get_source_object (task));
  out = g_dbus_proxy_call_finish (proxy, result, &error);

  if (out == NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Convert the output from a #GVariant to an #RsdAttributeMetadata. */
  in = g_task_get_task_data (task);
  g_variant_get (in, "(&s&s)", &zone_path, &attribute_name);

  metadata = rsd_attribute_metadata_from_variant (out, zone_path,
                                                  attribute_name, &current_time,
                                                  &error);

  if (metadata == NULL ||
      !validate_timestamp (self, current_time, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  self->last_seen_time = current_time;
  timestamped_pointer_g_task_return (task, g_steal_pointer (&metadata),
                                     (GDestroyNotify) rsd_attribute_metadata_free,
                                     current_time);
}

static void
rsd_proxy_vehicle_vehicle_get_metadata_async (RsdVehicle          *vehicle,
                                              RsdZone             *zone,
                                              const gchar         *attribute_name,
                                              GCancellable        *cancellable,
                                              GAsyncReadyCallback  callback,
                                              gpointer             user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GVariant) in = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task, rsd_proxy_vehicle_vehicle_get_metadata_async);

  if (!check_invalidated (self, task))
    return;

  /* Send the input parameters through with the #GTask so get_metadata_cb()
   * has them. */
  in = g_variant_new ("(ss)", rsd_zone_get_path (zone), attribute_name);
  g_task_set_task_data (task, g_variant_ref (in),
                        (GDestroyNotify) g_variant_unref);

  g_dbus_proxy_call (self->proxy, "GetAttributeMetadata", g_steal_pointer (&in),
                     G_DBUS_CALL_FLAGS_NONE, -1,
                     cancellable, get_metadata_cb, g_steal_pointer (&task));
}

static RsdAttributeMetadata *
rsd_proxy_vehicle_vehicle_get_metadata_finish (RsdVehicle                *vehicle,
                                               GAsyncResult              *result,
                                               RsdTimestampMicroseconds  *current_time,
                                               GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
set_attribute_cb (GObject      *obj,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  GDBusProxy *proxy = G_DBUS_PROXY (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GVariant) out = NULL;
  GError *error = NULL;

  out = g_dbus_proxy_call_finish (proxy, result, &error);

  if (out == NULL)
    g_task_return_error (task, error);
  else
    g_task_return_boolean (task, TRUE);
}

static void
rsd_proxy_vehicle_vehicle_set_attribute_async (RsdVehicle          *vehicle,
                                               RsdZone             *zone,
                                               const gchar         *attribute_name,
                                               GVariant            *value,
                                               GCancellable        *cancellable,
                                               GAsyncReadyCallback  callback,
                                               gpointer             user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, rsd_proxy_vehicle_vehicle_set_attribute_async);

  if (!check_invalidated (self, task))
    return;

  g_dbus_proxy_call (self->proxy, "SetAttribute",
                     g_variant_new ("(ssv)", rsd_zone_get_path (zone),
                                    attribute_name, value),
                     G_DBUS_CALL_FLAGS_NONE, -1,
                     cancellable, set_attribute_cb, g_steal_pointer (&task));
}

static void
rsd_proxy_vehicle_vehicle_set_attribute_finish (RsdVehicle    *vehicle,
                                               GAsyncResult  *result,
                                               GError       **error)
{
  GTask *task = G_TASK (result);

  g_task_propagate_pointer (task, error);
}

static void
get_all_attributes_cb (GObject      *obj,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  RsdProxyVehicle *self;
  GDBusProxy *proxy = G_DBUS_PROXY (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GVariant) out = NULL;
  g_autoptr (GVariant) out_array = NULL;
  g_autoptr (GPtrArray/*<RsdAttributeInfo>*/) props = NULL;
  RsdTimestampMicroseconds current_time;
  GError *error = NULL;

  self = RSD_PROXY_VEHICLE (g_task_get_source_object (task));
  out = g_dbus_proxy_call_finish (proxy, result, &error);

  if (out == NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Convert the variant to an array of #RsdAttributeInfos. */
  g_variant_get_child (out, 0, "x", &current_time);
  out_array = g_variant_get_child_value (out, 1);

  props = rsd_attribute_info_array_from_variant (out_array, current_time,
                                                 &error);

  if (error != NULL ||
      !validate_timestamp (self, current_time, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  self->last_seen_time = current_time;
  timestamped_pointer_g_task_return (task, g_steal_pointer (&props),
                                     (GDestroyNotify) g_ptr_array_unref,
                                     current_time);
}

static void
rsd_proxy_vehicle_vehicle_get_all_attributes_async (RsdVehicle          *vehicle,
                                                    RsdZone             *zone,
                                                    GCancellable        *cancellable,
                                                    GAsyncReadyCallback  callback,
                                                    gpointer             user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         rsd_proxy_vehicle_vehicle_get_all_attributes_async);

  if (!check_invalidated (self, task))
    return;

  g_dbus_proxy_call (self->proxy, "GetAllAttributes",
                     g_variant_new ("(s)", rsd_zone_get_path (zone)),
                     G_DBUS_CALL_FLAGS_NONE, -1,
                     cancellable, get_all_attributes_cb,
                     g_steal_pointer (&task));
}

static GPtrArray *
rsd_proxy_vehicle_vehicle_get_all_attributes_finish (RsdVehicle                *vehicle,
                                                     GAsyncResult              *result,
                                                     RsdTimestampMicroseconds  *current_time,
                                                     GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
get_all_metadata_cb (GObject      *obj,
                     GAsyncResult *result,
                     gpointer      user_data)
{
  RsdProxyVehicle *self;
  GDBusProxy *proxy = G_DBUS_PROXY (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GVariant) out = NULL;
  g_autoptr (GVariant) out_array = NULL;
  g_autoptr (GPtrArray/*<RsdAttributeMetadata>*/) metadatas = NULL;
  RsdTimestampMicroseconds current_time;
  GError *error = NULL;

  self = RSD_PROXY_VEHICLE (g_task_get_source_object (task));
  out = g_dbus_proxy_call_finish (proxy, result, &error);

  if (out == NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Convert the variant to an array of #RsdAttributeMetadata. */
  g_variant_get_child (out, 0, "x", &current_time);
  out_array = g_variant_get_child_value (out, 1);

  metadatas = rsd_attribute_metadata_array_from_variant (out_array, &error);

  if (error != NULL ||
      !validate_timestamp (self, current_time, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  self->last_seen_time = current_time;
  timestamped_pointer_g_task_return (task, g_steal_pointer (&metadatas),
                                     (GDestroyNotify) g_ptr_array_unref,
                                     current_time);
}

static void
rsd_proxy_vehicle_vehicle_get_all_metadata_async (RsdVehicle          *vehicle,
                                                  RsdZone             *zone,
                                                  GCancellable        *cancellable,
                                                  GAsyncReadyCallback  callback,
                                                  gpointer             user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         rsd_proxy_vehicle_vehicle_get_all_metadata_async);

  if (!check_invalidated (self, task))
    return;

  g_dbus_proxy_call (self->proxy, "GetAllAttributesMetadata",
                     g_variant_new ("(s)", rsd_zone_get_path (zone)),
                     G_DBUS_CALL_FLAGS_NONE, -1,
                     cancellable, get_all_metadata_cb,
                     g_steal_pointer (&task));
}

static GPtrArray *
rsd_proxy_vehicle_vehicle_get_all_metadata_finish (RsdVehicle                *vehicle,
                                                   GAsyncResult              *result,
                                                   RsdTimestampMicroseconds  *current_time,
                                                   GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
update_subscriptions_cb (GObject      *obj,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  GDBusProxy *proxy = G_DBUS_PROXY (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GVariant) out = NULL;
  GError *error = NULL;

  out = g_dbus_proxy_call_finish (proxy, result, &error);

  if (out == NULL)
    g_task_return_error (task, error);
  else
    g_task_return_boolean (task, TRUE);
}

static void
rsd_proxy_vehicle_vehicle_update_subscriptions_async (RsdVehicle          *vehicle,
                                                      GPtrArray           *subscriptions,
                                                      GPtrArray           *unsubscriptions,
                                                      GCancellable        *cancellable,
                                                      GAsyncReadyCallback  callback,
                                                      gpointer             user_data)
{
  RsdProxyVehicle *self = RSD_PROXY_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GVariant) subscriptions_variant = NULL;
  g_autoptr (GVariant) unsubscriptions_variant = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         rsd_proxy_vehicle_vehicle_update_subscriptions_async);

  if (!check_invalidated (self, task))
    return;

  subscriptions_variant = rsd_subscription_array_to_variant (subscriptions);
  unsubscriptions_variant = rsd_subscription_array_to_variant (unsubscriptions);

  rsd_subscription_manager_update_subscriptions (self->subscriptions,
                                                 subscriptions,
                                                 unsubscriptions);

  g_dbus_proxy_call (self->proxy, "UpdateSubscriptions",
                     g_variant_new ("(**)",
                                    g_steal_pointer (&subscriptions_variant),
                                    g_steal_pointer (&unsubscriptions_variant)),
                     G_DBUS_CALL_FLAGS_NONE, -1,
                     cancellable, update_subscriptions_cb,
                     g_steal_pointer (&task));
}

static void
rsd_proxy_vehicle_vehicle_update_subscriptions_finish (RsdVehicle    *vehicle,
                                                       GAsyncResult  *result,
                                                       GError       **error)
{
  GTask *task = G_TASK (result);

  g_task_propagate_boolean (task, error);
}

/**
 * rsd_proxy_vehicle_new_from_proxy:
 * @proxy: a #GDBusProxy for a `org.apertis.Rhosydd1.Vehicle` object
 * @error: return location for a #GError, or %NULL
 *
 * Create a #RsdProxyVehicle object to wrap the given existing @proxy. The
 * @proxy must have cached all the `Vehicle` properties already.
 *
 * If any of the properties are missing or invalid, an error is returned.
 *
 * Returns: (transfer full): a new #RsdProxyVehicle wrapping @proxy
 * Since: 0.2.0
 */
RsdProxyVehicle *
rsd_proxy_vehicle_new_from_proxy (GDBusProxy  *proxy,
                                  GError     **error)
{
  g_return_val_if_fail (G_IS_DBUS_PROXY (proxy), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  return g_initable_new (RSD_TYPE_PROXY_VEHICLE, NULL, error,
                         "connection", g_dbus_proxy_get_connection (proxy),
                         "name", g_dbus_proxy_get_name (proxy),
                         "object-path", g_dbus_proxy_get_object_path (proxy),
                         "proxy", proxy,
                         NULL);
}

/**
 * rsd_proxy_vehicle_new_async:
 * @connection: D-Bus connection to use
 * @name: (nullable): well-known or unique name of the peer to proxy from, or
 *    %NULL if @connection is not a message bus connection
 * @object_path: path of the object to proxy
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to invoke on completion
 * @user_data: user data to pass to @callback
 *
 * Create a new #RsdProxyVehicle for the given @object_path at @name on
 * @connection, and set up the proxy object. This is an asynchronous process
 * which might fail; object instantiation must be finished (or the error
 * returned) by calling rsd_proxy_vehicle_new_finish().
 *
 * Since: 0.2.0
 */
void
rsd_proxy_vehicle_new_async (GDBusConnection     *connection,
                             const gchar         *name,
                             const gchar         *object_path,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));
  g_return_if_fail (name == NULL || g_dbus_is_name (name));
  g_return_if_fail ((g_dbus_connection_get_unique_name (connection) == NULL) ==
                    (name == NULL));
  g_return_if_fail (object_path != NULL &&
                    g_variant_is_object_path (object_path));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  g_async_initable_new_async (RSD_TYPE_PROXY_VEHICLE, G_PRIORITY_DEFAULT,
                              cancellable, callback, user_data,
                              "connection", connection,
                              "name", name,
                              "object-path", object_path,
                              NULL);
}

/**
 * rsd_proxy_vehicle_new_finish:
 * @result: asynchronous operation result
 * @error: return location for a #GError
 *
 * Finish initialising a #RsdProxyVehicle. See rsd_proxy_vehicle_new_async().
 *
 * Returns: (transfer full): initialised #RsdProxyVehicle, or %NULL on error
 * Since: 0.2.0
 */
RsdProxyVehicle *
rsd_proxy_vehicle_new_finish (GAsyncResult     *result,
                              GError          **error)
{
  g_autoptr (GObject) source_object = NULL;

  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  source_object = g_async_result_get_source_object (result);
  return RSD_PROXY_VEHICLE (g_async_initable_new_finish (G_ASYNC_INITABLE (source_object),
                                                         result, error));
}
