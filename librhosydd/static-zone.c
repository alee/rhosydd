/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>

#include "librhosydd/types.h"
#include "librhosydd/zone.h"
#include "librhosydd/static-zone.h"


static void rsd_static_zone_zone_init    (RsdZoneInterface    *iface);
static void rsd_static_zone_dispose      (GObject             *object);
static void rsd_static_zone_get_property (GObject             *object,
                                          guint                property_id,
                                          GValue              *value,
                                          GParamSpec          *pspec);
static void rsd_static_zone_set_property (GObject             *object,
                                          guint                property_id,
                                          const GValue        *value,
                                          GParamSpec          *pspec);

static const gchar         *rsd_static_zone_zone_get_path        (RsdZone *zone);
static const gchar * const *rsd_static_zone_zone_get_tags        (RsdZone *zone);
static const gchar         *rsd_static_zone_zone_get_parent_path (RsdZone *zone);

/**
 * RsdStaticZone:
 *
 * An implementation of #RsdZone which stores static data previously grabbed off
 * D-Bus.
 *
 * Since: 0.2.0
 */
struct _RsdStaticZone
{
  GObject parent;

  const gchar *path;  /* interned */
  const gchar **tags;
  const gchar *parent_path;  /* interned */
};

typedef enum
{
  PROP_PATH = 1,
  PROP_TAGS,
  PROP_PARENT_PATH,
} RsdStaticZoneProperty;

G_DEFINE_TYPE_WITH_CODE (RsdStaticZone, rsd_static_zone, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RSD_TYPE_ZONE,
                                                rsd_static_zone_zone_init))

static void
rsd_static_zone_class_init (RsdStaticZoneClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GParamSpec *props[PROP_PARENT_PATH + 1] = { NULL, };

  object_class->dispose = rsd_static_zone_dispose;
  object_class->get_property = rsd_static_zone_get_property;
  object_class->set_property = rsd_static_zone_set_property;

  /**
   * RsdStaticZone:path:
   *
   * Path of the zone.
   *
   * Since: 0.2.0
   */
  props[PROP_PATH] =
      g_param_spec_string ("path", "Path",
                           "Path of the zone.",
                           "/",
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * RsdStaticZone:tags:
   *
   * Tags to differentiate the zone from its siblings.
   *
   * This is derived from #RsdStaticZone:path.
   *
   * Since: 0.2.0
   */
  props[PROP_TAGS] =
      g_param_spec_boxed ("tags", "Tags",
                          "Tags to differentiate the zone from its siblings.",
                          G_TYPE_STRV,
                          G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS);

  /**
   * RsdStaticZone:parent-path:
   *
   * Path of the zone’s parent. This is the empty string if and only if this
   * zone is the root zone. It is never %NULL.
   *
   * This is derived from #RsdStaticZone:path.
   *
   * Since: 0.2.0
   */
  props[PROP_PARENT_PATH] =
      g_param_spec_string ("parent-path", "Parent Path",
                           "Path of the zone’s parent.",
                           "",
                           G_PARAM_READABLE |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);
}

static void
rsd_static_zone_zone_init (RsdZoneInterface *iface)
{
  iface->get_path = rsd_static_zone_zone_get_path;
  iface->get_tags = rsd_static_zone_zone_get_tags;
  iface->get_parent_path = rsd_static_zone_zone_get_parent_path;
}

static void
rsd_static_zone_init (RsdStaticZone *self)
{
  /* Nothing here. */
}

static void
rsd_static_zone_get_property (GObject    *object,
                              guint       property_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  RsdStaticZone *self = RSD_STATIC_ZONE (object);

  switch ((RsdStaticZoneProperty) property_id)
    {
    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;
    case PROP_TAGS:
      g_value_set_boxed (value, self->tags);
      break;
    case PROP_PARENT_PATH:
      g_value_set_string (value, self->parent_path);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
rsd_static_zone_set_property (GObject      *object,
                              guint         property_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  RsdStaticZone *self = RSD_STATIC_ZONE (object);

  switch ((RsdStaticZoneProperty) property_id)
    {
    case PROP_PATH:
      /* Construct only. */
      g_assert (self->path == NULL);
      g_assert (rsd_zone_path_is_valid (g_value_get_string (value)));
      self->path = g_intern_string (g_value_get_string (value));

      /* Extract the parent path and tags. */
      self->tags = rsd_zone_path_tail (self->path, &self->parent_path);

      break;
    case PROP_TAGS:
      /* Read only. */
    case PROP_PARENT_PATH:
      /* Read only. */
    default:
      g_assert_not_reached ();
    }
}

static void
rsd_static_zone_dispose (GObject *object)
{
  RsdStaticZone *self = RSD_STATIC_ZONE (object);

  g_clear_pointer (&self->tags, g_free);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (rsd_static_zone_parent_class)->dispose (object);
}

static const gchar *
rsd_static_zone_zone_get_path (RsdZone *zone)
{
  RsdStaticZone *self = RSD_STATIC_ZONE (zone);

  return self->path;
}

static const gchar * const *
rsd_static_zone_zone_get_tags (RsdZone *zone)
{
  RsdStaticZone *self = RSD_STATIC_ZONE (zone);

  return self->tags;
}

static const gchar *
rsd_static_zone_zone_get_parent_path (RsdZone *zone)
{
  RsdStaticZone *self = RSD_STATIC_ZONE (zone);

  return self->parent_path;
}

/**
 * rsd_static_zone_new:
 * @path: new zone’s path
 *
 * Create a new #RsdStaticZone for the given @path.
 *
 * Since: 0.2.0
 */
RsdStaticZone *
rsd_static_zone_new (const gchar *path)
{
  g_return_val_if_fail (rsd_zone_path_is_valid (path), NULL);

  return g_object_new (RSD_TYPE_STATIC_ZONE,
                       "path", path,
                       NULL);
}
