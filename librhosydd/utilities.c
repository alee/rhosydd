/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <string.h>

#include "librhosydd/attribute.h"
#include "librhosydd/static-zone.h"
#include "librhosydd/subscription.h"
#include "librhosydd/utilities.h"
#include "librhosydd/vehicle.h"
#include "librhosydd/zone.h"


static gboolean
validate_zone_path (const gchar  *zone_path,
                    GError      **error)
{
  if (!rsd_zone_path_is_valid (zone_path))
    {
      g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                   _("Invalid zone path ‘%s’."), zone_path);
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_attribute_name (const gchar  *attribute_name,
                         GError      **error)
{
  if (!rsd_attribute_name_is_valid (attribute_name))
    {
      g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                   _("Invalid attribute name ‘%s’."), attribute_name);
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_attribute_accuracy (gdouble   accuracy,
                             GError  **error)
{
  if (accuracy < 0.0)
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                   _("Invalid attribute accuracy %f; must be non-negative."),
                   accuracy);
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_attribute_availability (RsdAttributeAvailability   availability,
                                 GError                  **error)
{
  if (!rsd_attribute_availability_is_valid (availability))
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                   _("Invalid attribute availability %u."),
                   availability);
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_attribute_flags (RsdAttributeFlags   flags,
                          GError           **error)
{
  if (!rsd_attribute_flags_is_valid (flags))
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                   _("Invalid attribute flags %u."),
                   flags);
      return FALSE;
    }

  return TRUE;
}

static gboolean
variant_is_numeric (GVariant *variant)
{
  GVariantClass type;

  type = g_variant_classify (variant);

  return (type == G_VARIANT_CLASS_BYTE ||
          type == G_VARIANT_CLASS_DOUBLE ||
          type == G_VARIANT_CLASS_INT16 ||
          type == G_VARIANT_CLASS_INT32 ||
          type == G_VARIANT_CLASS_INT64 ||
          type == G_VARIANT_CLASS_UINT16 ||
          type == G_VARIANT_CLASS_UINT32 ||
          type == G_VARIANT_CLASS_UINT64);
}

static gboolean
validate_value_range (GVariant  *minimum_value,
                      GVariant  *maximum_value,
                      GVariant  *hysteresis,
                      GError   **error)
{
  /* The types and NULLness of the two values must always be the same. */
  if ((minimum_value == NULL) != (maximum_value == NULL) ||
      ((hysteresis != NULL) && (minimum_value == NULL)) ||
      (minimum_value != NULL &&
       !g_variant_type_equal (g_variant_get_type (minimum_value),
                              g_variant_get_type (maximum_value))) ||
      (hysteresis != NULL &&
       !g_variant_type_equal (g_variant_get_type (hysteresis),
                              g_variant_get_type (minimum_value))))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
                           _("Invalid subscription value: types must be "
                             "equal."));
      return FALSE;
    }

  /* If the values are specified, they must be numeric. */
  if (minimum_value != NULL && !variant_is_numeric (minimum_value))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
                           _("Invalid subscription value: only numeric values "
                             "may be specified."));
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_update_periods (guint    minimum_period,
                         guint    maximum_period,
                         GError **error)
{
  if (maximum_period < minimum_period)
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
                   _("Invalid subscription period range %u–%uµs."),
                   minimum_period, maximum_period);
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_subscription_zone_path_and_attribute_name (const gchar  *zone_path,
                                                    const gchar  *attribute_name,
                                                    GError      **error)
{
  /* Allow @attribute_name to be the empty string iff @zone_path == '/'. */
  if (!validate_zone_path (zone_path, error))
    return FALSE;

  if (g_strcmp0 (zone_path, "/") == 0 && g_strcmp0 (attribute_name, "") == 0)
    return TRUE;

  return validate_attribute_name (attribute_name, error);
}

static gboolean
validate_attribute_timestamp (RsdTimestampMicroseconds   last_updated,
                              RsdTimestampMicroseconds   current_time,
                              GError                   **error)
{
  if (last_updated > current_time)
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                   _("Invalid last updated time %" G_GINT64_FORMAT " (current "
                     "time %" G_GINT64_FORMAT ")."),
                   last_updated, current_time);
      return FALSE;
    }

  return TRUE;
}

/**
 * rsd_attribute_info_array_from_variant:
 * @variant: a #GVariant of type `a(ss(vdx)(uu))`
 * @current_time: current time to which the attribute metadata is relative and
 *    should be validated against, in microseconds
 * @error: return location for a #GError, or %NULL
 *
 * Build an array of #RsdAttributeInfos from a #GVariant of type
 * `a(ss(vdx)(uu))`. The array may be empty.
 *
 * The @variant will be validated, and an error will be returned if its type or
 * any of its entries are invalid. The @variant must be in normal form.
 *
 * Returns: (element-type RsdAttributeInfo) (transfer container): attribute info
 *    array
 * Since: 0.4.0
 */
GPtrArray *
rsd_attribute_info_array_from_variant (GVariant                  *variant,
                                       RsdTimestampMicroseconds   current_time,
                                       GError                   **error)
{
  g_autoptr (GPtrArray/*<RsdAttributeInfo>*/) attributes = NULL;
  GVariantIter iter;
  const gchar *zone_path;
  const gchar *attribute_name;
  GVariant *value;
  gdouble accuracy;
  RsdTimestampMicroseconds last_updated;
  RsdAttributeAvailability availability;
  RsdAttributeFlags flags;
  g_autoptr (GHashTable/*<owned utf8, unowned utf8>*/) attributes_hash = NULL;

  g_return_val_if_fail (g_variant_is_normal_form (variant), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  /* Type check. */
  if (!g_variant_is_of_type (variant, G_VARIANT_TYPE ("a(ss(vdx)(uu))")))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                           _("Invalid type for attribute."));
      return NULL;
    }

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  attributes_hash = g_hash_table_new_full (g_str_hash, g_str_equal,
                                           g_free, NULL);
  g_variant_iter_init (&iter, variant);

  while (g_variant_iter_loop (&iter, "(&s&s(vdx)(uu))", &zone_path,
                              &attribute_name, &value, &accuracy, &last_updated,
                              &availability, &flags))
    {
      g_autoptr (RsdAttributeInfo) info = NULL;
      RsdAttribute prop;
      RsdAttributeMetadata metadata;
      g_autofree gchar *zone_attribute = NULL;

      if (!validate_zone_path (zone_path, error) ||
          !validate_attribute_name (attribute_name, error) ||
          !validate_attribute_accuracy (accuracy, error) ||
          !validate_attribute_availability (availability, error) ||
          !validate_attribute_flags (flags, error) ||
          !validate_attribute_timestamp (last_updated, current_time, error))
        {
          g_variant_unref (value);
          return NULL;
        }

      /* Check for duplicates. */
      zone_attribute = g_strconcat (zone_path, attribute_name, NULL);
      if (g_hash_table_contains (attributes_hash, zone_attribute))
        {
          g_variant_unref (value);
          g_set_error (error, RSD_VEHICLE_ERROR,
                       RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                       _("Duplicate attributes in list (attribute ‘%s’ in zone "
                         "‘%s’."), zone_path, attribute_name);
          return NULL;
        }

      g_hash_table_insert (attributes_hash, zone_attribute, zone_attribute);
      zone_attribute = NULL;  /* transferred above */

      /* Build the #RsdAttributeInfo. */
      prop.value = value;
      prop.accuracy = accuracy;
      prop.last_updated = last_updated;

      metadata.zone_path = g_intern_string (zone_path);
      metadata.name = g_intern_string (attribute_name);
      metadata.availability = availability;
      metadata.flags = flags;

      info = rsd_attribute_info_new (&prop, &metadata);
      g_ptr_array_add (attributes, g_steal_pointer (&info));
    }

  return g_steal_pointer (&attributes);
}

/**
 * rsd_attribute_info_array_to_variant:
 * @array: (transfer none) (element-type RsdAttributeInfo): potentially empty
 *     array of attribute info
 *
 * Convert the array of #RsdAttributeInfos to a #GVariant. The array may be
 * empty, in which case an empty #GVariant array is returned.
 *
 * Returns: (transfer full): #GVariant version of the info array
 * Since: 0.2.0
 */
GVariant *
rsd_attribute_info_array_to_variant (GPtrArray *array)
{
  GVariantBuilder builder;
  gsize i;

  g_return_val_if_fail (array != NULL, NULL);

  /* Convert the array of RsdAttributeInfos to a GVariant. */
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a(ss(vdx)(uu))"));

  for (i = 0; i < array->len; i++)
    {
      const RsdAttributeInfo *info = array->pdata[i];

      g_variant_builder_add (&builder, "(ss(vdx)(uu))",
                             info->metadata.zone_path, info->metadata.name,
                             info->attribute.value,
                             info->attribute.accuracy,
                             info->attribute.last_updated,
                             info->metadata.availability, info->metadata.flags);
    }

  return g_variant_builder_end (&builder);
}

/**
 * rsd_attribute_metadata_array_from_variant:
 * @variant: a #GVariant of type `a(ss(uu))`
 * @error: return location for a #GError, or %NULL
 *
 * Build an array of #RsdAttributeMetadatas from a #GVariant of type
 * `a(ss(uu))`. The array may be empty.
 *
 * The @variant will be validated, and an error will be returned if its type or
 * any of its entries are invalid. The @variant must be in normal form.
 *
 * Returns: (element-type RsdAttributeMetadata) (transfer container): attribute
 *    metadata array
 * Since: 0.2.0
 */
GPtrArray *
rsd_attribute_metadata_array_from_variant (GVariant  *variant,
                                           GError   **error)
{
  g_autoptr (GPtrArray/*<RsdAttributeMetadata>*/) metadatas = NULL;
  GVariantIter iter;
  const gchar *zone_path;
  const gchar *attribute_name;
  RsdAttributeAvailability availability;
  RsdAttributeFlags flags;
  g_autoptr (GHashTable/*<owned utf8, unowned utf8>*/) metadatas_hash = NULL;

  g_return_val_if_fail (g_variant_is_normal_form (variant), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  /* Type check. */
  if (!g_variant_is_of_type (variant, G_VARIANT_TYPE ("a(ss(uu))")))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                           _("Invalid type for attribute."));
      return NULL;
    }

  metadatas = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);
  metadatas_hash = g_hash_table_new_full (g_str_hash, g_str_equal,
                                          g_free, NULL);
  g_variant_iter_init (&iter, variant);

  while (g_variant_iter_loop (&iter, "(&s&s(uu))", &zone_path, &attribute_name,
                              &availability, &flags))
    {
      g_autoptr (RsdAttributeMetadata) metadata = NULL;
      g_autofree gchar *zone_attribute = NULL;

      if (!validate_zone_path (zone_path, error) ||
          !validate_attribute_name (attribute_name, error) ||
          !validate_attribute_availability (availability, error) ||
          !validate_attribute_flags (flags, error))
        {
          return NULL;
        }

      /* Check for duplicates. */
      zone_attribute = g_strconcat (zone_path, attribute_name, NULL);
      if (g_hash_table_contains (metadatas_hash, zone_attribute))
        {
          g_set_error (error, RSD_VEHICLE_ERROR,
                       RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                       _("Duplicate attributes in list (attribute ‘%s’ in zone "
                         "‘%s’."), zone_path, attribute_name);
          return NULL;
        }

      g_hash_table_insert (metadatas_hash, zone_attribute, zone_attribute);
      zone_attribute = NULL;  /* transferred above */

      /* Build the #RsdAttributeMetadata. */
      metadata = rsd_attribute_metadata_new (zone_path, attribute_name,
                                            availability, flags);
      g_ptr_array_add (metadatas, g_steal_pointer (&metadata));
    }

  return g_steal_pointer (&metadatas);
}

/**
 * rsd_attribute_metadata_array_to_variant:
 * @array: (transfer none) (element-type RsdAttributeMetadata): potentially empty
 *     array of metadata
 *
 * Convert the array of #RsdAttributeMetadatas to a #GVariant. The array may be
 * empty, in which case an empty #GVariant array is returned.
 *
 * Returns: (transfer full): #GVariant version of the metadata array
 * Since: 0.2.0
 */
GVariant *
rsd_attribute_metadata_array_to_variant (GPtrArray *array)
{
  GVariantBuilder builder;
  gsize i;

  g_return_val_if_fail (array != NULL, NULL);

  /* Convert the array of #RsdAttributeMetadatas to a GVariant. */
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a(ss(uu))"));

  for (i = 0; i < array->len; i++)
    {
      const RsdAttributeMetadata *metadata = array->pdata[i];

      g_variant_builder_add (&builder, "(ss(uu))", metadata->zone_path,
                             metadata->name, metadata->availability,
                             metadata->flags);
    }

  return g_variant_builder_end (&builder);
}

/**
 * rsd_attribute_info_from_variant:
 * @variant: a #GVariant of type `(x(vdx)(uu))`
 * @zone_path: the path of the zone containing the attribute
 * @attribute_name: the name of the attribute
 * @current_time: (out caller-allocates) (optional): return location for the
 *    current time to which the attribute metadata is relative, in microseconds
 * @error: return location for a #GError
 *
 * Build a #RsdAttributeInfo from a #GVariant of type `((vdx)(uu))`, plus the
 * @zone_path and @attribute_name.
 *
 * The @variant will be validated, and an error will be returned if its type or
 * any of its entries are invalid. The @variant must be in normal form.
 *
 * Returns: (transfer full): attribute info
 * Since: 0.4.0
 */
RsdAttributeInfo *
rsd_attribute_info_from_variant (GVariant                  *variant,
                                 const gchar               *zone_path,
                                 const gchar               *attribute_name,
                                 RsdTimestampMicroseconds  *current_time,
                                 GError                   **error)
{
  g_autoptr (GVariant) value = NULL;
  gdouble accuracy;
  RsdTimestampMicroseconds last_updated;
  RsdAttributeAvailability availability;
  RsdAttributeFlags flags;
  RsdAttribute attribute;
  RsdAttributeMetadata metadata;
  RsdTimestampMicroseconds local_current_time;

  /* FIXME: It’s a bit silly for this to have type (x(vdx)(uu)) rather than
   * (ssx(vdx)(uu)). */

  g_return_val_if_fail (g_variant_is_normal_form (variant), NULL);
  g_return_val_if_fail (rsd_zone_path_is_valid (zone_path), NULL);
  g_return_val_if_fail (rsd_attribute_name_is_valid (attribute_name), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  /* Type check. */
  if (!g_variant_is_of_type (variant, G_VARIANT_TYPE ("(x(vdx)(uu))")))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                           _("Invalid type for attribute."));
      return NULL;
    }

  /* Convert the output from a #GVariant to an #RsdAttribute. */
  g_variant_get (variant, "(x(vdx)(uu))", &local_current_time, &value,
                 &accuracy, &last_updated, &availability, &flags);

  if (!validate_attribute_accuracy (accuracy, error) ||
      !validate_attribute_availability (availability, error) ||
      !validate_attribute_flags (flags, error) ||
      !validate_attribute_timestamp (last_updated, local_current_time, error))
    {
      return NULL;
    }

  attribute.value = value;
  attribute.accuracy = accuracy;
  attribute.last_updated = last_updated;

  metadata.zone_path = g_intern_string (zone_path);
  metadata.name = g_intern_string (attribute_name);
  metadata.availability = availability;
  metadata.flags = flags;

  if (current_time != NULL)
    *current_time = local_current_time;

  return rsd_attribute_info_new (&attribute, &metadata);
}

/**
 * rsd_attribute_metadata_from_variant:
 * @variant: a #GVariant of type `((uu))`
 * @zone_path: the path of the zone containing the attribute
 * @attribute_name: the name of the attribute
 * @current_time: (out caller-allocates) (optional): return location for the
 *    current time to which the attribute metadata is relative, in microseconds
 * @error: return location for a #GError
 *
 * Build a #RsdAttributeMetadata from a #GVariant of type `((uu))`, plus the
 * @zone_path and @attribute_name.
 *
 * The @variant will be validated, and an error will be returned if its type or
 * any of its entries are invalid. The @variant must be in normal form.
 *
 * Returns: (transfer full): attribute metadata
 * Since: 0.4.0
 */
RsdAttributeMetadata *
rsd_attribute_metadata_from_variant (GVariant                  *variant,
                                     const gchar               *zone_path,
                                     const gchar               *attribute_name,
                                     RsdTimestampMicroseconds  *current_time,
                                     GError                   **error)
{
  RsdAttributeAvailability availability;
  RsdAttributeFlags flags;
  RsdTimestampMicroseconds local_current_time;

  /* FIXME: It’s a bit silly for this to have type (x(uu)) rather than (ssx(uu))
   * or just (uu). */

  g_return_val_if_fail (g_variant_is_normal_form (variant), NULL);
  g_return_val_if_fail (rsd_zone_path_is_valid (zone_path), NULL);
  g_return_val_if_fail (rsd_attribute_name_is_valid (attribute_name), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  /* Type check. */
  if (!g_variant_is_of_type (variant, G_VARIANT_TYPE ("(x(uu))")))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                           _("Invalid type for attribute."));
      return NULL;
    }

  g_variant_get (variant, "(x(uu))", &local_current_time, &availability,
                 &flags);

  if (!validate_attribute_availability (availability, error) ||
      !validate_attribute_flags (flags, error))
    {
      return NULL;
    }

  if (current_time != NULL)
    *current_time = local_current_time;

  return rsd_attribute_metadata_new (zone_path, attribute_name,
                                     availability, flags);
}

/**
 * rsd_zone_array_from_variant:
 * @variant: a #GVariant of type `as`
 * @error: return location for a #GError, or %NULL
 *
 * Build an array of #RsdZones from a #GVariant of type
 * `as`. The array must have at least one element (the root zone), or an error
 * is returned.
 *
 * The @variant will be validated, and an error will be returned if its type or
 * any of its entries are invalid. The @variant must be in normal form.
 *
 * Returns: (element-type RsdZone) (transfer container): zone array
 * Since: 0.2.0
 */
GPtrArray *
rsd_zone_array_from_variant (GVariant  *variant,
                             GError   **error)
{
  g_autoptr (GPtrArray/*<owned RsdZone>*/) zones = NULL;
  GVariantIter iter;
  const gchar *zone_path;
  g_autoptr (GHashTable/*<unowned utf8, unowned utf8>*/) zones_hash = NULL;
  gboolean has_root_zone = FALSE;

  g_return_val_if_fail (g_variant_is_normal_form (variant), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  /* Type check. */
  if (!g_variant_is_of_type (variant, G_VARIANT_TYPE ("as")))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                           _("Invalid type for zones array."));
      return NULL;
    }

  zones = g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  zones_hash = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, NULL);
  g_variant_iter_init (&iter, variant);

  while (g_variant_iter_loop (&iter, "&s", &zone_path))
    {
      if (!validate_zone_path (zone_path, error))
        {
          return NULL;
        }

      /* Check for duplicates. */
      if (g_hash_table_contains (zones_hash, zone_path))
        {
          g_set_error (error, RSD_VEHICLE_ERROR,
                       RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                       _("Duplicate zones in list (‘%s’)."), zone_path);
          return NULL;
        }

      g_hash_table_insert (zones_hash, (gpointer) zone_path,
                           (gpointer) zone_path);

      /* Build the #RsdZone. */
      g_ptr_array_add (zones, rsd_static_zone_new (zone_path));

      has_root_zone = has_root_zone || (g_strcmp0 (zone_path, "/") == 0);
    }

  /* Ensure a valid array of zones is returned. */
  if (!has_root_zone)
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                   _("Empty zone list; root zone missing."));
      return NULL;
    }

  return g_steal_pointer (&zones);
}

/**
 * rsd_subscription_array_to_variant:
 * @array: (transfer none) (element-type RsdSubscription): potentially empty
 *     array of subscriptions
 *
 * Convert the array of #RsdSubscriptions to a #GVariant. The array may be
 * empty, in which case an empty #GVariant array is returned.
 *
 * Returns: (transfer full): #GVariant version of the subscription array
 * Since: 0.2.0
 */
GVariant *
rsd_subscription_array_to_variant (GPtrArray *array)
{
  GVariantBuilder builder;
  gsize i;

  g_return_val_if_fail (array != NULL, NULL);

  /* Convert the array of #RsdSubscriptions to a GVariant. */
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a(ssa{sv})"));

  for (i = 0; i < array->len; i++)
    {
      GVariantBuilder asv_builder;
      const RsdSubscription *subscription = array->pdata[i];

      g_variant_builder_init (&asv_builder, G_VARIANT_TYPE ("a{sv}"));

      if (subscription->minimum_value != NULL)
        g_variant_builder_add (&asv_builder, "{sv}", "minimum-value",
                               subscription->minimum_value);
      if (subscription->maximum_value != NULL)
        g_variant_builder_add (&asv_builder, "{sv}", "maximum-value",
                               subscription->maximum_value);
      if (subscription->hysteresis != NULL)
        g_variant_builder_add (&asv_builder, "{sv}", "hysteresis",
                               subscription->hysteresis);

      if (subscription->minimum_period > 0)
        g_variant_builder_add (&asv_builder, "{sv}", "minimum-period",
                               g_variant_new_uint32 (subscription->minimum_period));
      if (subscription->maximum_period < G_MAXUINT32)
        g_variant_builder_add (&asv_builder, "{sv}", "maximum-period",
                               g_variant_new_uint32 (subscription->maximum_period));

      g_variant_builder_add (&builder, "(ssa{sv})", subscription->zone_path,
                             subscription->attribute_name, &asv_builder);
    }

  return g_variant_builder_end (&builder);
}

/* This will remove the entry from the dict, so that (ideally) the dict is
 * empty after all checks are complete. If non-empty, it contains unknown
 * keys. */
static gboolean
dict_steal_with_type_check (GVariantDict  *dict,
                            const gchar   *key,
                            const gchar   *type,
                            gpointer       value_out,
                            GError       **error)
{
  if (g_variant_dict_contains (dict, key) &&
      !g_variant_dict_lookup (dict, key, type, value_out))
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
                   _("Invalid type for ‘%s’ subscription key."), key);
      return FALSE;
    }

  g_variant_dict_remove (dict, key);

  return TRUE;
}

/**
 * rsd_subscription_array_from_variant:
 * @variant: a #GVariant of type `a(ssa{sv})`
 * @error: return location for a #GError, or %NULL
 *
 * Build an array of #RsdSubscriptions from a #GVariant of type `a(ssa{sv})`.
 * The array may be empty.
 *
 * The @variant will be validated, and an error will be returned if its type or
 * any of its entries are invalid. The @variant must be in normal form.
 *
 * Returns: (element-type RsdSubscription) (transfer container): subscription
 *    array
 * Since: 0.2.0
 */
GPtrArray *
rsd_subscription_array_from_variant (GVariant  *variant,
                                     GError   **error)
{
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;
  GVariantIter iter;
  const gchar *zone_path, *attribute_name;
  GVariant *asv;

  g_return_val_if_fail (g_variant_is_normal_form (variant), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  /* Type check. */
  if (!g_variant_is_of_type (variant, G_VARIANT_TYPE ("a(ssa{sv})")))
    {
      g_set_error_literal (error, RSD_VEHICLE_ERROR,
                           RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
                           _("Invalid type for subscriptions array."));
      return NULL;
    }

  subscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_variant_iter_init (&iter, variant);

  while (g_variant_iter_loop (&iter, "(&s&s@a{sv})", &zone_path, &attribute_name,
                              &asv))
    {
      GVariantDict dict;
      g_autoptr (GVariant) minimum_value = NULL, maximum_value = NULL;
      g_autoptr (GVariant) hysteresis = NULL;
      g_autoptr (GVariant) updated_dict = NULL;
      guint minimum_period = 0, maximum_period = G_MAXUINT;

      g_variant_dict_init (&dict, asv);

      if (!validate_subscription_zone_path_and_attribute_name (zone_path,
                                                               attribute_name,
                                                               error) ||
          !dict_steal_with_type_check (&dict, "minimum-value", "@?",
                                       &minimum_value, error) ||
          !dict_steal_with_type_check (&dict, "maximum-value", "@?",
                                       &maximum_value, error) ||
          !dict_steal_with_type_check (&dict, "hysteresis", "@?",
                                       &hysteresis, error) ||
          !dict_steal_with_type_check (&dict, "minimum-period", "u",
                                       &minimum_period, error) ||
          !dict_steal_with_type_check (&dict, "maximum-period", "u",
                                       &maximum_period, error) ||
          !validate_value_range (minimum_value, maximum_value, hysteresis,
                                 error) ||
          !validate_update_periods (minimum_period, maximum_period, error))
        {
          g_variant_unref (asv);
          g_variant_dict_clear (&dict);
          return NULL;
        }

      /* Are there any entries left in the dict? If so, they are unknown. */
      updated_dict = g_variant_dict_end (&dict);
      if (g_variant_n_children (updated_dict) != 0)
        {
          g_set_error_literal (error, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
                               _("Unknown key in subscription table."));
          g_variant_unref (asv);
          return NULL;
        }

      /* Build the #RsdSubscription. */
      g_ptr_array_add (subscriptions,
                       rsd_subscription_new (zone_path, attribute_name,
                                             minimum_value, maximum_value,
                                             hysteresis,
                                             minimum_period, maximum_period));
    }

  return g_steal_pointer (&subscriptions);
}
