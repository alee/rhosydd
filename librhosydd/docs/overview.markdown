### librhosydd Overview

[Mail the maintainers](mailto:rhosydd@apertis.org)

librhosydd is a library which provides applications access to the sensors and
actuators attached to a vehicle. It is an interface to the system’s sensors
and actuators daemon, which it communicates with over D-Bus. librhosydd is
intended to be used by applications and system services.

The overall design of the sensors and actuators system is given in the
[Sensors and Actuators design document](https://wiki.apertis.org/mediawiki/index.php/ConceptDesigns).
The design of the system daemon is discussed in the documentation for
libcroesor, a library used in the implementation of the service and its
backends.

The main interface for librhosydd is the vehicle manager ([RsdVehicleManager]())
which provides a list of the available vehicles. An [RsdVehicle](), as returned
by the manager, represents a physical vehicle, and there might be multiple
vehicles connected to the system. For example, if a car is towing a trailer, and
has a removable roof rack attached, the manager might list three vehicles:
 * Car
 * Trailer
 * Roof rack

Each vehicle has one or more zones, and zero or more attributes. Each attribute
is in a zone, and duplicate attributes are only allowed if they are in different
zones. The zones for a vehicle are unique to that vehicle, and form a tree which
can be used to give the physical location of an attribute on the vehicle.

For example, a car might have a `tire.pressure` attribute, which is present in
four different zones: the front–left, front–right, rear–left and rear–right
zones. These four attribute values give the pressures for all tyres.

Zones are nested in a hierarchy, so if (for example) an attribute needed to be
presented for the flow rate of two air conditioning vents in front of the front
passenger seat, they could be put in zones:
 * `/front,left/left/`
 * `/front,left/right/`
And the foot vent could be in zone:
 * `/front,left/bottom/`
In this example, the ‘path’ syntax for representing zones has been used, which
presents the zone hierarchy unambiguously and compactly. It is described in
detail in the [zone paths documentation]{librhosydd-api.markdown#zone-paths}.

Attributes can be readable, writable, or unavailable (for a variety of reasons).
An attribute which is read-only is known as a sensor, as it will typically
correspond to a physical sensor in the vehicle. Similarly, an attribute which is
writable is known as an actuator. Attributes may be unavailable due to security
policies imposed by the application framework, or by the vehicle OEM; meaning
that applications cannot access them at all. On the other hand, they may also
be unavailable because of some limitation on the physical device they are
associated with — for example, heated seat controls might be disabled for the
rear seats in a car if they are folded down to extend the boot space. Or they
may correspond to a feature which is only available in a higher-specification
model of the same family of vehicles.

An application’s access to attributes is subject to security policy imposed by
various parts of the system, including the application’s manifest. Permission to
//enumerate// attributes is separate from permission to //access// them, which
means that an application which is allowed to enumerate attributes may get the
metadata for all attributes, but cannot necessarily get their values. Metadata
for attributes covers their readability and writability, and their availability.

Applications can be notified of changes to attribute metadata and values by
connecting to the [#RsdVehicle::attributes-metadata-changed]() or
[#RsdVehicle::attributes-changed]() signals. First, an application must
//subscribe// to receive updates for the attributes it is interested in, using
rsd_vehicle_update_subscriptions_async(). To receive updates for all attributes
which the application is authorised to see, use a wildcard subscription
(rsd_subscription_new_wildcard()).

Note that the system may notify the application of attributes other than those
it is subscribed to, if doing so is more efficient than removing those
attributes from a signal emission. Applications should handle, but not rely on,
this behaviour.

Each attribute value has an update timestamp associated with it, which is set by
the backend at the same time as the value is set. Ideally, the timestamp gives
the time when that value was measured by the sensor; but due to limitations in
the automotive domain, it may give a later time, such as when the value was read
off the vehicle’s CAN bus. As long as the timestamps for all attribute values are
consistent, and come from the same clock, they are useful. The intention with
providing attribute timestamps is to allow applications to factor them into
predictions or models of how the value changes over time.

The clock domains for two vehicles may differ, as long as all the timestamps for
the attributes within a single vehicle are comparable with each other. Hence it
may not be possible to compare the timestamps for two attributes from different
vehicles.

As discussed in the design document, librhosydd and the system’s sensors and
actuators daemon are not designed for transmitting high bandwidth or low latency
sensor data. It should be transmitted out of band, using separate channels which
are set up via librhosydd (for example, a Unix socket which is advertised over
D-Bus).
