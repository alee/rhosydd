# API reference

## Zones

The W3C Vehicle Information Access API has a [concept of
‘zones’](http://www.w3.org/2014/automotive/vehicle_spec.html#zone-interface)
which indicate the physical location of a device in the vehicle. The current
version of the specification has a misleading `ZonePosition` enumerated type which
is not used elsewhere in the API. The zones which apply to an attribute are
specified as an array of opaque strings, which may have values other than those
in `ZonePosition`. Multiple strings can be used (like tags) to describe the
location of a device in several dimensions. Furthermore, zones may be nested
hierarchically to build an abstract map of the layout of a vehicle.

Each zone has a unique string path (its zone path), a parent zone, and a
(potentially empty) list of tags to differentiate it from its siblings. There is
always a root zone (path `/`, no tags) which represents the entire
vehicle. Each tree of zones is unique to a particular vehicle. Each zone is
uniquely identified within this tree by its path, which is the combination of
its parent path and tag list. Consequently, no two siblings may have the same
tag list. However, they may share entries in their tag lists, which allows
‘overlapping’ areas in the vehicle to be represented.

`ZonePosition` has been extended with additional strings to better describe
attribute locations. Strings which are not defined in this extended enumerated
type must not be used in [](RsdZone).

Attributes should be tagged with zone information which is likely to be useful to
application developers. For example, it is typically not useful to know whether
the engine is in the front or rear of the vehicle, but is useful to know that a
particular light is an interior light, above the driver.

## Zone Paths

Zone paths are formed of tag lists, separated by slashes. Each tag list is a
potentially empty list of tags, separated by commas, in alphabetical order. Each
tag must be a non-empty string from the letters a-z, A-Z and the digits 0-9.

Zone paths are *not* D-Bus object paths, and must not be confused with them,
even though they use similar syntax.

A zone path must start and end with a slash: the leading slash indicates the
root zone, and each tag list is followed by a slash. So the root zone has the
path `/`. A child zone of the root zone which has an empty tag list has path
`//`. A child zone of the root zone which is tagged `left` has path `/left/`.
One which is tagged `top` and `left` has path `/left,top/`. A lower descendant
of the root zone might be `/left,top//center/front/`.

As a special case, the parent zone of the root zone is the empty string. This is
not a valid zone path, but is accepted in certain situations (where documented).

## Attribute Names

Attributes are named following the [Vehicle Data
specification](https://www.w3.org/2014/automotive/data_spec.html), starting with
the [Vehicle
interface](https://www.w3.org/2014/automotive/vehicle_spec.html#idl-def-Vehicle).

Different parts of the specification add partial interfaces which extend the
Vehicle interface. For example, fuel configuration information is exposed as
attributes starting with
[`fuelConfiguration`](https://www.w3.org/2014/automotive/data_spec.html#idl-def-Vehicle):
 * [`fuelConfiguration.fuelType`](https://www.w3.org/2014/automotive/data_spec.html#idl-def-FuelConfiguration)
 * `fuelConfiguration.refuelPosition`

Attribute names are formed of components (which may contain the letters a-z,
A-Z, and the digits 0-9; they start with a letter a-z, and are in camelCase; or
start with a letter A-Z and are in capitals) separated by dots. Attribute names
start and end with a component (not a dot) and contain one or more components.

Custom (non-standardised) attributes are exposed beneath an OEM-specific
namespace, using reverse-DNS notation for a domain which is controlled by that
OEM. For example, for a vendor ‘My OEM’ whose website is myoem.com, the
following attributes might be used:
 * `com.myoem.fancySeatController.backTemperature`
 * `com.myoem.roofRack.open`
 * `com.myoem.roofRack.mass`

## Vehicle IDs

Each vehicle has an identifier, and if attributes from two backends are to be
aggregated, the backends must expose them using the same vehicle ID. It is
assumed that the backends acquire the vehicle ID through an out-of-band
mechanism.

Vehicle IDs are as described in [](rsd_vehicle_id_is_valid).
