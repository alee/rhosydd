/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>

#include "librhosydd/subscription.h"
#include "librhosydd/zone.h"


G_DEFINE_BOXED_TYPE (RsdSubscription, rsd_subscription,
                     rsd_subscription_copy, rsd_subscription_free)

static gboolean
variant_is_numeric (GVariant *variant)
{
  GVariantClass type;

  type = g_variant_classify (variant);

  return (type == G_VARIANT_CLASS_BYTE ||
          type == G_VARIANT_CLASS_DOUBLE ||
          type == G_VARIANT_CLASS_INT16 ||
          type == G_VARIANT_CLASS_INT32 ||
          type == G_VARIANT_CLASS_INT64 ||
          type == G_VARIANT_CLASS_UINT16 ||
          type == G_VARIANT_CLASS_UINT32 ||
          type == G_VARIANT_CLASS_UINT64);
}

/**
 * rsd_subscription_new:
 * @zone_path: zone containing the attribute
 * @attribute_name: name of the attribute, or the empty string to subscribe to all
 *    attributes (if, and only if, @zone_path is `/`)
 * @minimum_value: (nullable): minimum value of the attribute to subscribe to
 * @maximum_value: (nullable): maximum value of the attribute to subscribe to
 * @hysteresis: (nullable): symmetric hysteresis to apply to notifications; this
 *    must only be non-%NULL if @minimum_value and @maximum_value are non-%NULL
 * @minimum_period: minimum update period, in microseconds
 * @maximum_period: maximum update period, in microseconds
 *
 * Create a new #RsdSubscription structure with the given attributes. See the
 * documentation for #RsdSubscription for more information.
 *
 * To not impose a restriction on the signalling frequency for this
 * subscription, set @minimum_period to 0 and @maximum_period to %G_MAXUINT.
 *
 * Note that creating a #RsdSubscription does not activate it — use
 * rsd_vehicle_update_subscriptions_async() for that.
 *
 * Returns: (transfer full): a new #RsdSubscription
 * Since: 0.2.0
 */
RsdSubscription *
rsd_subscription_new (const gchar *zone_path,
                      const gchar *attribute_name,
                      GVariant    *minimum_value,
                      GVariant    *maximum_value,
                      GVariant    *hysteresis,
                      guint        minimum_period,
                      guint        maximum_period)
{
  g_autoptr (RsdSubscription) subscription = NULL;

  g_return_val_if_fail (rsd_zone_path_is_valid (zone_path), NULL);
  g_return_val_if_fail (attribute_name != NULL &&
                        (*attribute_name == '\0' ||
                         rsd_attribute_name_is_valid (attribute_name)), NULL);
  g_return_val_if_fail (*attribute_name != '\0' ||
                        g_strcmp0 (zone_path, "/") == 0, NULL);
  g_return_val_if_fail (minimum_value == NULL ||
                        (g_variant_is_normal_form (minimum_value) &&
                         variant_is_numeric (minimum_value)), NULL);
  g_return_val_if_fail (maximum_value == NULL ||
                        (g_variant_is_normal_form (maximum_value) &&
                         variant_is_numeric (maximum_value)), NULL);
  g_return_val_if_fail (hysteresis == NULL ||
                        (g_variant_is_normal_form (hysteresis) &&
                         variant_is_numeric (hysteresis)), NULL);
  g_return_val_if_fail (hysteresis == NULL || minimum_value != NULL, NULL);
  g_return_val_if_fail ((minimum_value == NULL) == (maximum_value == NULL),
                        NULL);
  g_return_val_if_fail (minimum_value == NULL || maximum_value == NULL ||
                        g_variant_type_equal (g_variant_get_type (minimum_value),
                                              g_variant_get_type (maximum_value)),
                        NULL);
  g_return_val_if_fail (minimum_value == NULL || hysteresis == NULL ||
                        g_variant_type_equal (g_variant_get_type (minimum_value),
                                              g_variant_get_type (hysteresis)),
                        NULL);
  g_return_val_if_fail (hysteresis == NULL || maximum_value == NULL ||
                        g_variant_type_equal (g_variant_get_type (hysteresis),
                                              g_variant_get_type (maximum_value)),
                        NULL);
  g_return_val_if_fail (maximum_period >= minimum_period, NULL);

  subscription = g_new0 (RsdSubscription, 1);
  subscription->zone_path = g_intern_string (zone_path);
  subscription->attribute_name = g_intern_string (attribute_name);

  if (minimum_value != NULL)
    subscription->minimum_value = g_variant_ref_sink (minimum_value);
  if (maximum_value != NULL)
    subscription->maximum_value = g_variant_ref_sink (maximum_value);
  if (hysteresis != NULL)
    subscription->hysteresis = g_variant_ref_sink (hysteresis);

  subscription->minimum_period = minimum_period;
  subscription->maximum_period = maximum_period;

  return g_steal_pointer (&subscription);
}

/**
 * rsd_subscription_copy:
 * @subscription: (transfer none): a #RsdSubscription
 *
 * Make a deep copy of an #RsdSubscription.
 *
 * Returns: (transfer full): a copy of @subscription
 * Since: 0.2.0
 */
RsdSubscription *
rsd_subscription_copy (const RsdSubscription *subscription)
{
  return rsd_subscription_new (subscription->zone_path,
                               subscription->attribute_name,
                               subscription->minimum_value,
                               subscription->maximum_value,
                               subscription->hysteresis,
                               subscription->minimum_period,
                               subscription->maximum_period);
}


/**
 * rsd_subscription_free:
 * @subscription: (transfer full): a #RsdSubscription
 *
 * Free an #RsdSubscription.
 *
 * Since: 0.2.0
 */
void
rsd_subscription_free (RsdSubscription *subscription)
{
  g_return_if_fail (subscription != NULL);

  g_clear_pointer (&subscription->maximum_value, g_variant_unref);
  g_clear_pointer (&subscription->minimum_value, g_variant_unref);
  g_clear_pointer (&subscription->hysteresis, g_variant_unref);

  g_free (subscription);
}

static gdouble
variant_to_double (GVariant *variant)
{
  GVariantClass type;

  type = g_variant_classify (variant);

  switch (type)
    {
    case G_VARIANT_CLASS_BYTE:
      return g_variant_get_byte (variant);
    case G_VARIANT_CLASS_DOUBLE:
      return g_variant_get_double (variant);
    case G_VARIANT_CLASS_INT16:
      return g_variant_get_int16 (variant);
    case G_VARIANT_CLASS_INT32:
      return g_variant_get_int32 (variant);
    case G_VARIANT_CLASS_INT64:
      return g_variant_get_int64 (variant);
    case G_VARIANT_CLASS_UINT16:
      return g_variant_get_uint16 (variant);
    case G_VARIANT_CLASS_UINT32:
      return g_variant_get_uint32 (variant);
    case G_VARIANT_CLASS_UINT64:
      return g_variant_get_uint64 (variant);
    case G_VARIANT_CLASS_BOOLEAN:
    case G_VARIANT_CLASS_HANDLE:
    case G_VARIANT_CLASS_STRING:
    case G_VARIANT_CLASS_OBJECT_PATH:
    case G_VARIANT_CLASS_SIGNATURE:
    case G_VARIANT_CLASS_VARIANT:
    case G_VARIANT_CLASS_MAYBE:
    case G_VARIANT_CLASS_ARRAY:
    case G_VARIANT_CLASS_TUPLE:
    case G_VARIANT_CLASS_DICT_ENTRY:
    default:
      g_assert_not_reached ();
    }
}

typedef struct
{
  gdouble upper;
  gdouble lower;
} Range;

static inline void
range_init (Range   *range,
            gdouble  lower,
            gdouble  upper)
{
  range->upper = upper;
  range->lower = lower;

  g_return_if_fail (range->lower <= range->upper);
}

static inline void
range_init_from_midpoint (Range   *range,
                          gdouble  midpoint,
                          gdouble  half_range)
{
  range->upper = midpoint + half_range;
  range->lower = midpoint - half_range;

  g_return_if_fail (range->lower <= range->upper);
}

static inline gboolean
range_lower_than (const Range *range,
                  gdouble      value)
{
  return (range->upper < value);
}

static inline gboolean
range_lower_than_or_equal (const Range *range,
                  gdouble      value)
{
  return (range->upper <= value);
}

static inline gboolean
range_higher_than (const Range *range,
                   gdouble      value)
{
  return (range->lower > value);
}

static inline gboolean
range_higher_than_or_equal (const Range *range,
                            gdouble      value)
{
  return (range->lower >= value);
}

static inline gboolean
range_contains (const Range *range,
                gdouble      value)
{
  return (value >= range->lower && value <= range->upper);
}

static inline gboolean
range_outside (const Range *a,
               const Range *b)
{
  return (range_lower_than (a, b->lower) ||
          range_higher_than (a, b->upper));
}

static inline gboolean
range_inside (const Range *a,
              const Range *b)
{
  return (range_higher_than (a, b->lower) &&
          range_lower_than (a, b->upper));
}

/**
 * rsd_subscription_matches:
 * @subscription: an #RsdSubscription
 * @new_attribute: a attribute to match against
 * @check_time: the timestamp when the check is performed
 *
 * Check whether the current value of @new_attribute matches the conditions given
 * in @subscription, including its value and the time between @check_time and
 * when the attribute was most recently notified (this function calls
 * rsd_subscription_matches_period()).
 *
 * All calls to this function should be paired with a subsequent call to
 * rsd_subscription_notify() to updated the stored values in the
 * #RsdSubscription; this function itself is idempotent, and
 * rsd_subscription_notify() is the non-idempotent part of the same overall
 * operation.
 *
 * See the documentation for #RsdSubscription for more details on the matching
 * criteria.
 *
 * @check_time will typically come from g_get_monotonic_time(). It must come
 * from a clock source which is comparable with that used for
 * #RsdAttribute.last_updated.
 *
 * This does //not// check whether the name and zone path of @attribute match
 * @subscription. The caller must do that if desired.
 *
 * Returns: %TRUE if @attribute matches @subscription; %FALSE otherwise
 * Since: 0.2.0
 */
gboolean
rsd_subscription_matches (const RsdSubscription *subscription,
                          const RsdAttribute    *new_attribute,
                          RsdTimestampMicroseconds           check_time)
{
  g_return_val_if_fail (subscription != NULL, FALSE);
  g_return_val_if_fail (new_attribute != NULL, FALSE);
  g_return_val_if_fail (check_time >= subscription->last_notify_time, FALSE);

  /* Check the notification frequency. */
  if (!rsd_subscription_matches_period (subscription, check_time))
    return FALSE;

  /* Check whether the values match. */
  if (subscription->minimum_value != NULL &&
      subscription->maximum_value != NULL)
    {
      gdouble minimum_value, maximum_value, hysteresis;
      Range old_attribute_value, new_attribute_value;

      /* Since this is all opportunistic, we can safely do it all using gdouble,
       * rather than having a separate code path for guint64, gint64 and gdouble. */
      minimum_value = variant_to_double (subscription->minimum_value);
      maximum_value = variant_to_double (subscription->maximum_value);

      if (subscription->hysteresis != NULL)
        hysteresis = variant_to_double (subscription->hysteresis);
      else
        hysteresis = 0.0;

      if (subscription->last_notify_attribute != NULL)
        range_init_from_midpoint (&old_attribute_value,
                                  variant_to_double (subscription->last_notify_attribute->value),
                                  subscription->last_notify_attribute->accuracy);
      else
        range_init_from_midpoint (&old_attribute_value,
                                  variant_to_double (new_attribute->value),
                                  new_attribute->accuracy);

      range_init_from_midpoint (&new_attribute_value,
                                variant_to_double (new_attribute->value),
                                new_attribute->accuracy);

      if (minimum_value < maximum_value)
        {
          gboolean hysteresis_entered, hysteresis_exited;
          gboolean old_attribute_value_in_range, new_attribute_value_in_range;
          Range hysteresis_outer, hysteresis_inner, value;

          range_init (&hysteresis_outer, minimum_value - hysteresis,
                      maximum_value + hysteresis);
          range_init (&hysteresis_inner, minimum_value + hysteresis,
                      maximum_value - hysteresis);
          range_init (&value, minimum_value, maximum_value);

          hysteresis_entered = ((!range_higher_than (&old_attribute_value, hysteresis_inner.lower) &&
                                 range_higher_than (&new_attribute_value, hysteresis_inner.lower)) ||
                                (!range_lower_than (&old_attribute_value, hysteresis_inner.upper) &&
                                 range_lower_than (&new_attribute_value, hysteresis_inner.upper)));
          hysteresis_exited = ((!range_lower_than (&old_attribute_value, hysteresis_outer.lower) &&
                                range_lower_than (&new_attribute_value, hysteresis_outer.lower)) ||
                               (!range_higher_than (&old_attribute_value, hysteresis_outer.upper) &&
                                range_higher_than (&new_attribute_value, hysteresis_outer.upper)));

          old_attribute_value_in_range = !range_outside (&old_attribute_value, &value);
          new_attribute_value_in_range = !range_outside (&new_attribute_value, &value);

          g_assert (!hysteresis_entered || new_attribute_value_in_range);
          g_assert (!hysteresis_exited || !new_attribute_value_in_range);

          return ((subscription->last_notify_attribute == NULL &&
                   new_attribute_value_in_range) ||
                  (!subscription->last_notify_emitted && hysteresis_entered) ||
                  (subscription->last_notify_emitted &&
                   old_attribute_value_in_range &&
                   new_attribute_value_in_range) ||
                  (subscription->last_notify_emitted && hysteresis_exited));
        }
      else if (minimum_value == maximum_value)
        {
          gboolean hysteresis_entered, hysteresis_exited;
          Range hysteresis_range;

          range_init (&hysteresis_range, minimum_value - hysteresis,
                      maximum_value + hysteresis);

          hysteresis_entered = ((!range_higher_than (&old_attribute_value, hysteresis_range.upper) &&
                                 range_higher_than_or_equal (&new_attribute_value, hysteresis_range.upper)) ||
                                (!range_lower_than (&old_attribute_value, hysteresis_range.lower) &&
                                 range_lower_than_or_equal (&new_attribute_value, hysteresis_range.lower)));
          hysteresis_exited = ((!range_lower_than (&old_attribute_value, hysteresis_range.lower) &&
                                range_lower_than_or_equal (&new_attribute_value, hysteresis_range.lower)) ||
                               (!range_higher_than (&old_attribute_value, hysteresis_range.upper) &&
                                range_higher_than_or_equal (&new_attribute_value, hysteresis_range.upper)));

          return (hysteresis_entered || hysteresis_exited);
        }
      else
        {
          gboolean hysteresis_entered, hysteresis_exited;
          gboolean old_attribute_value_in_range, new_attribute_value_in_range;
          Range value_lower, value_upper;

          range_init (&value_lower, -G_MAXDOUBLE, maximum_value);
          range_init (&value_upper, minimum_value, G_MAXDOUBLE);

          hysteresis_entered = ((!range_higher_than (&old_attribute_value, minimum_value + hysteresis) &&
                                 range_higher_than (&new_attribute_value, minimum_value + hysteresis)) ||
                                (!range_lower_than (&old_attribute_value, maximum_value - hysteresis) &&
                                 range_lower_than (&new_attribute_value, maximum_value - hysteresis)));
          hysteresis_exited = ((!range_lower_than (&old_attribute_value, minimum_value - hysteresis) &&
                                range_lower_than (&new_attribute_value, minimum_value - hysteresis)) ||
                               (!range_higher_than (&old_attribute_value, maximum_value + hysteresis) &&
                                range_higher_than (&new_attribute_value, maximum_value + hysteresis)));

          old_attribute_value_in_range = (!range_outside (&old_attribute_value, &value_lower) ||
                                         !range_outside (&old_attribute_value, &value_upper));
          new_attribute_value_in_range = (!range_outside (&new_attribute_value, &value_lower) ||
                                         !range_outside (&new_attribute_value, &value_upper));

          g_assert (!hysteresis_entered || new_attribute_value_in_range);
          g_assert (!hysteresis_exited || !new_attribute_value_in_range);

          return ((subscription->last_notify_attribute == NULL &&
                   new_attribute_value_in_range) ||
                  (!subscription->last_notify_emitted && hysteresis_entered) ||
                  (subscription->last_notify_emitted &&
                   old_attribute_value_in_range &&
                   new_attribute_value_in_range) ||
                  (subscription->last_notify_emitted && hysteresis_exited));
        }
    }

  return TRUE;
}

/**
 * rsd_subscription_matches_period:
 * @subscription: an #RsdSubscription
 * @check_time: the timestamp when the check is performed
 *
 * Check whether the current value of @new_attribute matches the time period
 * conditions given in @subscription: the time between @check_time and
 * when the attribute was most recently notified. It does not check the value
 * conditions; use rsd_subscription_matches() for that (which calls this as
 * part of its implementation).
 *
 * All calls to this function should be paired with a subsequent call to
 * rsd_subscription_notify() to updated the stored values in the
 * #RsdSubscription; this function itself is idempotent, and
 * rsd_subscription_notify() is the non-idempotent part of the same overall
 * operation.
 *
 * See the documentation for #RsdSubscription for more details on the matching
 * criteria.
 *
 * @check_time will typically come from g_get_monotonic_time(). It must come
 * from a clock source which is comparable with that used for
 * #RsdAttribute.last_updated.
 *
 * This does //not// check whether the name and zone path of @attribute match
 * @subscription. The caller must do that if desired.
 *
 * Returns: %TRUE if @attribute matches @subscription; %FALSE otherwise
 * Since: 0.2.0
 */
gboolean
rsd_subscription_matches_period (const RsdSubscription *subscription,
                                 RsdTimestampMicroseconds           check_time)
{
  gint64 period_since_last_notify;
  Range period;

  g_return_val_if_fail (subscription != NULL, FALSE);
  g_return_val_if_fail (check_time >= subscription->last_notify_time, FALSE);

  /* Check the notification frequency. */
  range_init (&period, subscription->minimum_period,
              subscription->maximum_period);
  period_since_last_notify = check_time - subscription->last_notify_time;

  return (subscription->last_notify_time == 0 ||
          range_contains (&period, period_since_last_notify));
}

/**
 * rsd_subscription_equal:
 * @a: an #RsdSubscription
 * @b: another #RsdSubscription
 *
 * Check whether the given #RsdSubscriptions are equal. This ignores mutable
 * state in the subscription, and only compares the fields provided at
 * construction time.
 *
 * Both @a and @b must be non-%NULL.
 *
 * Returns: %TRUE if @a and @b are equal; %FALSE otherwise
 * Since: 0.2.0
 */
gboolean
rsd_subscription_equal (const RsdSubscription *a,
                        const RsdSubscription *b)
{
  g_return_val_if_fail (a != NULL, FALSE);
  g_return_val_if_fail (b != NULL, FALSE);

  /* @zone_path and @attribute_name are interned, so can be compared directly. */
  return (a->zone_path == b->zone_path &&
          a->attribute_name == b->attribute_name &&
          (a->minimum_value == NULL) == (b->minimum_value == NULL) &&
          (a->maximum_value == NULL) == (b->maximum_value == NULL) &&
          (a->hysteresis == NULL) == (b->hysteresis == NULL) &&
          (a->minimum_value == NULL ||
           g_variant_equal (a->minimum_value, b->minimum_value)) &&
          (a->maximum_value == NULL ||
           g_variant_equal (a->maximum_value, b->maximum_value)) &&
          (a->hysteresis == NULL ||
           g_variant_equal (a->hysteresis, b->hysteresis)) &&
          a->minimum_period == b->minimum_period &&
          a->maximum_period == b->maximum_period);
}

/**
 * rsd_subscription_notify:
 * @subscription: an #RsdSubscription
 * @attribute: (nullable): current value of the attribute, or %NULL if unknown
 * @notify_time: the timestamp when the notification was emitted
 * @emitted: %TRUE if a signal has been (or will soon be) emitted for this
 *    value of the attribute; %FALSE otherwise
 *
 * Update the stored state of the @subscription, setting the stored attribute
 * value, and marking the subscription as having @emitted recently or in the
 * near future (or not).
 *
 * This should be called after rsd_subscription_matches() //regardless// of
 * whether @subscription matches @attribute — the stored attribute value must
 * be updated even if there is no match.
 *
 * It updates the stored state for the #RsdSubscription for the purposes of
 * checking it isn’t being emitted too often, or failing the hysteresis
 * criteria, next time rsd_subscription_matches() is called on it. This is
 * unnecessary but harmless for wildcard subscriptions, whose criteria are
 * unconstrained.
 *
 * Since: 0.2.0
 */
void
rsd_subscription_notify (RsdSubscription   *subscription,
                         const RsdAttribute *attribute,
                         RsdTimestampMicroseconds       notify_time,
                         gboolean           emitted)
{
  g_return_if_fail (subscription != NULL);

  g_clear_pointer (&subscription->last_notify_attribute, rsd_attribute_free);
  if (attribute != NULL)
    subscription->last_notify_attribute = rsd_attribute_copy (attribute);

  if (emitted)
    subscription->last_notify_time = notify_time;

  subscription->last_notify_emitted = emitted;
}

/**
 * rsd_subscription_new_aggregate:
 * @subscriptions: (element-type RsdSubscription): non-empty array of
 *    subscriptions to aggregate
 *
 * Create a new #RsdSubscription as the aggregate of all the given
 * @subscriptions. It will have the union of all their criteria.
 *
 * All the #RsdSubscriptions in @subscriptions must be for the same zone path
 * and attribute name — this is not checked. @subscriptions must have at least
 * one element.
 *
 * Returns: (transfer full): aggregate subscription
 * Since: 0.2.0
 */
RsdSubscription *
rsd_subscription_new_aggregate (GArray *subscriptions)
{
  gsize i;
  GVariantClass type = G_VARIANT_CLASS_TUPLE;
  GVariant *out_minimum_value = NULL;
  GVariant *out_maximum_value = NULL;
  GVariant *out_hysteresis = NULL;
  guint max_minimum_period = 0, min_minimum_period = G_MAXUINT;
  guint min_maximum_period = G_MAXUINT, max_maximum_period = 0;
  const RsdSubscription *first_subscription;

  g_return_val_if_fail (subscriptions->len > 0, NULL);

  first_subscription = &g_array_index (subscriptions, const RsdSubscription, 0);
  out_minimum_value = first_subscription->minimum_value;
  out_maximum_value = first_subscription->maximum_value;
  out_hysteresis = first_subscription->hysteresis;
  min_minimum_period = first_subscription->minimum_period;
  max_minimum_period = first_subscription->minimum_period;
  min_maximum_period = first_subscription->maximum_period;
  max_maximum_period = first_subscription->maximum_period;

  if (out_minimum_value != NULL)
    type = g_variant_classify (out_minimum_value);

  for (i = 1; i < subscriptions->len; i++)
    {
      const RsdSubscription *subscription;

      subscription = &g_array_index (subscriptions, const RsdSubscription, i);

      /* Take the maximum of all minimum periods, and the minimum of all maximum
       * periods. These aggregates are the ‘wrong’ way round because we actually
       * want to deal in terms of the frequency, which is the inverse of the
       * period. */
      max_minimum_period = MAX (max_minimum_period,
                                subscription->minimum_period);
      min_maximum_period = MIN (min_maximum_period,
                                subscription->maximum_period);
      min_minimum_period = MIN (min_minimum_period,
                                subscription->minimum_period);
      max_maximum_period = MAX (max_maximum_period,
                                subscription->maximum_period);

      /* Aggregate the minimum and maximum values for numeric GVariants, and
       * ignore other variants. */
      switch (type)
        {
#define CASE(CLASS, type, getter) \
        case G_VARIANT_CLASS_##CLASS: \
          { \
            type subscription_minimum, aggregate_minimum; \
            type subscription_maximum, aggregate_maximum; \
            type subscription_hysteresis, aggregate_hysteresis; \
\
            if (out_minimum_value != NULL && \
                subscription->minimum_value == NULL) \
              { \
                /* A NULL minimum_value is effectively unbounded, so clear the \
                 * aggregate. */ \
                out_minimum_value = NULL; \
                out_maximum_value = NULL; \
                out_hysteresis = NULL; \
              } \
            else if (out_minimum_value != NULL && \
                     subscription->minimum_value != NULL) \
              { \
                /* Does the value need widening? */ \
                subscription_minimum = (getter) (subscription->minimum_value); \
                subscription_maximum = (getter) (subscription->maximum_value); \
\
                aggregate_minimum = (getter) (out_minimum_value); \
                aggregate_maximum = (getter) (out_maximum_value); \
\
                if (subscription_minimum < aggregate_minimum) \
                  out_minimum_value = subscription->minimum_value; \
                if (subscription_maximum > aggregate_maximum) \
                  out_maximum_value = subscription->maximum_value; \
\
                if (out_hysteresis != NULL && \
                    subscription->hysteresis == NULL) \
                  { \
                    out_hysteresis = NULL; \
                  } \
                else if (out_hysteresis != NULL && \
                         subscription->hysteresis != NULL) \
                  { \
                    subscription_hysteresis = (getter) (subscription->hysteresis); \
                    aggregate_hysteresis = (getter) (out_hysteresis); \
\
                    if (subscription_hysteresis < aggregate_hysteresis) \
                      out_hysteresis = subscription->hysteresis; \
                  } \
              } \
 \
            break; \
          }
        CASE (BYTE, guchar, g_variant_get_byte)
        CASE (INT16, gint16, g_variant_get_int16)
        CASE (INT32, gint32, g_variant_get_int32)
        CASE (INT64, gint64, g_variant_get_int64)
        CASE (UINT16, guint16, g_variant_get_uint16)
        CASE (UINT32, guint32, g_variant_get_uint32)
        CASE (UINT64, guint64, g_variant_get_uint64)
        CASE (DOUBLE, gdouble, g_variant_get_double)
#undef CASE
        case G_VARIANT_CLASS_BOOLEAN:
        case G_VARIANT_CLASS_HANDLE:
        case G_VARIANT_CLASS_STRING:
        case G_VARIANT_CLASS_OBJECT_PATH:
        case G_VARIANT_CLASS_SIGNATURE:
        case G_VARIANT_CLASS_VARIANT:
        case G_VARIANT_CLASS_MAYBE:
        case G_VARIANT_CLASS_ARRAY:
        case G_VARIANT_CLASS_TUPLE:
        case G_VARIANT_CLASS_DICT_ENTRY:
        default:
          /* Do nothing. */
          break;
        }
    }

  /* This can happen if any of the period ranges are disjoint. In which case,
   * take the bounding range of them all, which will satisfy none of the
   * clients, but will at least mean they get notifications. */
  if (max_minimum_period > min_maximum_period)
    {
      max_minimum_period = min_minimum_period;
      min_maximum_period = max_maximum_period;
    }

  return rsd_subscription_new (first_subscription->zone_path,
                               first_subscription->attribute_name,
                               out_minimum_value,
                               out_maximum_value,
                               out_hysteresis,
                               max_minimum_period,
                               min_maximum_period);
}

/**
 * rsd_subscription_new_wildcard:
 *
 * Create a new #RsdSubscription structure with wildcards for all attributes,
 * zone path and attribute name. See the documentation for #RsdSubscription for
 * more information. This is essentially an aggregate subscription of all other
 * possible subscriptions (see rsd_subscription_new_aggregate()).
 *
 * Note that creating a #RsdSubscription does not activate it — use
 * rsd_vehicle_update_subscriptions_async() for that.
 *
 * Returns: (transfer full): a new #RsdSubscription
 * Since: 0.4.0
 */
RsdSubscription *
rsd_subscription_new_wildcard (void)
{
  return rsd_subscription_new ("/", "", NULL, NULL, NULL, 0, G_MAXUINT);
}
