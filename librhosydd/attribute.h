/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_ATTRIBUTE_H
#define RSD_ATTRIBUTE_H

#include <glib.h>
#include <glib-object.h>

#include <librhosydd/types.h>


G_BEGIN_DECLS

/**
 * RsdAttribute:
 *
 * The value and associated metadata for the current state of a vehicle
 * attribute. The @accuracy has the same units as the @value, and is given as the
 * one-sided confidence bound on the real value, i.e. the real value lies in the
 * range @value ± @accuracy; @accuracy is one standard deviation. If no accuracy
 * is known for a value, or accuracy makes no sense in the context (for example,
 * for a non-numeric @value), @accuracy must be 0.0.
 *
 * Attributes with type `Zone` in
 * [the specification](https://www.w3.org/2014/automotive/vehicle_spec.html#zone-interface)
 * are represented as a #GVariant of type %G_VARIANT_TYPE_UINT32 containing the
 * ID of the zone.
 *
 * Other types map from the specification to #GVariantType as follows:
 *
 *  * `octet` → %G_VARIANT_TYPE_BYTE
 *  * `short` → %G_VARIANT_TYPE_INT16
 *  * `unsigned short` → %G_VARIANT_TYPE_UINT16
 *  * `long` → %G_VARIANT_TYPE_INT64
 *  * `unsigned long` → %G_VARIANT_TYPE_UINT64
 *  * `DOMTimeStamp` → %G_VARIANT_TYPE_UINT64 (interpreted as a timestamp, in
 *       microseconds relative to the vehicle’s current time)
 *  * Enumerated types → %G_VARIANT_TYPE_STRING
 *
 * The @last_updated timestamp gives the time the value was last //measured//
 * by the sensor, which may be earlier than the time it was last updated in the
 * SDK API. It is relative to the current time of the vehicle containing the
 * attribute (which is emitted in #RsdVehicle::attributes-changed signals).
 *
 * Since: 0.1.0
 */
typedef struct
{
  GVariant     *value;
  gdouble       accuracy;
  RsdTimestampMicroseconds  last_updated;
} RsdAttribute;

RsdAttribute *rsd_attribute_new  (GVariant           *value,
                                  gdouble             accuracy,
                                  RsdTimestampMicroseconds        last_updated);
RsdAttribute *rsd_attribute_copy (const RsdAttribute  *attribute);
RsdAttribute *rsd_attribute_init (RsdAttribute        *attribute,
                                  const RsdAttribute  *tmpl);
void          rsd_attribute_clear (RsdAttribute       *attribute);
void          rsd_attribute_free (RsdAttribute        *attribute);

gboolean      rsd_attribute_name_is_valid (const gchar *attribute_name);

GType rsd_attribute_get_type (void);
G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (RsdAttribute, rsd_attribute_clear)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RsdAttribute, rsd_attribute_free)

/**
 * RsdAttributeAvailability:
 * @RSD_ATTRIBUTE_AVAILABLE: data is available
 * @RSD_ATTRIBUTE_NOT_SUPPORTED: not supported by this vehicle
 * @RSD_ATTRIBUTE_NOT_SUPPORTED_YET: not supported at this time, but may become
 *    supported later
 * @RSD_ATTRIBUTE_NOT_SUPPORTED_SECURITY_POLICY: not supported because of a
 *    security policy
* @RSD_ATTRIBUTE_NOT_SUPPORTED_BUSINESS_POLICY: not supported because of a
 *    business policy
 * @RSD_ATTRIBUTE_NOT_SUPPORTED_OTHER: not supported for other reasons
 *
 * An enumerated type detailing whether an attribute is available and, if not,
 * why it is not currently available.
 *
 * See: [Availability in the W3C
 * specification](https://www.w3.org/2014/automotive/vehicle_spec.html#idl-def-Availability)
 *
 * Since: 0.1.0
 */
typedef enum
{
  RSD_ATTRIBUTE_AVAILABLE = 1,
  RSD_ATTRIBUTE_NOT_SUPPORTED = 0,
  RSD_ATTRIBUTE_NOT_SUPPORTED_YET = 2,
  RSD_ATTRIBUTE_NOT_SUPPORTED_SECURITY_POLICY = 3,
  RSD_ATTRIBUTE_NOT_SUPPORTED_BUSINESS_POLICY = 4,
  RSD_ATTRIBUTE_NOT_SUPPORTED_OTHER = 5,
} RsdAttributeAvailability;

gboolean rsd_attribute_availability_is_valid (RsdAttributeAvailability availability);

/**
 * RsdAttributeFlags:
 * @RSD_ATTRIBUTE_FLAGS_NONE: no flags are set
 * @RSD_ATTRIBUTE_READABLE: the attribute is currently readable
 * @RSD_ATTRIBUTE_WRITABLE: the attribute is currently writable
 *
 * Flags indicating the current behaviour of an attribute.
 *
 * Since: 0.1.0
 */
typedef enum
{
  RSD_ATTRIBUTE_FLAGS_NONE = 0,
  RSD_ATTRIBUTE_READABLE = (1 << 0),
  RSD_ATTRIBUTE_WRITABLE = (1 << 1),
} RsdAttributeFlags;

gboolean rsd_attribute_flags_is_valid (RsdAttributeFlags flags);

/**
 * RsdAttributeMetadata:
 *
 * The current metadata for an attribute, covering whether it is available for
 * consumers to use, and what its access permissions are. These values can
 * change over time, so any code which stores #RsdAttributeMetadata must also
 * listen for signals about updating it.
 *
 * Since: 0.2.0
 */
typedef struct
{
  const gchar              *zone_path;
  const gchar              *name;
  RsdAttributeAvailability  availability;
  RsdAttributeFlags         flags;
} RsdAttributeMetadata;

RsdAttributeMetadata *rsd_attribute_metadata_new  (const gchar               *zone_path,
                                                 const gchar               *name,
                                                 RsdAttributeAvailability    availability,
                                                 RsdAttributeFlags           flags);
RsdAttributeMetadata *rsd_attribute_metadata_copy (const RsdAttributeMetadata *metadata);
RsdAttributeMetadata *rsd_attribute_metadata_init (RsdAttributeMetadata       *metadata,
                                                 const RsdAttributeMetadata *tmpl);
void                 rsd_attribute_metadata_free (RsdAttributeMetadata       *metadata);

GType rsd_attribute_metadata_get_type (void);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RsdAttributeMetadata, rsd_attribute_metadata_free)

/**
 * RsdAttributeInfo:
 *
 * Information about an attribute, including its zone, name, and current value.
 *
 * Since: 0.2.0
 */
typedef struct
{
  RsdAttribute          attribute;
  RsdAttributeMetadata  metadata;
} RsdAttributeInfo;

RsdAttributeInfo *rsd_attribute_info_new  (const RsdAttribute         *attribute,
                                           const RsdAttributeMetadata *metadata);
RsdAttributeInfo *rsd_attribute_info_copy (const RsdAttributeInfo     *info);
void              rsd_attribute_info_free (RsdAttributeInfo           *info);

GType rsd_attribute_info_get_type (void);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RsdAttributeInfo, rsd_attribute_info_free)

G_END_DECLS

#endif /* !RSD_ATTRIBUTE_H */
