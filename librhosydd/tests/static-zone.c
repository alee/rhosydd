/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <string.h>

#include "librhosydd/static-zone.h"
#include "librhosydd/zone.h"


/* Test construction of a static zone. */
static void
test_static_zone_construction (void)
{
  g_autoptr (RsdStaticZone) zone = NULL;

  zone = rsd_static_zone_new ("/");
}

static void
assert_is_interned (const gchar *str)
{
  g_assert (str == g_intern_string (str));
}

static void
assert_cmp_tags (const gchar * const *tags1,
                 const gchar * const *tags2)
{
  gsize i;

  /* Compare the tags, which must be in the expected order. */
  g_assert ((tags1 == NULL) == (tags2 == NULL));

  for (i = 0;
       tags1 != NULL && tags2 != NULL &&
       tags1[i] != NULL && tags2[i] != NULL;
       i++)
    {
      g_assert_cmpstr (tags1[i], ==, tags2[i]);
      assert_is_interned (tags2[i]);
    }
}

/* Test getting the path, tags and parent path from a static zone. */
static void
test_static_zone_interface_methods (void)
{
  guint i;
  const gchar *_tags_left[] = { "left", NULL };
  const gchar *_tags_empty[] = { NULL };
  const struct {
    const gchar *path;
    const gchar * const *tags;
    const gchar *parent_path;
  } vectors[] = {
    { "/", _tags_empty, "" },
    { "/left/", _tags_left, "/" },
    { "/left,right//", _tags_empty, "/left,right/" },
  };

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdStaticZone) zone = NULL;

      zone = rsd_static_zone_new (vectors[i].path);

      g_assert_cmpstr (rsd_zone_get_path (RSD_ZONE (zone)), ==,
                       vectors[i].path);
      assert_cmp_tags (vectors[i].tags, rsd_zone_get_tags (RSD_ZONE (zone)));
      g_assert_cmpstr (rsd_zone_get_parent_path (RSD_ZONE (zone)), ==,
                       vectors[i].parent_path);
    }
}

/* Test that retrieving attributes from a static zone works. */
static void
test_static_zone_attributes (void)
{
  g_autoptr (RsdStaticZone) zone = NULL;
  const gchar *bottom_top_tags[] = { "bottom", "top", NULL };
  g_autofree gchar *path = NULL;
  g_auto (GStrv) tags = NULL;
  g_autofree gchar *parent_path = NULL;

  zone = rsd_static_zone_new ("//left/bottom,top/");

  g_object_get (G_OBJECT (zone),
                "path", &path,
                "tags", &tags,
                "parent-path", &parent_path,
                NULL);

  g_assert_cmpstr (rsd_zone_get_path (RSD_ZONE (zone)), ==,
                   "//left/bottom,top/");
  assert_cmp_tags (bottom_top_tags, rsd_zone_get_tags (RSD_ZONE (zone)));
  g_assert_cmpstr (rsd_zone_get_parent_path (RSD_ZONE (zone)), ==, "//left/");
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/static-zone/construction", test_static_zone_construction);
  g_test_add_func ("/static-zone/interface-methods",
                   test_static_zone_interface_methods);
  g_test_add_func ("/static-zone/attributes", test_static_zone_attributes);

  return g_test_run ();
}
