/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_ZONE_H
#define RSD_ZONE_H

#include <glib.h>
#include <glib-object.h>

#include <librhosydd/types.h>


G_BEGIN_DECLS

/**
 * RsdZone:
 *
 * An object instance which implements #RsdZoneInterface.
 *
 * Since: 0.1.0
 */
#define RSD_TYPE_ZONE rsd_zone_get_type ()
G_DECLARE_INTERFACE (RsdZone, rsd_zone, RSD, ZONE, GObject)

/**
 * RsdZoneInterface:
 * @g_iface: parent interface
 * @get_path: Get the zone’s full path.
 * @get_tags: Get the zones’s tags, which must be a %NULL-terminated array of
 *    non-empty tags; or %NULL for no tags.
 * @get_parent_path: Get the zone’s parent path, or the empty string if this is
 *    the root zone. This must never return %NULL.
 *
 * A representation of a single zone within a vehicle, including all tags which
 * describe it. See the [documentation on zones](#zones).
 *
 * Since: 0.1.0
 */
struct _RsdZoneInterface
{
  GTypeInterface g_iface;

  const gchar         *(*get_path)        (RsdZone *self);
  const gchar * const *(*get_tags)        (RsdZone *self);
  const gchar         *(*get_parent_path) (RsdZone *self);
};

const gchar         *rsd_zone_get_path        (RsdZone *self);
const gchar * const *rsd_zone_get_tags        (RsdZone *self);
const gchar         *rsd_zone_get_parent_path (RsdZone *self);
gboolean             rsd_zone_is_root         (RsdZone *self);
gboolean             rsd_zone_is_descendant   (RsdZone *self,
                                               RsdZone *ancestor);

gboolean rsd_zone_tag_is_valid   (const gchar         *tag);
gboolean rsd_zone_tags_are_valid (const gchar * const *tags);

typedef struct
{
  gpointer dummy[2];
} RsdZonePathIter;

void         rsd_zone_path_iter_init (RsdZonePathIter     *iter,
                                      const gchar         *path);
gboolean     rsd_zone_path_iter_next (RsdZonePathIter     *iter,
                                      const gchar        **parent_path,
                                      const gchar       ***tags);

const gchar *rsd_zone_path_build     (const gchar         *parent_path,
                                      const gchar * const *tags);
const gchar **rsd_zone_path_tail     (const gchar         *path,
                                      const gchar        **parent_path);

gboolean     rsd_zone_path_is_valid  (const gchar         *path);

G_END_DECLS

#endif /* !RSD_ZONE_H */
