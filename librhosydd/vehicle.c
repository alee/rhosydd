/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>

#include "librhosydd/vehicle.h"


static const GDBusErrorEntry rsd_vehicle_error_entries[] =
{
  { RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE,
    "org.apertis.Rhosydd1.Vehicle.Error.UnknownVehicle" },
  { RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
    "org.apertis.Rhosydd1.Vehicle.Error.UnknownZone" },
  { RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
    "org.apertis.Rhosydd1.Vehicle.Error.UnknownAttribute" },
  { RSD_VEHICLE_ERROR_TOO_MANY_ZONES,
    "org.apertis.Rhosydd1.Vehicle.Error.TooManyZones" },
  { RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_READABLE,
    "org.apertis.Rhosydd1.Vehicle.Error.AttributeNotReadable" },
  { RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_WRITABLE,
    "org.apertis.Rhosydd1.Vehicle.Error.AttributeNotWritable" },
  { RSD_VEHICLE_ERROR_INVALID_TAGS,
    "org.apertis.Rhosydd1.Vehicle.Error.InvalidTags" },
  { RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
    "org.apertis.Rhosydd1.Vehicle.Error.InvalidAttributeValue" },
  { RSD_VEHICLE_ERROR_ATTRIBUTE_UNAVAILABLE,
    "org.apertis.Rhosydd1.Vehicle.Error.AttributeUnavailable" },
  { RSD_VEHICLE_ERROR_ILLEGAL_BEHAVIOUR,
    "org.apertis.Rhosydd1.Vehicle.Error.IllegalBehaviour" },
  { RSD_VEHICLE_ERROR_INVALIDATED,
    "org.apertis.Rhosydd1.Vehicle.Error.Invalidated" },
  { RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
    "org.apertis.Rhosydd1.Vehicle.Error.InvalidSubscription" },
};

/* Ensure that every error code has an associated D-Bus error name. */
G_STATIC_ASSERT (G_N_ELEMENTS (rsd_vehicle_error_entries) ==
                 RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION + 1);

GQuark
rsd_vehicle_error_quark (void)
{
  static volatile gsize quark = 0;

  g_dbus_error_register_error_domain ("rsd-vehicle-error-quark", &quark,
                                      rsd_vehicle_error_entries,
                                      G_N_ELEMENTS (rsd_vehicle_error_entries));

  return quark;
}

G_DEFINE_INTERFACE (RsdVehicle, rsd_vehicle, G_TYPE_OBJECT)

static void
rsd_vehicle_default_init (RsdVehicleInterface *iface)
{
  GParamSpec *pspec;

  /* No default implementations. */

  /**
   * RsdVehicle:id:
   *
   * A unique identifier for the vehicle.
   *
   * See [Vehicle IDs]{librhosydd-api.markdown#vehicle-ids} for more
   * information.
   *
   * Since: 0.3.0
   */
  pspec = g_param_spec_string ("id", "ID",
                               "A unique identifier for the vehicle.",
                               NULL,
                               G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                               G_PARAM_STATIC_STRINGS);
  g_object_interface_install_property (iface, pspec);

  /**
   * RsdVehicle::invalidated:
   * @self: a #RsdVehicle
   * @error: error which caused the vehicle to be invalidated: either
   *    %RSD_VEHICLE_ERROR_ILLEGAL_BEHAVIOUR, or %G_DBUS_ERROR_DISCONNECTED
   *
   * Emitted when the backing object underlying this #RsdVehicle disappears, or
   * it is otherwise disconnected (due to, for example, providing invalid
   * data). The most common reason for this signal to be emitted is if the
   * underlying D-Bus object for an #RsdProxyVehicle disappears.
   *
   * After this signal is emitted, all method calls to #RsdVehicle methods will
   * return %RSD_VEHICLE_ERROR_INVALIDATED. (It is the responsibility of
   * subclasses to implement this.)
   *
   * Since: 0.5.0
   */
  g_signal_new ("invalidated", G_TYPE_FROM_INTERFACE (iface),
                G_SIGNAL_RUN_LAST,
                0, NULL, NULL, NULL,
                G_TYPE_NONE, 1,
                G_TYPE_ERROR);

  /**
   * RsdVehicle::attributes-changed:
   * @self: a #RsdVehicle
   * @current_time: current time which all last updated timestamps in the
   *    @changed_attributes are relative to, in microseconds
   * @changed_attributes: (element-type RsdAttributeInfo): potentially empty
   *     array of #RsdAttributeInfos describing the attributes which have changed
   *     value, along with their updated values
   * @invalidated_attributes: (element-type RsdAttributeMetadata): potentially
   *     empty array of #RsdAttributeMetadatas describing the attributes which
   *     have changed value but whose updated values must be queried explicitly
   *
   * Emitted when one or more of the attributes of the vehicle have changed.
   * As with D-Bus attributes, attribute changes may include the updated value
   * (in @changed_attributes) or may omit it due to it being too large or too
   * infrequently used to be worth sending in the signal emission in the general
   * case (in @invalidated_attributes). The values for these invalidated
   * attributes must be explicitly queried using
   * rsd_vehicle_get_attribute_async() and related functions.
   *
   * If an attribute is listed in one of the two arrays, it will not be listed
   * in the other. There will be one attribute listed in at least one of the
   * arrays.
   *
   * The arrays may list attributes which have not been subscribed to using
   * rsd_vehicle_update_subscriptions_async(), if the server deems it to be more
   * efficient to include those attributes in the signal emission than to omit
   * them. Clients should ignore attributes they are not subscribed to and
   * should not rely on this behaviour; if a client uses an attribute, they must
   * subscribe to it.
   *
   * Since: 0.4.0
   */
  g_signal_new ("attributes-changed", G_TYPE_FROM_INTERFACE (iface),
                G_SIGNAL_RUN_LAST,
                0, NULL, NULL, NULL,
                G_TYPE_NONE, 3,
                G_TYPE_INT64,
                G_TYPE_PTR_ARRAY,
                G_TYPE_PTR_ARRAY);

  /**
   * RsdVehicle::attributes-metadata-changed:
   * @self: a #RsdVehicle
   * @current_time: current time which all last updated timestamps in the
   *    @changed_attributes are relative to, in microseconds
   * @changed_attributes_metadata: (element-type RsdAttributeMetadata): array of
   *     #RsdAttributeMetadatas describing the attributes whose metadata has
   *     changed, along with the updated metadata
   *
   * Emitted when the metadata of one or more attributes of the vehicle has
   * changed. For notifications of changes to the //values// of attributes, see
   * #RsdVehicle::attributes-changed.
   *
   * Since: 0.4.0
   */
  g_signal_new ("attributes-metadata-changed", G_TYPE_FROM_INTERFACE (iface),
                G_SIGNAL_RUN_LAST,
                0, NULL, NULL, NULL,
                G_TYPE_NONE, 2,
                G_TYPE_INT64,
                G_TYPE_PTR_ARRAY);
}

/**
 * rsd_vehicle_get_id:
 * @self: a #RsdVehicle
 *
 * Get the vehicle’s ID.
 *
 * Returns: vehicle ID
 * Since: 0.1.0
 */
const gchar *
rsd_vehicle_get_id (RsdVehicle *self)
{
  RsdVehicleInterface *iface;
  const gchar *retval;

  g_return_val_if_fail (RSD_IS_VEHICLE (self), NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);
  g_assert (iface->get_id != NULL);
  retval = iface->get_id (self);

  g_return_val_if_fail (retval != NULL && *retval != '\0', NULL);

  return retval;
}

/**
 * rsd_vehicle_get_zones:
 * @self: a #RsdVehicle
 *
 * Get the vehicle’s zones. At least one zone (the root zone, path `/`) will be
 * returned.
 *
 * Returns: (transfer container) (element-type RsdZone): vehicle zones
 * Since: 0.1.0
 */
GPtrArray *
rsd_vehicle_get_zones (RsdVehicle *self)
{
  RsdVehicleInterface *iface;
  g_autoptr (GPtrArray/*<RsdZone>*/) retval = NULL;

  g_return_val_if_fail (RSD_IS_VEHICLE (self), NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);
  g_assert (iface->get_zones != NULL);
  retval = iface->get_zones (self);

  /* Always has to return at least the root zone. */
  g_return_val_if_fail (retval != NULL && retval->len > 0, NULL);

  return g_steal_pointer (&retval);
}

/**
 * rsd_vehicle_get_attribute_async:
 * @self: a #RsdVehicle
 * @zone: zone to get the attribute from
 * @attribute_name: [name of the attribute](#attribute-names)
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to finish the operation
 * @user_data: user data to pass to @callback
 *
 * Get a single attribute from the given zone of the vehicle, or return an error
 * if the attribute is unknown or not currently available. Return the attribute
 * with all of its metadata.
 *
 * This is an asynchronous function; use rsd_vehicle_get_attribute_finish()
 * from @callback to get its return value.
 *
 * Since: 0.1.0
 */
void
rsd_vehicle_get_attribute_async (RsdVehicle          *self,
                                RsdZone             *zone,
                                const gchar         *attribute_name,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (RSD_IS_ZONE (zone));
  g_return_if_fail (attribute_name != NULL && *attribute_name != '\0');
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support reads. */
  if (iface->get_attribute_async == NULL)
    {
      g_task_report_new_error (self, callback, user_data,
                               rsd_vehicle_get_attribute_async,
                               RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_READABLE,
                               _("Attribute ‘%s’ in zone ‘%s’ of vehicle ‘%s’ "
                                 "is not readable."),
                               attribute_name, rsd_zone_get_path (zone),
                               rsd_vehicle_get_id (self));
      return;
    }

  iface->get_attribute_async (self, zone, attribute_name, cancellable,
                             callback, user_data);
}

/**
 * rsd_vehicle_get_attribute_finish:
 * @self: a #RsdVehicle
 * @result: asynchronous result
 * @current_time: (out caller-allocates) (optional): return location for the
 *    current time as measured by the backend
 * @error: return location for a #GError
 *
 * See rsd_vehicle_get_attribute_async().
 *
 * Returns: (transfer full): attribute value and metadata
 * Since: 0.4.0
 */
RsdAttributeInfo *
rsd_vehicle_get_attribute_finish (RsdVehicle                *self,
                                  GAsyncResult              *result,
                                  RsdTimestampMicroseconds  *current_time,
                                  GError                   **error)
{
  RsdVehicleInterface *iface;
  g_autoptr (RsdAttributeInfo) retval = NULL;
  RsdTimestampMicroseconds local_current_time = RSD_TIMESTAMP_UNKNOWN;
  GError *local_error = NULL;

  g_return_val_if_fail (RSD_IS_VEHICLE (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support reads. */
  if (iface->get_attribute_finish == NULL)
    {
      g_task_propagate_boolean (G_TASK (result), error);
      return NULL;
    }

  retval = iface->get_attribute_finish (self, result, &local_current_time,
                                        &local_error);

  g_return_val_if_fail ((retval == NULL) == (local_error != NULL), NULL);

  if (local_error != NULL)
    g_propagate_error (error, local_error);
  else if (current_time != NULL)
    *current_time = local_current_time;
  return g_steal_pointer (&retval);
}

/**
 * rsd_vehicle_get_metadata_async:
 * @self: a #RsdVehicle
 * @zone: zone to get the attribute from
 * @attribute_name: [name of the attribute](#attribute-names)
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to finish the operation
 * @user_data: user data to pass to @callback
 *
 * Get the metadata for a single attribute from the given zone of the vehicle, or
 * return an error if the attribute is unknown or not currently available.
 *
 * This is an asynchronous function; use rsd_vehicle_get_metadata_finish()
 * from @callback to get its return value.
 *
 * Since: 0.2.0
 */
void
rsd_vehicle_get_metadata_async (RsdVehicle          *self,
                                RsdZone             *zone,
                                const gchar         *attribute_name,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (RSD_IS_ZONE (zone));
  g_return_if_fail (attribute_name != NULL && *attribute_name != '\0');
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support reads. */
  if (iface->get_metadata_async == NULL)
    {
      g_task_report_new_error (self, callback, user_data,
                               rsd_vehicle_get_metadata_async,
                               RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_READABLE,
                               _("Attribute ‘%s’ in zone ‘%s’ of vehicle ‘%s’ "
                                 "is not readable."),
                               attribute_name, rsd_zone_get_path (zone),
                               rsd_vehicle_get_id (self));
      return;
    }

  iface->get_metadata_async (self, zone, attribute_name, cancellable,
                             callback, user_data);
}

/**
 * rsd_vehicle_get_metadata_finish:
 * @self: a #RsdVehicle
 * @result: asynchronous result
 * @current_time: (out caller-allocates) (optional): return location for the
 *    current time as measured by the backend
 * @error: return location for a #GError
 *
 * See rsd_vehicle_get_metadata_async().
 *
 * Returns: (transfer full): attribute metadata
 * Since: 0.4.0
 */
RsdAttributeMetadata *
rsd_vehicle_get_metadata_finish (RsdVehicle                *self,
                                 GAsyncResult              *result,
                                 RsdTimestampMicroseconds  *current_time,
                                 GError                   **error)
{
  RsdVehicleInterface *iface;
  g_autoptr (RsdAttributeMetadata) retval = NULL;
  RsdTimestampMicroseconds local_current_time = RSD_TIMESTAMP_UNKNOWN;
  GError *local_error = NULL;

  g_return_val_if_fail (RSD_IS_VEHICLE (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support reads. */
  if (iface->get_metadata_finish == NULL)
    {
      g_task_propagate_boolean (G_TASK (result), error);
      return NULL;
    }

  retval = iface->get_metadata_finish (self, result, &local_current_time,
                                       &local_error);

  g_return_val_if_fail ((retval == NULL) == (local_error != NULL), NULL);

  if (local_error != NULL)
    g_propagate_error (error, local_error);
  else if (current_time != NULL)
    *current_time = local_current_time;
  return g_steal_pointer (&retval);
}

/**
 * rsd_vehicle_set_attribute_async:
 * @self: a #RsdVehicle
 * @zone: zone to set the attribute in
 * @attribute_name: [name of the attribute](#attribute-names)
 * @value: (transfer none): attribute value
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to finish the operation
 * @user_data: user data to pass to @callback
 *
 * Set a single attribute in the given zone of the vehicle, or return an error if
 * the attribute is unknown, not currently available, or read-only. If @value has
 * a floating reference, it will be sunk.
 *
 * This is an asynchronous function; use rsd_vehicle_set_attribute_finish()
 * from @callback to get its #GError (if any).
 *
 * Since: 0.1.0
 */
void
rsd_vehicle_set_attribute_async (RsdVehicle          *self,
                                RsdZone             *zone,
                                const gchar         *attribute_name,
                                GVariant            *value,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (RSD_IS_ZONE (zone));
  g_return_if_fail (attribute_name != NULL && *attribute_name != '\0');
  g_return_if_fail (value != NULL);
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support writes. */
  if (iface->set_attribute_async == NULL)
    {
      g_task_report_new_error (self, callback, user_data,
                               rsd_vehicle_set_attribute_async,
                               RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_WRITABLE,
                               _("Attribute ‘%s’ in zone ‘%s’ of vehicle ‘%s’ "
                                 "is not writable."),
                               attribute_name, rsd_zone_get_path (zone),
                               rsd_vehicle_get_id (self));
      return;
    }

  iface->set_attribute_async (self, zone, attribute_name, value, cancellable,
                             callback, user_data);
}

/**
 * rsd_vehicle_set_attribute_finish:
 * @self: a #RsdVehicle
 * @result: asynchronous result
 * @error: return location for a #GError
 *
 * See rsd_vehicle_set_attribute_async().
 *
 * Since: 0.1.0
 */
void
rsd_vehicle_set_attribute_finish (RsdVehicle    *self,
                                 GAsyncResult  *result,
                                 GError       **error)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (G_IS_ASYNC_RESULT (result));
  g_return_if_fail (error == NULL || *error == NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support writes. */
  if (iface->set_attribute_finish == NULL)
    {
      g_task_propagate_boolean (G_TASK (result), error);
      return;
    }

  iface->set_attribute_finish (self, result, error);
}

/**
 * rsd_vehicle_get_all_attributes_async:
 * @self: a #RsdVehicle
 * @zone: zone to get all attributes from, or the root zone to get all
 *    attributes
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to finish the operation
 * @user_data: user data to pass to @callback
 *
 * Get all attributes in and below the given zone in the vehicle, or return an
 * error if the zone does not exist. If the zone is empty, an empty array is
 * returned. If the zone is the root zone, all attributes are returned.
 *
 * This is an asynchronous function; use rsd_vehicle_get_all_attributes_finish()
 * from @callback to get its return value.
 *
 * Since: 0.1.0
 */
void
rsd_vehicle_get_all_attributes_async (RsdVehicle          *self,
                                      RsdZone             *zone,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (RSD_IS_ZONE (zone));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = RSD_VEHICLE_GET_IFACE (self);
  g_assert (iface->get_all_attributes_async != NULL);
  iface->get_all_attributes_async (self, zone, cancellable, callback,
                                   user_data);
}

/**
 * rsd_vehicle_get_all_attributes_finish:
 * @self: a #RsdVehicle
 * @result: asynchronous result
 * @current_time: (out caller-allocates) (optional): return location for the
 *    current time as measured by the backend
 * @error: return location for a #GError
 *
 * See rsd_vehicle_get_all_attributes_async().
 *
 * Returns: (transfer container) (element-type RsdAttributeInfo): potentially
 *    empty array of attribute information
 * Since: 0.4.0
 */
GPtrArray *
rsd_vehicle_get_all_attributes_finish (RsdVehicle                *self,
                                       GAsyncResult              *result,
                                       RsdTimestampMicroseconds  *current_time,
                                       GError                   **error)
{
  RsdVehicleInterface *iface;
  GPtrArray/*<owned RsdAttributeInfo>*/ *retval = NULL;
  RsdTimestampMicroseconds local_current_time = RSD_TIMESTAMP_UNKNOWN;
  GError *local_error = NULL;

  g_return_val_if_fail (RSD_IS_VEHICLE (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);
  g_assert (iface->get_all_attributes_finish != NULL);
  retval = iface->get_all_attributes_finish (self, result, &local_current_time,
                                             &local_error);

  g_return_val_if_fail ((retval == NULL) == (local_error != NULL), NULL);

  if (local_error != NULL)
    g_propagate_error (error, local_error);
  else if (current_time != NULL)
    *current_time = local_current_time;
  return retval;
}

/**
 * rsd_vehicle_get_all_metadata_async:
 * @self: a #RsdVehicle
 * @zone: zone to get all attributes’ metadata from, or the root zone to get all
 *    attributes
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to finish the operation
 * @user_data: user data to pass to @callback
 *
 * Get the metadata for all attributes in and below the given zone in the
 * vehicle, or return an error if the zone does not exist. If the zone is empty,
 * an empty array is returned. If the zone is the root zone, all attribute
 * metadata is returned.
 *
 * This is an asynchronous function; use rsd_vehicle_get_all_metadata_finish()
 * from @callback to get its return value.
 *
 * Since: 0.2.0
 */
void
rsd_vehicle_get_all_metadata_async (RsdVehicle          *self,
                                    RsdZone             *zone,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (RSD_IS_ZONE (zone));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = RSD_VEHICLE_GET_IFACE (self);
  g_assert (iface->get_all_metadata_async != NULL);
  iface->get_all_metadata_async (self, zone, cancellable, callback,
                                 user_data);
}

/**
 * rsd_vehicle_get_all_metadata_finish:
 * @self: a #RsdVehicle
 * @result: asynchronous result
 * @current_time: (out caller-allocates) (optional): return location for the
 *    current time as measured by the backend
 * @error: return location for a #GError
 *
 * See rsd_vehicle_get_all_metadata_async().
 *
 * Returns: (transfer container) (element-type RsdAttributeMetadata):
 *    potentially empty array of attribute metadata information, where the
 *    #RsdAttributeMetadata is valid, but the #RsdAttribute is not
 * Since: 0.4.0
 */
GPtrArray *
rsd_vehicle_get_all_metadata_finish (RsdVehicle                *self,
                                     GAsyncResult              *result,
                                     RsdTimestampMicroseconds  *current_time,
                                     GError                   **error)
{
  RsdVehicleInterface *iface;
  GPtrArray/*<owned RsdAttributeMetadata>*/ *retval = NULL;
  RsdTimestampMicroseconds local_current_time = RSD_TIMESTAMP_UNKNOWN;
  GError *local_error = NULL;

  g_return_val_if_fail (RSD_IS_VEHICLE (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);
  g_assert (iface->get_all_metadata_finish != NULL);
  retval = iface->get_all_metadata_finish (self, result, &local_current_time,
                                           &local_error);

  g_return_val_if_fail ((retval == NULL) == (local_error != NULL), NULL);

  if (local_error != NULL)
    g_propagate_error (error, local_error);
  else if (current_time != NULL)
    *current_time = local_current_time;
  return retval;
}

/**
 * rsd_vehicle_update_subscriptions_async:
 * @self: a #RsdVehicle
 * @subscriptions: (transfer none) (element-type RsdSubscription): potentially
 *    empty list of #RsdSubscriptions giving the attributes to subscribe to
 * @unsubscriptions: (transfer none) (element-type RsdSubscription): potentially
 *    empty list of #RsdSubscriptions giving the attributes to unsubscribe from
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to finish the operation
 * @user_data: user data to pass to @callback
 *
 * Update the set of subscriptions for attributes in this vehicle. All
 * subscriptions in @unsubscriptions will be removed from the list; then those
 * in @subscriptions will be added. Subscriptions are matched using
 * rsd_subscription_equal(), rather than by pointer equality. Duplicate
 * subscriptions may be added, and will be treated individually; so removing one
 * will not remove all duplicates.
 *
 * In order to subscribe to signals for all attributes in the vehicle, a
 * wildcard subscription must be added; see the documentation for
 * #RsdSubscription for details. If no subscriptions are added, no attribute
 * changed signals will be emitted by the #RsdVehicle.
 *
 * This is an asynchronous function; use
 * rsd_vehicle_update_subscriptions_finish() from @callback to get its return
 * value.
 *
 * Since: 0.2.0
 */
void
rsd_vehicle_update_subscriptions_async (RsdVehicle          *self,
                                        GPtrArray           *subscriptions,
                                        GPtrArray           *unsubscriptions,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (subscriptions != NULL);
  g_return_if_fail (unsubscriptions != NULL);
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support subscriptions. Since the subscription
   * interface is defined to be optimistic (i.e. signals may still be emitted
   * for attributes the client is not subscribed to), this is fine. */
  if (iface->update_subscriptions_async == NULL)
    {
      g_autoptr (GTask) task = NULL;

      task = g_task_new (self, cancellable, callback, user_data);
      g_task_set_source_tag (task, rsd_vehicle_update_subscriptions_async);
      g_task_return_boolean (task, TRUE);
      return;
    }

  iface->update_subscriptions_async (self, subscriptions, unsubscriptions,
                                     cancellable, callback, user_data);
}

/**
 * rsd_vehicle_update_subscriptions_finish:
 * @self: a #RsdVehicle
 * @result: asynchronous result
 * @error: return location for a #GError
 *
 * See rsd_vehicle_update_subscriptions_async().
 *
 * Since: 0.2.0
 */
void
rsd_vehicle_update_subscriptions_finish (RsdVehicle    *self,
                                         GAsyncResult  *result,
                                         GError       **error)
{
  RsdVehicleInterface *iface;

  g_return_if_fail (RSD_IS_VEHICLE (self));
  g_return_if_fail (G_IS_ASYNC_RESULT (result));
  g_return_if_fail (error == NULL || *error == NULL);

  iface = RSD_VEHICLE_GET_IFACE (self);

  /* Implementation doesn’t support subscriptions. */
  if (iface->update_subscriptions_finish == NULL)
    {
      g_task_propagate_boolean (G_TASK (result), error);
      return;
    }

  iface->update_subscriptions_finish (self, result, error);
}

/**
 * rsd_vehicle_id_is_valid:
 * @vehicle_id: (nullable): a vehicle ID to validate
 *
 * Check whether the given @vehicle_id is valid. %NULL is an allowed input, and
 * returns %FALSE.
 *
 * Vehicle IDs must contain only the characters `0`–`9`, `a`–`z`, `A`–`Z` and
 * `-`, must start with a letter, and must be at least one character long.
 *
 * Returns: %TRUE if valid, %FALSE otherwise
 * Since: 0.1.0
 */
gboolean
rsd_vehicle_id_is_valid (const gchar *vehicle_id)
{
  if (vehicle_id == NULL)
    return FALSE;

  /* Check length and initial character. */
  if (*vehicle_id == '\0' || !g_ascii_isalpha (*vehicle_id))
    return FALSE;

  for (vehicle_id++; *vehicle_id != '\0'; vehicle_id++)
    if (!g_ascii_isalnum (*vehicle_id) && *vehicle_id != '-')
      return FALSE;

  return TRUE;
}
