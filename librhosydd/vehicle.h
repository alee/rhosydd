/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_VEHICLE_H
#define RSD_VEHICLE_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <librhosydd/attribute.h>
#include <librhosydd/types.h>
#include <librhosydd/zone.h>


G_BEGIN_DECLS

/**
 * RSD_VEHICLE_ERROR:
 *
 * Error domain for #RsdVehicleError.
 *
 * Since: 0.1.0
 */
#define RSD_VEHICLE_ERROR rsd_vehicle_error_quark ()
GQuark rsd_vehicle_error_quark (void);

/**
 * RsdVehicleError:
 * @RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE: The given vehicle does not exist.
 * @RSD_VEHICLE_ERROR_UNKNOWN_ZONE: The given zone does not exist in this
 *    vehicle.
 * @RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE: The given attribute does not exist in
 *    this zone.
 * @RSD_VEHICLE_ERROR_TOO_MANY_ZONES: Too many zones have been created in this
 *    vehicle, and no more can be created.
 * @RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_READABLE: Attribute is not readable and hence
 *    `get_attribute()` cannot be called on it.
 * @RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_WRITABLE: Attribute is not writable and hence
 *    `set_attribute()` cannot be called on it.
 * @RSD_VEHICLE_ERROR_INVALID_TAGS: Attribute tags are invalid.
 * @RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE: Attribute value or accuracy is
 *    invalid.
 * @RSD_VEHICLE_ERROR_ATTRIBUTE_UNAVAILABLE: Attribute is not currently available
 *    for access (see #RsdAttributeMetadata.availability).
 * @RSD_VEHICLE_ERROR_ILLEGAL_BEHAVIOUR: The given vehicle implementation
 *    broke the interface definition for an #RsdVehicle.
 * @RSD_VEHICLE_ERROR_INVALIDATED: The given vehicle has disappeared on the bus,
 *    either due to the service disappearing, or it triggering a
 *    @RSD_VEHICLE_ERROR_ILLEGAL_BEHAVIOUR error.
 * @RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION: A subscription (or unsubscription)
 *    structure was invalid, such as by having invalid trigger ranges.
 *
 * Errors from operations on vehicles.
 *
 * Since: 0.1.0
 */
typedef enum
{
  RSD_VEHICLE_ERROR_UNKNOWN_VEHICLE,
  RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
  RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
  RSD_VEHICLE_ERROR_TOO_MANY_ZONES,
  RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_READABLE,
  RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_WRITABLE,
  RSD_VEHICLE_ERROR_INVALID_TAGS,
  RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
  RSD_VEHICLE_ERROR_ATTRIBUTE_UNAVAILABLE,
  RSD_VEHICLE_ERROR_ILLEGAL_BEHAVIOUR,
  RSD_VEHICLE_ERROR_INVALIDATED,
  RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION,
} RsdVehicleError;

/**
 * RsdVehicle:
 *
 * An object instance which implements #RsdVehicleInterface.
 *
 * Since: 0.1.0
 */
#define RSD_TYPE_VEHICLE rsd_vehicle_get_type ()
G_DECLARE_INTERFACE (RsdVehicle, rsd_vehicle, RSD, VEHICLE, GObject)

/**
 * RsdVehicleInterface:
 * @g_iface: parent interface
 * @get_id: Get the vehicle’s ID.
 * @get_zones: Get the vehicle’s zones, as #RsdZones. At least one zone, `/`,
 *    must be returned.
 * @get_attribute_async: Get a single attribute from the given zone of the
 *    vehicle, or return an error if the attribute is unknown or not currently
 *    available.
 * @get_attribute_finish: Finish function for @get_attribute_async.
 * @get_metadata_async: Get the metadata for a single attribute from the given
 *    zone of the vehicle, or return an error if the attribute is unknown or
 *    not currently available.
 * @get_metadata_finish: Finish function for @get_metadata_async.
 * @set_attribute_async: Set a single attribute in the given zone of the vehicle,
 *    or return an error if the attribute is unknown, not currently available, or
 *    read-only.
 * @set_attribute_finish: Finish function for @set_attribute_async.
 * @get_all_attributes_async: Get all attributes in and below the given zone in
 *    the vehicle, or return an error if the zone does not exist. If the zone is
 *    empty, an empty array is returned. The array elements are
 *    #RsdAttributeInfos.
 * @get_all_attributes_finish: Finish function for @get_all_attributes_async.
 * @get_all_metadata_async: Get metadata for all attributes in and below the
 *    given zone in the vehicle, or return an error if the zone does not exist.
 *    If the zone is empty, an empty array is returned. The array elements are
 *    #RsdAttributeMetadatas.
 * @get_all_metadata_finish: Finish function for @get_all_metadata_async.
 * @update_subscriptions_async: Add and remove subscriptions to be notified
 *    about changes in attribute values.
 * @update_subscriptions_finish: Finish function for
 *    @update_subscriptions_async.
 *
 * An interface which presents attributes of a single vehicle, grouped into a
 * hierarchy of zones. All attributes may be read using @get_attribute_async or
 * @get_all_attributes_async; some attributes may be set using
 * @set_attribute_async.
 *
 * Since: 0.1.0
 */
struct _RsdVehicleInterface
{
  GTypeInterface g_iface;

  const gchar     *(*get_id)                    (RsdVehicle          *self);
  GPtrArray       *(*get_zones)                 (RsdVehicle          *self);

  void             (*get_attribute_async)       (RsdVehicle          *self,
                                                 RsdZone             *zone,
                                                 const gchar         *attribute_name,
                                                 GCancellable        *cancellable,
                                                 GAsyncReadyCallback  callback,
                                                 gpointer             user_data);
  RsdAttributeInfo *(*get_attribute_finish)     (RsdVehicle          *self,
                                                 GAsyncResult        *result,
                                                 RsdTimestampMicroseconds *current_time,
                                                 GError             **error);

  void             (*get_metadata_async)        (RsdVehicle          *self,
                                                 RsdZone             *zone,
                                                 const gchar         *attribute_name,
                                                 GCancellable        *cancellable,
                                                 GAsyncReadyCallback  callback,
                                                 gpointer             user_data);
  RsdAttributeMetadata *(*get_metadata_finish)  (RsdVehicle          *self,
                                                 GAsyncResult        *result,
                                                 RsdTimestampMicroseconds *current_time,
                                                 GError             **error);

  void             (*set_attribute_async)       (RsdVehicle          *self,
                                                 RsdZone             *zone,
                                                 const gchar         *attribute_name,
                                                 GVariant            *value,
                                                 GCancellable        *cancellable,
                                                 GAsyncReadyCallback  callback,
                                                 gpointer             user_data);
  void             (*set_attribute_finish)      (RsdVehicle          *self,
                                                 GAsyncResult        *result,
                                                 GError             **error);
  void             (*get_all_attributes_async)  (RsdVehicle          *self,
                                                 RsdZone             *zone,
                                                 GCancellable        *cancellable,
                                                 GAsyncReadyCallback  callback,
                                                 gpointer             user_data);
  GPtrArray       *(*get_all_attributes_finish) (RsdVehicle          *self,
                                                 GAsyncResult        *result,
                                                 RsdTimestampMicroseconds *current_time,
                                                 GError             **error);
  void             (*get_all_metadata_async)    (RsdVehicle          *self,
                                                 RsdZone             *zone,
                                                 GCancellable        *cancellable,
                                                 GAsyncReadyCallback  callback,
                                                 gpointer             user_data);
  GPtrArray       *(*get_all_metadata_finish)   (RsdVehicle          *self,
                                                 GAsyncResult        *result,
                                                 RsdTimestampMicroseconds *current_time,
                                                 GError             **error);

  void             (*update_subscriptions_async)  (RsdVehicle           *self,
                                                   GPtrArray            *subscriptions,
                                                   GPtrArray            *unsubscriptions,
                                                   GCancellable         *cancellable,
                                                   GAsyncReadyCallback   callback,
                                                   gpointer              user_data);
  void             (*update_subscriptions_finish) (RsdVehicle           *self,
                                                   GAsyncResult         *result,
                                                   GError              **error);
};

const gchar     *rsd_vehicle_get_id                    (RsdVehicle           *self);
GPtrArray       *rsd_vehicle_get_zones                 (RsdVehicle           *self);

void             rsd_vehicle_get_attribute_async       (RsdVehicle           *self,
                                                        RsdZone              *zone,
                                                        const gchar          *attribute_name,
                                                        GCancellable         *cancellable,
                                                        GAsyncReadyCallback   callback,
                                                        gpointer              user_data);
RsdAttributeInfo *rsd_vehicle_get_attribute_finish     (RsdVehicle           *self,
                                                        GAsyncResult         *result,
                                                        RsdTimestampMicroseconds *current_time,
                                                        GError              **error);

void             rsd_vehicle_get_metadata_async        (RsdVehicle           *self,
                                                        RsdZone              *zone,
                                                        const gchar          *attribute_name,
                                                        GCancellable         *cancellable,
                                                        GAsyncReadyCallback   callback,
                                                        gpointer              user_data);
RsdAttributeMetadata *rsd_vehicle_get_metadata_finish  (RsdVehicle           *self,
                                                        GAsyncResult         *result,
                                                        RsdTimestampMicroseconds *current_time,
                                                        GError              **error);

void             rsd_vehicle_set_attribute_async       (RsdVehicle           *self,
                                                        RsdZone              *zone,
                                                        const gchar          *attribute_name,
                                                        GVariant             *value,
                                                        GCancellable         *cancellable,
                                                        GAsyncReadyCallback   callback,
                                                        gpointer              user_data);
void             rsd_vehicle_set_attribute_finish      (RsdVehicle           *self,
                                                        GAsyncResult         *result,
                                                        GError              **error);
void             rsd_vehicle_get_all_attributes_async  (RsdVehicle           *self,
                                                        RsdZone              *zone,
                                                        GCancellable         *cancellable,
                                                        GAsyncReadyCallback   callback,
                                                        gpointer              user_data);
GPtrArray       *rsd_vehicle_get_all_attributes_finish (RsdVehicle           *self,
                                                        GAsyncResult         *result,
                                                        RsdTimestampMicroseconds *current_time,
                                                        GError              **error);

void             rsd_vehicle_get_all_metadata_async    (RsdVehicle           *self,
                                                        RsdZone              *zone,
                                                        GCancellable         *cancellable,
                                                        GAsyncReadyCallback   callback,
                                                        gpointer              user_data);
GPtrArray       *rsd_vehicle_get_all_metadata_finish   (RsdVehicle           *self,
                                                        GAsyncResult         *result,
                                                        RsdTimestampMicroseconds *current_time,
                                                        GError              **error);

void             rsd_vehicle_update_subscriptions_async  (RsdVehicle           *self,
                                                          GPtrArray            *subscriptions,
                                                          GPtrArray            *unsubscriptions,
                                                          GCancellable         *cancellable,
                                                          GAsyncReadyCallback   callback,
                                                          gpointer              user_data);
void             rsd_vehicle_update_subscriptions_finish (RsdVehicle           *self,
                                                          GAsyncResult         *result,
                                                          GError              **error);

gboolean         rsd_vehicle_id_is_valid                 (const gchar          *vehicle_id);

G_END_DECLS

#endif /* !RSD_VEHICLE_H */
