/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_SUBSCRIPTION_H
#define RSD_SUBSCRIPTION_H

#include <glib.h>

#include <librhosydd/attribute.h>


G_BEGIN_DECLS

/**
 * RsdSubscription:
 *
 * Structure which represents a subscription to be notified of changes to the
 * given @attribute_name in @zone_path (but not in any of its descendant zones),
 * if the value and period ranges match.
 *
 * @attribute_name must be a valid [attribute name](#attribute-names), or the empty
 * string. The empty string represents a ‘wildcard’ subscription, to all
 * attributes in the given zone and its descendants, but must only be used when
 * @zone_path is the root zone, `/`.
 *
 * @minimum_value and @maximum_value define a range of attribute values to notify
 * for. If non-%NULL, they must both be #GVariants with the same type, and must
 * be numeric. They can define a range (@minimum_value < @maximum_value), or be
 * equal (@minimum_value == @maximum_value), or define an inverse range
 * (@minimum_value > @maximum_value). In the latter case, the subscription will
 * notify for attribute values //outside// the range
 * @minimum_value–@maximum_value. For the second case, the subscription will
 * notify every time the attribute value crosses the given threshold. For
 * non-numeric attributes, they must both be %NULL.
 *
 * @hysteresis defines the symmetric hysteresis value to use to avoid emitting
 * too many signals near the @minimum_value and @maximum_value thresholds. It
 * must be %NULL if @minimum_value and @maximum_value are %NULL. If specified,
 * it ensures that notifications for values in a range only start being emitted
 * after the value has reached the range @minimum_value + @hysteresis to
 * @maximum_value - @hysteresis; and they will stop being emitted after the
 * value has gone below @minimum_value - @hysteresis or above
 * @maximum_value + @hysteresis. Similarly for the cases where
 * (@minimum_value == @maximum_value) or (@minimum_value > @maximum_value).
 *
 * @minimum_period and @maximum_period define a range of frequencies to emit
 * updates at. Attribute change notifications will be emitted every period which
 * is the lower of: the maximum of the @minimum_period of all subscriptions; and
 * the minimum of the @maximum_period of all subscriptions. This means that
 * applications may receive notifications more frequently than specified by
 * their @minimum_period, but this is avoided if possible.
 *
 * Since: 0.2.0
 */
typedef struct
{
  const gchar *zone_path;  /* interned */
  const gchar *attribute_name;  /* interned */

  GVariant *minimum_value;  /* nullable if non-numeric */
  GVariant *maximum_value;  /* nullable if non-numeric */
  GVariant *hysteresis;  /* nullable if non-numeric */

  guint minimum_period;  /* microseconds */
  guint maximum_period;  /* microseconds */

  /* Mutable state. */
  RsdAttribute *last_notify_attribute;  /* nullable if no notifications so far */
  RsdTimestampMicroseconds last_notify_time;
  gboolean last_notify_emitted;
} RsdSubscription;

RsdSubscription *rsd_subscription_new     (const gchar           *zone_path,
                                           const gchar           *attribute_name,
                                           GVariant              *minimum_value,
                                           GVariant              *maximum_value,
                                           GVariant              *hysteresis,
                                           guint                  minimum_period,
                                           guint                  maximum_period);
RsdSubscription *rsd_subscription_copy    (const RsdSubscription *subscription);
void             rsd_subscription_free    (RsdSubscription       *subscription);

gboolean         rsd_subscription_matches (const RsdSubscription *subscription,
                                           const RsdAttribute     *new_attribute,
                                           RsdTimestampMicroseconds           check_time);
gboolean         rsd_subscription_matches_period (const RsdSubscription *subscription,
                                                  RsdTimestampMicroseconds           check_time);
gboolean         rsd_subscription_equal   (const RsdSubscription *a,
                                           const RsdSubscription *b);

void             rsd_subscription_notify  (RsdSubscription       *subscription,
                                           const RsdAttribute     *attribute,
                                           RsdTimestampMicroseconds           notify_time,
                                           gboolean               emitted);

RsdSubscription *rsd_subscription_new_aggregate (GArray          *subscriptions);
RsdSubscription *rsd_subscription_new_wildcard  (void);

GType rsd_subscription_get_type (void);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RsdSubscription, rsd_subscription_free)

G_END_DECLS

#endif /* !RSD_SUBSCRIPTION_H */
