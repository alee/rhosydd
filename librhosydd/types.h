/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_TYPES_H
#define RSD_TYPES_H

#include <glib.h>


G_BEGIN_DECLS

/**
 * RsdTimestampMicroseconds:
 *
 * A timestamp, measured in microseconds since some system-specific epoch. It
 * is comparable with timestamps produced by g_get_monotonic_time().
 *
 * A value of %RSD_TIMESTAMP_UNKNOWN means the timestamp is unknown.
 *
 * Since: 0.1.0
 */
typedef gint64 RsdTimestampMicroseconds;

/**
 * RSD_TIMESTAMP_UNKNOWN:
 *
 * A timestamp representing an unknown time. See #RsdTimestampMicroseconds.
 *
 * Since: 0.1.0
 */
#define RSD_TIMESTAMP_UNKNOWN G_GINT64_CONSTANT (0)

G_END_DECLS

#endif /* !RSD_TYPES_H */
