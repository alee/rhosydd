/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib-object.h>
#include <string.h>

#include "librhosydd/zone.h"


G_DEFINE_INTERFACE (RsdZone, rsd_zone, G_TYPE_OBJECT)

static void
rsd_zone_default_init (RsdZoneInterface *iface)
{
  /* No default implementations. */
}

/**
 * rsd_zone_get_path:
 * @self: a #RsdZone
 *
 * Get the zone’s path.
 *
 * Returns: zone path, guaranteed to be interned
 * Since: 0.1.0
 */
const gchar *
rsd_zone_get_path (RsdZone *self)
{
  RsdZoneInterface *iface;
  const gchar *retval;

  g_return_val_if_fail (RSD_IS_ZONE (self), NULL);

  iface = RSD_ZONE_GET_IFACE (self);
  g_assert (iface->get_path != NULL);
  retval = iface->get_path (self);

  g_return_val_if_fail (rsd_zone_path_is_valid (retval), NULL);
  g_return_val_if_fail (retval == g_intern_string (retval),
                        g_intern_string (retval));

  return retval;
}

/**
 * rsd_zone_get_tags:
 * @self: a #RsdZone
 *
 * Get the zone’s tags. This is a %NULL-terminated array of non-empty tags for
 * the zone, which may be empty. %NULL will never be returned. The tags are
 * guaranteed to be sorted alphabetically, and all the tags are guaranteed to be
 * interned.
 *
 * Returns: (array zero-terminated): zone’s tags
 * Since: 0.1.0
 */
const gchar * const *
rsd_zone_get_tags (RsdZone *self)
{
  RsdZoneInterface *iface;
  const gchar * const *retval;
  static const gchar * const empty_tags[] = { NULL };

  g_return_val_if_fail (RSD_IS_ZONE (self), empty_tags);

  iface = RSD_ZONE_GET_IFACE (self);
  g_assert (iface->get_tags != NULL);
  retval = iface->get_tags (self);

  if (retval == NULL)
    retval = empty_tags;

  g_return_val_if_fail (retval != NULL, empty_tags);
  g_return_val_if_fail (g_strcmp0 (rsd_zone_get_path (self), "/") != 0 ||
                        *retval == NULL, empty_tags);

  return retval;
}

/**
 * rsd_zone_get_parent_path:
 * @self: a #RsdZone
 *
 * Get the zone’s parent zone path. This will be the empty string if, and only
 * if, this is the root zone. It will never be %NULL.
 *
 * Returns: parent zone path
 * Since: 0.1.0
 */
const gchar *
rsd_zone_get_parent_path (RsdZone *self)
{
  RsdZoneInterface *iface;
  const gchar *retval;

  g_return_val_if_fail (RSD_IS_ZONE (self), "");

  iface = RSD_ZONE_GET_IFACE (self);
  g_assert (iface->get_parent_path != NULL);
  retval = iface->get_parent_path (self);

  g_return_val_if_fail (retval != NULL, "");
  g_return_val_if_fail (*retval == '\0' || rsd_zone_path_is_valid (retval), "");
  g_return_val_if_fail (*retval != '\0' ||
                        g_strcmp0 (rsd_zone_get_path (self), "/") == 0,
                        "");

  return retval;
}

/**
 * rsd_zone_is_root:
 * @self: a #RsdZone
 *
 * Check whether this zone is the root zone, i.e. has the path `/`.
 *
 * Returns: %TRUE if this is the root zone, %FALSE otherwise
 * Since: 0.2.0
 */
gboolean
rsd_zone_is_root (RsdZone *self)
{
  g_return_val_if_fail (RSD_IS_ZONE (self), FALSE);

  return (g_strcmp0 (rsd_zone_get_path (self), "/") == 0);
}

/**
 * rsd_zone_is_descendant:
 * @self: an #RsdZone
 * @ancestor: a potential ancestor zone
 *
 * Check whether @self is equal to, or a descendant of, @ancestor.
 *
 * Returns: %TRUE if @self is a descedant of @ancestor, %FALSE otherwise
 * Since: 0.4.0
 */
gboolean
rsd_zone_is_descendant (RsdZone *self,
                        RsdZone *ancestor)
{
  g_return_val_if_fail (RSD_IS_ZONE (self), FALSE);
  g_return_val_if_fail (RSD_IS_ZONE (ancestor), FALSE);

  /* This works because zone paths always start and end with ‘/’, so we can
   * guarantee that ‘foodstuffs’ will not match as a descendant of ‘foo’,
   * because their paths are actually ‘/foodstuffs/’ and ‘/foo/’. */
  return g_str_has_prefix (rsd_zone_get_path (self),
                           rsd_zone_get_path (ancestor));
}

/**
 * rsd_zone_tag_is_valid:
 * @tag: (nullable): a tag to validate
 *
 * Check whether the given @tag is valid. %NULL is an allowed input, and returns
 * %FALSE.
 *
 * Returns: %TRUE if valid, %FALSE otherwise
 * Since: 0.1.0
 */
gboolean
rsd_zone_tag_is_valid (const gchar *tag)
{
  const gchar *i;

  if (tag == NULL || *tag == '\0')
    return FALSE;

  for (i = tag; *i != '\0'; i++)
    {
      if (!g_ascii_isalnum (*i))
        return FALSE;
    }

  return TRUE;
}

/**
 * rsd_zone_tags_are_valid:
 * @tags: (nullable) (array zero-terminated): an array of tags to validate
 *
 * Check whether the given @tags array is valid, including checking that the
 * tags are in string order. %NULL is an allowed input, and
 * returns %TRUE as it represents the empty tags array.
 *
 * Returns: %TRUE if valid, %FALSE otherwise
 * Since: 0.1.0
 */
gboolean
rsd_zone_tags_are_valid (const gchar * const *tags)
{
  const gchar * const *i;
  const gchar *previous_tag;

  if (tags == NULL)
    return TRUE;

  for (i = tags, previous_tag = NULL; *i != NULL; previous_tag = *i, i = i + 1)
    {
      if (!rsd_zone_tag_is_valid (*i))
        return FALSE;

      if (previous_tag != NULL && g_strcmp0 (previous_tag, *i) >= 0)
        return FALSE;
    }

  return TRUE;
}

/* Private version of the contents of #RsdZonePathIter. */
typedef struct
{
  const gchar *path;  /* interned */
  const gchar *i;
} RsdZonePathIterReal;

G_STATIC_ASSERT (sizeof (RsdZonePathIterReal) == sizeof (RsdZonePathIter));

/**
 * rsd_zone_path_iter_init:
 * @iter: an allocated #RsdZonePathIter to initialise
 * @path: zone path to iterate over
 *
 * Initialise a #RsdZonePathIter to iterate over the given @path. @path may be
 * freed after this function is called, as an interned copy is taken.
 *
 * Since: 0.2.0
 */
void
rsd_zone_path_iter_init (RsdZonePathIter *iter,
                         const gchar     *path)
{
  RsdZonePathIterReal *_iter = (RsdZonePathIterReal *) iter;

  g_return_if_fail (iter != NULL);
  g_return_if_fail (rsd_zone_path_is_valid (path));

  _iter->path = g_intern_string (path);
  _iter->i = _iter->path;
}

/* Version of g_intern_string() which takes the first @len bytes of @str. */
static const gchar *
intern_string_n (const gchar *str,
                 gsize        len)
{
  g_autofree gchar *tmp = NULL;

  tmp = g_strndup (str, len);
  return g_intern_string (tmp);
}

/* A version of strchrnul() which treats (s + len) as the nul terminator byte
 * if one doesn’t exist earlier. */
static const char *
strnchrnul (const char *s,
            size_t      len,
            int         c)
{
  const char *out;

  out = strchrnul (s, c);
  if (out > s + len)
    out = s + len;

  return out;
}

/* Parse @str (up to @len bytes) as a set of tags, and return them as a
 * %NULL-terminated array of interned strings. The @str is assumed to be valid
 * (i.e. a non-empty string of tags, separated by commas, all of the tags are
 * valid).
 *
 * If @len is zero, %NULL is returned.
 */
static const gchar **
tags_from_string (const gchar *str,
                  gsize        len)
{
  g_autoptr (GPtrArray) tag_array = NULL;
  g_autofree const gchar **tags = NULL;
  const gchar *i, *next_comma;
  gsize i_len;

  if (len == 0)
    return NULL;

  tag_array = g_ptr_array_new ();

  for (i = str, i_len = len, next_comma = strnchrnul (i, i_len, ',');
       i < str + len;
       i = next_comma + 1, i_len = len - (i - str),
       next_comma = strnchrnul (i, i_len, ','))
    {
      const gchar *tag;

      g_assert (next_comma > i);
      tag = intern_string_n (i, next_comma - i);
      g_assert (rsd_zone_tag_is_valid (tag));
      g_ptr_array_add (tag_array, (gpointer) tag);
    }

  /* NULL terminator. */
  g_ptr_array_add (tag_array, NULL);

  tags = (const gchar **) g_ptr_array_free (tag_array, FALSE);
  tag_array = NULL;

  g_return_val_if_fail (rsd_zone_tags_are_valid (tags), NULL);

  return g_steal_pointer (&tags);
}

/**
 * rsd_zone_path_iter_next:
 * @iter: an #RsdZonePathIter
 * @parent_path: (out callee-allocates) (transfer none) (optional): return
 *    location for the parent path, or %NULL to ignore
 * @tags: (out callee-allocates) (transfer full) (array zero-terminated=1)
 *    (optional): return location for the tags, or %NULL to ignore; the caller
 *    must free this with g_free() once done
 *
 * Advance @iter and retrieve the tags for the next element in the zone path
 * being iterated over. If %FALSE is returned, the end of the path has been
 * reached and neither @parent_path or @tags are valid. Iteration starts at the
 * root of the path (returning the empty string in @parent_path and an empty
 * tag list in @tags) and proceeds component-wise left-to-right.
 *
 * The returned @parent_path and @tags strings are guaranteed to be interned.
 * The @tags are guaranteed to be in order.
 *
 * Returns: %TRUE if @parent_path and @tags are valid; %FALSE otherwise
 * Since: 0.2.0
 */
gboolean
rsd_zone_path_iter_next (RsdZonePathIter   *iter,
                         const gchar      **parent_path,
                         const gchar     ***tags)
{
  RsdZonePathIterReal *_iter = (RsdZonePathIterReal *) iter;
  const gchar *next_slash;
  const gchar *_parent_path;
  const gchar **_tags;

  g_return_val_if_fail (iter != NULL, FALSE);

  /* Reached the end? */
  if (*_iter->i == '\0')
    return FALSE;

  /* i must be at the beginning, or just after a slash. */
  g_assert (_iter->i == _iter->path || *(_iter->i - 1) == '/');

  /* The path always ends with a slash, so we must always be able to find it. */
  next_slash = strchr (_iter->i, '/');
  g_assert (next_slash != NULL);

  /* Extract the tags between the current and next slashes; and grab the whole
   * of the previous path up to (and including) the current slash. */
  _tags = tags_from_string (_iter->i, next_slash - _iter->i);
  _parent_path = intern_string_n (_iter->path, _iter->i - _iter->path);

  /* Update internal state. Skip the next slash. */
  _iter->i = next_slash + 1;

  if (parent_path != NULL)
    *parent_path = _parent_path;
  if (tags != NULL)
    *tags = _tags;

  return TRUE;
}

/* g_ptr_array_sort() passes pointers to the gchar*s to the sort function. */
static gint
cmp_tags (gconstpointer a,
          gconstpointer b)
{
  const gchar *a_str = *((const gchar * const *) a);
  const gchar *b_str = *((const gchar * const *) b);

  return g_strcmp0 (a_str, b_str);
}

/**
 * rsd_zone_path_build:
 * @parent_path: path of the parent zone, or the empty string if this is the
 *    root zone
 * @tags: (nullable): array of tags to differentiate this zone from its
 *    siblings, or %NULL for the empty set of tags
 *
 * Build a zone path by formatting and appending @tags to @parent_path.
 *
 * See [Zone Paths]{librhosydd-api.markdown#zone-paths} for information on the
 * zone path format.
 *
 * Returns: new zone path
 * Since: 0.1.0
 */
const gchar *
rsd_zone_path_build (const gchar         *parent_path,
                     const gchar * const *tags)
{
  g_autofree gchar *joined_tags = NULL;
  g_autofree gchar *path = NULL;
  g_autoptr (GPtrArray/*<unowned gchar *>*/) processed_tags_array = NULL;

  g_return_val_if_fail (parent_path != NULL, "");
  g_return_val_if_fail (*parent_path != '\0' || tags == NULL, "");
  g_return_val_if_fail (*parent_path == '\0' ||
                        rsd_zone_path_is_valid (parent_path), "");

  /* Sort the tags. */
  if (tags != NULL)
    {
      processed_tags_array = g_ptr_array_new ();
      for (; tags != NULL && *tags != NULL; tags++)
        g_ptr_array_add (processed_tags_array, (gpointer) *tags);
      g_ptr_array_sort (processed_tags_array, cmp_tags);
      g_ptr_array_add (processed_tags_array, NULL);  /* terminator */

      joined_tags = g_strjoinv (",", (gchar **) processed_tags_array->pdata);
    }

  path = g_strdup_printf ("%s%s/", parent_path,
                          (joined_tags != NULL) ? joined_tags : "");

  g_return_val_if_fail (rsd_zone_path_is_valid (path), "");

  return g_intern_string (path);
}

/**
 * rsd_zone_path_tail:
 * @path: a path to split
 * @parent_path: (out callee-allocates) (transfer none) (optional): return
 *    location for the parent path, or %NULL to ignore
 *
 * Split a @path into its @parent_path and tags from the tail of the path. If
 * @path is the root zone (`/`), %NULL will be returned and @parent_path will be
 * set to the empty string. Otherwise, a valid path will be returned in
 * @parent_path, and an array of tags will be returned. %NULL will be returned
 * if there are no tags on the tail of the path.
 *
 * The returned tags array (if non-%NULL) is guaranteed to be sorted
 * alphabetically, and all the tags are guaranteed to be interned. The array
 * should be freed with g_free() but its members must not be freed.
 *
 * The returned @parent_path is guaranteed to be interned.
 *
 * Returns: (transfer container) (array zero-terminated=1) (nullable): array of
 *    tags from the end of @path, or %NULL if there were no tags
 * Since: 0.2.0
 */
const gchar **
rsd_zone_path_tail (const gchar  *path,
                    const gchar **parent_path)
{
  g_autofree const gchar **tags = NULL;
  const gchar *_parent_path;

  g_return_val_if_fail (rsd_zone_path_is_valid (path), NULL);

  if (strcmp (path, "/") == 0)
    {
      _parent_path = g_intern_static_string ("");
      tags = NULL;
    }
  else
    {
      g_autofree gchar *path_copy = NULL;
      gsize len;
      const gchar *final_slash;
      g_autofree gchar *_parent_path_allocated = NULL;

      path_copy = g_strdup (path);

      /* Remove the trailing slash. */
      len = strlen (path_copy);
      g_assert (path_copy[len - 1] == '/');
      path_copy[len - 1] = '\0';
      len--;

      final_slash = strrchr (path_copy, '/');
      g_assert (final_slash != NULL);

      _parent_path_allocated = g_strndup (path, (final_slash + 1) - path_copy);
      _parent_path = g_intern_string (_parent_path_allocated);
      tags = tags_from_string (final_slash + 1,
                               path_copy + len - (final_slash + 1));
    }

  if (parent_path != NULL)
    *parent_path = _parent_path;

  g_return_val_if_fail (_parent_path != NULL, NULL);
  g_return_val_if_fail (*_parent_path != '\0' || tags == NULL, NULL);
  g_return_val_if_fail (*_parent_path == '\0' ||
                        rsd_zone_path_is_valid (_parent_path), NULL);
  g_return_val_if_fail (rsd_zone_tags_are_valid (tags), NULL);

  return g_steal_pointer (&tags);
}

/**
 * rsd_zone_path_is_valid:
 * @path: (nullable): a path to validate
 *
 * Check whether the given @path is valid. %NULL and the empty string are
 * allowed inputs, and return %FALSE.
 *
 * See [Zone Paths]{librhosydd-api.markdown#zone-paths} for information on the
 * valid zone path format.
 *
 * Returns: %TRUE if valid, %FALSE otherwise
 * Since: 0.2.0
 */
gboolean
rsd_zone_path_is_valid (const gchar *path)
{
  g_auto (GStrv) sections = NULL;
  gsize i;

  if (path == NULL || *path == '\0')
    return FALSE;

  /* Format:
   *    / ((tag (,tag)*)? /)*
   * The tags in each section of the path must be in strcmp() order. Each tag
   * must be valid according to rsd_zone_tag_is_valid().
   */

  /* Check the leading slash. */
  if (*path != '/')
    return FALSE;

  /* Split into groups of tags. This is not very efficient, but is easy to
   * verify. */
  sections = g_strsplit (path, "/", -1);

  for (i = 0; sections[i] != NULL; i++)
    {
      const gchar *section = sections[i];
      g_auto (GStrv) tags = NULL;

      /* Validate each tag. There may be zero tags if @section == "". */
      tags = g_strsplit (section, ",", -1);

      if (!rsd_zone_tags_are_valid ((const gchar * const *) tags))
        return FALSE;
    }

  /* First and last sections must be empty due to the trailing and leading
   * slashes. */
  if (*sections[0] != '\0')
    return FALSE;
  if (*sections[i - 1] != '\0')
    return FALSE;

  return TRUE;
}
