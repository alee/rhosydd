#!/bin/sh

# Smoke-test for rhosydd-client: can it connect to the running daemon?

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e

exec 2>&1

case "$(id -n -u)" in
    (root)
        as_root=
        ;;

    (user)
        if ! sudo --non-interactive --validate; then
            echo "SKIP: cannot use sudo without password here"
            exit 0
        fi
        as_root="sudo --non-interactive"
        ;;

    (*)
        echo "SKIP: neither root nor user; is this really Apertis?"
        exit 0
        ;;
esac

$as_root systemctl status rhosydd.service || :

# Use sudo to get authorisation. This will probably end up listing zero
# vehicles, as no backends are running. But it should at least do so
# successfully.
$as_root rhosydd-client list-vehicles

$as_root  systemctl status rhosydd.service
