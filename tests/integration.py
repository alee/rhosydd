#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""Integration tests for the daemon and mock backends."""

import gi
from gi.repository import (GLib, Gio)

gi.require_version('Rhosydd', '0')
from gi.repository import Rhosydd  # pylint: disable=no-name-in-module

import subprocess
import time
import unittest
import xml.etree.ElementTree as ElementTree


class TestWithMockBackend(unittest.TestCase):
    """Integration test using the mock backends.

    It relies on service activation for the daemon and mock backends
    (rhosydd-mock-backend.service and rhosydd-speedo-backend.service) to be
    started, which means that the mock backends must be installed.

    It can only be run when installed, due to requiring all the polkit
    policy files, systemd unit files, and D-Bus service files to be in place
    for the system bus.

    It currently needs to be run as root in order for all the polkit
    authorisation checks to succeed, as no .rules file is installed to
    give it authorisation.
    """

    def setUp(self):
        self.timeout_seconds = 10  # seconds per test
        self.conn = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
        self.vehicle_manager = None
        self.main_context = GLib.main_context_default()
        self.__result = None
        self.__vehicle_changes = []
        self.__vehicle_num_attributes_changed = {}
        self.timed_out = False

        # Sanity check that everything’s running before starting.
        self._block_on_service_state('rhosydd.service', 'active')
        self._block_on_service_state('rhosydd-mock-backend.service', 'active')
        self._block_on_service_state('rhosydd-speedo-backend.service',
                                     'active')

        self.vehicle_manager = self._get_vehicle_manager(self.conn)
        self.vehicle_manager.connect('vehicles-changed',
                                     self.__vehicles_changed_cb)

    def tearDown(self):
        # Check there are no more sources pending dispatch.
        self.assertIsNone(self.__result)
        self.assertEqual(self.__vehicle_changes, [])
        # FIXME: This fails for test_unicast_attributes_changed_signals(); it
        # needs to be investigated properly.
        # self.assertFalse(self.main_context.iteration(False))
        self.main_context = None

        self.vehicle_manager = None
        self.conn = None
        self.__vehicle_num_attributes_changed = {}

    def _get_vehicle_manager(self, connection):
        """Build a VehicleManager for the given D-Bus connection."""
        Rhosydd.VehicleManager.new_async(connection=connection,
                                         cancellable=None,
                                         callback=self._block_on_result_cb,
                                         user_data=self)
        return Rhosydd.VehicleManager.new_finish(self._block_on_result())

    def __vehicles_changed_cb(self, vehicle_manager, added, removed):
        """Callback from Rhosydd.VehicleManager.vehicles_changed."""
        self.__vehicle_changes.append((added, removed))

    def __timeout_cb(self):
        """Callback when a timeout is reached on a long-running operation."""
        self.fail('Timeout reached')
        self.timed_out = True
        self.main_context.wakeup()

        return GLib.Source.REMOVE

    def _block_on_result(self):
        """Block on receiving an async result.

        Pass callback=self._block_on_result_cb to the asynchronous function,
        with user_data=self, then call this function in order to block on
        retrieving the Gio.AsyncResult from an asynchronous operation, which
        will be returned. You can then call the appropriate finish() function
        on it.
        """
        timeout_id = GLib.timeout_add_seconds(self.timeout_seconds,
                                              self.__timeout_cb)

        while self.__result is None and not self.timed_out:
            self.main_context.iteration(True)
        self.assertFalse(self.timed_out)

        GLib.Source.remove(timeout_id)

        result = self.__result
        self.__result = None

        return result

    def _block_on_result_cb(self, source_object, result, user_data):
        """Callback to use with _block_on_result()."""
        self.__result = result

    def _systemctl_command(self, command, unit_name):
        """Execute `systemctl $command $unit_name` and assert it worked."""
        subprocess.check_call(['systemctl', '--no-pager', command, unit_name])

    def _block_on_service_state(self, unit_name, expected_state):
        """Execute `systemctl show --property=ActiveState $unit_name` and
           wait until it returns expected_status."""
        start_time = GLib.get_monotonic_time()
        active_state = None

        while (active_state != expected_state and
               GLib.get_monotonic_time() <
               start_time + self.timeout_seconds * GLib.USEC_PER_SEC):
            out = subprocess.check_output(['systemctl', '--no-pager', 'show',
                                           '--property', 'ActiveState',
                                           unit_name])
            parts = out.decode('utf-8').strip().split('=', 1)
            self.assertEqual(parts[0], 'ActiveState')
            active_state = parts[1]
            time.sleep(0.05)  # avoid a busy loop
        self.assertEqual(active_state, expected_state)

    def _assert_vehicles(self, expected_vehicles):
        """Assert that the manager lists exactly expected_vehicles."""
        vehicles = self.vehicle_manager.list_vehicles()
        ids = [v.get_id() for v in vehicles]
        self.assertCountEqual(ids, expected_vehicles)

    def _assert_vehicles_added(self, expected_added):
        """Assert that the manager emits one or more vehicles-changed signals
           contiguously resulting in expected_added being added overall."""
        timeout_id = GLib.timeout_add_seconds(self.timeout_seconds,
                                              self.__timeout_cb)

        expected_added = set(expected_added)

        while expected_added:
            # Grab the changes.
            while not self.__vehicle_changes and not self.timed_out:
                self.main_context.iteration(True)
            self.assertFalse(self.timed_out)

            # Check that only vehicles which are still in the expected set have
            # been added, and none have been removed.
            (added, removed) = self.__vehicle_changes.pop(0)
            added = set([v.get_id() for v in added])

            self.assertEqual(removed, [])
            self.assertTrue(added.issubset(expected_added))

            expected_added = expected_added - added

        GLib.Source.remove(timeout_id)

    def _assert_vehicles_removed(self, expected_removed):
        """Assert that the manager emits one or more vehicles-changed signals
           contiguously resulting in expected_removed being removed overall."""
        timeout_id = GLib.timeout_add_seconds(self.timeout_seconds,
                                              self.__timeout_cb)

        expected_removed = set(expected_removed)

        while expected_removed:
            # Grab the changes.
            while not self.__vehicle_changes and not self.timed_out:
                self.main_context.iteration(True)
            self.assertFalse(self.timed_out)

            # Check that only vehicles which are still in the expected set have
            # been removed, and none have been added.
            (added, removed) = self.__vehicle_changes.pop(0)
            removed = set([v.get_id() for v in removed])

            self.assertEqual(added, [])
            self.assertTrue(removed.issubset(expected_removed))

            expected_removed = expected_removed - removed

        GLib.Source.remove(timeout_id)

    def test_list_vehicles(self):
        """Test that the mock backend’s vehicles are listed correctly."""
        self._assert_vehicles(['mock', 'speedo'])

    def __get_vehicle(self, vehicle_id):
        """Helper function to get the given vehicle, or None."""
        vehicles = self.vehicle_manager.list_vehicles()

        for vehicle in vehicles:
            if vehicle.get_id() == vehicle_id:
                return vehicle

        return None

    def test_vehicle_zones(self):
        """Test that the mock backend’s only vehicle’s zones are listed."""
        mock_vehicle = self.__get_vehicle('mock')

        zones = mock_vehicle.get_zones()
        self.assertEqual(len(zones), 1)
        root_zone = zones[0]

        self.assertEqual(root_zone.get_path(), '/')
        self.assertEqual(root_zone.get_parent_path(), '')
        self.assertEqual(root_zone.get_tags(), [])

    def test_vehicle_all_metadata(self):
        """Test getting all metadata from a vehicle."""
        mock_vehicle = self.__get_vehicle('mock')
        root_zone = Rhosydd.StaticZone.new('/')

        mock_vehicle.get_all_metadata_async(
            zone=root_zone,
            callback=self._block_on_result_cb,
            user_data=self)
        (metadata, current_time) = \
            mock_vehicle.get_all_metadata_finish(self._block_on_result())

        # The mock backend currently exports 70 attributes.
        self.assertEqual(len(metadata), 70)

        # Pick a particular attribute to spot-check.
        for meta in metadata:
            if meta.name != 'lightStatus.automaticHeadlights':
                continue

            self.assertEqual(meta.name, 'lightStatus.automaticHeadlights')
            self.assertEqual(meta.zone_path, '/')
            self.assertEqual(meta.availability,
                             Rhosydd.AttributeAvailability.AVAILABLE)
            self.assertEqual(meta.flags, Rhosydd.AttributeFlags.READABLE)

    def test_vehicle_all_attributes(self):
        """Test getting all attributes from a vehicle."""
        mock_vehicle = self.__get_vehicle('mock')
        root_zone = Rhosydd.StaticZone.new('/')

        mock_vehicle.get_all_attributes_async(
            zone=root_zone,
            callback=self._block_on_result_cb,
            user_data=self)
        (attributes, current_time) = \
            mock_vehicle.get_all_attributes_finish(self._block_on_result())

        # The mock backend currently exports 70 attributes.
        self.assertEqual(len(attributes), 70)

        # Pick a particular attribute to spot-check.
        for attr in attributes:
            if attr.metadata.name != 'lightStatus.automaticHeadlights':
                continue

            self.assertEqual(attr.attribute.accuracy, 0.0)
            self.assertEqual(attr.attribute.last_updated, 0)
            self.assertLessEqual(attr.attribute.last_updated, current_time)
            self.assertEqual(attr.attribute.value, GLib.Variant('b', True))

            self.assertEqual(attr.metadata.name,
                             'lightStatus.automaticHeadlights')
            self.assertEqual(attr.metadata.zone_path, '/')
            self.assertEqual(attr.metadata.availability,
                             Rhosydd.AttributeAvailability.AVAILABLE)
            self.assertEqual(attr.metadata.flags,
                             Rhosydd.AttributeFlags.READABLE)

    def test_vehicle_metadata(self):
        """Test getting metadata for a specific attribute from a vehicle."""
        mock_vehicle = self.__get_vehicle('mock')
        root_zone = Rhosydd.StaticZone.new('/')

        mock_vehicle.get_metadata_async(
            zone=root_zone,
            attribute_name='lightStatus.automaticHeadlights',
            callback=self._block_on_result_cb,
            user_data=self)
        (metadata, current_time) = \
            mock_vehicle.get_metadata_finish(self._block_on_result())

        self.assertEqual(metadata.name, 'lightStatus.automaticHeadlights')
        self.assertEqual(metadata.zone_path, '/')
        self.assertEqual(metadata.availability,
                         Rhosydd.AttributeAvailability.AVAILABLE)
        self.assertEqual(metadata.flags, Rhosydd.AttributeFlags.READABLE)

    def test_vehicle_attribute(self):
        """Test getting a specific attribute from a vehicle."""
        mock_vehicle = self.__get_vehicle('mock')
        root_zone = Rhosydd.StaticZone.new('/')

        mock_vehicle.get_attribute_async(
            zone=root_zone,
            attribute_name='lightStatus.automaticHeadlights',
            callback=self._block_on_result_cb,
            user_data=self)
        (attribute, current_time) = \
            mock_vehicle.get_attribute_finish(self._block_on_result())

        self.assertEqual(attribute.attribute.accuracy, 0.0)
        self.assertEqual(attribute.attribute.last_updated, 0)
        self.assertLessEqual(attribute.attribute.last_updated, current_time)
        self.assertEqual(attribute.attribute.value, GLib.Variant('b', True))

        self.assertEqual(attribute.metadata.name,
                         'lightStatus.automaticHeadlights')
        self.assertEqual(attribute.metadata.zone_path, '/')
        self.assertEqual(attribute.metadata.availability,
                         Rhosydd.AttributeAvailability.AVAILABLE)
        self.assertEqual(attribute.metadata.flags,
                         Rhosydd.AttributeFlags.READABLE)

    def test_vehicle_attribute_repeatedly(self):
        """Test getting a specific attribute from a vehicle, several times.

        Check that its value changes and hence that it’s not getting stale in
        the cache because we haven’t added a subscription (see T2042).
        """
        speedo_vehicle = self.__get_vehicle('speedo')
        root_zone = Rhosydd.StaticZone.new('/front,left/')

        speedo_vehicle.get_attribute_async(
            zone=root_zone,
            attribute_name='wheelSpeed.value',
            callback=self._block_on_result_cb,
            user_data=self)
        (attribute, current_time) = \
            speedo_vehicle.get_attribute_finish(self._block_on_result())

        initial_value = attribute.attribute.value
        subsequent_value = initial_value

        start_time = GLib.get_monotonic_time()

        while (subsequent_value.equal(initial_value) and
               GLib.get_monotonic_time() <
               start_time + self.timeout_seconds * GLib.USEC_PER_SEC):
            time.sleep(0.5)  # avoid a busy loop

            speedo_vehicle.get_attribute_async(
                zone=root_zone,
                attribute_name='wheelSpeed.value',
                callback=self._block_on_result_cb,
                user_data=self)
            (attribute, current_time) = \
                speedo_vehicle.get_attribute_finish(self._block_on_result())

            subsequent_value = attribute.attribute.value

    def test_exported_objects(self):
        """Test the SDK API only exports the objects we expect.

        If it’s exporting more, that could leak confidential information from
        the daemon.

        Do this by introspecting the root object.
        """
        paths = self.__introspect_object_paths('org.apertis.Rhosydd1',
                                               '/org/apertis/Rhosydd1')

        self.assertCountEqual(paths, [
            '/org/apertis/Rhosydd1',
            '/org/apertis/Rhosydd1/mock',
            '/org/apertis/Rhosydd1/speedo',
        ])

        # FIXME: For some reason we end up with some undispatched sources in
        # the main context. Handle those before finishing.
        while self.main_context.iteration(False):
            pass

    def __introspect_object_paths(self, service, object_path):
        """List objects beneath a given root.

        Recursively build a list of all the objects available beneath (and
        including) object_path on the well-known name, service.
        """
        object_paths = [object_path]
        Gio.DBusProxy.new(connection=self.conn,
                          flags=Gio.DBusProxyFlags.NONE,
                          info=None,
                          name=service,
                          object_path=object_path,
                          interface_name='org.freedesktop.DBus.Introspectable',
                          cancellable=None,
                          callback=self._block_on_result_cb,
                          user_data=self)
        proxy = Gio.DBusProxy.new_finish(self._block_on_result())

        # Introspect the object to find its immediate child nodes.
        proxy.Introspect(result_handler=self._block_on_result_cb,
                         user_data=self)
        introspection_data = self._block_on_result()
        if isinstance(introspection_data, Exception):
            raise introspection_data  # pylint: disable=raising-bad-type

        for node in ElementTree.fromstring(introspection_data):
            if node.tag == 'node':
                if object_path == '/':
                    object_path = ''
                node_path = '/'.join((object_path, node.attrib['name']))
                node_child_paths = self.__introspect_object_paths(service,
                                                                  node_path)
                object_paths += node_child_paths

        return object_paths

    def test_no_backend_access(self):
        """Test that we cannot access backends directly.

        We expect this to raise an AccessDenied error.
        """
        self.conn.call(bus_name='org.apertis.Rhosydd1.Backends.Mock',
                       object_path='/org/apertis/Rhosydd1/Backends/Mock/mock',
                       interface_name='org.apertis.Rhosydd1.Vehicle',
                       method_name='GetAllAttributesMetadata',
                       parameters=GLib.Variant('(s)', ('/',)),
                       reply_type=GLib.VariantType('(a(ss(uu)))'),
                       flags=Gio.DBusCallFlags.NONE,
                       timeout_msec=-1,
                       cancellable=None,
                       callback=self._block_on_result_cb,
                       user_data=self)

        with self.assertRaises(GLib.Error) as err:
            self.conn.call_finish(self._block_on_result())

        self.assertEqual(err.exception.domain,
                         GLib.quark_to_string(Gio.DBusError.quark()))
        self.assertEqual(err.exception.code, Gio.DBusError.ACCESS_DENIED)

    def test_stopping_backend_removes_vehicles(self):
        """Test that stopping a backend removes its vehicles.

        This also tests that the daemon doesn’t crash if a backend is stopped.
        """
        self._assert_vehicles(['mock', 'speedo'])
        self._systemctl_command('stop', 'rhosydd-mock-backend.service')
        self._assert_vehicles_removed(['mock'])
        self._systemctl_command('stop', 'rhosydd-speedo-backend.service')
        self._assert_vehicles_removed(['speedo'])

        self._assert_vehicles([])

        self._systemctl_command('start', 'rhosydd-mock-backend.service')
        self._assert_vehicles_added(['mock'])
        self._systemctl_command('start', 'rhosydd-speedo-backend.service')
        self._assert_vehicles_added(['speedo'])

        self._assert_vehicles(['mock', 'speedo'])

    def test_stopping_daemon_stops_backends(self):
        """Test that stopping the daemon stops the backends too."""
        self._systemctl_command('stop', 'rhosydd.service')

        self._block_on_service_state('rhosydd-mock-backend.service', 'failed')
        self._block_on_service_state('rhosydd-speedo-backend.service',
                                     'failed')

        self._assert_vehicles_removed(['speedo', 'mock'])

        self._systemctl_command('start', 'rhosydd.service')
        self._block_on_service_state('rhosydd-mock-backend.service', 'active')
        self._block_on_service_state('rhosydd-speedo-backend.service',
                                     'active')

        self._assert_vehicles_added(['speedo', 'mock'])

        # FIXME: For some reason we end up with some undispatched sources in
        # the main context. Handle those before finishing.
        while self.main_context.iteration(False):
            pass

    def __vehicle_attributes_changed_cb(self, vehicle, current_time,
                                        changed_attributes,
                                        invalidated_attributes):
        self.__vehicle_num_attributes_changed[vehicle] = \
            self.__vehicle_num_attributes_changed.get(vehicle, 0) + 1

    def test_unicast_attributes_changed_signals(self):
        """Test that AttributesChanged signals are sent unicast to clients.

        Create two clients (with separate D-Bus connections), subscribe one
        with a wildcard subscription, and create no subscriptions for the
        other. As the speedo backend emits AttributesChanged every second, one
        connection should receive signals regularly and the other should not.
        If unicast is not working correctly, either neither client will receive
        signals; or both will.
        """
        system_address = Gio.dbus_address_get_for_bus_sync(Gio.BusType.SYSTEM)
        flags = (Gio.DBusConnectionFlags.AUTHENTICATION_CLIENT |
                 Gio.DBusConnectionFlags.MESSAGE_BUS_CONNECTION)
        Gio.DBusConnection.new_for_address(
            address=system_address,
            flags=flags,
            observer=None,
            cancellable=None,
            callback=self._block_on_result_cb,
            user_data=self)
        second_conn = \
            Gio.DBusConnection.new_for_address_finish(self._block_on_result())

        second_manager = self._get_vehicle_manager(second_conn)

        speedo1 = self.vehicle_manager.get_vehicle('speedo')
        speedo2 = second_manager.get_vehicle('speedo')

        speedo1.connect('attributes-changed',
                        self.__vehicle_attributes_changed_cb)
        speedo2.connect('attributes-changed',
                        self.__vehicle_attributes_changed_cb)

        subscriptions2 = [
            Rhosydd.Subscription.new_wildcard(),
        ]

        speedo2.update_subscriptions_async(subscriptions=subscriptions2,
                                           unsubscriptions=[],
                                           cancellable=None,
                                           callback=self._block_on_result_cb,
                                           user_data=self)
        speedo2.update_subscriptions_finish(self._block_on_result())

        while self.__vehicle_num_attributes_changed.get(speedo2, 0) < 2:
            self.main_context.iteration(False)

        self.assertGreaterEqual(
            self.__vehicle_num_attributes_changed.get(speedo2, 0), 2)
        self.assertEqual(
            self.__vehicle_num_attributes_changed.get(speedo1, 0), 0)

        # Unsubscribe before finishing so we don’t get an unbounded number of
        # undispatched sources on cleaning up.
        speedo2.update_subscriptions_async(subscriptions=[],
                                           unsubscriptions=subscriptions2,
                                           cancellable=None,
                                           callback=self._block_on_result_cb,
                                           user_data=self)
        speedo2.update_subscriptions_finish(self._block_on_result())

        # FIXME: For some reason we end up with some undispatched sources in
        # the main context. Handle those before finishing.
        while self.main_context.iteration(False):
            pass


if __name__ == '__main__':
    unittest.main()
