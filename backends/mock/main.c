/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <locale.h>
#include <signal.h>

#include "libcroesor/backend.h"
#include "libcroesor/static-vehicle.h"
#include "libcroesor/vehicle-manager.h"
#include "libcroesor/vehicle-service.h"


/* Attribute data to expose. All attributes are in zone %RSD_ZONE_ROOT and have
 * last_updated timestamp of %RSD_TIMESTAMP_UNKNOWN. */
typedef struct
{
  const gchar *name;
  const gchar *type;
  const gchar *value;  /* in GVariant text format */
  gdouble      accuracy;
} AttributeInfo;

/* NOTE: The interfaces from the following specification sections are missing
 * from this array because it already covers a reasonable variety of attributes
 * already. The given values are not necessarily realistic.
 *  • https://www.w3.org/2014/automotive/data_spec.html#maintenance-interfaces
 *  • https://www.w3.org/2014/automotive/data_spec.html#personalization-interfaces
 *  • https://www.w3.org/2014/automotive/data_spec.html#drivingsafety-interfaces
 *  • https://www.w3.org/2014/automotive/data_spec.html#Climate-interfaces
 *  • https://www.w3.org/2014/automotive/data_spec.html#vision-parking-interfaces
 */
static const AttributeInfo mock_vehicle_attributes[] =
{
  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Identification */
  { "identification.VIN", "s", "'Example VIN'", 0.0 },
  { "identification.WMI", "s", "'WMI'", 0.0 },
  { "identification.vehicleType", "s", "'passengerCarMini'", 0.0 },
  { "identification.brand", "s", "'Apertis'", 0.0 },
  { "identification.model", "s", "'Rhosydd'", 0.0 },
  { "identification.year", "q", "2016", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-SizeConfiguration */
  { "sizeConfiguration.width", "q", "2000" /* mm */, 20.0 },
  { "sizeConfiguration.height", "q", "1500" /* mm */, 20.0 },
  { "sizeConfiguration.length", "q", "2300" /* mm */, 20.0 },
  { "sizeConfiguration.doorsCount", "aq", "[2, 2, 1]", 0.0 },
  { "sizeConfiguration.totalDoors", "q", "5", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-FuelConfiguration */
  { "fuelConfiguration.fuelType", "as", "['gasoline', 'electric']", 0.0 },
  { "fuelConfiguration.refuelPosition", "s", "'/'", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-TransmissionConfiguration */
  { "transmissionConfiguration.transmissionGearType", "s", "'manual'", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-WheelConfiguration */
  { "wheelConfiguration.wheelRadius", "q", "500" /* mm */, 10.0 },
  { "wheelConfiguration.zone", "s", "'/'", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-SteeringWheelConfiguration */
  { "steeringWheelConfiguration.steeringWheelLeft", "b", "false", 0.0 },
  { "steeringWheelConfiguration.steeringWheelTelescopingPosition", "q",
    "50" /* % */, 1.0 },
  { "steeringWheelConfiguration.steeringWheelPositionTilt", "q",
    "50" /* % */, 1.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Acceleration */
  { "acceleration.x", "x", "1" /* cm/s^2 */, 0.01 },
  { "acceleration.y", "x", "0" /* cm/s^2 */, 0.01 },
  { "acceleration.z", "x", "0" /* cm/s^2 */, 0.01 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-AcceleratorPedalPosition */
  { "acceleratorPedalPosition.value", "q", "20" /* % */, 1.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-BrakeOperation */
  { "brakeOperation.brakePedalDepressed", "b", "true", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-ButtonEvent */
  { "buttonEvent.button", "a(ss)", "[]", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Chime */
  { "chime.status", "b", "false", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-CruiseControlStatus */
  { "cruiseControlStatus.status", "b", "false", 0.0 },
  { "cruiseControlStatus.speed", "q", "50" /* km/h */, 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-DrivingMode */
  { "drivingMode.mode", "b", "true", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-EngineCoolant */
  { "engineCoolant.level", "y", "98" /* % */, 1.0 },
  { "engineCoolant.temperature", "n", "45" /* C */, 1.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-EngineOil */
  { "engineOil.level", "q", "89" /* % */, 1.0 },
  { "engineOil.lifeRemaining", "q", "72" /* % */, 1.0 },
  { "engineOil.temperature", "x", "67" /* C */, 1.0 },
  { "engineOil.pressure", "q", "3" /* kPa */, 0.1 },
  { "engineOil.change", "b", "false", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-EngineSpeed */
  { "engineSpeed.speed", "t", "60000" /* rpm */, 10.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Fuel */
  { "fuel.level", "q", "68" /* % */, 1.0 },
  { "fuel.range", "t", "200000" /* m */, 7000.0 },
  { "fuel.instantConsumption", "t", "6" /* ml/100km */, 0.1 },
  { "fuel.averageConsumption", "t", "4" /* ml/100km */, 0.1 },
  { "fuel.fuelConsumedSinceRestart", "t", "4" /* ml/100km */, 0.1 },
  { "fuel.timeSinceRestart", "t", "7334" /* s */, 1.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Horn */
  { "horn.status", "b", "false", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-IgnitionTime */
  { "ignitionTime.ignitionOnTime", "t", "166" /* µs */, 5.0 },
  { "ignitionTime.ignitionOffTime", "t", "201" /* µs */, 5.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-InteriorLightStatus */
  { "interiorLightStatus.status", "b", "true", 0.0 },
  /* NOTE: Same as wheelSpeed.value — this basically asserts that there’s only
   * one interior light in the car. */

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-LightStatus */
  { "lightStatus.head", "b", "true", 0.0 },
  { "lightStatus.rightTurn", "b", "false", 0.0 },
  { "lightStatus.leftTurn", "b", "false", 0.0 },
  { "lightStatus.brake", "b", "false", 0.0 },
  { "lightStatus.fog", "b", "false", 0.0 },
  { "lightStatus.hazard", "b", "false", 0.0 },
  { "lightStatus.parking", "b", "true", 0.0 },
  { "lightStatus.highBeam", "b", "false", 0.0 },
  { "lightStatus.automaticHeadlights", "b", "true", 0.0 },
  { "lightStatus.dynamicHighBeam", "b", "false", 0.0 },
  /* NOTE: Same as wheelSpeed.value — this basically asserts that there’s only
   * one exterior light cluster on the car, rather than (at least) four. */

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-NightMode */
  { "nightMode.mode", "b", "false"  /* day time */, 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-PowerTrainTorque */
  { "powerTrainTorque.value", "q", "100" /* Nm */, 5.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-SteeringWheel */
  { "steeringWheel.angle", "n", "-5" /* degrees */, 1.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-ThrottlePosition */
  { "throttlePosition.value", "q", "80" /* % */, 1.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Transmission */
  { "transmission.gear", "y", "3", 0.0 },
  { "transmission.mode", "s", "'drive'", 0.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-TripMeters */
  { "trip.meters.distance", "at", "[1235, 763, 11064]" /* m */, 20.0 },
  { "trip.meters.averageSpeed", "aq", "[50, 35, 71]" /* km/h */, 1.0 },
  { "trip.meters.fuelConsumption", "aq", "[5, 6, 4]" /* ml/100km */, 0.1 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-VehicleSpeed */
  { "vehicleSpeed.speed", "q", "50000" /* m/h */, 20.0 },

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-WheelSpeed */
  { "wheelSpeed.value", "q", "50000" /* m/h */, 20.0 },
  /* NOTE: This basically asserts that the vehicle only has one wheel, which is
   * in the root zone. In a realistic vehicle, there would be four
   * wheelSpeed.value attributes, one in each of four zones representing the
   * quadrants of the vehicle. */

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-WheelTick */
  { "wheelTick.value", "t", "15", 0.2 },
  /* NOTE: Same as wheelSpeed.value. */

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-YawRate */
  { "yawRate.value", "q", "1" /* degrees per second */, 0.5 },
};

static GPtrArray/*<owned RsdAttributeInfo>*/ *
attributes_to_array (const AttributeInfo *attributes,
                     gsize                n_elements)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) out = NULL;
  gsize i;

  out = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

  for (i = 0; i < n_elements; i++)
    {
      const AttributeInfo *info = &attributes[i];
      RsdAttribute attribute = { NULL, };
      RsdAttributeMetadata metadata = { 0, };
      GError *error = NULL;

      attribute.value = g_variant_parse (G_VARIANT_TYPE (info->type),
                                        info->value, NULL, NULL, &error);
      g_assert_no_error (error);
      g_assert (attribute.value != NULL);

      attribute.accuracy = info->accuracy;

      metadata.zone_path = g_intern_string ("/");
      metadata.name = g_intern_string (info->name);
      metadata.availability = RSD_ATTRIBUTE_AVAILABLE;
      metadata.flags = RSD_ATTRIBUTE_READABLE;

      g_ptr_array_add (out, rsd_attribute_info_new (&attribute, &metadata));

      g_variant_unref (attribute.value);
    }

  return g_steal_pointer (&out);
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr (GError) error = NULL;
  CsrVehicleManager *vehicle_manager;
  g_autoptr (CsrStaticVehicle) vehicle = NULL;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) added = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) props = NULL;
  g_autoptr (CsrBackend) backend = NULL;

  /* Localisation */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* Create the backend. */
  backend = csr_backend_new ("org.apertis.Rhosydd1.Backends.Mock",
                             "/org/apertis/Rhosydd1/Backends/Mock",
                             GETTEXT_PACKAGE,
                             _("Export a mock vehicle object for use by the "
                               "vehicle device daemon."));
  vehicle_manager = csr_backend_get_vehicle_manager (backend);

  /* Add an example vehicle. */
  props = attributes_to_array (mock_vehicle_attributes,
                               G_N_ELEMENTS (mock_vehicle_attributes));
  vehicle = csr_static_vehicle_new ("mock", props);

  added = g_ptr_array_new ();
  g_ptr_array_add (added, vehicle);
  csr_vehicle_manager_update_vehicles (vehicle_manager, added, NULL);

  /* Run until we are killed. */
  csr_service_run (CSR_SERVICE (backend), argc, argv, &error);

  if (error != NULL)
    {
      int code;

      if (g_error_matches (error,
                           CSR_SERVICE_ERROR, CSR_SERVICE_ERROR_SIGNALLED))
        raise (csr_service_get_exit_signal (CSR_SERVICE (backend)));

      g_printerr ("%s: %s\n", argv[0], error->message);
      code = error->code;

      return code;
    }

  return 0;
}
