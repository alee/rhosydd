/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include <math.h>

#include "libinternal/timestamped-pointer.h"
#include "librhosydd/static-zone.h"
#include "librhosydd/subscription-manager.h"
#include "librhosydd/types.h"
#include "librhosydd/vehicle.h"
#include "librhosydd/zone.h"
#include "vehicle.h"


static void speedo_vehicle_vehicle_init (RsdVehicleInterface *iface);

static void speedo_vehicle_dispose      (GObject             *object);

static void speedo_vehicle_get_property (GObject             *object,
                                         guint                property_id,
                                         GValue              *value,
                                         GParamSpec          *pspec);
static void speedo_vehicle_set_property (GObject             *object,
                                         guint                property_id,
                                         const GValue        *value,
                                         GParamSpec          *pspec);

static const gchar         *speedo_vehicle_vehicle_get_id                    (RsdVehicle          *vehicle);
static GPtrArray           *speedo_vehicle_vehicle_get_zones                 (RsdVehicle          *vehicle);
static void                 speedo_vehicle_vehicle_get_attribute_async        (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              const gchar         *attribute_name,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static RsdAttributeInfo     *speedo_vehicle_vehicle_get_attribute_finish     (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);

static void                 speedo_vehicle_vehicle_get_metadata_async        (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              const gchar         *attribute_name,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static RsdAttributeMetadata *speedo_vehicle_vehicle_get_metadata_finish       (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                               RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);

static void                 speedo_vehicle_vehicle_get_all_attributes_async  (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static GPtrArray           *speedo_vehicle_vehicle_get_all_attributes_finish (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);
static void                 speedo_vehicle_vehicle_get_all_metadata_async    (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static GPtrArray           *speedo_vehicle_vehicle_get_all_metadata_finish   (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);

static void                 speedo_vehicle_vehicle_update_subscriptions_async  (RsdVehicle          *vehicle,
                                                                                GPtrArray           *subscriptions,
                                                                                GPtrArray           *unsubscriptions,
                                                                                GCancellable        *cancellable,
                                                                                GAsyncReadyCallback  callback,
                                                                                gpointer             user_data);
static void                 speedo_vehicle_vehicle_update_subscriptions_finish (RsdVehicle          *vehicle,
                                                                                GAsyncResult        *result,
                                                                                GError             **error);

/**
 * SpeedoVehicle:
 *
 * An implementation of #RsdVehicle which simulates the continual acceleration
 * and deceleration of a car, in steps triggered by calls to
 * speedo_vehicle_update().
 *
 * This is meant as a demonstration, mock backend, **not for production use**.
 *
 * The vehicle has the constant ID of `speedo`. The acceleration and
 * deceleration vary constantly but deterministically with time.
 *
 * Since: 0.2.0
 */
struct _SpeedoVehicle
{
  GObject parent;

  gchar *id;  /* owned */
  RsdSubscriptionManager *subscriptions;  /* owned */

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-Acceleration */
  struct
  {
    RsdAttribute x;  /* cm/s^2 */
    RsdAttribute y;  /* cm/s^2 */
    RsdAttribute z;  /* cm/s^2 */
  } acceleration;

  /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-VehicleSpeed */
  RsdAttribute speed;  /* m/h */

  /* One wheel per quadrant of the vehicle. */
  struct
  {
    /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-WheelSpeed */
    RsdAttribute speed;  /* m/h */

    /* https://www.w3.org/2014/automotive/data_spec.html#idl-def-WheelTick */
    RsdAttribute tick;  /* ticks/s */
  } wheels[4];
};

typedef enum
{
  PROP_ID = 1,
} SpeedoVehicleProperty;

G_DEFINE_TYPE_WITH_CODE (SpeedoVehicle, speedo_vehicle, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RSD_TYPE_VEHICLE,
                                                speedo_vehicle_vehicle_init))

static void
speedo_vehicle_class_init (SpeedoVehicleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = speedo_vehicle_get_property;
  object_class->set_property = speedo_vehicle_set_property;
  object_class->dispose = speedo_vehicle_dispose;

  g_object_class_override_property (object_class, PROP_ID, "id");
}

static void
speedo_vehicle_vehicle_init (RsdVehicleInterface *iface)
{
  iface->get_id = speedo_vehicle_vehicle_get_id;
  iface->get_zones = speedo_vehicle_vehicle_get_zones;
  iface->get_attribute_async = speedo_vehicle_vehicle_get_attribute_async;
  iface->get_attribute_finish = speedo_vehicle_vehicle_get_attribute_finish;
  iface->get_metadata_async = speedo_vehicle_vehicle_get_metadata_async;
  iface->get_metadata_finish = speedo_vehicle_vehicle_get_metadata_finish;
  iface->get_all_attributes_async = speedo_vehicle_vehicle_get_all_attributes_async;
  iface->get_all_attributes_finish = speedo_vehicle_vehicle_get_all_attributes_finish;
  iface->get_all_metadata_async = speedo_vehicle_vehicle_get_all_metadata_async;
  iface->get_all_metadata_finish = speedo_vehicle_vehicle_get_all_metadata_finish;
  iface->update_subscriptions_async = speedo_vehicle_vehicle_update_subscriptions_async;
  iface->update_subscriptions_finish = speedo_vehicle_vehicle_update_subscriptions_finish;
}

static void
set_attribute (RsdAttribute *attribute,
               GVariant     *value,
               gdouble       accuracy,
               RsdTimestampMicroseconds  timestamp)
{
  g_clear_pointer (&attribute->value, g_variant_unref);
  attribute->value = g_variant_ref_sink (value);
  attribute->accuracy = accuracy;
  attribute->last_updated = timestamp;
}

static void
speedo_vehicle_init (SpeedoVehicle *self)
{
  gsize i;

  /* Create a subscription manager. */
  self->subscriptions = rsd_subscription_manager_new ();

  /* Set up the initial simulation values. */
  set_attribute (&self->acceleration.x, g_variant_new_int64 (0),
                 0.01, RSD_TIMESTAMP_UNKNOWN);
  set_attribute (&self->acceleration.y, g_variant_new_int64 (0),
                 0.01, RSD_TIMESTAMP_UNKNOWN);
  set_attribute (&self->acceleration.z, g_variant_new_int64 (0),
                 0.01, RSD_TIMESTAMP_UNKNOWN);

  set_attribute (&self->speed, g_variant_new_uint16 (0),
                 20.0, RSD_TIMESTAMP_UNKNOWN);

  for (i = 0; i < G_N_ELEMENTS (self->wheels); i++)
    {
      set_attribute (&self->wheels[i].speed, g_variant_new_uint16 (0),
                     20.0, RSD_TIMESTAMP_UNKNOWN);
      set_attribute (&self->wheels[i].tick, g_variant_new_uint64 (0),
                     0.2, RSD_TIMESTAMP_UNKNOWN);
    }
}

static void
speedo_vehicle_dispose (GObject *object)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (object);

  g_clear_object (&self->subscriptions);
  g_clear_pointer (&self->id, g_free);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (speedo_vehicle_parent_class)->dispose (object);
}

static void
speedo_vehicle_get_property (GObject    *object,
                             guint       property_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (object);

  switch ((SpeedoVehicleProperty) property_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
speedo_vehicle_set_property (GObject      *object,
                             guint         property_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (object);

  switch ((SpeedoVehicleProperty) property_id)
    {
    case PROP_ID:
      /* Construct only. */
      g_assert (self->id == NULL);
      g_assert (rsd_vehicle_id_is_valid (g_value_get_string (value)));
      self->id = g_value_dup_string (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

static const gchar *
speedo_vehicle_vehicle_get_id (RsdVehicle *vehicle)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);

  return self->id;
}

static const gchar *zone_paths[] = {
  "/front,left/",
  "/front,right/",
  "/rear,right/",
  "/left,rear/",
};

static GPtrArray *
speedo_vehicle_vehicle_get_zones (RsdVehicle *vehicle)
{
  g_autoptr (GPtrArray/*<unowned RsdZone>*/) retval = NULL;
  gsize i;

  retval = g_ptr_array_new_with_free_func (g_object_unref);
  g_ptr_array_add (retval, rsd_static_zone_new ("/"));

  for (i = 0; i < G_N_ELEMENTS (zone_paths); i++)
    g_ptr_array_add (retval, rsd_static_zone_new (zone_paths[i]));

  return g_steal_pointer (&retval);
}

/* Convert a zone path into an index into the wheels array; or -1 if it’s not
 * a path for a wheel. */
static gint
zone_to_wheel_index (RsdZone *zone)
{
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (zone_paths); i++)
    {
      if (g_strcmp0 (rsd_zone_get_path (zone), zone_paths[i]) == 0)
        return i;
    }

   return -1;
}

static RsdZone *
wheel_index_to_zone (guint wheel_index)
{
  if (wheel_index >= G_N_ELEMENTS (zone_paths))
    return NULL;

  return RSD_ZONE (rsd_static_zone_new (zone_paths[wheel_index]));
}

/* Find the RsdAttribute struct for a given @attribute_name and @zone. */
static RsdAttribute *
zone_and_attribute_name_to_attribute (SpeedoVehicle  *self,
                                      RsdZone        *zone,
                                      const gchar    *attribute_name,
                                      GError        **error)
{
  gint wheel_index;

  wheel_index = zone_to_wheel_index (zone);

  if (rsd_zone_is_root (zone))
    {
      if (g_strcmp0 (attribute_name, "acceleration.x") == 0)
        return &self->acceleration.x;
      else if (g_strcmp0 (attribute_name, "acceleration.y") == 0)
        return &self->acceleration.y;
      else if (g_strcmp0 (attribute_name, "acceleration.z") == 0)
        return &self->acceleration.z;
      else if (g_strcmp0 (attribute_name, "vehicleSpeed.speed") == 0)
        return &self->speed;
    }
  else if (wheel_index != -1)
    {
      if (g_strcmp0 (attribute_name, "wheelSpeed.value") == 0)
        return &self->wheels[wheel_index].speed;
      else if (g_strcmp0 (attribute_name, "wheelTick.value") == 0)
        return &self->wheels[wheel_index].tick;
    }
  else
    {
      g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                   _("Zone ‘%s’ is not known."), rsd_zone_get_path (zone));
      return NULL;
    }

  /* No attribute found. */
  g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
               _("Attribute ‘%s’ is not known."), attribute_name);
  return NULL;
}

/* (transfer full) */
static RsdAttributeInfo *
zone_and_attribute_name_to_attribute_info (SpeedoVehicle  *self,
                                           RsdZone        *zone,
                                           const gchar    *attribute_name,
                                           GError        **error)
{
  RsdAttribute *attribute;
  g_autoptr (RsdAttributeMetadata) metadata = NULL;

  attribute = zone_and_attribute_name_to_attribute (self, zone, attribute_name,
                                                    error);
  if (attribute == NULL)
    return NULL;

  /* Build the attribute info. */
  metadata = rsd_attribute_metadata_new (rsd_zone_get_path (zone),
                                         attribute_name,
                                         RSD_ATTRIBUTE_AVAILABLE,
                                         RSD_ATTRIBUTE_READABLE);
  return rsd_attribute_info_new (attribute, metadata);
}

static void
speedo_vehicle_vehicle_get_attribute_async (RsdVehicle          *vehicle,
                                            RsdZone             *zone,
                                            const gchar         *attribute_name,
                                            GCancellable        *cancellable,
                                            GAsyncReadyCallback  callback,
                                            gpointer             user_data)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (RsdAttributeInfo) info = NULL;
  GError *error = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task, speedo_vehicle_vehicle_get_attribute_async);

  info = zone_and_attribute_name_to_attribute_info (self, zone, attribute_name,
                                                    &error);

  if (info == NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  timestamped_pointer_g_task_return (task, g_steal_pointer (&info),
                                     (GDestroyNotify) rsd_attribute_info_free,
                                     self->speed.last_updated);
}

static RsdAttributeInfo *
speedo_vehicle_vehicle_get_attribute_finish (RsdVehicle                *vehicle,
                                             GAsyncResult              *result,
                                             RsdTimestampMicroseconds  *current_time,
                                             GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
speedo_vehicle_vehicle_get_metadata_async (RsdVehicle          *vehicle,
                                           RsdZone             *zone,
                                           const gchar         *attribute_name,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  RsdAttribute *attribute = NULL;
  g_autoptr (RsdAttributeMetadata) metadata = NULL;
  GError *error = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task, speedo_vehicle_vehicle_get_metadata_async);

  attribute = zone_and_attribute_name_to_attribute (self, zone, attribute_name,
                                                    &error);

  if (attribute == NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Build the attribute info. */
  metadata = rsd_attribute_metadata_new (rsd_zone_get_path (zone),
                                         attribute_name,
                                         RSD_ATTRIBUTE_AVAILABLE,
                                         RSD_ATTRIBUTE_READABLE);
  timestamped_pointer_g_task_return (task, g_steal_pointer (&metadata),
                                     (GDestroyNotify) rsd_attribute_metadata_free,
                                     self->speed.last_updated);
}

static RsdAttributeMetadata *
speedo_vehicle_vehicle_get_metadata_finish (RsdVehicle                *vehicle,
                                            GAsyncResult              *result,
                                            RsdTimestampMicroseconds  *current_time,
                                            GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
speedo_vehicle_vehicle_get_all_attributes_async (RsdVehicle          *vehicle,
                                                 RsdZone             *zone,
                                                 GCancellable        *cancellable,
                                                 GAsyncReadyCallback  callback,
                                                 gpointer             user_data)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  gsize i;
  gint wheel_index;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task, speedo_vehicle_vehicle_get_all_attributes_async);

  /* Grab all attributes. */
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

#define ADD_ATTRIBUTE(z, p) \
    g_ptr_array_add (attributes, \
                     zone_and_attribute_name_to_attribute_info (self, (z), \
                                                                (p), NULL));

  wheel_index = zone_to_wheel_index (zone);

  if (rsd_zone_is_root (zone))
    {
      ADD_ATTRIBUTE (zone, "acceleration.x")
      ADD_ATTRIBUTE (zone, "acceleration.y")
      ADD_ATTRIBUTE (zone, "acceleration.z")
      ADD_ATTRIBUTE (zone, "vehicleSpeed.speed")
    }
  else if (wheel_index == -1)
    {
      g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                               _("Zone ‘%s’ is not known."),
                               rsd_zone_get_path (zone));
      return;
    }

  for (i = 0; i < G_N_ELEMENTS (self->wheels); i++)
    {
      g_autoptr (RsdZone) wheel_zone = NULL;

      if (wheel_index != -1 && i != (gsize) wheel_index)
        continue;

      wheel_zone = wheel_index_to_zone (i);

      ADD_ATTRIBUTE (wheel_zone, "wheelSpeed.value");
      ADD_ATTRIBUTE (wheel_zone, "wheelTick.value");
    }

#undef ADD_ATTRIBUTE

  timestamped_pointer_g_task_return (task, g_steal_pointer (&attributes),
                                     (GDestroyNotify) g_ptr_array_unref,
                                     self->speed.last_updated);
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
speedo_vehicle_vehicle_get_all_attributes_finish (RsdVehicle                *vehicle,
                                                  GAsyncResult              *result,
                                                  RsdTimestampMicroseconds  *current_time,
                                                  GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
speedo_vehicle_vehicle_get_all_metadata_async (RsdVehicle          *vehicle,
                                               RsdZone             *zone,
                                               GCancellable        *cancellable,
                                               GAsyncReadyCallback  callback,
                                               gpointer             user_data)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) attributes = NULL;
  gsize i;
  gint wheel_index;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         speedo_vehicle_vehicle_get_all_metadata_async);

  /* Grab all metadata. */
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

#define ADD_ATTRIBUTE(z, p) \
    g_ptr_array_add (attributes, \
                     rsd_attribute_metadata_new (rsd_zone_get_path (z), p, \
                                                 RSD_ATTRIBUTE_AVAILABLE, \
                                                 RSD_ATTRIBUTE_READABLE));

  wheel_index = zone_to_wheel_index (zone);

  if (rsd_zone_is_root (zone))
    {
      ADD_ATTRIBUTE (zone, "acceleration.x")
      ADD_ATTRIBUTE (zone, "acceleration.y")
      ADD_ATTRIBUTE (zone, "acceleration.z")
      ADD_ATTRIBUTE (zone, "vehicleSpeed.speed")
    }
  else if (wheel_index == -1)
    {
      g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                               _("Zone ‘%s’ is not known."),
                               rsd_zone_get_path (zone));
      return;
    }

  for (i = 0; i < G_N_ELEMENTS (self->wheels); i++)
    {
      g_autoptr (RsdZone) wheel_zone = NULL;

      if (wheel_index != -1 && i != (gsize) wheel_index)
        continue;

      wheel_zone = wheel_index_to_zone (i);

      ADD_ATTRIBUTE (wheel_zone, "wheelSpeed.value");
      ADD_ATTRIBUTE (wheel_zone, "wheelTick.value");
    }

#undef ADD_ATTRIBUTE

  timestamped_pointer_g_task_return (task, g_steal_pointer (&attributes),
                                     (GDestroyNotify) g_ptr_array_unref,
                                     self->speed.last_updated);
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
speedo_vehicle_vehicle_get_all_metadata_finish (RsdVehicle                *vehicle,
                                                GAsyncResult              *result,
                                                RsdTimestampMicroseconds  *current_time,
                                                GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void
speedo_vehicle_vehicle_update_subscriptions_async (RsdVehicle          *vehicle,
                                                   GPtrArray           *subscriptions,
                                                   GPtrArray           *unsubscriptions,
                                                   GCancellable        *cancellable,
                                                   GAsyncReadyCallback  callback,
                                                   gpointer             user_data)
{
  SpeedoVehicle *self = SPEEDO_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         speedo_vehicle_vehicle_update_subscriptions_async);

  rsd_subscription_manager_update_subscriptions (self->subscriptions,
                                                 subscriptions,
                                                 unsubscriptions);

  g_task_return_boolean (task, TRUE);
}

static void
speedo_vehicle_vehicle_update_subscriptions_finish (RsdVehicle    *vehicle,
                                                    GAsyncResult  *result,
                                                    GError       **error)
{
  GTask *task = G_TASK (result);

  g_task_propagate_boolean (task, error);
}

/**
 * speedo_vehicle_new:
 *
 * Create a new #SpeedoVehicle. Its ID is `speedo`.
 *
 * Returns: (transfer full): a new #SpeedoVehicle
 * Since: 0.2.0
 */
SpeedoVehicle *
speedo_vehicle_new (void)
{
  return g_object_new (SPEEDO_TYPE_VEHICLE,
                       "id", "speedo",
                       NULL);
}

/**
 * speedo_vehicle_update:
 * @self: a #SpeedoVehicle
 * @timestamp: simulation timestamp, in microseconds since some system-specific
 *     epoch
 *
 * Update the simulation state, which means updating the vehicle’s attributes
 * and emitting a #RsdVehicle::attributes-changed notification about them.
 *
 * The @timestamp should typically come from g_get_real_time(), but can come
 * from any other clock source in microseconds.
 *
 * Since: 0.2.0
 */
void
speedo_vehicle_update (SpeedoVehicle *self,
                       RsdTimestampMicroseconds   timestamp)
{
  gsize i;
  gint64 old_acceleration_x, new_acceleration_x;  /* cm/s^2 */
  gint64 old_acceleration_y, new_acceleration_y;  /* cm/s^2 */
  gint64 old_acceleration_scalar;  /* cm/s^2 */
  guint16 old_speed, new_speed;  /* m/h */
  guint64 period;  /* µs */
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) changed_attributes = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) invalidated_attributes = NULL;
  g_autoptr (RsdZone) root_zone = NULL;

  const gint64 acceleration_scale = 400;  /* cm/s^2 */

  /* Ignore the update if we’re going backwards in time. */
  if (timestamp < self->speed.last_updated)
    return;

  /* Work out the resultant of the old acceleration components. */
  old_acceleration_x = g_variant_get_int64 (self->acceleration.x.value);
  old_acceleration_y = g_variant_get_int64 (self->acceleration.y.value);

  old_acceleration_scalar = sqrt (old_acceleration_x * old_acceleration_x +
                                  old_acceleration_y * old_acceleration_y);

  /* Update the speed given the time passed since the last update, and the
   * previous acceleration values. */
  period = timestamp - self->speed.last_updated;

  old_speed = g_variant_get_uint16 (self->speed.value);
  new_speed = old_speed +
              period / 1000000 *  /* convert from µs to s */
              old_acceleration_scalar / 100 * 60 * 60 * 60 * 60;  /* convert from cm/s^2 to m/h^2 */

  set_attribute (&self->speed, g_variant_new_uint16 (new_speed),
                 self->speed.accuracy, timestamp);

  /* Model the acceleration as a sine wave in the X and Y axes, and zero in the
   * Z (vertical) axis. The average rate of acceleration of a car is
   * 3–4m/s^2; these measurements are in cm/s^2. */
  new_acceleration_x = sin (timestamp) * acceleration_scale;
  new_acceleration_y = cos (timestamp) * acceleration_scale;

  set_attribute (&self->acceleration.x,
                 g_variant_new_int64 (new_acceleration_x),
                 self->acceleration.x.accuracy, timestamp);
  set_attribute (&self->acceleration.y,
                 g_variant_new_int64 (new_acceleration_y),
                 self->acceleration.y.accuracy, timestamp);

  self->acceleration.z.last_updated = timestamp;

  /* Update the wheel speeds as the actual vehicle speed plus some noise. */
  for (i = 0; i < G_N_ELEMENTS (self->wheels); i++)
    {
      guint16 new_wheel_speed;  /* m/h */
      guint64 new_wheel_ticks;  /* ticks/s */
      guint32 noise;  /* m/h */

      const gdouble wheel_circumference = 2.3114;  /* m */

      /* Measurement noise is typically Gaussian, but for the purposes of making
       * this mock backend deterministic, let’s base it on the timestamp. */
      noise = timestamp % 10;

      new_wheel_speed = new_speed + noise;
      new_wheel_ticks = new_wheel_speed /
                        wheel_circumference / 60 / 60;  /* convert from m/h to ticks/s */

      set_attribute (&self->wheels[i].speed,
                     g_variant_new_uint16 (new_wheel_speed),
                     self->wheels[i].speed.accuracy, timestamp);
      set_attribute (&self->wheels[i].tick,
                     g_variant_new_uint64 (new_wheel_ticks),
                     self->wheels[i].tick.accuracy, timestamp);
    }

  /* Emit an update signal. */
  changed_attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  invalidated_attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);

#define ADD_ATTRIBUTE(z, p) \
  g_ptr_array_add (changed_attributes, \
                   zone_and_attribute_name_to_attribute_info (self, (z), \
                                                              (p), NULL));

  root_zone = RSD_ZONE (rsd_static_zone_new ("/"));

  ADD_ATTRIBUTE (root_zone, "acceleration.x")
  ADD_ATTRIBUTE (root_zone, "acceleration.y")
  ADD_ATTRIBUTE (root_zone, "acceleration.z")
  ADD_ATTRIBUTE (root_zone, "vehicleSpeed.speed")

  for (i = 0; i < G_N_ELEMENTS (self->wheels); i++)
    {
      g_autoptr (RsdZone) zone = NULL;

      zone = wheel_index_to_zone (i);

      ADD_ATTRIBUTE (zone, "wheelSpeed.value");
      ADD_ATTRIBUTE (zone, "wheelTick.value");
    }

#undef ADD_ATTRIBUTE

  if (rsd_subscription_manager_is_subscribed (self->subscriptions,
                                              changed_attributes,
                                              invalidated_attributes,
                                              g_get_monotonic_time ()))
    g_signal_emit_by_name (self, "attributes-changed", timestamp,
                           changed_attributes,
                           invalidated_attributes);
}
