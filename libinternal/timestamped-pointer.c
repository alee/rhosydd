/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include "librhosydd/types.h"
#include "timestamped-pointer.h"


/**
 * timestamped_pointer_new:
 * @data: (transfer full) (nullable): pointer to store
 * @destroy_notify: (nullable): function to free @data with once it is no longer
 *    needed
 * @timestamp: timestamp to associate with the pointer (in microseconds, in
 *    no specific clock domain)
 *
 * Create a new #TimestampedPointer which associates @timestamp with @data.
 *
 * Returns: (transfer full): a new #TimestampedPointer
 * Since: 0.4.0
 */
TimestampedPointer *
timestamped_pointer_new (gpointer                 data,
                         GDestroyNotify           destroy_notify,
                         RsdTimestampMicroseconds timestamp)
{
  g_autoptr (TimestampedPointer) out = NULL;

  out = g_new0 (TimestampedPointer, 1);
  out->data = data;
  out->destroy_notify = destroy_notify;
  out->timestamp = timestamp;

  return g_steal_pointer (&out);
}

/**
 * timestamped_pointer_free:
 * @data: (transfer full): timestamped pointer to free
 *
 * Free the given timestamped pointer. This will call the @destroy_notify
 * function passed at construction time on the data pointer, if both of them are
 * non-%NULL.
 *
 * Since: 0.4.0
 */
void
timestamped_pointer_free (TimestampedPointer *data)
{
  if (data->destroy_notify != NULL && data->data != NULL)
    data->destroy_notify (data->data);

  g_free (data);
}

/**
 * timestamped_pointer_g_task_return:
 * @task: a #GTask
 * @data: (transfer full) (nullable): pointer to return
 * @destroy_notify: (nullable): function to free @data with once it is no longer
 *    needed
 * @timestamp: timestamp to return as associated with the pointer (in
 *    microseconds, in no specific clock domain)
 *
 * Convenience function which essentially calls g_task_return_pointer() on a
 * new #TimestampedPointer created with the given parameters.
 *
 * Use in conjunction with timestamped_pointer_g_task_propagate() to return a
 * timestamp and pointer via a #GTask.
 *
 * Since: 0.4.0
 */
void
timestamped_pointer_g_task_return (GTask                    *task,
                                   gpointer                  data,
                                   GDestroyNotify            destroy_notify,
                                   RsdTimestampMicroseconds  timestamp)
{
  g_autoptr (TimestampedPointer) timestamped = NULL;

  timestamped = timestamped_pointer_new (data, destroy_notify, timestamp);
  g_task_return_pointer (task, g_steal_pointer (&timestamped),
                         (GDestroyNotify) timestamped_pointer_free);
}

/**
 * timestamped_pointer_g_task_propagate:
 * @task: a #GTask
 * @timestamp: (out caller-allocates) (optional): return location for the
 *    propagated timestamp, or %NULL to not return it
 * @error: return location for a #GError, or %NULL
 *
 * Convenience function which essentially calls g_task_propagate_pointer() on
 * the pointer in a #TimestampedPointer returned in a #GTask using
 * timestamped_pointer_g_task_return(). It returns the pointer and the timestamp
 * separately.
 *
 * Returns: (transfer full): propagated pointer
 * Since: 0.4.0
 */
gpointer
timestamped_pointer_g_task_propagate (GTask                     *task,
                                      RsdTimestampMicroseconds  *timestamp,
                                      GError                   **error)
{
  g_autoptr (TimestampedPointer) data = NULL;

  data = g_task_propagate_pointer (task, error);

  if (data == NULL)
    return NULL;

  if (timestamp != NULL)
    *timestamp = data->timestamp;

  return g_steal_pointer (&data->data);
}
