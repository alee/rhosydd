/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef INTERNAL_TIMESTAMPED_POINTER_H
#define INTERNAL_TIMESTAMPED_POINTER_H

#include <glib.h>
#include <gio/gio.h>

#include <librhosydd/types.h>

G_BEGIN_DECLS

/**
 * TimestampedPointer:
 * @timestamp: timestamp associated with the pointer, in microseconds and in no
 *    specific clock domain
 * @data: (nullable): a stored pointer
 * @destroy_notify: (nullable): function to free @data, or %NULL
 *
 * A small data structure to associate a timestamp with a pointer. Typically
 * used with timestamped_pointer_g_task_return() and
 * timestamped_pointer_g_task_propagate() as a closure to bind vehicle
 * attributes and the timestamps they were retrieved.
 *
 * Since: 0.4.0
 */
typedef struct
{
  RsdTimestampMicroseconds timestamp;
  gpointer data;
  GDestroyNotify destroy_notify;
} TimestampedPointer;

TimestampedPointer *timestamped_pointer_new              (gpointer                   data,
                                                          GDestroyNotify             destroy_notify,
                                                          RsdTimestampMicroseconds   timestamp);
void                timestamped_pointer_free             (TimestampedPointer        *data);
void                timestamped_pointer_g_task_return    (GTask                     *task,
                                                          gpointer                   data,
                                                          GDestroyNotify             destroy_notify,
                                                          RsdTimestampMicroseconds   timestamp);
gpointer            timestamped_pointer_g_task_propagate (GTask                     *task,
                                                          RsdTimestampMicroseconds  *timestamp,
                                                          GError                   **error);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (TimestampedPointer, timestamped_pointer_free);

G_END_DECLS

#endif /* !INTERNAL_TIMESTAMPED_POINTER_H */
