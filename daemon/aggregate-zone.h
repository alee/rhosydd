/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef VDD_AGGREGATE_ZONE_H
#define VDD_AGGREGATE_ZONE_H

#include <glib.h>

#include "librhosydd/attribute.h"
#include "librhosydd/zone.h"

G_BEGIN_DECLS

#define VDD_TYPE_AGGREGATE_ZONE vdd_aggregate_zone_get_type ()
G_DECLARE_FINAL_TYPE (VddAggregateZone, vdd_aggregate_zone, VDD,
                      AGGREGATE_ZONE, GObject)

const RsdAttribute  *vdd_aggregate_zone_get_attribute      (VddAggregateZone           *self,
                                                            const gchar                *attribute_name,
                                                            const RsdAttributeMetadata **metadata);
GPtrArray           *vdd_aggregate_zone_get_all_attributes (VddAggregateZone           *self);
GPtrArray           *vdd_aggregate_zone_get_all_metadata   (VddAggregateZone           *self);

VddAggregateZone    *vdd_aggregate_zone_get_parent         (VddAggregateZone           *self);
GPtrArray           *vdd_aggregate_zone_get_descendants    (VddAggregateZone           *self,
                                                            const gchar * const        *tags);

VddAggregateZone    *vdd_aggregate_zone_add_child          (VddAggregateZone           *self,
                                                            const gchar * const        *tags);

void                 vdd_aggregate_zone_clear_attributes   (VddAggregateZone           *self);
void                 vdd_aggregate_zone_add_attribute      (VddAggregateZone           *self,
                                                            RsdAttributeInfo           *attribute_info,
                                                            gint64                      clock_delta);

G_END_DECLS

#endif /* !VDD_AGGREGATE_ZONE_H */
