/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <signal.h>

#include "libcroesor/service.h"
#include "service.h"


int
main (int   argc,
      char *argv[])
{
  g_autoptr (GError) error = NULL;
  g_autoptr (VddService) service = NULL;

  /* Localisation */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* Create the service. */
  service = vdd_service_new ();

  /* Run until we are killed. */
  csr_service_run (CSR_SERVICE (service), argc, argv, &error);

  if (error != NULL)
    {
      int code;

      if (g_error_matches (error,
                           CSR_SERVICE_ERROR, CSR_SERVICE_ERROR_SIGNALLED))
        raise (csr_service_get_exit_signal (CSR_SERVICE (service)));

      g_printerr ("%s: %s\n", argv[0], error->message);
      code = error->code;

      return code;
    }

  return 0;
}
