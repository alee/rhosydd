/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <string.h>

#include <libinternal/logging.h>

#include "aggregate-zone.h"


static void vdd_aggregate_zone_zone_init     (RsdZoneInterface *iface);
static void vdd_aggregate_zone_get_property  (GObject          *object,
                                              guint             property_id,
                                              GValue           *value,
                                              GParamSpec       *pspec);
static void vdd_aggregate_zone_set_property  (GObject          *object,
                                              guint             property_id,
                                              const GValue     *value,
                                              GParamSpec       *pspec);
static void vdd_aggregate_zone_dispose       (GObject          *object);

static const gchar         *vdd_aggregate_zone_zone_get_path        (RsdZone *zone);
static const gchar * const *vdd_aggregate_zone_zone_get_tags        (RsdZone *zone);
static const gchar         *vdd_aggregate_zone_zone_get_parent_path (RsdZone *zone);

static const gchar **intern_and_sort_tags (const gchar * const *tags);

/**
 * VddAggregateZone:
 *
 * An implementation of #RsdZone which represents a single aggregated zone. It
 * Contains the attributes and child zones from one or more vehicles which
 * correspond to this location in the zone hierarchy.
 *
 * The structure is not thread safe. It is optimised for low memory use, on the
 * assumption that there will be few attributes (10 or fewer) per zone, and a
 * small number (5 or fewer) of child zones. Similarly, it assumes that there
 * will be a small number (5 or fewer) of tags per zone. Accordingly, most data
 * is stored in arrays rather than in hash tables, to conserve space.
 *
 * All strings (tags and attribute names) are interned to allow for fast pointer
 * comparison instead of character comparison. The @tags array is additionally
 * sorted by tag name, so that two tag arrays can be compared for equality in
 * `O(N)` time rather than `O(N^2)`.
 *
 * All timestamps for the attributes are stored in a single aggregated clock
 * domain, and are adjusted from their individual vehicle clock domains when
 * added to the aggregate zone with vdd_aggregate_zone_add_attribute().
 *
 * Since: 0.1.0
 */
struct _VddAggregateZone
{
  GObject parent;

  const gchar *path;  /* interned */
  VddAggregateZone *parent_zone;  /* unowned; NULL if this is the root */
  GPtrArray/*<owned VddAggregateZone>*/ *children;  /* owned; may be empty */

  /* %NULL-terminated potentially empty list of tags which apply to this
   * zone, interned and sorted to allow for rapid comparisons */
  const gchar **tags;  /* owned; all interned, pre-sorted */

  GArray/*<RsdAttributeInfo>*/ *attributes;  /* owned; may be empty */
};

typedef enum
{
  PROP_PATH = 1,
  PROP_TAGS,
  PROP_PARENT_ZONE,
} VddAggregateZoneProperty;

G_DEFINE_TYPE_WITH_CODE (VddAggregateZone, vdd_aggregate_zone,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RSD_TYPE_ZONE,
                                                vdd_aggregate_zone_zone_init))

static void
vdd_aggregate_zone_class_init (VddAggregateZoneClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GParamSpec *props[PROP_PARENT_ZONE + 1] = { NULL, };

  object_class->dispose = vdd_aggregate_zone_dispose;
  object_class->get_property = vdd_aggregate_zone_get_property;
  object_class->set_property = vdd_aggregate_zone_set_property;

  /**
   * VddAggregateZone:path:
   *
   * Path of the zone.
   *
   * Since: 0.1.0
   */
  props[PROP_PATH] =
      g_param_spec_string ("path", "Path",
                           "Path of the zone.",
                           "/",
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * VddAggregateZone:tags:
   *
   * Tags to differentiate the zone from its siblings
   *
   * Since: 0.1.0
   */
  props[PROP_TAGS] =
      g_param_spec_boxed ("tags", "Tags",
                          "Tags to differentiate the zone from its siblings.",
                          G_TYPE_STRV,
                          G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS);

  /**
   * VddAggregateZone:parent-zone:
   *
   * The parent zone, which may be %NULL if and only if this zone is the root
   * zone.
   *
   * Since: 0.1.0
   */
  props[PROP_PARENT_ZONE] =
      g_param_spec_object ("parent-zone", "Parent Zone",
                           "Zone’s parent zone.",
                           VDD_TYPE_AGGREGATE_ZONE,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);
}

static void
vdd_aggregate_zone_zone_init (RsdZoneInterface *iface)
{
  iface->get_path = vdd_aggregate_zone_zone_get_path;
  iface->get_tags = vdd_aggregate_zone_zone_get_tags;
  iface->get_parent_path = vdd_aggregate_zone_zone_get_parent_path;
}

static void
vdd_aggregate_zone_init (VddAggregateZone *self)
{
  self->children = g_ptr_array_new_with_free_func (g_object_unref);
  self->attributes = g_array_new (FALSE, FALSE, sizeof (RsdAttributeInfo));
  g_array_set_clear_func (self->attributes,
                          (GDestroyNotify) rsd_attribute_clear);
}

static void
vdd_aggregate_zone_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  VddAggregateZone *self = VDD_AGGREGATE_ZONE (object);

  switch ((VddAggregateZoneProperty) property_id)
    {
    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;
    case PROP_TAGS:
      g_value_set_boxed (value, self->tags);
      break;
    case PROP_PARENT_ZONE:
      g_value_set_object (value, self->parent_zone);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
vdd_aggregate_zone_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  VddAggregateZone *self = VDD_AGGREGATE_ZONE (object);

  switch ((VddAggregateZoneProperty) property_id)
    {
    case PROP_PATH:
      /* Construct only. */
      g_assert (g_value_get_string (value) != NULL);
      self->path = g_intern_string (g_value_get_string (value));
      break;
    case PROP_TAGS:
      /* Construct only. */
      g_assert (self->tags == NULL);
      g_assert (rsd_zone_tags_are_valid (g_value_get_boxed (value)));
      self->tags = intern_and_sort_tags (g_value_get_boxed (value));
      break;
    case PROP_PARENT_ZONE:
      /* Construct only. */
      g_assert (self->parent_zone == NULL);
      self->parent_zone = g_value_dup_object (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
vdd_aggregate_zone_dispose (GObject *object)
{
  VddAggregateZone *self = VDD_AGGREGATE_ZONE (object);

  g_clear_pointer (&self->children, g_ptr_array_unref);
  g_clear_pointer (&self->tags, g_free);
  g_clear_pointer (&self->attributes, g_array_unref);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (vdd_aggregate_zone_parent_class)->dispose (object);
}

static const gchar *
vdd_aggregate_zone_zone_get_path (RsdZone *zone)
{
  VddAggregateZone *self = VDD_AGGREGATE_ZONE (zone);

  return self->path;
}

static const gchar * const *
vdd_aggregate_zone_zone_get_tags (RsdZone *zone)
{
  VddAggregateZone *self = VDD_AGGREGATE_ZONE (zone);

  return self->tags;
}

static const gchar *
vdd_aggregate_zone_zone_get_parent_path (RsdZone *zone)
{
  VddAggregateZone *self = VDD_AGGREGATE_ZONE (zone);

  if (self->parent_zone != NULL)
    return rsd_zone_get_path (RSD_ZONE (self->parent_zone));

  return "";
}

/**
 * vdd_aggregate_zone_get_attribute:
 * @self: a #VddAggregateZone
 * @attribute_name: name of the attribute to retrieve
 * @metadata: (out caller-allocates) (optional): return location for the
 *    attribute metadata
 *
 * Get the value of the given attribute from the aggregated zone. If no
 * attribute exists with that name, return %NULL.
 *
 * Returns: (nullable) (transfer none): attribute value, or %NULL if not found
 * Since: 0.1.0
 */
const RsdAttribute *
vdd_aggregate_zone_get_attribute (VddAggregateZone           *self,
                                  const gchar                *attribute_name,
                                  const RsdAttributeMetadata **metadata)
{
  gsize i;

  g_return_val_if_fail (VDD_IS_AGGREGATE_ZONE (self), NULL);

  /* Intern the @attribute_name for comparison. */
  attribute_name = g_intern_string (attribute_name);

  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *attribute;

      attribute = &g_array_index (self->attributes, RsdAttributeInfo, i);

      if (attribute->metadata.name == attribute_name)
        {
          if (metadata != NULL)
            *metadata = &attribute->metadata;
          return &attribute->attribute;
        }
    }

  return NULL;
}

static void
get_all_attributes_for_zone (VddAggregateZone                      *self,
                             GPtrArray/*<owned RsdAttributeInfo>*/ *attributes)
{
  gsize i;

  /* Get the attributes for this zone. */
  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *attribute;
      g_autoptr (RsdAttributeInfo) info = NULL;

      attribute = &g_array_index (self->attributes, RsdAttributeInfo, i);
      info = rsd_attribute_info_new (&attribute->attribute,
                                     &attribute->metadata);
      g_ptr_array_add (attributes, g_steal_pointer (&info));
    }

  /* Recurse. */
  for (i = 0; i < self->children->len; i++)
    {
      VddAggregateZone *child_zone = self->children->pdata[i];

      get_all_attributes_for_zone (child_zone, attributes);
    }
}

/**
 * vdd_aggregate_zone_get_all_attributes:
 * @self: a #VddAggregateZone
 *
 * Get all attributes from this zone and all its descendants, recursively. Each
 * attribute is returned as a #RsdAttributeInfo which includes metadata about
 * its location in the zone hierarchy.
 *
 * Returns: (element-type RsdAttributeInfo) (transfer container): potentially
 *    empty array of attribute values
 * Since: 0.1.0
 */
GPtrArray *
vdd_aggregate_zone_get_all_attributes (VddAggregateZone *self)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;

  g_return_val_if_fail (VDD_IS_AGGREGATE_ZONE (self), NULL);

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  get_all_attributes_for_zone (self, attributes);
  return g_steal_pointer (&attributes);
}

static void
get_all_metadata_for_zone (VddAggregateZone                         *self,
                           GPtrArray/*<owned RsdAttributeMetadata>*/ *metadatas)
{
  gsize i;

  /* Get the attributes for this zone. */
  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *attribute;
      g_autoptr (RsdAttributeMetadata) metadata = NULL;

      attribute = &g_array_index (self->attributes, RsdAttributeInfo, i);
      metadata = rsd_attribute_metadata_copy (&attribute->metadata);
      g_ptr_array_add (metadatas, g_steal_pointer (&metadata));
    }

  /* Recurse. */
  for (i = 0; i < self->children->len; i++)
    {
      VddAggregateZone *child_zone = self->children->pdata[i];

      get_all_attributes_for_zone (child_zone, metadatas);
    }
}

/**
 * vdd_aggregate_zone_get_all_metadata:
 * @self: a #VddAggregateZone
 *
 * Get metadata for all attributes from this zone and all its descendants,
 * recursively. Each attribute metadata is returned as a #RsdAttributeMetadata
 * which does not include the #RsdAttribute values.
 *
 * Returns: (element-type RsdAttributeMetadata) (transfer container):
 *    potentially empty array of attribute metadata
 * Since: 0.2.0
 */
GPtrArray *
vdd_aggregate_zone_get_all_metadata (VddAggregateZone *self)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;

  g_return_val_if_fail (VDD_IS_AGGREGATE_ZONE (self), NULL);

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);
  get_all_metadata_for_zone (self, attributes);
  return g_steal_pointer (&attributes);
}

/**
 * vdd_aggregate_zone_get_parent:
 * @self: a #VddAggregateZone
 *
 * Get the parent zone of this one, or %NULL if this is the root zone.
 *
 * Returns: (transfer none) (nullable): parent zone, or %NULL if this is the
 *    root
 * Since: 0.1.0
 */
VddAggregateZone *
vdd_aggregate_zone_get_parent (VddAggregateZone *self)
{
  g_return_val_if_fail (VDD_IS_AGGREGATE_ZONE (self), NULL);

  return self->parent_zone;
}

/*
 * interned_strv_equal:
 * @a: (array zero-terminated): %NULL-terminated sorted array of interned
 *    strings
 * @b: (array zero-terminated): %NULL-terminated sorted array of interned
 *    strings
 *
 * Check that @a and @b are equal. They must both be sorted and interned before
 * being passed to this function (for example, using intern_and_sort_tags()).
 *
 * Returns: %TRUE if equal, %FALSE otherwise
 * Since: 0.1.0
 */
static gboolean
interned_strv_equal (const gchar * const *a,
                     const gchar * const *b)
{
  for (; *a != NULL && *b != NULL; a++, b++)
    {
      if (*a != *b)
        return FALSE;
    }

  return (*a == NULL && *b == NULL);
}

/*
 * interned_strv_is_superset:
 * @superset: (array zero-terminated): %NULL-terminated sorted array of interned
 *    strings
 * @subset: (array zero-terminated): %NULL-terminated sorted array of interned
 *    strings
 *
 * Check that @superset is a superset of, or equal to, @subset. Both arrays must
 * be sorted and interned before being passed to this function (for example,
 * using intern_and_sort_tags()).
 *
 * Returns: %TRUE if @superset is a superset of, or equal to, @subset; %FALSE
 *    otherwise
 * Since: 0.1.0
 */
static gboolean
interned_strv_is_superset (const gchar * const *superset,
                           const gchar * const *subset)
{
  for (; *superset != NULL && *subset != NULL; superset++)
    {
      if (*superset == *subset)
        subset++;
    }

  return (*subset == NULL);
}

/* g_ptr_array_sort() passes pointers to the gchar*s to the sort function. */
static gint
cmp_tags (gconstpointer a,
          gconstpointer b)
{
  const gchar *a_str = *((const gchar * const *) a);
  const gchar *b_str = *((const gchar * const *) b);

  return g_strcmp0 (a_str, b_str);
}

/*
 * intern_and_sort_tags:
 * @tags: (array zero-terminated): %NULL-terminated array of tags to sort
 *
 * Pre-process @tags into a sorted array of interned strings, which is returned.
 *
 * Returns: (transfer container): sorted array of interned tags
 * Since: 0.1.0
 */
static const gchar **
intern_and_sort_tags (const gchar * const *tags)
{
  GPtrArray/*<unowned gchar *>*/ *processed_tags_array = NULL;

  processed_tags_array = g_ptr_array_new ();
  for (; tags != NULL && *tags != NULL; tags++)
    g_ptr_array_add (processed_tags_array, (gpointer) g_intern_string (*tags));
  g_ptr_array_sort (processed_tags_array, cmp_tags);
  g_ptr_array_add (processed_tags_array, NULL);  /* terminator */

  return (const gchar **) g_ptr_array_free (processed_tags_array, FALSE);
}

/**
 * vdd_aggregate_zone_add_child:
 * @self: a #VddAggregateZone
 * @tags: (array zero-terminated): potentially empty list of tags for the zone
 *
 * Add a child zone to this zone, with the given set of @tags. If a child zone
 * already exists with those tags, its path is returned. Otherwise a new child
 * zone is created.
 *
 * Returns: (transfer none): new child zone
 * Since: 0.1.0
 */
VddAggregateZone *
vdd_aggregate_zone_add_child (VddAggregateZone    *self,
                              const gchar * const *tags)
{
  gsize i;
  g_autofree const gchar **processed_tags = NULL;
  g_autoptr (VddAggregateZone) zone = NULL;

  g_return_val_if_fail (VDD_IS_AGGREGATE_ZONE (self), NULL);

  /* Pre-process the tags to intern and sort them. */
  processed_tags = intern_and_sort_tags (tags);

  /* See if any of the existing children have the same tags — if so, return
   * their ID. */
  for (i = 0; i < self->children->len; i++)
    {
      VddAggregateZone *child = self->children->pdata[i];

      if (interned_strv_equal (child->tags, processed_tags))
        return child;
    }

  /* If not, create a new child zone. */
  zone = g_object_new (VDD_TYPE_AGGREGATE_ZONE,
                       "path", rsd_zone_path_build (self->path, processed_tags),
                       "parent-zone", self,
                       "tags", processed_tags,
                       NULL);

  g_ptr_array_add (self->children, zone);

  return g_steal_pointer (&zone);
}

static void
get_descendants (VddAggregateZone  *self,
                 const gchar      **tags,
                 GPtrArray         *zones)
{
  gsize i;

  /* Find children which match @tags. */
  for (i = 0; i < self->children->len; i++)
    {
      VddAggregateZone *child = self->children->pdata[i];

      if (interned_strv_is_superset (child->tags, tags))
        {
          /* Add the child and all its descendants (ignoring @tags). */
          g_ptr_array_add (zones, g_object_ref (child));
          get_descendants (child, NULL, zones);
        }
    }
}

/**
 * vdd_aggregate_zone_get_descendants:
 * @self: a #VddAggregateZone
 * @tags: array of tags to match against, or %NULL to match all children
 *
 * Get a list of all descendants of children of this #VddAggregateZone whose
 * tags contain all of the given @tags. Grandchildren and lower descendants do
 * not have to match @tags.
 *
 * Returns: (transfer container) (element-type VddAggregateZone): array of
 *    matching descendants of this zone
 * Since: 0.2.0
 */
GPtrArray *
vdd_aggregate_zone_get_descendants (VddAggregateZone    *self,
                                    const gchar * const *tags)
{
  g_autofree const gchar **processed_tags = NULL;
  g_autoptr (GPtrArray/*<owned VddAggregateZone>*/) zones = NULL;

  g_return_val_if_fail (VDD_IS_AGGREGATE_ZONE (self), NULL);

  processed_tags = intern_and_sort_tags (tags);
  zones = g_ptr_array_new_with_free_func (g_object_unref);
  get_descendants (self, processed_tags, zones);

  return g_steal_pointer (&zones);
}

/**
 * vdd_aggregate_zone_clear_attributes:
 * @self: a #VddAggregateZone
 *
 * Clear all cached attributes from this zone but not its descendants.
 *
 * Since: 0.1.0
 */
void
vdd_aggregate_zone_clear_attributes (VddAggregateZone *self)
{
  g_return_if_fail (VDD_IS_AGGREGATE_ZONE (self));

  /* Clear from this zone. */
  if (self->attributes->len > 0)
    g_array_remove_range (self->attributes, 0, self->attributes->len);
}

/**
 * vdd_aggregate_zone_add_attribute:
 * @self: a #VddAggregateZone
 * @attribute_info: attribute information to add
 * @clock_delta: difference (in microseconds) between the vehicle’s clock and
 *    the aggregate clock domain, such that //adding// this delta to a timestamp
 *    from the vehicle gives a timestamp in the aggregate clock domain
 *
 * Add an attribute to the #VddAggregateZone.
 *
 * Since: 0.4.0
 */
void
vdd_aggregate_zone_add_attribute (VddAggregateZone *self,
                                  RsdAttributeInfo *attribute_info,
                                  gint64            clock_delta)
{
  RsdAttributeInfo attribute;
  RsdTimestampMicroseconds old_last_updated;

  g_return_if_fail (VDD_IS_AGGREGATE_ZONE (self));
  g_return_if_fail (attribute_info != NULL);

  old_last_updated = attribute_info->attribute.last_updated;

  rsd_attribute_init (&attribute.attribute, &attribute_info->attribute);
  rsd_attribute_metadata_init (&attribute.metadata, &attribute_info->metadata);

  if (attribute.attribute.last_updated != RSD_TIMESTAMP_UNKNOWN)
    attribute.attribute.last_updated += clock_delta;

  DEBUG ("Attribute ‘%s’ set last_updated = %" G_GINT64_FORMAT " + %"
         G_GINT64_FORMAT " = %" G_GINT64_FORMAT ".",
         attribute_info->metadata.name, old_last_updated,
         clock_delta, attribute.attribute.last_updated);

  g_array_append_val (self->attributes, attribute);
}
