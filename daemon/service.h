/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef VDD_SERVICE_H
#define VDD_SERVICE_H

#include <glib.h>
#include <glib-object.h>

#include "service.h"

G_BEGIN_DECLS

#define VDD_TYPE_SERVICE vdd_service_get_type ()
G_DECLARE_FINAL_TYPE (VddService, vdd_service, VDD, SERVICE, CsrService)

VddService *vdd_service_new (void);

G_END_DECLS

#endif /* !VDD_SERVICE_H */
