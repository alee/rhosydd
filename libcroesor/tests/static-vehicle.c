/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <string.h>

#include "libcroesor/static-vehicle.h"
#include "librhosydd/attribute.h"
#include "librhosydd/static-zone.h"
#include "librhosydd/vehicle.h"
#include "librhosydd/zone.h"


/* Build a generic static vehicle to test against. */
static CsrStaticVehicle *
build_vehicle (void)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  g_autoptr (RsdAttribute) attribute = NULL;
  g_autoptr (RsdAttributeMetadata) metadata = NULL;

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

  /* Normal attribute. */
  attribute = rsd_attribute_new (g_variant_new_string ("hello"), 0.0,
                                 RSD_TIMESTAMP_UNKNOWN);
  metadata = rsd_attribute_metadata_new ("/", "testAttribute",
                                        RSD_ATTRIBUTE_AVAILABLE,
                                        RSD_ATTRIBUTE_READABLE);
  g_ptr_array_add (attributes, rsd_attribute_info_new (attribute, metadata));

  return csr_static_vehicle_new ("vehicle0", attributes);
}

/* Test construction of a static vehicle. */
static void
test_static_vehicle_construction (void)
{
  g_autoptr (CsrStaticVehicle) vehicle = NULL;

  vehicle = build_vehicle ();
}

/* Test that the ID is correct for a static vehicle. */
static void
test_static_vehicle_id (void)
{
  g_autoptr (CsrStaticVehicle) vehicle = NULL;

  vehicle = build_vehicle ();

  g_assert_cmpstr (rsd_vehicle_get_id (RSD_VEHICLE (vehicle)), ==, "vehicle0");
}

/* Test that the zones are correct for a static vehicle. */
static void
test_static_vehicle_zones (void)
{
  g_autoptr (CsrStaticVehicle) vehicle = NULL;
  g_autoptr (GPtrArray/*<owned RsdZone>*/) zones = NULL;
  RsdZone *zone;

  vehicle = build_vehicle ();
  zones = rsd_vehicle_get_zones (RSD_VEHICLE (vehicle));

  g_assert_cmpuint (zones->len, ==, 1);

  zone = zones->pdata[0];

  g_assert_cmpstr (rsd_zone_get_path (zone), ==, "/");
}

static void
get_result_cb (GObject      *obj,
               GAsyncResult *result,
               gpointer      user_data)
{
  GAsyncResult **result_out = user_data;

  *result_out = g_object_ref (result);
}

/* Test successfully retrieving an attribute from a static vehicle. */
static void
test_static_vehicle_get_attribute_normal (void)
{
  g_autoptr (CsrStaticVehicle) vehicle = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (RsdAttributeInfo) info = NULL;
  g_autoptr (RsdZone) zone = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) expected_value = NULL;
  RsdTimestampMicroseconds current_time;
  gint64 local_start_time, local_finish_time;

  vehicle = build_vehicle ();
  zone = RSD_ZONE (rsd_static_zone_new ("/"));
  local_start_time = g_get_monotonic_time ();

  rsd_vehicle_get_attribute_async (RSD_VEHICLE (vehicle), zone, "testAttribute",
                                  NULL, get_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  info = rsd_vehicle_get_attribute_finish (RSD_VEHICLE (vehicle), result,
                                           &current_time, &error);
  local_finish_time = g_get_monotonic_time ();

  g_assert_no_error (error);
  g_assert_nonnull (info);
  g_assert_cmpstr (info->metadata.zone_path, ==, "/");
  g_assert_cmpstr (info->metadata.name, ==, "testAttribute");
  g_assert_cmpuint (info->metadata.availability, ==, RSD_ATTRIBUTE_AVAILABLE);
  g_assert_cmpuint (info->metadata.flags, ==, RSD_ATTRIBUTE_READABLE);
  expected_value = g_variant_new_string ("hello");
  g_assert (g_variant_equal (info->attribute.value, expected_value));
  g_assert_cmpfloat (info->attribute.accuracy, ==, 0.0);
  g_assert_cmpuint (info->attribute.last_updated, ==, RSD_TIMESTAMP_UNKNOWN);

  /* We know the static vehicle uses the g_get_monotonic_time() clock domain. */
  g_assert_cmpuint (current_time, >=, local_start_time);
  g_assert_cmpuint (current_time, <=, local_finish_time);
}

/* Test retrieving an attribute from a non-existent zone fails */
static void
test_static_vehicle_get_attribute_unknown_zone (void)
{
  g_autoptr (CsrStaticVehicle) vehicle = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (RsdAttributeInfo) info = NULL;
  g_autoptr (RsdZone) zone = NULL;
  RsdTimestampMicroseconds current_time = 42;
  g_autoptr (GError) error = NULL;

  vehicle = build_vehicle ();
  zone = RSD_ZONE (rsd_static_zone_new ("/nope/"));

  rsd_vehicle_get_attribute_async (RSD_VEHICLE (vehicle), zone,
                                  "testAttribute",
                                  NULL, get_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  info = rsd_vehicle_get_attribute_finish (RSD_VEHICLE (vehicle), result,
                                           &current_time, &error);

  g_assert_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ZONE);
  g_assert_null (info);
  g_assert_cmpuint (current_time, ==, 42);
}

/* Test retrieving a non-existent attribute fails */
static void
test_static_vehicle_get_attribute_unknown_attribute (void)
{
  g_autoptr (CsrStaticVehicle) vehicle = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (RsdAttributeInfo) info = NULL;
  g_autoptr (RsdZone) zone = NULL;
  RsdTimestampMicroseconds current_time = 42;
  g_autoptr (GError) error = NULL;

  vehicle = build_vehicle ();
  zone = RSD_ZONE (rsd_static_zone_new ("/"));

  rsd_vehicle_get_attribute_async (RSD_VEHICLE (vehicle), zone,
                                   "unknownAttribute",
                                  NULL, get_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  info = rsd_vehicle_get_attribute_finish (RSD_VEHICLE (vehicle), result,
                                           &current_time, &error);

  g_assert_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE);
  g_assert_null (info);
  g_assert_cmpuint (current_time, ==, 42);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/static-vehicle/construction",
                   test_static_vehicle_construction);
  g_test_add_func ("/static-vehicle/id", test_static_vehicle_id);
  g_test_add_func ("/static-vehicle/zones", test_static_vehicle_zones);

  g_test_add_func ("/static-vehicle/get-attribute/normal",
                   test_static_vehicle_get_attribute_normal);
  g_test_add_func ("/static-vehicle/get-attribute/unknown-zone",
                   test_static_vehicle_get_attribute_unknown_zone);
  g_test_add_func ("/static-vehicle/get-attribute/unknown-attribute",
                   test_static_vehicle_get_attribute_unknown_attribute);

  return g_test_run ();
}
