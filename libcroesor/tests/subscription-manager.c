/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <string.h>

#include "libcroesor/static-vehicle.h"
#include "libcroesor/subscription-manager.h"
#include "librhosydd/subscription.h"


/* Test construction of a subscription manager. */
static void
test_subscription_manager_construction (void)
{
  g_autoptr (CsrSubscriptionManager) manager = NULL;
  g_autoptr (RsdVehicle) vehicle1 = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;

  manager = csr_subscription_manager_new ();

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  vehicle1 = RSD_VEHICLE (csr_static_vehicle_new ("vehicle1", attributes));

  g_assert_null (csr_subscription_manager_get_subscriptions (manager,
                                                             vehicle1,
                                                             ":1.1"));
}

/* Version of g_variant_compare() which is NULL-safe. NULLs compare as equal;
 * NULL and non-NULL compare as unequal. */
static gint
variant_compare0 (GVariant *a,
                  GVariant *b)
{
  if (a == NULL && b == NULL)
    return 0;
  else if (a == NULL && b != NULL)
    return -1;
  else if (a != NULL && b == NULL)
    return 1;
  else
    return g_variant_compare (a, b);
}

/* Comparison function for #RsdSubscriptions. Ordering is arbitrary. Ignores
 * mutable state in the subscriptions.
 *
 * Note that the parameters are provided as pointers to
 * (const RsdSubscription*). */
static gint
compare_subscriptions (gconstpointer a_,
                       gconstpointer b_)
{
  gint retval;
  const RsdSubscription *a = *((const RsdSubscription **) a_);
  const RsdSubscription *b = *((const RsdSubscription **) b_);

  retval = g_strcmp0 (a->zone_path, b->zone_path);
  if (retval != 0)
    return retval;

  retval = g_strcmp0 (a->attribute_name, b->attribute_name);
  if (retval != 0)
    return retval;

  retval = variant_compare0 (a->minimum_value, b->minimum_value);
  if (retval != 0)
    return retval;

  retval = variant_compare0 (a->maximum_value, b->maximum_value);
  if (retval != 0)
    return retval;

  retval = variant_compare0 (a->hysteresis, b->hysteresis);
  if (retval != 0)
    return retval;

  retval = a->minimum_period - b->minimum_period;
  if (retval != 0)
    return retval;

  retval = a->maximum_period - b->maximum_period;
  if (retval != 0)
    return retval;

  return 0;
}

/* Assert that the two subscription arrays are equal in contents, but not
 * necessarily in order. The arrays may contain duplicate entries: if so, the
 * number of duplicates for each entry must match between the two arrays. */
static void
assert_subscriptions_equal (GPtrArray *subscriptions1,
                            GPtrArray *subscriptions2)
{
  gsize i;
  g_autoptr (GPtrArray/*<unowned RsdSubscription>*/) sorted1 = NULL;
  g_autoptr (GPtrArray/*<unowned RsdSubscription>*/) sorted2 = NULL;

  /* Sort the arrays first, then compare pairwise. */
  sorted1 = g_ptr_array_new ();
  sorted2 = g_ptr_array_new ();

  for (i = 0; i < subscriptions1->len; i++)
    g_ptr_array_add (sorted1, subscriptions1->pdata[i]);
  g_ptr_array_sort (sorted1, compare_subscriptions);

  for (i = 0; i < subscriptions2->len; i++)
    g_ptr_array_add (sorted2, subscriptions2->pdata[i]);
  g_ptr_array_sort (sorted2, compare_subscriptions);

  for (i = 0; i < sorted1->len && i < sorted2->len; i++)
    {
      const RsdSubscription *subscription1 = sorted1->pdata[i];
      const RsdSubscription *subscription2 = sorted2->pdata[i];

      g_test_message ("Testing pair %" G_GSIZE_FORMAT " (%s, %s; %s, %s)", i,
                      subscription1->zone_path, subscription1->attribute_name,
                      subscription2->zone_path, subscription2->attribute_name);
      g_assert_true (rsd_subscription_equal (subscription1, subscription2));
    }

  g_assert_cmpuint (sorted1->len, ==, sorted2->len);
}

/* Test that a sequence of all operations on the #CsrSubscriptionManager with
 * two vehicles and two peers, and two attribute subscriptions between each pair
 * (so 8 subscriptions in total) works as expected. */
static void
test_subscription_manager_subscriptions (void)
{
  g_autoptr (CsrSubscriptionManager) manager = NULL;
  g_autoptr (RsdVehicle) vehicle_a = NULL, vehicle_b = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions1 = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions2 = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions3 = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions4 = NULL;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) removed_vehicles = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) unsubscriptions = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) updated_subscriptions1 = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) empty_array = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;

  manager = csr_subscription_manager_new ();

  empty_array = g_ptr_array_new ();

  /* Set up the vehicles. */
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  vehicle_a = RSD_VEHICLE (csr_static_vehicle_new ("vehicleA", attributes));
  vehicle_b = RSD_VEHICLE (csr_static_vehicle_new ("vehicleB", attributes));

  /* Set up the subscriptions. The attribute naming convention is ‘attr’ plus the
   * vehicle letter, plus an abbreviation of the peer name (necessary to conform
   * to the syntax for attribute names). */
  subscriptions1 = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions1,
                   rsd_subscription_new ("/", "attrA11",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions1,
                   rsd_subscription_new ("/", "anotherAttrA11",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  subscriptions2 = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions2,
                   rsd_subscription_new ("/", "attrA12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions2,
                   rsd_subscription_new ("/", "anotherAttrA12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  subscriptions3 = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions3,
                   rsd_subscription_new ("/", "attrB11",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions3,
                   rsd_subscription_new ("/", "anotherAttrB11",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  subscriptions4 = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions4,
                   rsd_subscription_new ("/", "attrB12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions4,
                   rsd_subscription_new ("/", "anotherAttrB12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  /* The subscription manager should be empty to start with. */
  g_assert_null (csr_subscription_manager_get_subscriptions (manager,
                                                             vehicle_a,
                                                             ":1.1"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager,
                                                             vehicle_a,
                                                             ":1.2"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager,
                                                             vehicle_b,
                                                             ":1.1"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager,
                                                             vehicle_b,
                                                             ":1.2"));

  /* Add all the subscriptions. */
  csr_subscription_manager_update_subscriptions (manager, vehicle_a, ":1.1",
                                                 subscriptions1, empty_array);
  csr_subscription_manager_update_subscriptions (manager, vehicle_a, ":1.2",
                                                 subscriptions2, empty_array);
  csr_subscription_manager_update_subscriptions (manager, vehicle_b, ":1.1",
                                                 subscriptions3, empty_array);
  csr_subscription_manager_update_subscriptions (manager, vehicle_b, ":1.2",
                                                 subscriptions4, empty_array);

  /* Check all the subscriptions were added correctly. */
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_a,
                                                                          ":1.1"),
                              subscriptions1);
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_a,
                                                                          ":1.2"),
                              subscriptions2);
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_b,
                                                                          ":1.1"),
                              subscriptions3);
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_b,
                                                                          ":1.2"),
                              subscriptions4);

  /* Remove one of the peers: the subscriptions from that peer to vehicles A and
   * B should be removed automatically. */
  csr_subscription_manager_remove_peer (manager, ":1.1");

  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_a,
                                                             ":1.1"));
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_a,
                                                                          ":1.2"),
                              subscriptions2);
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.1"));
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_b,
                                                                          ":1.2"),
                              subscriptions4);

  /* Remove vehicle B: the subscriptions from the remaining peer to it should be
   * removed automatically. That should leave only the two subscriptions from
   * peer :1.2 to vehicle A. */
  removed_vehicles = g_ptr_array_new ();
  g_ptr_array_add (removed_vehicles, vehicle_b);

  csr_subscription_manager_update_vehicles (manager, empty_array,
                                            removed_vehicles);

  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_a,
                                                             ":1.1"));
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_a,
                                                                          ":1.2"),
                              subscriptions2);
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.1"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.2"));

  /* Remove one of the remaining subscriptions, and add a new one between peer
   * :1.2 and vehicle A. The result should be @updated_subscriptions1. */
  unsubscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (unsubscriptions,
                   rsd_subscription_new ("/", "attrA12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  subscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions,
                   rsd_subscription_new ("/", "thirdAttrA12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  updated_subscriptions1 = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (updated_subscriptions1,
                   rsd_subscription_new ("/", "anotherAttrA12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (updated_subscriptions1,
                   rsd_subscription_new ("/", "thirdAttrA12",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  csr_subscription_manager_update_subscriptions (manager, vehicle_a, ":1.2",
                                                 subscriptions, unsubscriptions);

  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_a,
                                                             ":1.1"));
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_a,
                                                                          ":1.2"),
                              updated_subscriptions1);
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.1"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.2"));

  /* Remove all the remaining subscriptions from peer :1.2 to vehicle A. The
   * result should be an empty array of subscriptions between the two, as the
   * peer has not yet vanished (unlike in the tests above). */
  g_ptr_array_set_size (subscriptions, 0);

  csr_subscription_manager_update_subscriptions (manager, vehicle_a, ":1.2",
                                                 subscriptions,
                                                 updated_subscriptions1);

  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_a,
                                                             ":1.1"));
  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle_a,
                                                                          ":1.2"),
                              empty_array);
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.1"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.2"));

  /* Remove peer :1.2; at this point, all the subscriptions and pairings should
   * be gone and the manager should be entirely empty. */
  csr_subscription_manager_remove_peer (manager, ":1.2");

  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_a,
                                                             ":1.1"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_a,
                                                             ":1.2"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.1"));
  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle_b,
                                                             ":1.2"));
}

/* Test that duplicate subscriptions from a peer to a vehicle are handled
 * independently. */
static void
test_subscription_manager_subscriptions_duplicates (void)
{
  g_autoptr (CsrSubscriptionManager) manager = NULL;
  g_autoptr (RsdVehicle) vehicle = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions1 = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions2 = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) unsubscriptions = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) empty_array = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;

  manager = csr_subscription_manager_new ();

  empty_array = g_ptr_array_new ();

  /* Set up the vehicle. */
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  vehicle = RSD_VEHICLE (csr_static_vehicle_new ("vehicle1", attributes));

  /* Set up the subscriptions. */
  subscriptions1 = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions1,
                   rsd_subscription_new ("/", "attr1",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions1,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions1,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions1,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  /* Add the subscriptions. */
  csr_subscription_manager_update_subscriptions (manager, vehicle, ":1.1",
                                                 subscriptions1, empty_array);

  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle,
                                                                          ":1.1"),
                              subscriptions1);

  /* Remove one of the duplicate subscriptions: only that subscription should
   * be removed, not all of its duplicates. */
  subscriptions2 = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions2,
                   rsd_subscription_new ("/", "attr1",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions2,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions2,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  unsubscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (unsubscriptions,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  csr_subscription_manager_update_subscriptions (manager, vehicle, ":1.1",
                                                 empty_array, unsubscriptions);

  assert_subscriptions_equal (csr_subscription_manager_get_subscriptions (manager,
                                                                          vehicle,
                                                                          ":1.1"),
                              subscriptions2);

  /* Remove the peer: the remaining duplicate subscriptions should be
   * removed. */
  csr_subscription_manager_remove_peer (manager, ":1.1");

  g_assert_null (csr_subscription_manager_get_subscriptions (manager, vehicle,
                                                             ":1.1"));
}

/* Helper function to assert that the result of
 * csr_subscription_manager_get_peers_for_subscriptions() is a given set of
 * peers, for the given vehicle and set of attribute names. The attributes are
 * passed to the function as invalidated attributes.
 *
 * The varargs are: a potentially empty NULL-terminated list of attribute names
 * to pass to the function; followed by a potentially empty NULL-terminated
 * list of expected peer names.
 */
static void
assert_peers_for_subscriptions (CsrSubscriptionManager *manager,
                                RsdVehicle             *vehicle,
                                ...)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) invalidated_attributes = NULL;
  g_autoptr (GPtrArray/*<owned utf8>*/) peers = NULL;
  g_autoptr (GHashTable/*<unowned utf8, unowned utf8>*/) expected_peers = NULL;
  va_list args;
  const gchar *attribute_name, *peer;
  gsize i;

  /* Grab the attributes and peers lists from the varargs. */
  invalidated_attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);
  expected_peers = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, NULL);

  va_start (args, vehicle);

  for (attribute_name = va_arg (args, const gchar *);
       attribute_name != NULL;
       attribute_name = va_arg (args, const gchar *))
    {
      g_autoptr (RsdAttributeMetadata) metadata = NULL;

      metadata = rsd_attribute_metadata_new ("/", attribute_name,
                                             RSD_ATTRIBUTE_AVAILABLE,
                                             RSD_ATTRIBUTE_READABLE);
      g_ptr_array_add (invalidated_attributes, g_steal_pointer (&metadata));
    }

  for (peer = va_arg (args, const gchar *);
       peer != NULL;
       peer = va_arg (args, const gchar *))
    {
      g_hash_table_add (expected_peers, (gpointer) peer);
    }

  va_end (args);

  /* Retrieve and check the peers. */
  peers = csr_subscription_manager_get_peers_for_subscriptions (manager,
                                                                vehicle,
                                                                NULL,
                                                                invalidated_attributes,
                                                                g_get_monotonic_time ());

  for (i = 0; i < peers->len; i++)
    {
      const gchar *_peer = peers->pdata[i];

      g_test_message ("Checking ‘%s’", _peer);
      g_assert_true (g_hash_table_remove (expected_peers, _peer));
    }

  g_assert_cmpuint (g_hash_table_size (expected_peers), ==, 0);
}

/* Test that listing the peers for a set of subscriptions works correctly. Do
 * this with two peers, each subscribed to a set of two attributes; the sets
 * intersect but are not equal. Repeat for a second vehicle. Repeat for a third
 * peer which has a wildcard subscription. */
static void
test_subscription_manager_subscriptions_peers (void)
{
  g_autoptr (CsrSubscriptionManager) manager = NULL;
  g_autoptr (RsdVehicle) vehicle_a = NULL, vehicle_b = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions11a = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions12a = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions12b = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions13b = NULL;
  g_autoptr (GPtrArray/*<owned RsdSubscription>*/) empty_array = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;

  manager = csr_subscription_manager_new ();

  empty_array = g_ptr_array_new ();

  /* Set up the vehicles. */
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);
  vehicle_a = RSD_VEHICLE (csr_static_vehicle_new ("vehicleA", attributes));
  vehicle_b = RSD_VEHICLE (csr_static_vehicle_new ("vehicleB", attributes));

  /* Add the subscriptions for peer :1.1 to vehicle A. */
  subscriptions11a = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions11a,
                   rsd_subscription_new ("/", "attr1",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions11a,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  csr_subscription_manager_update_subscriptions (manager, vehicle_a, ":1.1",
                                                 subscriptions11a, empty_array);

  /* Add the subscriptions for peer :1.2 to vehicle A. */
  subscriptions12a = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions12a,
                   rsd_subscription_new ("/", "attr2",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions12a,
                   rsd_subscription_new ("/", "attr3",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  csr_subscription_manager_update_subscriptions (manager, vehicle_a, ":1.2",
                                                 subscriptions12a, empty_array);

  /* Add the subscriptions for peer :1.2 to vehicle B. */
  subscriptions12b = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions12b,
                   rsd_subscription_new ("/", "attr3",
                                         NULL, NULL, NULL, 0, G_MAXUINT));
  g_ptr_array_add (subscriptions12b,
                   rsd_subscription_new ("/", "attr4",
                                         NULL, NULL, NULL, 0, G_MAXUINT));

  csr_subscription_manager_update_subscriptions (manager, vehicle_b, ":1.2",
                                                 subscriptions12b, empty_array);

  /* Add the subscriptions for peer :1.3 to vehicle B. */
  subscriptions13b = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
  g_ptr_array_add (subscriptions13b, rsd_subscription_new_wildcard ());

  csr_subscription_manager_update_subscriptions (manager, vehicle_b, ":1.3",
                                                 subscriptions13b, empty_array);

  /* Try getting peers for various inputs. */
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr1", NULL,
                                  ":1.1", NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr1", "attr2", NULL,
                                  ":1.1", ":1.2", NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr1", "attr2", "attr3", NULL,
                                  ":1.1", ":1.2", NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr3", NULL,
                                  ":1.2", NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr4", NULL,
                                  NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "notAnAttribute", NULL,
                                  NULL);
  assert_peers_for_subscriptions (manager, vehicle_b,
                                  "attr1", NULL,
                                  ":1.3", NULL);
  assert_peers_for_subscriptions (manager, vehicle_b,
                                  "attr3", "attr4", NULL,
                                  ":1.2", ":1.3", NULL);

  /* Remove peer :1.2; all their subscriptions should be removed. */
  csr_subscription_manager_remove_peer (manager, ":1.2");

  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr1", NULL,
                                  ":1.1", NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr1", "attr2", NULL,
                                  ":1.1", NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr1", "attr2", "attr3", NULL,
                                  ":1.1", NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr3", NULL,
                                  NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "attr4", NULL,
                                  NULL);
  assert_peers_for_subscriptions (manager, vehicle_a,
                                  "notAnAttribute", NULL,
                                  NULL);
  assert_peers_for_subscriptions (manager, vehicle_b,
                                  "attr1", NULL,
                                  ":1.3", NULL);
  assert_peers_for_subscriptions (manager, vehicle_b,
                                  "attr3", "attr4", NULL,
                                  ":1.3", NULL);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/subscription-manager/construction",
                   test_subscription_manager_construction);
  g_test_add_func ("/subscription-manager/subscriptions",
                   test_subscription_manager_subscriptions);
  g_test_add_func ("/subscription-manager/subscriptions/duplicates",
                   test_subscription_manager_subscriptions_duplicates);
  g_test_add_func ("/subscription-manager/subscriptions/peers",
                   test_subscription_manager_subscriptions_peers);

  return g_test_run ();
}
