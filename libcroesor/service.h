/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_SERVICE_H
#define CSR_SERVICE_H

#include <glib.h>
#include <glib-object.h>

#include <libcroesor/vehicle-manager.h>

G_BEGIN_DECLS

/**
 * CSR_SERVICE_ERROR:
 *
 * Error domain for #CsrServiceError.
 *
 * Since: 0.2.0
 */
#define CSR_SERVICE_ERROR csr_service_error_quark ()
GQuark csr_service_error_quark (void);

/**
 * CsrServiceError:
 * @CSR_SERVICE_ERROR_SIGNALLED: Process was signalled with `SIGINT` or
 *    `SIGTERM`.
 * @CSR_SERVICE_ERROR_INVALID_OPTIONS: Invalid command line options.
 * @CSR_SERVICE_ERROR_NAME_UNAVAILABLE: Bus or well-known name unavailable.
 * @CSR_SERVICE_ERROR_INVALID_ENVIRONMENT: Runtime environment is insecure or
 *    otherwise invalid for running the daemon.
 *
 * Errors from running a service.
 *
 * Since: 0.2.0
 */
typedef enum
{
  CSR_SERVICE_ERROR_SIGNALLED,
  CSR_SERVICE_ERROR_INVALID_OPTIONS,
  CSR_SERVICE_ERROR_NAME_UNAVAILABLE,
  CSR_SERVICE_ERROR_INVALID_ENVIRONMENT,
} CsrServiceError;

#define CSR_TYPE_SERVICE csr_service_get_type ()
G_DECLARE_DERIVABLE_TYPE (CsrService, csr_service, CSR, SERVICE, GObject)

struct _CsrServiceClass
{
  GObjectClass parent_class;

  GOptionEntry *(*get_main_option_entries) (CsrService *service);

  void (*startup_async)  (CsrService          *service,
                          GCancellable        *cancellable,
                          GAsyncReadyCallback  callback,
                          gpointer             user_data);
  void (*startup_finish) (CsrService          *service,
                          GAsyncResult        *result,
                          GError             **error);
  void (*shutdown)       (CsrService          *service);

  gpointer padding[12];
};

CsrService *csr_service_new              (const gchar   *object_path,
                                          const gchar   *translation_domain,
                                          const gchar   *summary);
void        csr_service_add_option_group (CsrService    *self,
                                          GOptionGroup  *group);

void        csr_service_run              (CsrService    *self,
                                          int            argc,
                                          char         **argv,
                                          GError       **error);
void        csr_service_exit             (CsrService    *self,
                                          const GError  *error,
                                          gint           signum);

GDBusConnection *csr_service_get_dbus_connection (CsrService *self);
gint             csr_service_get_exit_signal     (CsrService *self);

G_END_DECLS

#endif /* !CSR_SERVICE_H */
