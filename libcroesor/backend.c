/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include <locale.h>

#include "libcroesor/backend.h"
#include "libcroesor/backend-manager.h"
#include "libcroesor/service.h"
#include "libcroesor/vehicle-manager.h"
#include "libcroesor/vehicle-service.h"


/* These errors do not need to be registered with
 * g_dbus_error_register_error_domain() as they never go over the bus. */
GQuark
csr_backend_error_quark (void)
{
  return g_quark_from_static_string ("csr-backend-error-quark");
}

/* A way of automatically removing sources when going out of scope. */
typedef guint SourceId;
G_DEFINE_AUTO_CLEANUP_FREE_FUNC (SourceId, g_source_remove, 0)

/* The same for bus watches. */
typedef guint WatchId;
G_DEFINE_AUTO_CLEANUP_FREE_FUNC (WatchId, g_bus_unwatch_name, 0);

static void csr_backend_dispose      (GObject      *object);
static void csr_backend_get_property (GObject      *object,
                                      guint         property_id,
                                      GValue       *value,
                                      GParamSpec   *pspec);
static void csr_backend_set_property (GObject      *object,
                                      guint         property_id,
                                      const GValue *value,
                                      GParamSpec   *pspec);

static void csr_backend_startup_async  (CsrService          *service,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data);
static void csr_backend_startup_finish (CsrService          *service,
                                        GAsyncResult        *result,
                                        GError             **error);
static void csr_backend_shutdown       (CsrService          *service);

/**
 * CsrBackend:
 *
 * A skeleton implementation of a backend for the vehicle device daemon, which
 * exposes itself on the bus and connects to the device daemon on the bus. It
 * creates a #CsrVehicleManager which will automatically expose vehicles which
 * are added to it on the bus.
 *
 * Since: 0.2.0
 */
struct _CsrBackend
{
  CsrService parent;

  gchar *backend_path;  /* owned */
  CsrVehicleService *vehicle_service;  /* owned */
  CsrVehicleManager *vehicle_manager;  /* owned */
  guint watch_id;
};

typedef enum
{
  PROP_BACKEND_PATH = 1,
} CsrBackendProperty;

G_DEFINE_TYPE (CsrBackend, csr_backend, CSR_TYPE_SERVICE)

static void
csr_backend_class_init (CsrBackendClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  CsrServiceClass *service_class = (CsrServiceClass *) klass;
  GParamSpec *props[PROP_BACKEND_PATH + 1] = { NULL, };

  object_class->dispose = csr_backend_dispose;
  object_class->get_property = csr_backend_get_property;
  object_class->set_property = csr_backend_set_property;

  service_class->startup_async = csr_backend_startup_async;
  service_class->startup_finish = csr_backend_startup_finish;
  service_class->shutdown = csr_backend_shutdown;

  /**
   * CsrBackend:backend-path:
   *
   * D-Bus object path to expose the vehicle service on. This must be specific
   * to your backend, and must use reverse-DNS notation prefixed with
   * `/org/apertis/Rhosydd1/Backends/`.
   *
   * For example, `/org/apertis/Rhosydd1/Backends/Speedo`.
   *
   * Since: 0.2.0
   */
  props[PROP_BACKEND_PATH] =
      g_param_spec_string ("backend-path", "Backend Path",
                           "D-Bus object path to expose the vehicle service "
                           "on.",
                           NULL,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);
}

static void
csr_backend_init (CsrBackend *self)
{
  self->vehicle_manager = csr_vehicle_manager_new ();
}

static void
csr_backend_dispose (GObject *object)
{
  CsrBackend *self = CSR_BACKEND (object);

  g_clear_pointer (&self->backend_path, g_free);
  g_clear_object (&self->vehicle_manager);
  g_clear_object (&self->vehicle_service);

  if (self->watch_id != 0)
    {
      g_bus_unwatch_name (self->watch_id);
      self->watch_id = 0;
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS (csr_backend_parent_class)->dispose (object);
}

static void
csr_backend_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  CsrBackend *self = CSR_BACKEND (object);

  switch ((CsrBackendProperty) property_id)
    {
    case PROP_BACKEND_PATH:
      g_value_set_string (value, self->backend_path);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
csr_backend_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  CsrBackend *self = CSR_BACKEND (object);

  switch ((CsrBackendProperty) property_id)
    {
    case PROP_BACKEND_PATH:
      /* Construct only. */
      g_assert (self->backend_path == NULL);
      g_assert (g_variant_is_object_path (g_value_get_string (value)));
      self->backend_path = g_value_dup_string (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

/**
 * csr_backend_new:
 * @service_id: unique, well-known name to identify the service
 * @backend_path: D-Bus object path to use for the main object exposed by the
 *     backend, for example `/com/my_vendor/sensors/SpeedoBackend1`
 * @translation_domain: gettext translation domain to use for command line
 *     help; typically `GETTEXT_PACKAGE`
 * @summary: human-readable summary of what the backend does, such as what
 *     devices it provides access to
 *
 * Create a new #CsrBackend.
 *
 * Returns: (transfer full): a new #CsrBackend
 * Since: 0.2.0
 */
CsrBackend *
csr_backend_new (const gchar *service_id,
                 const gchar *backend_path,
                 const gchar *translation_domain,
                 const gchar *summary)
{
  return g_object_new (CSR_TYPE_BACKEND,
                       "bus-type", G_BUS_TYPE_SYSTEM,
                       "service-id", service_id,
                       "backend-path", backend_path,
                       "translation-domain", translation_domain,
                       "parameter-string", _("— vehicle device daemon backend"),
                       "summary", summary,
                       NULL);
}

static void
daemon_disappeared_cb (GDBusConnection *connection,
                       const gchar     *name,
                       gpointer         user_data)
{
  CsrService *service = CSR_SERVICE (user_data);
  g_autoptr (GError) error = NULL;

  g_set_error_literal (&error, CSR_SERVICE_ERROR,
                       CSR_SERVICE_ERROR_NAME_UNAVAILABLE,
                       _("Daemon disappeared from D-Bus; exiting."));
  csr_service_exit (service, error, 0);
}

static void
csr_backend_startup_async (CsrService          *service,
                           GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data)
{
  CsrBackend *self = CSR_BACKEND (service);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GError) child_error = NULL;
  GDBusConnection *connection;
  g_autoptr (CsrPeerManager) peer_manager = NULL;
  g_autoptr (CsrSubscriptionManager) subscription_manager = NULL;
  g_autoptr (CsrVehicleService) vehicle_service = NULL;

  task = g_task_new (service, cancellable, callback, user_data);
  g_task_set_source_tag (task, csr_backend_startup_async);

  /* Create a vehicle manager and export it on D-Bus. */
  peer_manager = CSR_PEER_MANAGER (csr_backend_manager_new ());
  subscription_manager = csr_subscription_manager_new ();
  connection = csr_service_get_dbus_connection (CSR_SERVICE (self));

  vehicle_service = csr_vehicle_service_new (connection,
                                             self->backend_path,
                                             self->vehicle_manager,
                                             peer_manager,
                                             subscription_manager);

  if (!csr_vehicle_service_register (vehicle_service, &child_error))
    {
      g_task_return_new_error (task, CSR_SERVICE_ERROR,
                               CSR_SERVICE_ERROR_NAME_UNAVAILABLE,
                               _("Error registering objects on bus: %s"),
                               child_error->message);
      return;
    }

  /* Watch for the daemon. */
  self->vehicle_service = g_steal_pointer (&vehicle_service);

  self->watch_id = g_bus_watch_name_on_connection (connection,
                                                   "org.apertis.Rhosydd1",
                                                   G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                                   NULL,
                                                   daemon_disappeared_cb,
                                                   self, NULL);

  g_task_return_boolean (task, TRUE);
}

static void
csr_backend_startup_finish (CsrService    *service,
                            GAsyncResult  *result,
                            GError       **error)
{
  g_task_propagate_boolean (G_TASK (result), error);
}

static void
csr_backend_shutdown (CsrService *service)
{
  CsrBackend *self = CSR_BACKEND (service);

  csr_vehicle_service_unregister (self->vehicle_service);

  g_bus_unwatch_name (self->watch_id);
  self->watch_id = 0;
}

/**
 * csr_backend_get_vehicle_manager:
 * @self: a #CsrBackend
 *
 * Get the #CsrVehicleManager used by the backend. To add a vehicle to be
 * exposed by the backend, add it to this vehicle manager.
 *
 * Returns: (transfer none): the vehicle manager
 * Since: 0.2.0
 */
CsrVehicleManager *
csr_backend_get_vehicle_manager (CsrBackend *self)
{
  g_return_val_if_fail (CSR_IS_BACKEND (self), NULL);

  return self->vehicle_manager;
}
