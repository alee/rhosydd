/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <errno.h>
#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include <string.h>

#include "libcroesor/object-manager-interface-generated.c"
#include "libcroesor/vehicle-manager.h"
#include "libcroesor/vehicle-service.h"
#include "libinternal/logging.h"
#include "librhosydd/static-zone.h"
#include "librhosydd/subscription.h"
#include "librhosydd/utilities.h"
#include "librhosydd/vehicle-interface-generated.c"
#include "librhosydd/vehicle.h"
#include "librhosydd/zone.h"


static void csr_vehicle_service_constructed  (GObject *object);
static void csr_vehicle_service_dispose      (GObject      *object);
static void csr_vehicle_service_get_property (GObject      *object,
                                              guint         property_id,
                                              GValue       *value,
                                              GParamSpec   *pspec);
static void csr_vehicle_service_set_property (GObject      *object,
                                              guint         property_id,
                                              const GValue *value,
                                              GParamSpec   *pspec);

static gchar **csr_vehicle_service_vehicle_enumerate (GDBusConnection *connection,
                                                      const gchar     *sender,
                                                      const gchar     *object_path,
                                                      gpointer         user_data);
static GDBusInterfaceInfo **csr_vehicle_service_vehicle_introspect (GDBusConnection *connection,
                                                                    const gchar     *sender,
                                                                    const gchar     *object_path,
                                                                    const gchar     *node,
                                                                    gpointer         user_data);
static const GDBusInterfaceVTable *csr_vehicle_service_vehicle_dispatch (GDBusConnection *connection,
                                                                         const gchar     *sender,
                                                                         const gchar     *object_path,
                                                                         const gchar     *interface_name,
                                                                         const gchar     *node,
                                                                         gpointer        *out_user_data,
                                                                         gpointer         user_data);
static void csr_vehicle_service_vehicle_method_call (GDBusConnection       *connection,
                                                     const gchar           *sender,
                                                     const gchar           *object_path,
                                                     const gchar           *interface_name,
                                                     const gchar           *method_name,
                                                     GVariant              *parameters,
                                                     GDBusMethodInvocation *invocation,
                                                     gpointer               user_data);

static void csr_vehicle_service_vehicle_properties_get     (CsrVehicleService     *self,
                                                            RsdVehicle            *vehicle,
                                                            GDBusConnection       *connection,
                                                            const gchar           *sender,
                                                            GVariant              *parameters,
                                                            GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_properties_set     (CsrVehicleService     *self,
                                                            RsdVehicle            *vehicle,
                                                            GDBusConnection       *connection,
                                                            const gchar           *sender,
                                                            GVariant              *parameters,
                                                            GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_properties_get_all (CsrVehicleService     *self,
                                                            RsdVehicle            *vehicle,
                                                            GDBusConnection       *connection,
                                                            const gchar           *sender,
                                                            GVariant              *parameters,
                                                            GDBusMethodInvocation *invocation);

static PolkitDetails *csr_vehicle_service_vehicle_get_attribute_details (GVariant              *parameters,
                                                                         GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_get_attribute (CsrVehicleService     *self,
                                                       RsdVehicle            *vehicle,
                                                       GDBusConnection       *connection,
                                                       const gchar           *sender,
                                                       GVariant              *parameters,
                                                       GDBusMethodInvocation *invocation);
static PolkitDetails *csr_vehicle_service_vehicle_set_attribute_details (GVariant              *parameters,
                                                                         GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_set_attribute (CsrVehicleService     *self,
                                                       RsdVehicle            *vehicle,
                                                       GDBusConnection       *connection,
                                                       const gchar           *sender,
                                                       GVariant              *parameters,
                                                       GDBusMethodInvocation *invocation);
static PolkitDetails *csr_vehicle_service_vehicle_get_all_attributes_details (GVariant              *parameters,
                                                                              GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_get_all_attributes (CsrVehicleService     *self,
                                                            RsdVehicle            *vehicle,
                                                            GDBusConnection       *connection,
                                                            const gchar           *sender,
                                                            GVariant              *parameters,
                                                            GDBusMethodInvocation *invocation);
static PolkitDetails *csr_vehicle_service_vehicle_get_attribute_metadata_details (GVariant              *parameters,
                                                                                  GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_get_attribute_metadata (CsrVehicleService     *self,
                                                                RsdVehicle            *vehicle,
                                                                GDBusConnection       *connection,
                                                                const gchar           *sender,
                                                                GVariant              *parameters,
                                                                GDBusMethodInvocation *invocation);
static PolkitDetails *csr_vehicle_service_vehicle_get_all_attributes_metadata_details (GVariant              *parameters,
                                                                                       GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_get_all_attributes_metadata (CsrVehicleService     *self,
                                                                     RsdVehicle            *vehicle,
                                                                     GDBusConnection       *connection,
                                                                     const gchar           *sender,
                                                                     GVariant              *parameters,
                                                                     GDBusMethodInvocation *invocation);
static void csr_vehicle_service_vehicle_update_subscriptions (CsrVehicleService     *self,
                                                              RsdVehicle            *vehicle,
                                                              GDBusConnection       *connection,
                                                              const gchar           *sender,
                                                              GVariant              *parameters,
                                                              GDBusMethodInvocation *invocation);

static void csr_vehicle_service_object_manager_method_call (GDBusConnection       *connection,
                                                            const gchar           *sender,
                                                            const gchar           *object_path,
                                                            const gchar           *interface_name,
                                                            const gchar           *method_name,
                                                            GVariant              *parameters,
                                                            GDBusMethodInvocation *invocation,
                                                            gpointer               user_data);

static void csr_vehicle_service_object_manager_properties_get     (CsrVehicleService     *self,
                                                                   GDBusConnection       *connection,
                                                                   const gchar           *sender,
                                                                   GVariant              *parameters,
                                                                   GDBusMethodInvocation *invocation);
static void csr_vehicle_service_object_manager_properties_set     (CsrVehicleService     *self,
                                                                   GDBusConnection       *connection,
                                                                   const gchar           *sender,
                                                                   GVariant              *parameters,
                                                                   GDBusMethodInvocation *invocation);
static void csr_vehicle_service_object_manager_properties_get_all (CsrVehicleService     *self,
                                                                   GDBusConnection       *connection,
                                                                   const gchar           *sender,
                                                                   GVariant              *parameters,
                                                                   GDBusMethodInvocation *invocation);

static void csr_vehicle_service_object_manager_get_managed_objects (CsrVehicleService     *self,
                                                                    GDBusConnection       *connection,
                                                                    const gchar           *sender,
                                                                    GVariant              *parameters,
                                                                    GDBusMethodInvocation *invocation);

static GPtrArray *hash_table_values_to_ptr_array (GHashTable *table);

static gchar *vehicle_id_to_object_path (CsrVehicleService *self,
                                         const gchar       *vehicle_id);
static GVariant *zones_to_variant (GPtrArray/*<owned RsdZone>*/ *zones);

static void vehicles_changed_cb                    (CsrVehicleManager *manager,
                                                    GPtrArray         *added,
                                                    GPtrArray         *removed,
                                                    gpointer           user_data);
static void vehicle_attributes_changed_cb          (RsdVehicle               *vehicle,
                                                    RsdTimestampMicroseconds  current_time,
                                                    GPtrArray                *changed_attributes,
                                                    GPtrArray                *invalidated_attributes,
                                                    gpointer                  user_data);
static void vehicle_attributes_metadata_changed_cb (RsdVehicle               *vehicle,
                                                    RsdTimestampMicroseconds  current_time,
                                                    GPtrArray                *changed_attributes,
                                                    gpointer                  user_data);
static void peer_vanished_cb                       (GDBusConnection   *connection,
                                                    const gchar       *name,
                                                    gpointer           user_data);

/**
 * CsrVehicleService:
 *
 * An implementation of a D-Bus interface to expose a collection of vehicles on
 * the bus. This will expose all the necessary objects on the bus for peers
 * to interact with them, and hooks them up to internal state management
 * using #CsrVehicleService:vehicle-manager.
 *
 * Since: 0.1.0
 */
struct _CsrVehicleService
{
  GObject parent;

  GDBusConnection *connection;  /* owned */
  gchar *object_path;  /* owned */
  guint vehicle_subtree_id;

  /* Used to cancel any pending operations when the object is unregistered. */
  GCancellable *cancellable;  /* owned */

  CsrVehicleManager *vehicle_manager;  /* owned */
  CsrPeerManager *peer_manager;  /* owned */
  CsrSubscriptionManager *subscription_manager;  /* owned */
};

typedef enum
{
  PROP_CONNECTION = 1,
  PROP_OBJECT_PATH,
  PROP_VEHICLE_MANAGER,
  PROP_PEER_MANAGER,
  PROP_SUBSCRIPTION_MANAGER,
} CsrVehicleServiceProperty;

G_DEFINE_TYPE (CsrVehicleService, csr_vehicle_service, G_TYPE_OBJECT)

static void
csr_vehicle_service_class_init (CsrVehicleServiceClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GParamSpec *props[PROP_SUBSCRIPTION_MANAGER + 1] = { NULL, };

  object_class->constructed = csr_vehicle_service_constructed;
  object_class->dispose = csr_vehicle_service_dispose;
  object_class->get_property = csr_vehicle_service_get_property;
  object_class->set_property = csr_vehicle_service_set_property;

  /**
   * CsrVehicleService:connection:
   *
   * D-Bus connection to export objects on.
   *
   * Since: 0.1.0
   */
  props[PROP_CONNECTION] =
      g_param_spec_object ("connection", "Connection",
                           "D-Bus connection to export objects on.",
                           G_TYPE_DBUS_CONNECTION,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * CsrVehicleService:object-path:
   *
   * Object path to root all exported objects at. If this does not end in a
   * slash, one will be added.
   *
   * Since: 0.1.0
   */
  props[PROP_OBJECT_PATH] =
      g_param_spec_string ("object-path", "Object Path",
                           "Object path to root all exported objects at.",
                           "/",
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * CsrVehicleService:vehicle-manager:
   *
   * Vehicle manager to contain all vehicle instances.
   *
   * Since: 0.1.0
   */
  props[PROP_VEHICLE_MANAGER] =
      g_param_spec_object ("vehicle-manager", "Vehicle Manager",
                           "Vehicle manager to contain all vehicle instances.",
                           CSR_TYPE_VEHICLE_MANAGER,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * CsrVehicleService:peer-manager:
   *
   * Peer manager to track all known peers.
   *
   * Since: 0.2.0
   */
  props[PROP_PEER_MANAGER] =
      g_param_spec_object ("peer-manager", "Peer Manager",
                           "Peer manager to track all known peers.",
                           CSR_TYPE_PEER_MANAGER,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * CsrVehicleService:subscription-manager:
   *
   * Subscription manager to track all connections between peers and vehicles.
   *
   * Since: 0.2.0
   */
  props[PROP_SUBSCRIPTION_MANAGER] =
      g_param_spec_object ("subscription-manager", "Subscription Manager",
                           "Subscription manager to track all connections "
                           "between peers and vehicles.",
                           CSR_TYPE_SUBSCRIPTION_MANAGER,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);
}

static void
csr_vehicle_service_init (CsrVehicleService *self)
{
  self->cancellable = g_cancellable_new ();
}

static void
csr_vehicle_service_constructed (GObject *object)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (object);
  GHashTable/*<unowned utf8, unowned RsdVehicle>*/ *vehicles;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) vehicles_array = NULL;

  /* Chain up. */
  G_OBJECT_CLASS (csr_vehicle_service_parent_class)->constructed (object);

  /* Connect to the initial set of vehicles. */
  vehicles = csr_vehicle_manager_get_vehicles (self->vehicle_manager);
  vehicles_array = hash_table_values_to_ptr_array (vehicles);
  vehicles_changed_cb (self->vehicle_manager, vehicles_array, NULL, self);
}

static void
csr_vehicle_service_dispose (GObject *object)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (object);

  g_assert (self->vehicle_subtree_id == 0);

  /* Disconnect from signals from the vehicle manager, and from any remaining
   * vehicles. */
  if (self->vehicle_manager != NULL)
    {
      GHashTable/*<unowned utf8, unowned RsdVehicle>*/ *vehicles;
      g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) vehicles_array = NULL;

      g_signal_handlers_disconnect_by_data (self->vehicle_manager, self);

      vehicles = csr_vehicle_manager_get_vehicles (self->vehicle_manager);
      vehicles_array = hash_table_values_to_ptr_array (vehicles);
      vehicles_changed_cb (self->vehicle_manager, NULL, vehicles_array, self);
    }

  g_clear_object (&self->vehicle_manager);

  if (self->peer_manager != NULL)
    g_signal_handlers_disconnect_by_data (self->peer_manager, self);

  g_clear_object (&self->peer_manager);

  if (self->subscription_manager != NULL)
    g_signal_handlers_disconnect_by_data (self->subscription_manager, self);

  g_clear_object (&self->subscription_manager);

  g_clear_object (&self->connection);
  g_clear_pointer (&self->object_path, g_free);
  g_clear_object (&self->cancellable);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (csr_vehicle_service_parent_class)->dispose (object);
}

static void
csr_vehicle_service_get_property (GObject    *object,
                                  guint       property_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (object);

  switch ((CsrVehicleServiceProperty) property_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->connection);
      break;
    case PROP_OBJECT_PATH:
      g_value_set_string (value, self->object_path);
      break;
    case PROP_VEHICLE_MANAGER:
      g_value_set_object (value, self->vehicle_manager);
      break;
    case PROP_PEER_MANAGER:
      g_value_set_object (value, self->peer_manager);
      break;
    case PROP_SUBSCRIPTION_MANAGER:
      g_value_set_object (value, self->subscription_manager);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
csr_vehicle_service_set_property (GObject      *object,
                                  guint         property_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (object);

  switch ((CsrVehicleServiceProperty) property_id)
    {
    case PROP_CONNECTION:
      /* Construct only. */
      g_assert (self->connection == NULL);
      self->connection = g_value_dup_object (value);
      break;
    case PROP_OBJECT_PATH:
      /* Construct only. */
      g_assert (self->object_path == NULL);
      g_assert (g_variant_is_object_path (g_value_get_string (value)));
      self->object_path = g_value_dup_string (value);
      break;
    case PROP_VEHICLE_MANAGER: {
      /* Construct only. */
      g_assert (self->vehicle_manager == NULL);
      self->vehicle_manager = g_value_dup_object (value);

      /* Connect to signals from the vehicle manager. Connect to the initial
       * set of vehicles in constructed(). */
      g_signal_connect (self->vehicle_manager, "vehicles-changed",
                        (GCallback) vehicles_changed_cb, self);

      break;
    }
    case PROP_PEER_MANAGER: {
      /* Construct only. */
      g_assert (self->peer_manager == NULL);
      self->peer_manager = g_value_dup_object (value);

      /* Connect to signals from the peer manager. */
      g_signal_connect (self->peer_manager, "peer-vanished",
                        (GCallback) peer_vanished_cb, self);

      break;
    }
    case PROP_SUBSCRIPTION_MANAGER: {
      /* Construct only. */
      g_assert (self->subscription_manager == NULL);
      self->subscription_manager = g_value_dup_object (value);
      break;
    }
    default:
      g_assert_not_reached ();
    }
}

static GPtrArray *
hash_table_values_to_ptr_array (GHashTable *table)
{
  GHashTableIter iter;
  g_autoptr (GPtrArray) out = NULL;
  gpointer value;

  out = g_ptr_array_new ();
  g_hash_table_iter_init (&iter, table);

  while (g_hash_table_iter_next (&iter, NULL, &value))
    g_ptr_array_add (out, value);

  return g_steal_pointer (&out);
}

static void
vehicles_changed_cb (CsrVehicleManager *manager,
                     GPtrArray         *added,
                     GPtrArray         *removed,
                     gpointer           user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  gsize i;
  g_autoptr (GError) error = NULL;

  /* Update the subscriptions manager. */
  csr_subscription_manager_update_vehicles (self->subscription_manager,
                                            added, removed);

  /* Update signal subscriptions for the added and removed vehicles. */
  for (i = 0; removed != NULL && i < removed->len; i++)
    {
      RsdVehicle *vehicle = RSD_VEHICLE (removed->pdata[i]);

      DEBUG ("Removing vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));

      g_signal_handlers_disconnect_by_data (vehicle, self);
    }

  for (i = 0; added != NULL && i < added->len; i++)
    {
      RsdVehicle *vehicle = RSD_VEHICLE (added->pdata[i]);

      DEBUG ("Adding vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));

      g_signal_connect (vehicle, "attributes-changed",
                        (GCallback) vehicle_attributes_changed_cb, self);
      g_signal_connect (vehicle, "attributes-metadata-changed",
                        (GCallback) vehicle_attributes_metadata_changed_cb,
                        self);
    }

  /* Emit ObjectManager signals accordingly. Do this as a second step so that
   * we have our internal state consistent first. Emit as a broadcast, as
   * changes to the set of vehicles are relevant to all clients. */
  for (i = 0; removed != NULL && i < removed->len; i++)
    {
      RsdVehicle *vehicle = RSD_VEHICLE (removed->pdata[i]);
      g_autoptr (GVariant) params = NULL;
      g_autofree gchar *vehicle_path = NULL;

      vehicle_path = vehicle_id_to_object_path (self,
                                                rsd_vehicle_get_id (vehicle));
      params = g_variant_new_parsed ("(%o, ["
                                       "'org.apertis.Rhosydd1.Vehicle',"
                                       "'org.freedesktop.DBus.Introspectable',"
                                       "'org.freedesktop.DBus.Peer',"
                                       "'org.freedesktop.DBus.Properties'"
                                     "])", vehicle_path);
      g_dbus_connection_emit_signal (self->connection,
                                     NULL,
                                     self->object_path,
                                     "org.freedesktop.DBus.ObjectManager",
                                     "InterfacesRemoved",
                                     g_steal_pointer (&params),
                                     &error);

      if (error != NULL)
        WARNING ("Error emitting org.freedesktop.DBus.ObjectManager."
                 "InterfacesRemoved signal on ‘%s’: %s", self->object_path,
                 error->message);
      g_clear_error (&error);
    }

  for (i = 0; added != NULL && i < added->len; i++)
    {
      RsdVehicle *vehicle = RSD_VEHICLE (added->pdata[i]);
      g_autoptr (GVariant) params = NULL;
      g_autofree gchar *vehicle_path = NULL;
      const gchar *vehicle_id;
      g_autoptr (GPtrArray/*<owned RsdZone>*/) zones = NULL;

      vehicle_id = rsd_vehicle_get_id (vehicle);
      zones = rsd_vehicle_get_zones (vehicle);

      vehicle_path = vehicle_id_to_object_path (self, vehicle_id);
      params = g_variant_new_parsed ("(%o, {"
                                       "'org.apertis.Rhosydd1.Vehicle': {"
                                         "'Zones': <%@as>,"
                                         "'VehicleId': <%s>"
                                       "},"
                                       "'org.freedesktop.DBus.Introspectable':"
                                       " @a{sv} [],"
                                       "'org.freedesktop.DBus.Peer': @a{sv} [],"
                                       "'org.freedesktop.DBus.Properties':"
                                       " @a{sv} []"
                                     "})", vehicle_path,
                                     zones_to_variant (zones), vehicle_id);
      g_dbus_connection_emit_signal (self->connection,
                                     NULL,
                                     self->object_path,
                                     "org.freedesktop.DBus.ObjectManager",
                                     "InterfacesAdded",
                                     g_steal_pointer (&params),
                                     &error);

      if (error != NULL)
        WARNING ("Error emitting org.freedesktop.DBus.ObjectManager."
                 "InterfacesAdded signal on ‘%s’: %s", self->object_path,
                 error->message);
      g_clear_error (&error);
    }
}

static void
vehicle_attributes_changed_cb (RsdVehicle               *vehicle,
                               RsdTimestampMicroseconds  current_time,
                               GPtrArray                *changed_attributes,
                               GPtrArray                *invalidated_attributes,
                               gpointer                  user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) changed_attributes_variant = NULL;
  g_autoptr (GVariant) invalidated_attributes_variant = NULL;
  g_autoptr (GVariant) parameters = NULL;
  g_autofree gchar *vehicle_path = NULL;
  g_autoptr (GPtrArray/*<owned gchar *>*/) peers = NULL;
  gsize i;

  if (changed_attributes->len == 0 && invalidated_attributes->len == 0)
    return;

  /* Propagate the signal as a D-Bus signal. Expect no errors, since the
   * documentation says this can only fail if the GVariant type is wrong.
   *
   * Emit as unicast signals to each subscribed client. */
  changed_attributes_variant = rsd_attribute_info_array_to_variant (changed_attributes);
  invalidated_attributes_variant = rsd_attribute_metadata_array_to_variant (invalidated_attributes);

  parameters = g_variant_new ("(x@a(ss(vdx)(uu))@a(ss(uu)))",
                              current_time,
                              g_steal_pointer (&changed_attributes_variant),
                              g_steal_pointer (&invalidated_attributes_variant));
  g_variant_ref_sink (parameters);
  vehicle_path = vehicle_id_to_object_path (self, rsd_vehicle_get_id (vehicle));

  peers = csr_subscription_manager_get_peers_for_subscriptions (self->subscription_manager,
                                                                vehicle,
                                                                changed_attributes,
                                                                invalidated_attributes,
                                                                current_time);

  for (i = 0; i < peers->len; i++)
    {
      const gchar *peer_name = peers->pdata[i];

      g_dbus_connection_emit_signal (self->connection,
                                     peer_name,  /* unicast */
                                     vehicle_path,
                                     "org.apertis.Rhosydd1.Vehicle",
                                     "AttributesChanged",
                                     parameters,
                                     &error);
      g_assert_no_error (error);
    }
}

static void
vehicle_attributes_metadata_changed_cb (RsdVehicle               *vehicle,
                                        RsdTimestampMicroseconds  current_time,
                                        GPtrArray                *changed_attributes,
                                        gpointer                  user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) changed_attributes_variant = NULL;
  g_autoptr (GVariant) parameters = NULL;
  g_autofree gchar *vehicle_path = NULL;
  g_autoptr (GPtrArray/*<owned gchar *>*/) peers = NULL;
  gsize i;

  if (changed_attributes->len == 0)
    return;

  /* Propagate the signal as a D-Bus signal. Expect no errors, since the
   * documentation says this can only fail if the GVariant type is wrong.
   *
   * Emit as unicast signals to each subscribed client. */
  changed_attributes_variant = rsd_attribute_metadata_array_to_variant (changed_attributes);

  parameters = g_variant_new ("(x@a(ss(uu)))",
                              current_time,
                              g_steal_pointer (&changed_attributes_variant));
  g_variant_ref_sink (parameters);
  vehicle_path = vehicle_id_to_object_path (self, rsd_vehicle_get_id (vehicle));

  peers = csr_subscription_manager_get_peers_for_subscriptions (self->subscription_manager,
                                                                vehicle,
                                                                changed_attributes,
                                                                NULL,
                                                                current_time);

  for (i = 0; i < peers->len; i++)
    {
      const gchar *peer_name = peers->pdata[i];

      g_dbus_connection_emit_signal (self->connection,
                                     peer_name,  /* unicast */
                                     vehicle_path,
                                     "org.apertis.Rhosydd1.Vehicle",
                                     "AttributesMetadataChanged",
                                     parameters,
                                     &error);
      g_assert_no_error (error);
    }
}

/**
 * csr_vehicle_service_register:
 * @self: a #CsrVehicleService
 * @error: return location for a #GError
 *
 * Register the vehicle service objects on D-Bus using the connection details
 * given in #CsrVehicleService:connection and #CsrVehicleService:object-path.
 *
 * Use csr_vehicle_service_unregister() to unregister them. Calls to these two
 * functions must be well paired.
 *
 * Returns: %TRUE on success, %FALSE otherwise
 * Since: 0.1.0
 */
gboolean
csr_vehicle_service_register (CsrVehicleService  *self,
                              GError            **error)
{
  guint id;
  GDBusSubtreeVTable subtree_vtable = {
    csr_vehicle_service_vehicle_enumerate,
    csr_vehicle_service_vehicle_introspect,
    csr_vehicle_service_vehicle_dispatch,
  };

  g_return_val_if_fail (CSR_IS_VEHICLE_SERVICE (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  id = g_dbus_connection_register_subtree (self->connection,
                                           self->object_path,
                                           &subtree_vtable,
                                           G_DBUS_SUBTREE_FLAGS_NONE,
                                           g_object_ref (self),
                                           g_object_unref,
                                           error);

  if (id == 0)
    return FALSE;

  self->vehicle_subtree_id = id;

  return TRUE;
}

/**
 * csr_vehicle_service_unregister:
 * @self: a #CsrVehicleService
 *
 * Unregister objects from D-Bus which were previously registered using
 * csr_vehicle_service_register(). Calls to these two functions must be well
 * paired.
 *
 * Since: 0.1.0
 */
void
csr_vehicle_service_unregister (CsrVehicleService *self)
{
  g_return_if_fail (CSR_IS_VEHICLE_SERVICE (self));

  g_dbus_connection_unregister_subtree (self->connection,
                                        self->vehicle_subtree_id);
  self->vehicle_subtree_id = 0;
}

static gchar **
csr_vehicle_service_vehicle_enumerate (GDBusConnection *connection,
                                       const gchar     *sender,
                                       const gchar     *object_path,
                                       gpointer         user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  GPtrArray/*<owned gchar*>*/ *paths = NULL;
  GHashTable/*<unowned utf8, unowned RsdVehicle>*/ *vehicles;
  GHashTableIter iter;
  const gchar *vehicle_id;

  /* FIXME: Should check for the org.apertis.rhosydd1.enumerate-vehicles
   * permission here, but that would mean that any D-Bus peer which wants to
   * interact with this service must have the permission, which makes it
   * equivalent to the AppArmor permission to talk to this service, so what is
   * the point? Implementing an enumerate-vehicles check here would also be
   * difficult due to this method being sync, but authorization checks being
   * async. */
  vehicles = csr_vehicle_manager_get_vehicles (self->vehicle_manager);

  /* Output a list of vehicle objects. */
  paths = g_ptr_array_new_with_free_func (g_free);

  g_hash_table_iter_init (&iter, vehicles);
  while (g_hash_table_iter_next (&iter, (gpointer *) &vehicle_id, NULL))
    g_ptr_array_add (paths, g_strdup (vehicle_id));
  g_ptr_array_add (paths, NULL);  /* terminator */

  return (gchar **) g_ptr_array_free (paths, FALSE);
}

static RsdVehicle *
object_path_to_vehicle (CsrVehicleService *self,
                        const gchar       *object_path)
{
  /* Convert the object path into a vehicle ID and check it’s known to the
   * vehicle manager. */
  return csr_vehicle_manager_get_vehicle (self->vehicle_manager, object_path);
}

static gchar *
vehicle_id_to_object_path (CsrVehicleService *self,
                           const gchar       *vehicle_id)
{
  gboolean ends_in_slash;
  gchar *out = NULL;

  g_return_val_if_fail (rsd_vehicle_id_is_valid (vehicle_id), NULL);

  ends_in_slash = g_str_has_suffix (self->object_path, "/");

  out = g_strconcat (self->object_path, ends_in_slash ? "" : "/",
                     vehicle_id, NULL);
  g_return_val_if_fail (g_variant_is_object_path (out), NULL);

  return out;
}

static GDBusInterfaceInfo **
csr_vehicle_service_vehicle_introspect (GDBusConnection *connection,
                                        const gchar     *sender,
                                        const gchar     *object_path,
                                        const gchar     *node,
                                        gpointer         user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  GDBusInterfaceInfo **interfaces = NULL;

  /* Don’t implement any permissions checks here, as they should be specific to
   * the APIs being called and objects being accessed. */

  if (node == NULL)
    {
      /* The root node implements org.fdo.DBus.ObjectManager only. */
      interfaces = g_new0 (GDBusInterfaceInfo *, 2);
      interfaces[0] = (GDBusInterfaceInfo *) &csrpriv_org_freedesktop_dbus_object_manager_interface;
      interfaces[1] = NULL;
    }
  else if (object_path_to_vehicle (self, node) != NULL)
    {
      /* Build the array of interfaces which are implemented by the vehicle
      * object. */
      interfaces = g_new0 (GDBusInterfaceInfo *, 2);
      interfaces[0] = (GDBusInterfaceInfo *) &rsdpriv_rhosydd1_vehicle_interface;
      interfaces[1] = NULL;
    }

  return interfaces;
}

static const GDBusInterfaceVTable *
csr_vehicle_service_vehicle_dispatch (GDBusConnection *connection,
                                      const gchar     *sender,
                                      const gchar     *object_path,
                                      const gchar     *interface_name,
                                      const gchar     *node,
                                      gpointer        *out_user_data,
                                      gpointer         user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  RsdVehicle *vehicle;
  static const GDBusInterfaceVTable vehicle_interface_vtable = {
    csr_vehicle_service_vehicle_method_call,
    NULL,  /* handled in csr_vehicle_service_vehicle_method_call() */
    NULL,  /* handled in csr_vehicle_service_vehicle_method_call() */
  };
  static const GDBusInterfaceVTable object_manager_interface_vtable = {
    csr_vehicle_service_object_manager_method_call,
    NULL,  /* handled in csr_vehicle_service_object_manager_method_call() */
    NULL,  /* handled in csr_vehicle_service_object_manager_method_call() */
  };

  /* Don’t implement any permissions checks here, as they should be specific to
   * the APIs being called and objects being accessed. */

  /* ObjectManager is implemented on the root of the tree. */
  if (node == NULL &&
      g_strcmp0 (interface_name, "org.freedesktop.DBus.ObjectManager") == 0)
    {
      *out_user_data = user_data;
      return &object_manager_interface_vtable;
    }
  else if (node == NULL)
    {
      return NULL;
    }

  /* We only handle the Vehicle interface on other objects. */
  if (g_strcmp0 (interface_name, "org.apertis.Rhosydd1.Vehicle") != 0)
    return NULL;

  /* Find the vehicle. */
  vehicle = object_path_to_vehicle (self, node);

  if (vehicle == NULL)
    return NULL;

  *out_user_data = user_data;
  return &vehicle_interface_vtable;
}

typedef void (*VehicleMethodCallFunc) (CsrVehicleService     *self,
                                       RsdVehicle            *vehicle,
                                       GDBusConnection       *connection,
                                       const gchar           *sender,
                                       GVariant              *parameters,
                                       GDBusMethodInvocation *invocation);
typedef PolkitDetails *(*MethodDetailsFunc) (GVariant              *parameters,
                                             GDBusMethodInvocation *invocation);

static const struct {
  const gchar *interface_name;
  const gchar *method_name;
  const gchar *action_id;  /* **must** be defined in daemon/org.apertis.Rhosydd1.policy; nullable */
  const gchar *message;  /* translatable */
  MethodDetailsFunc details_func;  /* nullable */
  VehicleMethodCallFunc func;
} vehicle_methods[] = {
  /* Handle properties. We have to do this here so we can handle them
   * asynchronously for authorisation checks. */
  { "org.freedesktop.DBus.Properties", "Get",
    "org.apertis.rhosydd1.enumerate-vehicles", N_("Get vehicle ID or zones."),
    NULL,
    csr_vehicle_service_vehicle_properties_get },
  { "org.freedesktop.DBus.Properties", "Set",
    NULL  /* read-only */, N_("Set vehicle ID or zones."),
    NULL,
    csr_vehicle_service_vehicle_properties_set },
  { "org.freedesktop.DBus.Properties", "GetAll",
    "org.apertis.rhosydd1.enumerate-vehicles", N_("Get vehicle ID or zones."),
    NULL,
    csr_vehicle_service_vehicle_properties_get_all },

  /* Vehicle methods. */
  { "org.apertis.Rhosydd1.Vehicle", "GetAttribute",
    "org.apertis.rhosydd1.get-attribute",
    N_("Get attribute $(attribute_name) in zone $(zone_path)."),
    csr_vehicle_service_vehicle_get_attribute_details,
    csr_vehicle_service_vehicle_get_attribute },
  { "org.apertis.Rhosydd1.Vehicle", "SetAttibute",
    "org.apertis.rhosydd1.set-attribute",
    N_("Set attribute $(attribute_name) in zone $(zone_path)."),
    csr_vehicle_service_vehicle_set_attribute_details,
    csr_vehicle_service_vehicle_set_attribute },
  { "org.apertis.Rhosydd1.Vehicle", "GetAllAttributes",
    "org.apertis.rhosydd1.get-attribute",
    N_("Get all attributes in zone $(zone_path)."),
    csr_vehicle_service_vehicle_get_all_attributes_details,
    csr_vehicle_service_vehicle_get_all_attributes },
  { "org.apertis.Rhosydd1.Vehicle", "GetAttributeMetadata",
    "org.apertis.rhosydd1.enumerate-attributes",
    N_("Get metadata for attribute $(attribute_name) in zone $(zone_path)."),
    csr_vehicle_service_vehicle_get_attribute_metadata_details,
    csr_vehicle_service_vehicle_get_attribute_metadata },
  { "org.apertis.Rhosydd1.Vehicle", "GetAllAttributesMetadata",
    "org.apertis.rhosydd1.enumerate-attributes",
    N_("Get metadata for all attributes in zone $(zone_path)."),
    csr_vehicle_service_vehicle_get_all_attributes_metadata_details,
    csr_vehicle_service_vehicle_get_all_attributes_metadata },
  { "org.apertis.Rhosydd1.Vehicle", "UpdateSubscriptions",
    "org.apertis.rhosydd1.get-attribute",
    N_("Update subscriptions for attributes."),
    NULL,
    csr_vehicle_service_vehicle_update_subscriptions },
};

G_STATIC_ASSERT (G_N_ELEMENTS (vehicle_methods) ==
                 G_N_ELEMENTS (rsdpriv_rhosydd1_vehicle_interface_methods) +
                 -1  /* NULL terminator */ +
                 3  /* o.fdo.DBus.Properties */);

/*
 * vehicle_method_call_to_polkit_action_id_and_details:
 * @invocation: a #GDBusMethodInvocation
 * @action_id: (out) (transfer none) (nullable): return location for the action
 *    ID, which may be %NULL if no authorisation is needed for this action
 *
 * Utility function to convert an incoming #GDBusMethodInvocation to a
 * #PolkitDetails instance giving more details for polkit authorisation checks,
 * plus an action ID which corresponds to it for polkit authorisation checks.
 *
 * NOTE: The parameters from the caller in @invocation have not yet been
 * validated, so the returned #PolkitDetails cannot be trusted by the polkit
 * policy implementation (JavaScript) to, for example, contain valid attribute
 * names.
 *
 * Returns: (transfer full) (nullable): a new #PolkitDetails
 */
static PolkitDetails *
vehicle_method_call_to_polkit_action_id_and_details (GDBusMethodInvocation  *invocation,
                                                     const gchar           **action_id)
{
  g_autoptr (PolkitDetails) details = NULL;
  const gchar *interface_name, *method_name;
  const gchar *message;
  GVariant *parameters;
  gsize i;

  interface_name = g_dbus_method_invocation_get_interface_name (invocation);
  method_name = g_dbus_method_invocation_get_method_name (invocation);
  parameters = g_dbus_method_invocation_get_parameters (invocation);

  /* Calculate the details for the given method. */
  for (i = 0; i < G_N_ELEMENTS (vehicle_methods); i++)
    {
      if (g_strcmp0 (vehicle_methods[i].interface_name, interface_name) == 0 &&
          g_strcmp0 (vehicle_methods[i].method_name, method_name) == 0)
        {
          *action_id = vehicle_methods[i].action_id;
          message = vehicle_methods[i].message;

          if (vehicle_methods[i].details_func != NULL)
            details = vehicle_methods[i].details_func (parameters, invocation);

          break;
        }
    }

  /* Make sure we actually called a method implementation. */
  g_assert (i < G_N_ELEMENTS (vehicle_methods));

  if (details == NULL)
    details = polkit_details_new ();

  /* Human-readable description of the action. */
  polkit_details_insert (details, "polkit.message", message);
  polkit_details_insert (details, "polkit.gettext_domain", GETTEXT_PACKAGE);

  return g_steal_pointer (&details);
}

static void
peer_vanished_cb (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);

  /* Remove the subscriptions for this peer. */
  csr_subscription_manager_remove_peer (self->subscription_manager, name);
}

typedef struct
{
  CsrVehicleService *service;  /* owned */
  GDBusMethodInvocation *invocation;  /* owned */
} MethodCallData;

static void
method_call_data_free (MethodCallData *data)
{
  g_clear_object (&data->service);
  g_clear_object (&data->invocation);
  g_free (data);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (MethodCallData, method_call_data_free)

static MethodCallData *
method_call_data_new (CsrVehicleService     *self,
                      GDBusMethodInvocation *invocation)
{
  g_autoptr (MethodCallData) data = NULL;

  data = g_new0 (MethodCallData, 1);
  data->service = g_object_ref (self);
  data->invocation = g_object_ref (invocation);

  return g_steal_pointer (&data);
}

static void vehicle_method_call_cb (GObject      *obj,
                                    GAsyncResult *result,
                                    gpointer      user_data);

/* Main handler for incoming D-Bus method calls. */
static void
csr_vehicle_service_vehicle_method_call (GDBusConnection       *connection,
                                         const gchar           *sender,
                                         const gchar           *object_path,
                                         const gchar           *interface_name,
                                         const gchar           *method_name,
                                         GVariant              *parameters,
                                         GDBusMethodInvocation *invocation,
                                         gpointer               user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  const gchar *action_id;
  g_autoptr (PolkitDetails) details = NULL;
  g_autoptr (MethodCallData) data = NULL;

  details = vehicle_method_call_to_polkit_action_id_and_details (invocation,
                                                                 &action_id);
  data = method_call_data_new (self, invocation);

  /* Iff (action_id == NULL), no authorisation is needed. */
  if (action_id != NULL)
    csr_peer_manager_check_authorization_async (self->peer_manager, sender,
                                                connection, action_id, details,
                                                self->cancellable,
                                                vehicle_method_call_cb,
                                                g_steal_pointer (&data));
  else
    vehicle_method_call_cb (G_OBJECT (self->peer_manager), NULL,
                            g_steal_pointer (&data));
}

static void
vehicle_method_call_cb (GObject      *obj,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  g_autoptr (MethodCallData) data = user_data;
  CsrVehicleService *self = data->service;
  GDBusMethodInvocation *invocation = data->invocation;
  GDBusConnection *connection;
  const gchar *sender;
  const gchar *object_path;
  const gchar *interface_name;
  const gchar *method_name;
  GVariant *parameters;
  RsdVehicle *vehicle;
  gsize i;
  g_autoptr (GError) error = NULL;

  connection = g_dbus_method_invocation_get_connection (invocation);
  sender = g_dbus_method_invocation_get_sender (invocation);
  object_path = g_dbus_method_invocation_get_object_path (invocation);
  interface_name = g_dbus_method_invocation_get_interface_name (invocation);
  method_name = g_dbus_method_invocation_get_method_name (invocation);
  parameters = g_dbus_method_invocation_get_parameters (invocation);

  /* We can be called with (result == NULL) if no authorisation check is
   * needed. */
  if (result != NULL)
    csr_peer_manager_check_authorization_finish (CSR_PEER_MANAGER (obj),
                                                 result, &error);

  /* Errors will be returned for all non-authorised cases, including polkit not
   * running, or authorisation being explicitly denied. */
  if (error != NULL)
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return;
    }

  /* Remove the service prefix from the path. */
  g_assert (g_str_has_prefix (object_path, self->object_path));
  g_assert (object_path[strlen (self->object_path)] == '/');

  vehicle = object_path_to_vehicle (self,
                                    object_path +
                                    strlen (self->object_path) + 1);
  g_assert (vehicle != NULL);

  /* Work out which method to call. */
  for (i = 0; i < G_N_ELEMENTS (vehicle_methods); i++)
    {
      if (g_strcmp0 (vehicle_methods[i].interface_name, interface_name) == 0 &&
          g_strcmp0 (vehicle_methods[i].method_name, method_name) == 0)
        {
          vehicle_methods[i].func (self, vehicle, connection, sender,
                                   parameters, invocation);
          break;
        }
    }

  /* Make sure we actually called a method implementation. GIO guarantees that
   * this function is only called with methods we’ve declared in the interface
   * info, so this should never fail. */
  g_assert (i < G_N_ELEMENTS (vehicle_methods));
}

static GVariant *
zones_to_variant (GPtrArray/*<owned RsdZone>*/ *zones)
{
  GVariantBuilder builder;
  gsize i;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("as"));

  for (i = 0; i < zones->len; i++)
    {
      RsdZone *zone = zones->pdata[i];
      g_variant_builder_add (&builder, "s", rsd_zone_get_path (zone));
    }

  return g_variant_builder_end (&builder);
}

static gboolean
validate_zone_path (GDBusMethodInvocation  *invocation,
                    const gchar            *zone_path,
                    RsdZone               **zone)
{
  if (!rsd_zone_path_is_valid (zone_path))
    {
      g_dbus_method_invocation_return_error (invocation, RSD_VEHICLE_ERROR,
                                             RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                                             _("Invalid zone ID ‘%s’."),
                                             zone_path);
      return FALSE;
    }

  /* Return an #RsdZone object for the path. */
  *zone = RSD_ZONE (rsd_static_zone_new (zone_path));

  return TRUE;
}

static gboolean
validate_attribute_name (GDBusMethodInvocation *invocation,
                         const gchar           *attribute_name)
{
  if (!rsd_attribute_name_is_valid (attribute_name))
    {
      g_dbus_method_invocation_return_error (invocation, RSD_VEHICLE_ERROR,
                                             RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                                             _("Invalid attribute name ‘%s’."),
                                             attribute_name);
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_attribute_value (GDBusMethodInvocation *invocation,
                          GVariant              *attribute_value)
{
  if (attribute_value == NULL || !g_variant_is_normal_form (attribute_value))
    {
      g_autofree gchar *str = NULL;

      if (attribute_value != NULL)
        str = g_variant_print (attribute_value, TRUE);

      g_dbus_method_invocation_return_error (invocation, RSD_VEHICLE_ERROR,
                                             RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE,
                                             _("Invalid attribute value ‘%s’."),
                                             str);
      return FALSE;
    }

  return TRUE;
}

static gboolean
validate_dbus_interface_name (GDBusMethodInvocation *invocation,
                              const gchar           *interface_name)
{
  if (!g_dbus_is_interface_name (interface_name))
    {
      g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                             G_DBUS_ERROR_UNKNOWN_INTERFACE,
                                             _("Invalid interface name ‘%s’."),
                                             interface_name);
      return FALSE;
    }

  return TRUE;
}

static void
csr_vehicle_service_vehicle_properties_get (CsrVehicleService     *self,
                                            RsdVehicle            *vehicle,
                                            GDBusConnection       *connection,
                                            const gchar           *sender,
                                            GVariant              *parameters,
                                            GDBusMethodInvocation *invocation)
{
  const gchar *interface_name, *property_name;
  g_autoptr (GVariant) value = NULL;

  g_variant_get (parameters, "(&s&s)", &interface_name, &property_name);

  /* D-Bus property names can be anything. */
  if (!validate_dbus_interface_name (invocation, interface_name))
    return;

  /* Try the property. */
  if (g_strcmp0 (interface_name, "org.apertis.Rhosydd1.Vehicle") == 0)
    {
      if (g_strcmp0 (property_name, "VehicleId") == 0)
        {
          value = g_variant_new_string (rsd_vehicle_get_id (vehicle));
        }
      else if (g_strcmp0 (property_name, "Zones") == 0)
        {
          g_autoptr (GPtrArray/*<owned RsdZone>*/) zones = NULL;

          zones = rsd_vehicle_get_zones (vehicle);
          value = zones_to_variant (zones);
        }
    }

  if (value != NULL)
    g_dbus_method_invocation_return_value (invocation,
                                           g_variant_new ("(v)", g_steal_pointer (&value)));
  else
    g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                           G_DBUS_ERROR_UNKNOWN_PROPERTY,
                                           _("Unknown property ‘%s.%s’."),
                                           interface_name, property_name);
}

static void
csr_vehicle_service_vehicle_properties_set (CsrVehicleService     *self,
                                            RsdVehicle            *vehicle,
                                            GDBusConnection       *connection,
                                            const gchar           *sender,
                                            GVariant              *parameters,
                                            GDBusMethodInvocation *invocation)
{
  const gchar *interface_name, *property_name;
  gboolean read_only = FALSE;

  g_variant_get (parameters, "(&s&sv)", &interface_name, &property_name, NULL);

  /* D-Bus property names can be anything. */
  if (!validate_dbus_interface_name (invocation, interface_name))
    return;

  /* Try the property. */
  if (g_strcmp0 (interface_name, "org.apertis.Rhosydd1.Vehicle") == 0)
    {
      if (g_strcmp0 (property_name, "VehicleId") == 0 ||
          g_strcmp0 (property_name, "Zones") == 0)
        read_only = TRUE;
    }

  if (read_only)
    g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                           G_DBUS_ERROR_PROPERTY_READ_ONLY,
                                           _("Attribute ‘%s.%s’ is read-only."),
                                           interface_name, property_name);
  else
    g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                           G_DBUS_ERROR_UNKNOWN_PROPERTY,
                                           _("Unknown property ‘%s.%s’."),
                                           interface_name, property_name);
}

static void
csr_vehicle_service_vehicle_properties_get_all (CsrVehicleService     *self,
                                                RsdVehicle            *vehicle,
                                                GDBusConnection       *connection,
                                                const gchar           *sender,
                                                GVariant              *parameters,
                                                GDBusMethodInvocation *invocation)
{
  const gchar *interface_name;
  g_autoptr (GVariantDict) dict = NULL;

  g_variant_get (parameters, "(&s)", &interface_name);

  if (!validate_dbus_interface_name (invocation, interface_name))
    return;

  /* Try the interface. */
  if (g_strcmp0 (interface_name, "org.apertis.Rhosydd1.Vehicle") == 0)
    {
      g_autoptr (GPtrArray/*<owned RsdZone>*/) zones = NULL;

      zones = rsd_vehicle_get_zones (vehicle);
      dict = g_variant_dict_new (NULL);

      g_variant_dict_insert (dict, "VehicleId",
                             "s", rsd_vehicle_get_id (vehicle));
      g_variant_dict_insert_value (dict, "Zones", zones_to_variant (zones));
    }

  if (dict != NULL)
    g_dbus_method_invocation_return_value (invocation,
                                           g_variant_new ("(@a{sv})",
                                                          g_variant_dict_end (dict)));
  else
    g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                           G_DBUS_ERROR_UNKNOWN_INTERFACE,
                                           _("Unknown interface ‘%s’."),
                                           interface_name);
}

static PolkitDetails *
csr_vehicle_service_vehicle_get_attribute_details (GVariant              *parameters,
                                                  GDBusMethodInvocation *invocation)
{
  g_autoptr (PolkitDetails) details = NULL;
  const gchar *zone_path, *attribute_name;

  g_variant_get (parameters, "(&s&s)", &zone_path, &attribute_name);

  details = polkit_details_new ();
  polkit_details_insert (details, "rhosydd1.attribute_name", attribute_name);
  polkit_details_insert (details, "rhosydd1.zone_path", zone_path);

  return g_steal_pointer (&details);
}

static void
get_attribute_cb (GObject      *obj,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GError *error = NULL;
  g_autoptr (RsdAttributeInfo) value = NULL;
  GDBusMethodInvocation *invocation = G_DBUS_METHOD_INVOCATION (user_data);
  RsdTimestampMicroseconds current_time;

  value = rsd_vehicle_get_attribute_finish (vehicle, result, &current_time,
                                            &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return;
    }

  g_dbus_method_invocation_return_value (invocation,
                                         g_variant_new ("(x(vdx)(uu))",
                                                        current_time,
                                                        value->attribute.value,
                                                        value->attribute.accuracy,
                                                        value->attribute.last_updated,
                                                        value->metadata.availability,
                                                        value->metadata.flags));
}

static void
csr_vehicle_service_vehicle_get_attribute (CsrVehicleService     *self,
                                           RsdVehicle            *vehicle,
                                           GDBusConnection       *connection,
                                           const gchar           *sender,
                                           GVariant              *parameters,
                                           GDBusMethodInvocation *invocation)
{
  const gchar *zone_path;
  const gchar *attribute_name;
  g_autoptr (RsdZone) zone = NULL;

  g_variant_get (parameters, "(&s&s)", &zone_path, &attribute_name);

  if (!validate_zone_path (invocation, zone_path, &zone) ||
      !validate_attribute_name (invocation, attribute_name))
    return;

  rsd_vehicle_get_attribute_async (vehicle, zone, attribute_name,
                                  self->cancellable, get_attribute_cb,
                                  invocation);
}

static PolkitDetails *
csr_vehicle_service_vehicle_set_attribute_details (GVariant              *parameters,
                                                   GDBusMethodInvocation *invocation)
{
  g_autoptr (PolkitDetails) details = NULL;
  const gchar *zone_path, *attribute_name;

  g_variant_get (parameters, "(&s&sv)", &zone_path, &attribute_name, NULL);

  details = polkit_details_new ();
  polkit_details_insert (details, "rhosydd1.attribute_name", attribute_name);
  polkit_details_insert (details, "rhosydd1.zone_path", zone_path);

  return g_steal_pointer (&details);
}

static void
set_attribute_cb (GObject      *obj,
                 GAsyncResult *result,
                 gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GError *error = NULL;
  GDBusMethodInvocation *invocation = G_DBUS_METHOD_INVOCATION (user_data);

  rsd_vehicle_set_attribute_finish (vehicle, result, &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return;
    }

  g_dbus_method_invocation_return_value (invocation, NULL);
}

static void
csr_vehicle_service_vehicle_set_attribute (CsrVehicleService     *self,
                                           RsdVehicle            *vehicle,
                                           GDBusConnection       *connection,
                                           const gchar           *sender,
                                           GVariant              *parameters,
                                           GDBusMethodInvocation *invocation)
{
  const gchar *zone_path;
  const gchar *attribute_name;
  g_autoptr (GVariant) value = NULL;
  g_autoptr (RsdZone) zone = NULL;

  g_variant_get (parameters, "(&s&sv)", &zone_path, &attribute_name, &value);

  if (!validate_zone_path (invocation, zone_path, &zone) ||
      !validate_attribute_name (invocation, attribute_name) ||
      !validate_attribute_value (invocation, value))
    return;

  rsd_vehicle_set_attribute_async (vehicle, zone, attribute_name, value,
                                  self->cancellable, set_attribute_cb,
                                  invocation);
}

static PolkitDetails *
csr_vehicle_service_vehicle_get_all_attributes_details (GVariant              *parameters,
                                                        GDBusMethodInvocation *invocation)
{
  g_autoptr (PolkitDetails) details = NULL;
  const gchar *zone_path;

  g_variant_get (parameters, "(&s)", &zone_path);

  details = polkit_details_new ();
  polkit_details_insert (details, "rhosydd1.zone_path", zone_path);

  /* FIXME: How do we limit to the attributes visible to this application? */

  return g_steal_pointer (&details);
}

static void
get_all_attributes_cb (GObject      *obj,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GError *error = NULL;
  GDBusMethodInvocation *invocation = G_DBUS_METHOD_INVOCATION (user_data);
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  RsdTimestampMicroseconds current_time;

  attributes = rsd_vehicle_get_all_attributes_finish (vehicle, result,
                                                      &current_time, &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return;
    }

  g_dbus_method_invocation_return_value (invocation,
                                         g_variant_new ("(x@a(ss(vdx)(uu)))",
                                                        current_time,
                                                        rsd_attribute_info_array_to_variant (attributes)));
}

static void
csr_vehicle_service_vehicle_get_all_attributes (CsrVehicleService     *self,
                                                RsdVehicle            *vehicle,
                                                GDBusConnection       *connection,
                                                const gchar           *sender,
                                                GVariant              *parameters,
                                                GDBusMethodInvocation *invocation)
{
  const gchar *zone_path;
  g_autoptr (RsdZone) zone = NULL;

  g_variant_get (parameters, "(&s)", &zone_path);

  if (!validate_zone_path (invocation, zone_path, &zone))
    return;

  rsd_vehicle_get_all_attributes_async (vehicle, zone, self->cancellable,
                                        get_all_attributes_cb, invocation);
}

static PolkitDetails *
csr_vehicle_service_vehicle_get_attribute_metadata_details (GVariant              *parameters,
                                                            GDBusMethodInvocation *invocation)
{
  g_autoptr (PolkitDetails) details = NULL;
  const gchar *zone_path, *attribute_name;

  g_variant_get (parameters, "(&s&s)", &zone_path, &attribute_name);

  details = polkit_details_new ();
  polkit_details_insert (details, "rhosydd1.attribute_name", attribute_name);
  polkit_details_insert (details, "rhosydd1.zone_path", zone_path);

  return g_steal_pointer (&details);
}

static void
get_attribute_metadata_cb (GObject      *obj,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GError *error = NULL;
  GDBusMethodInvocation *invocation = G_DBUS_METHOD_INVOCATION (user_data);
  g_autoptr (RsdAttributeInfo) attribute = NULL;
  RsdTimestampMicroseconds current_time;

  attribute = rsd_vehicle_get_attribute_finish (vehicle, result, &current_time,
                                                &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return;
    }

  /* Extract the metadata from the #RsdAttributeInfo. */
  g_dbus_method_invocation_return_value (invocation,
                                         g_variant_new ("(x(uu))",
                                                        current_time,
                                                        attribute->metadata.availability,
                                                        attribute->metadata.flags));
}

static void
csr_vehicle_service_vehicle_get_attribute_metadata (CsrVehicleService     *self,
                                                    RsdVehicle            *vehicle,
                                                    GDBusConnection       *connection,
                                                    const gchar           *sender,
                                                    GVariant              *parameters,
                                                    GDBusMethodInvocation *invocation)
{
  const gchar *zone_path;
  const gchar *attribute_name;
  g_autoptr (RsdZone) zone = NULL;

  g_variant_get (parameters, "(&s&s)", &zone_path, &attribute_name);

  if (!validate_zone_path (invocation, zone_path, &zone) ||
      !validate_attribute_name (invocation, attribute_name))
    return;

  rsd_vehicle_get_attribute_async (vehicle, zone, attribute_name,
                                  self->cancellable, get_attribute_metadata_cb,
                                  invocation);
}

static PolkitDetails *
csr_vehicle_service_vehicle_get_all_attributes_metadata_details (GVariant              *parameters,
                                                                 GDBusMethodInvocation *invocation)
{
  g_autoptr (PolkitDetails) details = NULL;
  const gchar *zone_path;

  g_variant_get (parameters, "(&s)", &zone_path);

  details = polkit_details_new ();
  polkit_details_insert (details, "rhosydd1.zone_path", zone_path);

  /* FIXME: How do we limit to the attributes visible to this application? */

  return g_steal_pointer (&details);
}

static void
get_all_attributes_metadata_cb (GObject      *obj,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GError *error = NULL;
  GDBusMethodInvocation *invocation = G_DBUS_METHOD_INVOCATION (user_data);
  g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) metadatas = NULL;
  RsdTimestampMicroseconds current_time;

  metadatas = rsd_vehicle_get_all_metadata_finish (vehicle, result,
                                                   &current_time, &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_take_error (invocation, error);
      return;
    }

  g_dbus_method_invocation_return_value (invocation,
                                         g_variant_new ("(x@a(ss(uu)))",
                                                        current_time,
                                                        rsd_attribute_metadata_array_to_variant (metadatas)));
}

static void
csr_vehicle_service_vehicle_get_all_attributes_metadata (CsrVehicleService     *self,
                                                         RsdVehicle            *vehicle,
                                                         GDBusConnection       *connection,
                                                         const gchar           *sender,
                                                         GVariant              *parameters,
                                                         GDBusMethodInvocation *invocation)
{
  const gchar *zone_path;
  g_autoptr (RsdZone) zone = NULL;

  g_variant_get (parameters, "(&s)", &zone_path);

  if (!validate_zone_path (invocation, zone_path, &zone))
    return;

  rsd_vehicle_get_all_metadata_async (vehicle, zone, self->cancellable,
                                      get_all_attributes_metadata_cb,
                                      invocation);
}

typedef struct
{
  CsrVehicleService *service;  /* owned */
  GDBusMethodInvocation *invocation;  /* owned */
  gchar *peer_name;  /* owned */
  RsdVehicle *vehicle;  /* owned */
  GPtrArray/*<owned RsdSubscription>*/ *subscriptions;  /* owned */
  GPtrArray/*<owned RsdSubscription>*/ *unsubscriptions;  /* owned */
} UpdateSubscriptionsData;

static void
update_subscriptions_data_free (UpdateSubscriptionsData *data)
{
  g_clear_object (&data->service);
  g_clear_object (&data->invocation);
  g_free (data->peer_name);
  g_clear_object (&data->vehicle);
  g_clear_pointer (&data->subscriptions, g_ptr_array_unref);
  g_clear_pointer (&data->unsubscriptions, g_ptr_array_unref);
  g_free (data);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (UpdateSubscriptionsData,
                               update_subscriptions_data_free)

static void
update_subscriptions_cb (GObject      *obj,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  g_autoptr (UpdateSubscriptionsData) data = user_data;
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  GError *error = NULL;

  rsd_vehicle_update_subscriptions_finish (vehicle, result, &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_take_error (data->invocation, error);
      return;
    }

  /* Update the subscriptions in the subscription manager. */
  csr_subscription_manager_update_subscriptions (data->service->subscription_manager,
                                                 data->vehicle, data->peer_name,
                                                 data->subscriptions,
                                                 data->unsubscriptions);

  g_dbus_method_invocation_return_value (data->invocation, NULL);
}

static void
csr_vehicle_service_vehicle_update_subscriptions (CsrVehicleService     *self,
                                                  RsdVehicle            *vehicle,
                                                  GDBusConnection       *connection,
                                                  const gchar           *sender,
                                                  GVariant              *parameters,
                                                  GDBusMethodInvocation *invocation)
{
  g_autoptr (UpdateSubscriptionsData) data = NULL;
  g_autoptr (GVariant) subscriptions_variant = NULL;
  g_autoptr (GVariant) unsubscriptions_variant = NULL;
  GPtrArray/*<owned RsdSubscription>*/ *subscriptions, *unsubscriptions;
  g_autoptr (GError) error = NULL;

  data = g_new0 (UpdateSubscriptionsData, 1);
  data->service = g_object_ref (self);
  data->invocation = g_object_ref (invocation);
  data->peer_name = g_strdup (sender);
  data->vehicle = g_object_ref (vehicle);

  subscriptions_variant = g_variant_get_child_value (parameters, 0);
  unsubscriptions_variant = g_variant_get_child_value (parameters, 1);

  subscriptions = rsd_subscription_array_from_variant (subscriptions_variant,
                                                       &error);
  if (error != NULL)
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return;
    }

  unsubscriptions = rsd_subscription_array_from_variant (unsubscriptions_variant,
                                                         &error);
  if (error != NULL)
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return;
    }

  data->subscriptions = subscriptions;
  data->unsubscriptions = unsubscriptions;

  rsd_vehicle_update_subscriptions_async (vehicle, subscriptions,
                                          unsubscriptions, self->cancellable,
                                          update_subscriptions_cb,
                                          g_steal_pointer (&data));
}

typedef void (*ObjectManagerMethodCallFunc) (CsrVehicleService     *self,
                                             GDBusConnection       *connection,
                                             const gchar           *sender,
                                             GVariant              *parameters,
                                             GDBusMethodInvocation *invocation);

static const struct {
  const gchar *interface_name;
  const gchar *method_name;
  const gchar *action_id;  /* **must** be defined in daemon/org.apertis.Rhosydd1.policy; nullable */
  const gchar *message;  /* translatable */
  MethodDetailsFunc details_func;  /* nullable */
  ObjectManagerMethodCallFunc func;
} object_manager_methods[] = {
  /* Handle properties. We have to do this here so we can handle them
   * asynchronously for authorisation checks. */
  { "org.freedesktop.DBus.Properties", "Get",
    "org.apertis.rhosydd1.enumerate-vehicles", N_("Get vehicle ID or zones."),
    NULL,
    csr_vehicle_service_object_manager_properties_get },
  { "org.freedesktop.DBus.Properties", "Set",
    "org.apertis.rhosydd1.enumerate-vehicles", N_("Set vehicle ID or zones."),
    NULL,
    csr_vehicle_service_object_manager_properties_set },
  { "org.freedesktop.DBus.Properties", "GetAll",
    "org.apertis.rhosydd1.enumerate-vehicles", N_("Get vehicle ID or zones."),
    NULL,
    csr_vehicle_service_object_manager_properties_get_all },

  /* ObjectManager methods. */
  { "org.freedesktop.DBus.ObjectManager", "GetManagedObjects",
    "org.apertis.rhosydd1.enumerate-vehicles",
    N_("List available vehicles."),
    NULL,
    csr_vehicle_service_object_manager_get_managed_objects },
};

G_STATIC_ASSERT (G_N_ELEMENTS (object_manager_methods) ==
                 G_N_ELEMENTS (csrpriv_org_freedesktop_dbus_object_manager_interface_methods) +
                 -1  /* NULL terminator */ +
                 3  /* o.fdo.DBus.Properties */);

/*
 * object_manager_method_call_to_polkit_action_id_and_details:
 * @invocation: a #GDBusMethodInvocation
 * @action_id: (out) (transfer none) (nullable): return location for the action
 *    ID, which may be %NULL if no authorisation is needed for this action
 *
 * Utility function to convert an incoming #GDBusMethodInvocation to a
 * #PolkitDetails instance giving more details for polkit authorisation checks,
 * plus an action ID which corresponds to it for polkit authorisation checks.
 *
 * NOTE: The parameters from the caller in @invocation have not yet been
 * validated, so the returned #PolkitDetails cannot be trusted by the polkit
 * policy implementation (JavaScript) to, for example, contain valid attribute
 * names.
 *
 * Returns: (transfer full) (nullable): a new #PolkitDetails
 */
static PolkitDetails *
object_manager_method_call_to_polkit_action_id_and_details (GDBusMethodInvocation  *invocation,
                                                            const gchar           **action_id)
{
  g_autoptr (PolkitDetails) details = NULL;
  const gchar *interface_name, *method_name;
  const gchar *message;
  GVariant *parameters;
  gsize i;

  interface_name = g_dbus_method_invocation_get_interface_name (invocation);
  method_name = g_dbus_method_invocation_get_method_name (invocation);
  parameters = g_dbus_method_invocation_get_parameters (invocation);

  /* Calculate the details for the given method. */
  for (i = 0; i < G_N_ELEMENTS (object_manager_methods); i++)
    {
      if (g_strcmp0 (object_manager_methods[i].interface_name, interface_name) == 0 &&
          g_strcmp0 (object_manager_methods[i].method_name, method_name) == 0)
        {
          *action_id = object_manager_methods[i].action_id;
          message = object_manager_methods[i].message;

          if (object_manager_methods[i].details_func != NULL)
            details = object_manager_methods[i].details_func (parameters, invocation);

          break;
        }
    }

  /* Make sure we actually called a method implementation. */
  g_assert (i < G_N_ELEMENTS (object_manager_methods));

  if (details == NULL)
    details = polkit_details_new ();

  /* Human-readable description of the action. */
  polkit_details_insert (details, "polkit.message", message);
  polkit_details_insert (details, "polkit.gettext_domain", GETTEXT_PACKAGE);

  return g_steal_pointer (&details);
}

static void object_manager_method_call_cb (GObject      *obj,
                                           GAsyncResult *result,
                                           gpointer      user_data);

static void
csr_vehicle_service_object_manager_method_call (GDBusConnection       *connection,
                                                const gchar           *sender,
                                                const gchar           *object_path,
                                                const gchar           *interface_name,
                                                const gchar           *method_name,
                                                GVariant              *parameters,
                                                GDBusMethodInvocation *invocation,
                                                gpointer               user_data)
{
  CsrVehicleService *self = CSR_VEHICLE_SERVICE (user_data);
  const gchar *action_id;
  g_autoptr (PolkitDetails) details = NULL;
  g_autoptr (MethodCallData) data = NULL;

  details = object_manager_method_call_to_polkit_action_id_and_details (invocation,
                                                                        &action_id);
  data = method_call_data_new (self, invocation);

  /* Iff (action_id == NULL), no authorisation is needed. */
  if (action_id != NULL)
    csr_peer_manager_check_authorization_async (self->peer_manager, sender,
                                                connection, action_id, details,
                                                self->cancellable,
                                                object_manager_method_call_cb,
                                                g_steal_pointer (&data));
  else
    object_manager_method_call_cb (G_OBJECT (self->peer_manager), NULL,
                                   g_steal_pointer (&data));
}

static void
object_manager_method_call_cb (GObject      *obj,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  g_autoptr (MethodCallData) data = user_data;
  CsrVehicleService *self = data->service;
  GDBusMethodInvocation *invocation = data->invocation;
  GDBusConnection *connection;
  const gchar *sender;
  const gchar *object_path;
  const gchar *interface_name;
  const gchar *method_name;
  GVariant *parameters;
  gsize i;
  g_autoptr (GError) error = NULL;

  connection = g_dbus_method_invocation_get_connection (invocation);
  sender = g_dbus_method_invocation_get_sender (invocation);
  object_path = g_dbus_method_invocation_get_object_path (invocation);
  interface_name = g_dbus_method_invocation_get_interface_name (invocation);
  method_name = g_dbus_method_invocation_get_method_name (invocation);
  parameters = g_dbus_method_invocation_get_parameters (invocation);

  /* We can be called with (result == NULL) if no authorisation check is
   * needed. */
  if (result != NULL)
    csr_peer_manager_check_authorization_finish (CSR_PEER_MANAGER (obj),
                                                 result, &error);

  /* Errors will be returned for all non-authorised cases, including polkit not
   * running, or authorisation being explicitly denied. */
  if (error != NULL)
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return;
    }

  /* Remove the service prefix from the path. */
  g_assert (g_strcmp0 (object_path, self->object_path) == 0);

  /* Work out which method to call. */
  for (i = 0; i < G_N_ELEMENTS (object_manager_methods); i++)
    {
      if (g_strcmp0 (object_manager_methods[i].interface_name,
                     interface_name) == 0 &&
          g_strcmp0 (object_manager_methods[i].method_name, method_name) == 0)
        {
          object_manager_methods[i].func (self, connection, sender,
                                          parameters, invocation);
          break;
        }
    }

  /* Make sure we actually called a method implementation. GIO guarantees that
   * this function is only called with methods we’ve declared in the interface
   * info, so this should never fail. */
  g_assert (i < G_N_ELEMENTS (object_manager_methods));
}

static void
csr_vehicle_service_object_manager_properties_get (CsrVehicleService     *self,
                                                   GDBusConnection       *connection,
                                                   const gchar           *sender,
                                                   GVariant              *parameters,
                                                   GDBusMethodInvocation *invocation)
{
  const gchar *interface_name, *property_name;

  g_variant_get (parameters, "(&s&s)", &interface_name, &property_name);

  /* D-Bus property names can be anything. */
  if (!validate_dbus_interface_name (invocation, interface_name))
    return;

  /* No properties exposed. */
  g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                         G_DBUS_ERROR_UNKNOWN_PROPERTY,
                                         _("Unknown property ‘%s.%s’."),
                                         interface_name, property_name);
}

static void
csr_vehicle_service_object_manager_properties_set (CsrVehicleService     *self,
                                                   GDBusConnection       *connection,
                                                   const gchar           *sender,
                                                   GVariant              *parameters,
                                                   GDBusMethodInvocation *invocation)
{
  const gchar *interface_name, *property_name;

  g_variant_get (parameters, "(&s&sv)", &interface_name, &property_name, NULL);

  /* D-Bus property names can be anything. */
  if (!validate_dbus_interface_name (invocation, interface_name))
    return;

  /* No properties exposed. */
  g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                         G_DBUS_ERROR_UNKNOWN_PROPERTY,
                                         _("Unknown property ‘%s.%s’."),
                                         interface_name, property_name);
}

static void
csr_vehicle_service_object_manager_properties_get_all (CsrVehicleService     *self,
                                                       GDBusConnection       *connection,
                                                       const gchar           *sender,
                                                       GVariant              *parameters,
                                                       GDBusMethodInvocation *invocation)
{
  const gchar *interface_name;

  g_variant_get (parameters, "(&s)", &interface_name);

  if (!validate_dbus_interface_name (invocation, interface_name))
    return;

  /* Try the interface. */
  if (g_strcmp0 (interface_name, "org.apertis.Rhosydd1.Vehicle") == 0)
    g_dbus_method_invocation_return_value (invocation,
                                           g_variant_new_parsed ("(@a{sv} {})"));
  else
    g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR,
                                           G_DBUS_ERROR_UNKNOWN_INTERFACE,
                                           _("Unknown interface ‘%s’."),
                                           interface_name);
}

static void
csr_vehicle_service_object_manager_get_managed_objects (CsrVehicleService     *self,
                                                        GDBusConnection       *connection,
                                                        const gchar           *sender,
                                                        GVariant              *parameters,
                                                        GDBusMethodInvocation *invocation)
{
  GHashTable/*<unowned utf8, unowned RsdVehicle>*/ *vehicles;
  GHashTableIter iter;
  const gchar *vehicle_id;
  RsdVehicle *vehicle;
  GVariantBuilder builder;

  vehicles = csr_vehicle_manager_get_vehicles (self->vehicle_manager);

  /* Build the object list. */
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("(a{oa{sa{sv}}})"));
  g_variant_builder_open (&builder, G_VARIANT_TYPE ("a{oa{sa{sv}}}"));

  g_hash_table_iter_init (&iter, vehicles);
  while (g_hash_table_iter_next (&iter, (gpointer *) &vehicle_id,
                                 (gpointer *) &vehicle))
    {
      g_autofree gchar *vehicle_object_path = NULL;
      g_autoptr (GPtrArray/*<owned RsdZone>*/) zones = NULL;

      g_variant_builder_open (&builder, G_VARIANT_TYPE ("{oa{sa{sv}}}"));

      /* Object path. */
      vehicle_object_path = vehicle_id_to_object_path (self, vehicle_id);
      g_variant_builder_add (&builder, "o", vehicle_object_path);

      /* Interfaces. */
      g_variant_builder_open (&builder, G_VARIANT_TYPE ("a{sa{sv}}"));

      /* org.apertis.Rhosydd1.Vehicle. */
      zones = rsd_vehicle_get_zones (vehicle);
      g_variant_builder_add_parsed (&builder,
                                    "{'org.apertis.Rhosydd1.Vehicle', @a{sv} ["
                                      "{'Zones', <%@as>},"
                                      "{'VehicleId', <%s>}"
                                    "]}", zones_to_variant (zones), vehicle_id);

      /* Generic D-Bus interfaces. */
      g_variant_builder_add_parsed (&builder,
                                    "{'org.freedesktop.DBus.Introspectable',"
                                    " @a{sv} []}");
      g_variant_builder_add_parsed (&builder,
                                    "{'org.freedesktop.DBus.Peer', @a{sv} []}");
      g_variant_builder_add_parsed (&builder,
                                    "{'org.freedesktop.DBus.Properties',"
                                    " @a{sv} []}");

      g_variant_builder_close (&builder);
      g_variant_builder_close (&builder);
    }

  g_variant_builder_close (&builder);

  g_dbus_method_invocation_return_value (invocation,
                                         g_variant_builder_end (&builder));
}

/**
 * csr_vehicle_service_new:
 * @connection: (transfer none): D-Bus connection to export objects on
 * @object_path: root path to export objects below; must be a valid D-Bus object
 *    path
 * @vehicle_manager: (transfer none): vehicle manager to expose
 * @peer_manager: (transfer none): a peer manager to use
 * @subscription_manager: (transfer none): a subscription manager to use
 *
 * Create a new #CsrVehicleService instance which is set up to run as a service.
 *
 * Returns: (transfer full): a new #CsrVehicleService
 * Since: 0.2.0
 */
CsrVehicleService *
csr_vehicle_service_new (GDBusConnection        *connection,
                         const gchar            *object_path,
                         CsrVehicleManager      *vehicle_manager,
                         CsrPeerManager         *peer_manager,
                         CsrSubscriptionManager *subscription_manager)
{
  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);
  g_return_val_if_fail (g_variant_is_object_path (object_path), NULL);
  g_return_val_if_fail (CSR_IS_VEHICLE_MANAGER (vehicle_manager), NULL);
  g_return_val_if_fail (CSR_IS_PEER_MANAGER (peer_manager), NULL);
  g_return_val_if_fail (CSR_IS_SUBSCRIPTION_MANAGER (subscription_manager),
                        NULL);

  return g_object_new (CSR_TYPE_VEHICLE_SERVICE,
                       "connection", connection,
                       "object-path", object_path,
                       "vehicle-manager", vehicle_manager,
                       "peer-manager", peer_manager,
                       "subscription-manager", subscription_manager,
                       NULL);
}
