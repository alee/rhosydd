/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>

#include "libcroesor/static-vehicle.h"
#include "libinternal/arrays.h"
#include "librhosydd/static-zone.h"
#include "librhosydd/types.h"
#include "librhosydd/vehicle.h"
#include "librhosydd/zone.h"


static void csr_static_vehicle_vehicle_init (RsdVehicleInterface *iface);
static void csr_static_vehicle_dispose      (GObject             *object);
static void csr_static_vehicle_get_property (GObject             *object,
                                             guint                property_id,
                                             GValue              *value,
                                             GParamSpec          *pspec);
static void csr_static_vehicle_set_property (GObject             *object,
                                             guint                property_id,
                                             const GValue        *value,
                                             GParamSpec          *pspec);

static const gchar     *csr_static_vehicle_vehicle_get_id                    (RsdVehicle          *vehicle);
static GPtrArray       *csr_static_vehicle_vehicle_get_zones                 (RsdVehicle          *vehicle);
static void             csr_static_vehicle_vehicle_get_attribute_async       (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              const gchar         *attribute_name,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static RsdAttributeInfo *csr_static_vehicle_vehicle_get_attribute_finish     (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);

static void             csr_static_vehicle_vehicle_get_metadata_async        (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              const gchar         *attribute_name,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static RsdAttributeMetadata *csr_static_vehicle_vehicle_get_metadata_finish  (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);

static void             csr_static_vehicle_vehicle_get_all_attributes_async  (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static GPtrArray       *csr_static_vehicle_vehicle_get_all_attributes_finish (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);
static void             csr_static_vehicle_vehicle_get_all_metadata_async    (RsdVehicle          *vehicle,
                                                                              RsdZone             *zone,
                                                                              GCancellable        *cancellable,
                                                                              GAsyncReadyCallback  callback,
                                                                              gpointer             user_data);
static GPtrArray       *csr_static_vehicle_vehicle_get_all_metadata_finish   (RsdVehicle          *vehicle,
                                                                              GAsyncResult        *result,
                                                                              RsdTimestampMicroseconds *current_time,
                                                                              GError             **error);

/**
 * CsrStaticVehicle:
 *
 * An implementation of #RsdVehicle which presents a fixed set of attributes
 * with fixed values and metadata. All the attributes are exposed as read-only
 * (%RSD_ATTRIBUTE_READABLE only). The vehicle can expose zero or more
 * attributes, and they can be in any zone.
 *
 * This is intended for use in very simple backends, and in unit tests.
 *
 * Since: 0.1.0
 */
struct _CsrStaticVehicle
{
  GObject parent;

  gchar *id;  /* owned */
  GPtrArray/*<owned RsdAttributeInfo>*/ *attributes;  /* owned */

  CsrClockFunc clock_func;
  gpointer clock_user_data;
  GDestroyNotify clock_free_func;
};

typedef enum
{
  PROP_ATTRIBUTES = 1,
  /* Overridden properties: */
  PROP_ID,
} CsrStaticVehicleProperty;

G_DEFINE_TYPE_WITH_CODE (CsrStaticVehicle, csr_static_vehicle,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RSD_TYPE_VEHICLE,
                                                csr_static_vehicle_vehicle_init))

static void
csr_static_vehicle_class_init (CsrStaticVehicleClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GParamSpec *props[PROP_ATTRIBUTES + 1] = { NULL, };

  object_class->dispose = csr_static_vehicle_dispose;
  object_class->get_property = csr_static_vehicle_get_property;
  object_class->set_property = csr_static_vehicle_set_property;

  /**
   * CsrStaticVehicle:attributes: (type GLib.PtrArray(RsdAttributeInfo)) (transfer none)
   *
   * Attributes to expose in the vehicle. These may be in any zone.
   *
   * Since: 0.1.0
   */
  props[PROP_ATTRIBUTES] =
      g_param_spec_boxed ("attributes", "Attributes",
                           "Attributes to expose in the root zone of the "
                           "vehicle.",
                           G_TYPE_PTR_ARRAY,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);

  g_object_class_override_property (object_class, PROP_ID, "id");
}

static void
csr_static_vehicle_vehicle_init (RsdVehicleInterface *iface)
{
  iface->get_id = csr_static_vehicle_vehicle_get_id;
  iface->get_zones = csr_static_vehicle_vehicle_get_zones;
  iface->get_attribute_async = csr_static_vehicle_vehicle_get_attribute_async;
  iface->get_attribute_finish = csr_static_vehicle_vehicle_get_attribute_finish;
  iface->get_metadata_async = csr_static_vehicle_vehicle_get_metadata_async;
  iface->get_metadata_finish = csr_static_vehicle_vehicle_get_metadata_finish;
  iface->get_all_attributes_async = csr_static_vehicle_vehicle_get_all_attributes_async;
  iface->get_all_attributes_finish = csr_static_vehicle_vehicle_get_all_attributes_finish;
  iface->get_all_metadata_async = csr_static_vehicle_vehicle_get_all_metadata_async;
  iface->get_all_metadata_finish = csr_static_vehicle_vehicle_get_all_metadata_finish;
}

static void
csr_static_vehicle_init (CsrStaticVehicle *self)
{
  /* Set the default clock function. */
  csr_static_vehicle_set_clock_func (self, NULL, NULL, NULL);
}

static void
csr_static_vehicle_dispose (GObject *object)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (object);

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->attributes, g_ptr_array_unref);

  if (self->clock_user_data != NULL && self->clock_free_func != NULL)
    {
      self->clock_free_func (self->clock_user_data);
      self->clock_free_func = NULL;
      self->clock_user_data = NULL;
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS (csr_static_vehicle_parent_class)->dispose (object);
}

static void
csr_static_vehicle_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (object);

  switch ((CsrStaticVehicleProperty) property_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;
    case PROP_ATTRIBUTES:
      g_value_set_boxed (value, self->attributes);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
csr_static_vehicle_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (object);

  switch ((CsrStaticVehicleProperty) property_id)
    {
    case PROP_ID:
      /* Construct only. */
      g_assert (self->id == NULL);
      g_assert (rsd_vehicle_id_is_valid (g_value_get_string (value)));
      self->id = g_value_dup_string (value);
      break;
    case PROP_ATTRIBUTES:
      /* Construct only. */
      g_assert (self->attributes == NULL);
      self->attributes = g_value_dup_boxed (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

static const gchar *
csr_static_vehicle_vehicle_get_id (RsdVehicle *vehicle)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);

  return self->id;
}

static gint
zones_compare_cb (gconstpointer a,
                  gconstpointer b)
{
  RsdZone *zone_a, *zone_b;

  zone_a = *((RsdZone **) a);
  zone_b = *((RsdZone **) b);

  return g_strcmp0 (rsd_zone_get_path (zone_a),
                    rsd_zone_get_path (zone_b));
}

static GPtrArray *
csr_static_vehicle_vehicle_get_zones (RsdVehicle *vehicle)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);
  g_autoptr (GPtrArray/*<owned RsdZone>*/) retval = NULL;
  gsize i;

  retval = g_ptr_array_new_with_free_func (g_object_unref);

  /* Don’t worry about duplicates; we sort and uniquify this array below. */
  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *info = self->attributes->pdata[i];

      g_ptr_array_add (retval, rsd_static_zone_new (info->metadata.zone_path));
    }

  if (self->attributes->len == 0)
    {
      g_ptr_array_add (retval, rsd_static_zone_new ("/"));
    }

  /* Sort and uniqueify. */
  ptr_array_uniqueify (retval, zones_compare_cb);

  return g_steal_pointer (&retval);
}

static const RsdAttributeInfo *
find_attribute (CsrStaticVehicle  *self,
                RsdZone           *zone,
                const gchar       *attribute_name,
                GError           **error)
{
  const RsdAttributeInfo *attribute = NULL;
  gsize i;
  gboolean zone_seen = FALSE;

  /* Look up the attribute. */
  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *info = self->attributes->pdata[i];

      if (g_strcmp0 (info->metadata.name, attribute_name) == 0 &&
          g_strcmp0 (info->metadata.zone_path, rsd_zone_get_path (zone)) == 0)
        {
          attribute = info;
          break;
        }

      /* Have we seen the wanted zone at all? This affects error reporting
       * below. */
      zone_seen = (zone_seen ||
                   (g_strcmp0 (info->metadata.zone_path,
                               rsd_zone_get_path (zone)) == 0));
    }

  if (attribute == NULL && !zone_seen)
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_UNKNOWN_ZONE,
                   _("Zone ‘%s’ is not known."), rsd_zone_get_path (zone));
    }
  else if (attribute == NULL)
    {
      g_set_error (error, RSD_VEHICLE_ERROR,
                   RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                   _("Attribute ‘%s’ is not known in zone ‘%s’."),
                   attribute_name, rsd_zone_get_path (zone));
    }

  return attribute;
}

static void
csr_static_vehicle_vehicle_get_attribute_async (RsdVehicle          *vehicle,
                                                RsdZone             *zone,
                                                const gchar         *attribute_name,
                                                GCancellable        *cancellable,
                                                GAsyncReadyCallback  callback,
                                                gpointer             user_data)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  const RsdAttributeInfo *attribute = NULL;
  g_autoptr (GError) error = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         csr_static_vehicle_vehicle_get_attribute_async);

  attribute = find_attribute (self, zone, attribute_name, &error);

  if (attribute != NULL)
    g_task_return_pointer (task, rsd_attribute_info_copy (attribute),
                           (GDestroyNotify) rsd_attribute_info_free);
  else
    g_task_return_error (task, g_steal_pointer (&error));
}

static RsdAttributeInfo *
csr_static_vehicle_vehicle_get_attribute_finish (RsdVehicle                *vehicle,
                                                 GAsyncResult              *result,
                                                 RsdTimestampMicroseconds  *current_time,
                                                 GError                   **error)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);

  /* Technically this should be the time from the
   * csr_static_vehicle_vehicle_get_attribute_async() call, but this is
   * self-consistent so should be acceptable. */
  if (current_time != NULL)
    *current_time = self->clock_func (self->clock_user_data);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
csr_static_vehicle_vehicle_get_metadata_async (RsdVehicle          *vehicle,
                                               RsdZone             *zone,
                                               const gchar         *attribute_name,
                                               GCancellable        *cancellable,
                                               GAsyncReadyCallback  callback,
                                               gpointer             user_data)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  const RsdAttributeInfo *attribute = NULL;
  g_autoptr (GError) error = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         csr_static_vehicle_vehicle_get_metadata_async);

  attribute = find_attribute (self, zone, attribute_name, &error);

  if (attribute != NULL)
    g_task_return_pointer (task,
                           rsd_attribute_metadata_copy (&attribute->metadata),
                           (GDestroyNotify) rsd_attribute_metadata_free);
  else
    g_task_return_error (task, g_steal_pointer (&error));
}

static RsdAttributeMetadata *
csr_static_vehicle_vehicle_get_metadata_finish (RsdVehicle    *vehicle,
                                                GAsyncResult  *result,
                                                RsdTimestampMicroseconds *current_time,
                                                GError       **error)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);

  /* Technically this should be the time from the
   * csr_static_vehicle_vehicle_get_attribute_async() call, but this is
   * self-consistent so should be acceptable. */
  if (current_time != NULL)
    *current_time = self->clock_func (self->clock_user_data);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
csr_static_vehicle_vehicle_get_all_attributes_async (RsdVehicle          *vehicle,
                                                     RsdZone             *zone,
                                                     GCancellable        *cancellable,
                                                     GAsyncReadyCallback  callback,
                                                     gpointer             user_data)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  gsize i;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         csr_static_vehicle_vehicle_get_all_attributes_async);

  /* Grab all attributes. */
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *info = self->attributes->pdata[i];
      g_autoptr (RsdZone) attribute_zone = NULL;

      attribute_zone = RSD_ZONE (rsd_static_zone_new (info->metadata.zone_path));

      if (rsd_zone_is_descendant (attribute_zone, zone))
        g_ptr_array_add (attributes, rsd_attribute_info_copy (info));
    }

  g_task_return_pointer (task, g_steal_pointer (&attributes),
                         (GDestroyNotify) g_ptr_array_unref);
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
csr_static_vehicle_vehicle_get_all_attributes_finish (RsdVehicle                *vehicle,
                                                      GAsyncResult              *result,
                                                      RsdTimestampMicroseconds  *current_time,
                                                      GError                   **error)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);

  /* Technically this should be the time from the
   * csr_static_vehicle_vehicle_get_attribute_async() call, but this is
   * self-consistent so should be acceptable. */
  if (current_time != NULL)
    *current_time = self->clock_func (self->clock_user_data);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
csr_static_vehicle_vehicle_get_all_metadata_async (RsdVehicle          *vehicle,
                                                   RsdZone             *zone,
                                                   GCancellable        *cancellable,
                                                   GAsyncReadyCallback  callback,
                                                   gpointer             user_data)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) attributes = NULL;
  gsize i;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         csr_static_vehicle_vehicle_get_all_metadata_async);

  /* Grab all attributes. */
  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);

  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *info = self->attributes->pdata[i];
      g_autoptr (RsdAttributeMetadata) metadata = NULL;
      g_autoptr (RsdZone) attribute_zone = NULL;

      attribute_zone = RSD_ZONE (rsd_static_zone_new (info->metadata.zone_path));

      if (rsd_zone_is_descendant (attribute_zone, zone))
        {
          metadata = rsd_attribute_metadata_copy (&info->metadata);
          g_ptr_array_add (attributes, g_steal_pointer (&metadata));
        }
    }

  g_task_return_pointer (task, g_steal_pointer (&attributes),
                         (GDestroyNotify) g_ptr_array_unref);
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
csr_static_vehicle_vehicle_get_all_metadata_finish (RsdVehicle                *vehicle,
                                                    GAsyncResult              *result,
                                                    RsdTimestampMicroseconds  *current_time,
                                                    GError                   **error)
{
  CsrStaticVehicle *self = CSR_STATIC_VEHICLE (vehicle);

  /* Technically this should be the time from the
   * csr_static_vehicle_vehicle_get_attribute_async() call, but this is
   * self-consistent so should be acceptable. */
  if (current_time != NULL)
    *current_time = self->clock_func (self->clock_user_data);

  return g_task_propagate_pointer (G_TASK (result), error);
}

/**
 * csr_static_vehicle_new:
 * @id: ID for the vehicle
 * @attributes: (element-type RsdAttributeInfo) (transfer none):
 *    attributes for the vehicle, may be empty
 *
 * Create a new #CsrStaticVehicle with the given @id and @attributes.
 *
 * Returns: (transfer full): a new #CsrStaticVehicle
 * Since: 0.1.0
 */
CsrStaticVehicle *
csr_static_vehicle_new (const gchar *id,
                        GPtrArray   *attributes)
{
  g_return_val_if_fail (rsd_vehicle_id_is_valid (id), NULL);
  g_return_val_if_fail (attributes != NULL, NULL);

  return g_object_new (CSR_TYPE_STATIC_VEHICLE,
                       "id", id,
                       "attributes", attributes,
                       NULL);
}

static RsdTimestampMicroseconds
default_clock_cb (gpointer user_data)
{
  return g_get_monotonic_time ();
}

/**
 * csr_static_vehicle_set_clock_func:
 * @self: a #CsrStaticVehicle
 * @clock_func: (nullable): new clock function, or %NULL to use
 *    g_get_monotonic_time()
 * @user_data: user data to pass to @clock_func
 * @free_func: function to free @user_data once it is no longer needed
 *
 * Set the clock function to be used by the static vehicle. This will be called
 * whenever the vehicle needs to know the current time, for the purposes of
 * returning its current time to callers of methods on its #RsdVehicle
 * interface. This is used for unit testing, to allow the clock domain of a
 * #CsrStaticVehicle to be controlled.
 *
 * @clock_func should return the current time each time it is called. If
 * @clock_func is set to %NULL (the default), g_get_monotonic_time() will be
 * used.
 *
 * Since: 0.4.0
 */
void
csr_static_vehicle_set_clock_func (CsrStaticVehicle *self,
                                   CsrClockFunc      clock_func,
                                   gpointer          user_data,
                                   GDestroyNotify    free_func)
{
  g_return_if_fail (CSR_IS_STATIC_VEHICLE (self));
  g_return_if_fail (clock_func != NULL || user_data == NULL);

  if (clock_func != NULL)
    self->clock_func = clock_func;
  else
    self->clock_func = default_clock_cb;

  self->clock_user_data = user_data;
  self->clock_free_func = free_func;
}
