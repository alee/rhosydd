/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <canterbury/canterbury.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <polkit/polkit.h>

#include <libcroesor/peer-manager.h>
#include <libinternal/logging.h>

#include "backend-manager.h"


static void            csr_backend_manager_peer_manager_init          (CsrPeerManagerInterface  *iface);
static void            csr_backend_manager_dispose                    (GObject                  *object);

static void            csr_backend_manager_ensure_peer_info_async     (CsrPeerManager           *peer_manager,
                                                                       const gchar              *sender,
                                                                       GDBusConnection          *connection,
                                                                       GCancellable             *cancellable,
                                                                       GAsyncReadyCallback       callback,
                                                                       gpointer                  user_data);
static CbyProcessInfo *csr_backend_manager_ensure_peer_info_finish    (CsrPeerManager           *peer_manager,
                                                                       GAsyncResult             *result,
                                                                       GError                  **error);
static void            csr_backend_manager_check_authorization_async  (CsrPeerManager           *peer_manager,
                                                                       const gchar              *sender,
                                                                       GDBusConnection          *connection,
                                                                       const gchar              *action_id,
                                                                       PolkitDetails            *details,
                                                                       GCancellable             *cancellable,
                                                                       GAsyncReadyCallback       callback,
                                                                       gpointer                  user_data);
static void            csr_backend_manager_check_authorization_finish (CsrPeerManager           *peer_manager,
                                                                       GAsyncResult             *result,
                                                                       GError                  **error);

static void            csr_backend_manager_get_property               (GObject                  *object,
                                                                       guint                     property_id,
                                                                       GValue                   *value,
                                                                       GParamSpec               *pspec);
static void            csr_backend_manager_set_property               (GObject                  *object,
                                                                       guint                     property_id,
                                                                       const GValue             *value,
                                                                       GParamSpec               *pspec)
                                                                      G_GNUC_NORETURN;

static void            clear_daemon                                   (CsrBackendManager        *self);

/**
 * CsrBackendManager:
 *
 * An implementation of #CsrPeerManager which manages a single peer: the daemon.
 * The daemon is authenticated using its AppArmor label, and subsequently all
 * authorisation requests from it are allowed. All requests from other processes
 * are denied.
 *
 * This class does not perform any polkit checks; all authentication is based on
 * the peer’s AppArmor label.
 *
 * This is intended to be used as the #CsrPeerManager for a backend, which
 * expects to only receive requests from the daemon. Note that the
 * implementation as it currently stands does not allow for debug clients to
 * connect to the backend, for example. Support for this could be added in
 * future.
 *
 * Since: 0.4.0
 */
struct _CsrBackendManager
{
  GObject parent;

  /* Information about the daemon */
  gchar *daemon_unique_name;  /* owned; nullable */
  CbyProcessInfo *process_info;  /* owned; nullable */
  guint watch_id;
};

typedef enum
{
  /* Overridden properties: */
  PROP_PEERS = 1,
} CsrBackendManagerProperty;

G_DEFINE_TYPE_WITH_CODE (CsrBackendManager, csr_backend_manager, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (CSR_TYPE_PEER_MANAGER,
                                                csr_backend_manager_peer_manager_init))

static void
csr_backend_manager_class_init (CsrBackendManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = csr_backend_manager_dispose;
  object_class->get_property = csr_backend_manager_get_property;
  object_class->set_property = csr_backend_manager_set_property;

  g_object_class_override_property (object_class, PROP_PEERS, "peers");
}

static void
csr_backend_manager_init (CsrBackendManager *self)
{
  /* Nothing here. */
}

static void
csr_backend_manager_peer_manager_init (CsrPeerManagerInterface *iface)
{
  iface->ensure_peer_info_async = csr_backend_manager_ensure_peer_info_async;
  iface->ensure_peer_info_finish = csr_backend_manager_ensure_peer_info_finish;
  iface->check_authorization_async = csr_backend_manager_check_authorization_async;
  iface->check_authorization_finish = csr_backend_manager_check_authorization_finish;
}

static void
csr_backend_manager_dispose (GObject *object)
{
  CsrBackendManager *self = CSR_BACKEND_MANAGER (object);

  clear_daemon (self);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (csr_backend_manager_parent_class)->dispose (object);
}

static void
csr_backend_manager_get_property (GObject    *object,
                                  guint       property_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  CsrBackendManager *self = CSR_BACKEND_MANAGER (object);

  switch ((CsrBackendManagerProperty) property_id)
    {
    case PROP_PEERS:
      {
        const gchar *peers[2] = { NULL, };

        if (self->daemon_unique_name != NULL)
          peers[0] = self->daemon_unique_name;

        g_value_set_boxed (value, peers);
        break;
      }
    default:
      g_assert_not_reached ();
    }
}

static void
csr_backend_manager_set_property (GObject      *object,
                                  guint         property_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  switch ((CsrBackendManagerProperty) property_id)
    {
    case PROP_PEERS:
      /* Read only. */
      g_assert_not_reached ();
      break;
    default:
      g_assert_not_reached ();
    }
}

/**
 * csr_backend_manager_new:
 *
 * Create a new #CsrBackendManager with no initial peer.
 *
 * Returns: (transfer full): a new #CsrBackendManager
 * Since: 0.4.0
 */
CsrBackendManager *
csr_backend_manager_new (void)
{
  return g_object_new (CSR_TYPE_BACKEND_MANAGER, NULL);
}

static void ensure_peer_info_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data);
static void peer_vanished_cb    (GDBusConnection *connection,
                                 const gchar     *name,
                                 gpointer         user_data);

typedef struct
{
  gchar *sender;  /* owned */
  GDBusConnection *connection;  /* owned */
} EnsurePeerInfoData;

static void
ensure_peer_info_data_free (EnsurePeerInfoData *data)
{
  g_clear_pointer (&data->sender, g_free);
  g_clear_object (&data->connection);

  g_free (data);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (EnsurePeerInfoData, ensure_peer_info_data_free)

static void
csr_backend_manager_ensure_peer_info_async (CsrPeerManager      *peer_manager,
                                            const gchar         *sender,
                                            GDBusConnection     *connection,
                                            GCancellable        *cancellable,
                                            GAsyncReadyCallback  callback,
                                            gpointer             user_data)
{
  CsrBackendManager *self = CSR_BACKEND_MANAGER (peer_manager);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, csr_backend_manager_ensure_peer_info_async);

  if (g_strcmp0 (self->daemon_unique_name, sender) == 0)
    {
      g_task_return_pointer (task, g_object_ref (self->process_info),
                             g_object_unref);
    }
  else
    {
      g_autoptr (EnsurePeerInfoData) data = NULL;

      data = g_new0 (EnsurePeerInfoData, 1);
      data->sender = g_strdup (sender);
      data->connection = g_object_ref (connection);

      g_task_set_task_data (task, g_steal_pointer (&data),
                            (GDestroyNotify) ensure_peer_info_data_free);

      cby_process_info_new_for_dbus_sender_async (sender, connection,
                                                  cancellable,
                                                  ensure_peer_info_cb,
                                                  g_steal_pointer (&task));
    }
}

static void
ensure_peer_info_cb (GObject      *object,
                     GAsyncResult *result,
                     gpointer      user_data)
{
  CsrBackendManager *self;
  g_autoptr (GTask) task = G_TASK (user_data);
  EnsurePeerInfoData *data;
  g_autoptr (CbyProcessInfo) process_info = NULL;
  GError *error = NULL;

  self = CSR_BACKEND_MANAGER (g_task_get_source_object (task));
  process_info = cby_process_info_new_for_dbus_sender_finish (result, &error);
  data = g_task_get_task_data (task);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Check that the peer is actually the daemon. */
  if (cby_process_info_get_process_type (process_info) !=
      CBY_PROCESS_TYPE_PLATFORM ||
      g_strcmp0 (cby_process_info_get_apparmor_label (process_info),
                 "/usr/bin/rhosydd") != 0)
    {
      g_task_return_new_error (task, G_DBUS_ERROR,
                               G_DBUS_ERROR_ACCESS_DENIED,
                               _("Only the Rhosydd daemon can access vehicle "
                                 "data."));
      return;
    }

  /* If there were two concurrent calls to
   * csr_backend_manager_ensure_peer_info_async() the process info might have
   * been saved already; if so, just return it, since it’s constant. */
  if (self->daemon_unique_name == NULL)
    {
      self->daemon_unique_name = g_strdup (data->sender);
      self->process_info = g_object_ref (process_info);
      self->watch_id = g_bus_watch_name_on_connection (data->connection,
                                                       data->sender,
                                                       G_BUS_NAME_WATCHER_FLAGS_NONE,
                                                       NULL,
                                                       peer_vanished_cb, self,
                                                       NULL);

      g_object_notify (G_OBJECT (self), "peers");
    }

  g_task_return_pointer (task, g_object_ref (self->process_info),
                         g_object_unref);
}

static void
clear_daemon (CsrBackendManager *self)
{
  g_clear_object (&self->process_info);
  g_clear_pointer (&self->daemon_unique_name, g_free);

  if (self->watch_id != 0)
    {
      g_bus_unwatch_name (self->watch_id);
      self->watch_id = 0;
    }
}

static void
peer_vanished_cb (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
  CsrBackendManager *self = CSR_BACKEND_MANAGER (user_data);
  g_autofree gchar *name_copy = g_strdup (name);

  /* Signal the vanishing. Keep a copy of the name for the signal emission. */
  if (g_strcmp0 (self->daemon_unique_name, name) == 0)
    {
      DEBUG ("Daemon ‘%s’ vanished.", name_copy);

      clear_daemon (self);

      g_signal_emit_by_name (self, "peer-vanished", name_copy);
      g_object_notify (G_OBJECT (self), "peers");
    }
}

static CbyProcessInfo *
csr_backend_manager_ensure_peer_info_finish (CsrPeerManager  *peer_manager,
                                             GAsyncResult    *result,
                                             GError         **error)
{
  GTask *task = G_TASK (result);

  g_return_val_if_fail (g_task_is_valid (result, peer_manager), NULL);

  return g_task_propagate_pointer (task, error);
}

static void check_authorization_cb (GObject      *obj,
                                    GAsyncResult *result,
                                    gpointer      user_data);

static void
csr_backend_manager_check_authorization_async (CsrPeerManager      *peer_manager,
                                               const gchar         *sender,
                                               GDBusConnection     *connection,
                                               const gchar         *action_id,
                                               PolkitDetails       *details,
                                               GCancellable        *cancellable,
                                               GAsyncReadyCallback  callback,
                                               gpointer             user_data)
{
  CsrBackendManager *self = CSR_BACKEND_MANAGER (peer_manager);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, csr_backend_manager_check_authorization_async);

  csr_peer_manager_ensure_peer_info_async (peer_manager, sender, connection,
                                           cancellable,
                                           check_authorization_cb,
                                           g_steal_pointer (&task));
}

static void
check_authorization_cb (GObject      *obj,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyProcessInfo) process_info = NULL;

  /* Get the peer’s info. All the authorisation checks we want to impleemnt
   * (i.e. is this peer the daemon?) are performed by ensure_peer_info(). */
  process_info = csr_peer_manager_ensure_peer_info_finish (CSR_PEER_MANAGER (obj),
                                                           result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_boolean (task, TRUE);
}

static void
csr_backend_manager_check_authorization_finish (CsrPeerManager  *peer_manager,
                                                GAsyncResult    *result,
                                                GError         **error)
{
  GTask *task = G_TASK (result);

  g_return_if_fail (g_task_is_valid (result, peer_manager));

  g_task_propagate_boolean (task, error);
}
