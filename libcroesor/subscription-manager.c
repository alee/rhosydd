/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include "libcroesor/subscription-manager.h"
#include "libinternal/logging.h"
#include "librhosydd/subscription.h"
#include "librhosydd/vehicle.h"


static void csr_subscription_manager_dispose (GObject *object);

typedef struct
{
  RsdVehicle *vehicle;  /* owend */
  gchar *peer_name;  /* owned */
} VehiclePeerPair;

static void
vehicle_peer_pair_free (VehiclePeerPair *pair)
{
  g_clear_object (&pair->vehicle);
  g_clear_pointer (&pair->peer_name, g_free);
  g_free (pair);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (VehiclePeerPair, vehicle_peer_pair_free)

static guint
vehicle_peer_pair_hash (gconstpointer ptr)
{
  const VehiclePeerPair *pair = ptr;

  return (g_direct_hash (pair->vehicle) ^ g_str_hash (pair->peer_name));
}

static gboolean
vehicle_peer_pair_equal (gconstpointer a,
                         gconstpointer b)
{
  const VehiclePeerPair *pair_a = a;
  const VehiclePeerPair *pair_b = b;

  return (pair_a->vehicle == pair_b->vehicle &&
          g_strcmp0 (pair_a->peer_name, pair_b->peer_name) == 0);
}

/**
 * CsrSubscriptionManager:
 *
 * A manager object which stores the sets of subscriptions between pairs of
 * vehicles and peers, so that those subscriptions can be updated when vehicles
 * or peers are removed.
 *
 * This manager does not subscribe to signals from the vehicles it manages; the
 * owner is responsible for calling the #CsrSubscriptionManager methods in
 * response to events. This needed to keep this object testable.
 *
 * This differs from #RsdSubscriptionManager, which tracks the aggregate of all
 * the subscriptions for each zone path and attribute name in a particular
 * vehicle. It does not distinguish between subscriptions coming from different
 * peers, and only contains the subscriptions for a single vehicle. In contrast,
 * #CsrSubscriptionManager associates subscriptions with pairs of vehicles and
 * peers, and does not track the aggregate subscriptions for individual
 * attributes.
 *
 * Since: 0.2.0
 */
struct _CsrSubscriptionManager
{
  GObject parent;

  GHashTable/*<owned VehiclePeerPair, owned GPtrArray<owned RsdSubscription>>*/ *subscriptions;  /* owned */
  GCancellable *cancellable;  /* owned */
};

G_DEFINE_TYPE (CsrSubscriptionManager, csr_subscription_manager, G_TYPE_OBJECT)

static void
csr_subscription_manager_class_init (CsrSubscriptionManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = csr_subscription_manager_dispose;
}

static void
csr_subscription_manager_init (CsrSubscriptionManager *self)
{
  self->subscriptions = g_hash_table_new_full (vehicle_peer_pair_hash,
                                               vehicle_peer_pair_equal,
                                               (GDestroyNotify) vehicle_peer_pair_free,
                                               (GDestroyNotify) g_ptr_array_unref);
  self->cancellable = g_cancellable_new ();
}

static void
csr_subscription_manager_dispose (GObject *object)
{
  CsrSubscriptionManager *self = CSR_SUBSCRIPTION_MANAGER (object);

  /* Cancel any ongoing unsubscription operations; they will return themselves
   * afterwards, without holding any references to the
   * #CsrSubscriptionManager (otherwise dispose() would never have been
   * called). */
  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  g_clear_pointer (&self->subscriptions, g_hash_table_unref);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (csr_subscription_manager_parent_class)->dispose (object);
}

/**
 * csr_subscription_manager_new:
 *
 * Create a new #CsrSubscriptionManager instance, with no vehicles or peers to
 * begin with.
 *
 * Returns: (transfer full): a new #CsrSubscriptionManager
 * Since: 0.2.0
 */
CsrSubscriptionManager *
csr_subscription_manager_new (void)
{
  return g_object_new (CSR_TYPE_SUBSCRIPTION_MANAGER, NULL);
}

/**
 * csr_subscription_manager_update_vehicles:
 * @self: a #CsrSubscriptionManager
 * @added: (element-type RsdVehicle) (transfer none) (nullable): potentially
 *    empty array of added vehicles, or %NULL to signify an empty array
 * @removed: (element-type RsdVehicle) (transfer none) (nullable): potentially
 *    empty array of removed vehicles, or %NULL to signify an empty array
 *
 * Update the set of vehicles tracked by this subscription manager. Removed
 * vehicles have their subscriptions removed from the set of tracked
 * subscriptions.
 *
 * Since: 0.2.0
 */
void
csr_subscription_manager_update_vehicles (CsrSubscriptionManager *self,
                                          GPtrArray              *added,
                                          GPtrArray              *removed)
{
  gsize i;

  g_return_if_fail (CSR_IS_SUBSCRIPTION_MANAGER (self));

  for (i = 0; removed != NULL && i < removed->len; i++)
    {
      RsdVehicle *vehicle;
      VehiclePeerPair *pair;
      GHashTableIter iter;

      vehicle = RSD_VEHICLE (removed->pdata[i]);

      DEBUG ("Removing vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));

      /* For each peer, clean up this vehicle’s entries in the subscriptions
       * table. */
      g_hash_table_iter_init (&iter, self->subscriptions);

      while (g_hash_table_iter_next (&iter, (gpointer *) &pair, NULL))
        {
          if (pair->vehicle == vehicle)
            g_hash_table_iter_remove (&iter);
        }
    }

  /* Check that all the added vehicles are sane. */
  for (i = 0; added != NULL && i < added->len; i++)
    {
      RsdVehicle *vehicle;

      vehicle = RSD_VEHICLE (added->pdata[i]);

      DEBUG ("Adding vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));
    }
}

static void peer_unsubscribed_cb (GObject      *obj,
                                  GAsyncResult *result,
                                  gpointer      user_data);

/**
 * csr_subscription_manager_remove_peer:
 * @self: a #CsrSubscriptionManager
 * @peer_name: unique D-Bus name of the peer which has vanished
 *
 * Remove a peer from the set of peers being tracked, and notify all vehicles
 * which it was paired with that the relevant subscriptions have been removed,
 * by calling rsd_vehicle_update_subscriptions_async().
 *
 * Since: 0.2.0
 */
void
csr_subscription_manager_remove_peer (CsrSubscriptionManager *self,
                                      const gchar            *peer_name)
{
  VehiclePeerPair *pair;
  GHashTableIter iter;
  GPtrArray/*<owned RsdSubscription>*/ *unsubscriptions;

  g_return_if_fail (CSR_IS_SUBSCRIPTION_MANAGER (self));
  g_return_if_fail (peer_name != NULL && g_dbus_is_unique_name (peer_name));

  DEBUG ("Removing peer ‘%s’.", peer_name);

  g_hash_table_iter_init (&iter, self->subscriptions);

  while (g_hash_table_iter_next (&iter, (gpointer *) &pair,
                                 (gpointer *) &unsubscriptions))
    {
      if (g_strcmp0 (pair->peer_name, peer_name) == 0)
        {
          g_autoptr (GPtrArray) subscriptions = NULL;

          /* Note: Don't hold any references to @self here so that the pending
           * unsubscription calls can't hold the #CsrSubscriptionManager alive
           * indefinitely. */
          subscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
          rsd_vehicle_update_subscriptions_async (pair->vehicle, subscriptions,
                                                  unsubscriptions,
                                                  self->cancellable,
                                                  peer_unsubscribed_cb,
                                                  NULL);

          g_hash_table_iter_remove (&iter);
        }
    }
}

static void
peer_unsubscribed_cb (GObject      *obj,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  RsdVehicle *vehicle;
  g_autoptr (GError) error = NULL;

  vehicle = RSD_VEHICLE (obj);
  rsd_vehicle_update_subscriptions_finish (vehicle, result, &error);

  if (error != NULL)
    WARNING ("Error updating subscriptions for vanished client for vehicle "
             "‘%s’: %s", rsd_vehicle_get_id (vehicle), error->message);
}

/**
 * csr_subscription_manager_update_subscriptions:
 * @self: a #CsrSubscriptionManager
 * @vehicle: the vehicle whose subscriptions are being updated
 * @peer_name: the D-Bus peer whose subscriptions are being updated
 * @subscriptions: (element-type RsdSubscription) (transfer none): potentially
 *    empty array of subscriptions to add
 * @unsubscriptions: (element-type RsdSubscription) (transfer none): potentially
 *    empty array of subscriptions to remove
 *
 * Update the set of subscriptions being tracked for the given @vehicle and
 * @peer_name pair. Either of the @subscriptions and @unsubscriptions arrays
 * may be empty — for example, if a pairing is being added, the @unsubscriptions
 * array must be empty.
 *
 * Since: 0.2.0
 */
void
csr_subscription_manager_update_subscriptions (CsrSubscriptionManager *self,
                                               RsdVehicle             *vehicle,
                                               const gchar            *peer_name,
                                               GPtrArray              *subscriptions,
                                               GPtrArray              *unsubscriptions)
{
  VehiclePeerPair pair;
  GPtrArray/*<owned RsdSubscription>*/ *stored_subscriptions;
  gsize i, j;

  g_return_if_fail (CSR_IS_SUBSCRIPTION_MANAGER (self));
  g_return_if_fail (RSD_IS_VEHICLE (vehicle));
  g_return_if_fail (peer_name != NULL && g_dbus_is_unique_name (peer_name));
  g_return_if_fail (subscriptions != NULL);
  g_return_if_fail (unsubscriptions != NULL);

  pair.vehicle = vehicle;
  pair.peer_name = (gchar *) peer_name;

  stored_subscriptions = g_hash_table_lookup (self->subscriptions, &pair);

  if (stored_subscriptions == NULL && unsubscriptions->len > 0)
    WARNING ("Trying to unsubscribe from %u subscriptions which were never "
             "registered.", unsubscriptions->len);

  if (stored_subscriptions == NULL)
    {
      g_autoptr (VehiclePeerPair) key = NULL;

      stored_subscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);
      key = g_new0 (VehiclePeerPair, 1);
      key->vehicle = g_object_ref (vehicle);
      key->peer_name = g_strdup (peer_name);

      g_hash_table_insert (self->subscriptions, g_steal_pointer (&key),
                           stored_subscriptions);
    }

  for (i = 0; i < unsubscriptions->len; i++)
    {
      RsdSubscription *unsubscription = unsubscriptions->pdata[i];

      for (j = 0; j < stored_subscriptions->len; j++)
        {
          RsdSubscription *subscription = stored_subscriptions->pdata[j];

          /* Since we don’t do deduplication and rely on the caller to call with
           * balanced numbers of subscriptions and unsubscriptions (or do their
           * own deduplication), only remove the first instance found. */
          if (rsd_subscription_equal (subscription, unsubscription))
            {
              g_ptr_array_remove_index_fast (stored_subscriptions, j);
              break;
            }
        }
    }

  for (i = 0; i < subscriptions->len; i++)
    {
      RsdSubscription *subscription = subscriptions->pdata[i];

      g_ptr_array_add (stored_subscriptions,
                       rsd_subscription_copy (subscription));
    }
}

/**
 * csr_subscription_manager_get_subscriptions:
 * @self: a #CsrSubscriptionManager
 * @vehicle: the vehicle whose subscriptions are being looked up
 * @peer_name: the D-Bus peer whose subscriptions are being looked up
 *
 * Look up the subscriptions registered for the pair of @vehicle and @peer_name.
 * If the pair exists, an array will be returned, although it may be empty if
 * no subscriptions are currently registered for the pair. If either @vehicle or
 * @peer_name are unknown to the #CsrSubscriptionManager, %NULL will be
 * returned.
 *
 * Returns: (element-type RsdSubscription) (transfer none) (nullable):
 *    potentially empty array of subscriptions for the given @vehicle and
 *    @peer_name pair; or %NULL if the pair does not exist
 * Since: 0.2.0
 */
GPtrArray *
csr_subscription_manager_get_subscriptions (CsrSubscriptionManager *self,
                                            RsdVehicle             *vehicle,
                                            const gchar            *peer_name)
{
  VehiclePeerPair pair;

  g_return_val_if_fail (CSR_IS_SUBSCRIPTION_MANAGER (self), NULL);
  g_return_val_if_fail (RSD_IS_VEHICLE (vehicle), NULL);
  g_return_val_if_fail (peer_name != NULL && g_dbus_is_unique_name (peer_name),
                        NULL);

  pair.vehicle = vehicle;
  pair.peer_name = (gchar *) peer_name;

  return g_hash_table_lookup (self->subscriptions, &pair);
}

/**
 * csr_subscription_manager_get_peers_for_subscriptions:
 * @self: a #CsrSubscriptionManager
 * @vehicle: a #RsdVehicle to get subscriptions from
 * @changed_attributes: (nullable) (transfer none) (element-type RsdAttributeInfo):
 *    potentially empty array of attributes whose values have changed
 * @invalidated_attributes: (nullable) (transfer none) (element-type RsdAttributeMetadata):
 *    potentially empty array of attribute whose values have changed, but the
 *    new value is not known
 * @notify_time: time when the notification signal was received
 *
 * Get a set of D-Bus unique addresses for peers who are currently subscribed
 * to one or more of the attributes listed in @changed_attributes or
 * @invalidated_attributes for the given @vehicle, using
 * rsd_subscription_matches() to check against the new attribute values.
 *
 * Either of @changed_attributes and @invalidated_attributes may be %NULL to
 * act as a zero-length array.
 *
 * Returns: (transfer container) (element-type utf8): potentially empty array
 *    of D-Bus unique addresses for the peers who are subscribed to the listed
 *    attributes
 * Since: 0.4.0
 */
GPtrArray *
csr_subscription_manager_get_peers_for_subscriptions (CsrSubscriptionManager   *self,
                                                      RsdVehicle               *vehicle,
                                                      GPtrArray                *changed_attributes,
                                                      GPtrArray                *invalidated_attributes,
                                                      RsdTimestampMicroseconds  notify_time)
{
  g_autoptr (GPtrArray/*<owned gchar *>*/) peers = NULL;
  gsize i, j;
  GHashTableIter iter;
  const VehiclePeerPair *pair;
  GPtrArray/*<owned RsdSubscription>*/ *subscriptions;

  g_return_val_if_fail (CSR_IS_SUBSCRIPTION_MANAGER (self), NULL);
  g_return_val_if_fail (RSD_IS_VEHICLE (vehicle), NULL);

  peers = g_ptr_array_new_with_free_func (g_free);

  /* Iterate through each of our #VehiclePeerPairs which match @vehicle and see
   * if any of their subscriptions are listed in @changed_attributes or
   * @invalidated_attributes. */
  g_hash_table_iter_init (&iter, self->subscriptions);

  while (g_hash_table_iter_next (&iter,
                                 (gpointer *) &pair,
                                 (gpointer *) &subscriptions))
    {
       gboolean add_peer = FALSE;

       if (g_strcmp0 (rsd_vehicle_get_id (pair->vehicle),
                      rsd_vehicle_get_id (vehicle)) != 0)
         continue;

      /* Check each of the @changed_attributes against the subscriptions. */
      for (i = 0;
           !add_peer &&
           changed_attributes != NULL &&
           i < changed_attributes->len;
           i++)
        {
          const RsdAttributeInfo *info = changed_attributes->pdata[i];

          for (j = 0; j < subscriptions->len; j++)
            {
              const RsdSubscription *subscription = subscriptions->pdata[j];

              if (((subscription->zone_path == info->metadata.zone_path &&
                    subscription->attribute_name == info->metadata.name) ||
                   (g_strcmp0 (subscription->zone_path, "/") == 0 &&
                    g_strcmp0 (subscription->attribute_name, "") == 0)) &&
                  rsd_subscription_matches (subscription, &info->attribute,
                                            notify_time))
                {
                  add_peer = TRUE;
                  break;
                }
            }
        }

      /* We can’t check the values for invalidated attributes, but we can check
       * their time periods. */
      for (i = 0;
           !add_peer &&
           invalidated_attributes != NULL &&
           i < invalidated_attributes->len;
           i++)
        {
          const RsdAttributeMetadata *metadata = invalidated_attributes->pdata[i];

          for (j = 0; j < subscriptions->len; j++)
            {
              const RsdSubscription *subscription = subscriptions->pdata[j];

              if (((subscription->zone_path == metadata->zone_path &&
                    subscription->attribute_name == metadata->name) ||
                   (g_strcmp0 (subscription->zone_path, "/") == 0 &&
                    g_strcmp0 (subscription->attribute_name, "") == 0)) &&
                  rsd_subscription_matches_period (subscription, notify_time))
                {
                  add_peer = TRUE;
                  break;
                }
            }
        }

      if (add_peer)
        g_ptr_array_add (peers, g_strdup (pair->peer_name));

      /* Update all the attributes to set whether they were notified. */
      for (i = 0;
           add_peer &&
           changed_attributes != NULL &&
           i < changed_attributes->len;
           i++)
        {
          const RsdAttributeInfo *info = changed_attributes->pdata[i];

          for (j = 0; j < subscriptions->len; j++)
            {
              RsdSubscription *subscription = subscriptions->pdata[j];

              if (subscription->zone_path == info->metadata.zone_path &&
                  subscription->attribute_name == info->metadata.name)
                {
                  rsd_subscription_notify (subscription, &info->attribute,
                                           notify_time, TRUE);
                }
            }
        }

      for (i = 0;
           add_peer &&
           invalidated_attributes != NULL &&
           i < invalidated_attributes->len;
           i++)
        {
          const RsdAttributeMetadata *metadata = invalidated_attributes->pdata[i];

          for (j = 0; j < subscriptions->len; j++)
            {
              RsdSubscription *subscription = subscriptions->pdata[j];

              if (subscription->zone_path == metadata->zone_path &&
                  subscription->attribute_name == metadata->name)
                {
                  rsd_subscription_notify (subscription, NULL,
                                           notify_time, TRUE);
                }
            }
        }
    }

  return g_steal_pointer (&peers);
}
